
# This is a fix for InnoDB in MySQL >= 4.1.x
# It "suspends judgement" for fkey relationships until are tables are set.
SET FOREIGN_KEY_CHECKS = 0;

-- ---------------------------------------------------------------------
-- product
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `product`;

CREATE TABLE `product`
(
    `slug` VARCHAR(255) NOT NULL,
    `name` VARCHAR(255) NOT NULL,
    `show_order` INTEGER,
    `description` TEXT,
    `descendant_class` VARCHAR(100),
    PRIMARY KEY (`slug`)
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------
-- stockable
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `stockable`;

CREATE TABLE `stockable`
(
    `sku` VARCHAR(255) NOT NULL,
    `stock_amount` INTEGER NOT NULL,
    `descendant_class` VARCHAR(100),
    PRIMARY KEY (`sku`)
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------
-- bikini
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `bikini`;

CREATE TABLE `bikini`
(
    `slug` VARCHAR(255) NOT NULL,
    `name` VARCHAR(255) NOT NULL,
    `show_order` INTEGER,
    `description` TEXT,
    PRIMARY KEY (`slug`),
    CONSTRAINT `bikini_fk_6857a4`
        FOREIGN KEY (`slug`)
        REFERENCES `product` (`slug`)
        ON DELETE CASCADE
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------
-- bikini_top
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `bikini_top`;

CREATE TABLE `bikini_top`
(
    `size` CHAR(10) NOT NULL,
    `pushup_enabled` TINYINT(1) NOT NULL,
    `bikini_slug` VARCHAR(255) NOT NULL,
    `sku` VARCHAR(255) NOT NULL,
    `stock_amount` INTEGER NOT NULL,
    PRIMARY KEY (`sku`),
    INDEX `bikini_top_fi_2f973f` (`bikini_slug`),
    CONSTRAINT `bikini_top_fk_2f973f`
        FOREIGN KEY (`bikini_slug`)
        REFERENCES `bikini` (`slug`),
    CONSTRAINT `bikini_top_fk_e3692b`
        FOREIGN KEY (`sku`)
        REFERENCES `stockable` (`sku`)
        ON DELETE CASCADE
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------
-- bikini_bottom
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `bikini_bottom`;

CREATE TABLE `bikini_bottom`
(
    `size` CHAR(10) NOT NULL,
    `bikini_slug` VARCHAR(255) NOT NULL,
    `sku` VARCHAR(255) NOT NULL,
    `stock_amount` INTEGER NOT NULL,
    PRIMARY KEY (`sku`),
    INDEX `bikini_bottom_fi_2f973f` (`bikini_slug`),
    CONSTRAINT `bikini_bottom_fk_2f973f`
        FOREIGN KEY (`bikini_slug`)
        REFERENCES `bikini` (`slug`),
    CONSTRAINT `bikini_bottom_fk_e3692b`
        FOREIGN KEY (`sku`)
        REFERENCES `stockable` (`sku`)
        ON DELETE CASCADE
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------
-- one_piece
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `one_piece`;

CREATE TABLE `one_piece`
(
    `slug` VARCHAR(255) NOT NULL,
    `name` VARCHAR(255) NOT NULL,
    `show_order` INTEGER,
    `description` TEXT,
    PRIMARY KEY (`slug`),
    CONSTRAINT `one_piece_fk_6857a4`
        FOREIGN KEY (`slug`)
        REFERENCES `product` (`slug`)
        ON DELETE CASCADE
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------
-- one_piece_stockable
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `one_piece_stockable`;

CREATE TABLE `one_piece_stockable`
(
    `size` CHAR(10) NOT NULL,
    `one_piece_slug` VARCHAR(255) NOT NULL,
    `sku` VARCHAR(255) NOT NULL,
    `stock_amount` INTEGER NOT NULL,
    PRIMARY KEY (`sku`),
    INDEX `one_piece_stockable_fi_254894` (`one_piece_slug`),
    CONSTRAINT `one_piece_stockable_fk_254894`
        FOREIGN KEY (`one_piece_slug`)
        REFERENCES `one_piece` (`slug`),
    CONSTRAINT `one_piece_stockable_fk_e3692b`
        FOREIGN KEY (`sku`)
        REFERENCES `stockable` (`sku`)
        ON DELETE CASCADE
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------
-- product_images
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `product_images`;

CREATE TABLE `product_images`
(
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `featured` TINYINT(1),
    `url` VARCHAR(255) NOT NULL,
    `product_slug` VARCHAR(255) NOT NULL,
    PRIMARY KEY (`id`),
    INDEX `product_images_fi_3d486b` (`product_slug`),
    CONSTRAINT `product_images_fk_3d486b`
        FOREIGN KEY (`product_slug`)
        REFERENCES `product` (`slug`)
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------
-- prices_cop
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `prices_cop`;

CREATE TABLE `prices_cop`
(
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `product_slug` VARCHAR(255) NOT NULL,
    `price` DECIMAL NOT NULL,
    PRIMARY KEY (`id`),
    UNIQUE INDEX `prices_cop_u_f8e02b` (`product_slug`),
    CONSTRAINT `prices_cop_fk_3d486b`
        FOREIGN KEY (`product_slug`)
        REFERENCES `product` (`slug`)
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------
-- prices_usd
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `prices_usd`;

CREATE TABLE `prices_usd`
(
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `product_slug` VARCHAR(255) NOT NULL,
    `price` DECIMAL NOT NULL,
    PRIMARY KEY (`id`),
    UNIQUE INDEX `prices_usd_u_f8e02b` (`product_slug`),
    CONSTRAINT `prices_usd_fk_3d486b`
        FOREIGN KEY (`product_slug`)
        REFERENCES `product` (`slug`)
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------
-- baobab_order
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `baobab_order`;

CREATE TABLE `baobab_order`
(
    `id` VARCHAR(255) NOT NULL,
    `user_id` VARCHAR(255) NOT NULL,
    `order_status` TINYINT NOT NULL,
    `total_price` DECIMAL NOT NULL,
    `currency` VARCHAR(255) NOT NULL,
    `address` VARCHAR(255),
    `phone` VARCHAR(255),
    `city` VARCHAR(255),
    `neighborhood` VARCHAR(255),
    `postal_code` VARCHAR(255),
    `created_at` DATETIME,
    `updated_at` DATETIME,
    PRIMARY KEY (`id`)
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------
-- bikini_order
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `bikini_order`;

CREATE TABLE `bikini_order`
(
    `id` INTEGER(255) NOT NULL AUTO_INCREMENT,
    `order_id` VARCHAR(255) NOT NULL,
    `slug` VARCHAR(255) NOT NULL,
    `top_sku` VARCHAR(255) NOT NULL,
    `bottom_sku` VARCHAR(255) NOT NULL,
    `pushup` TINYINT(1) NOT NULL,
    `amount` DECIMAL NOT NULL,
    `unitPrice` DECIMAL NOT NULL,
    PRIMARY KEY (`id`),
    INDEX `bikini_order_fi_16b283` (`order_id`),
    CONSTRAINT `bikini_order_fk_16b283`
        FOREIGN KEY (`order_id`)
        REFERENCES `baobab_order` (`id`)
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------
-- one_piece_order
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `one_piece_order`;

CREATE TABLE `one_piece_order`
(
    `id` INTEGER(255) NOT NULL AUTO_INCREMENT,
    `order_id` VARCHAR(255) NOT NULL,
    `top_sku` VARCHAR(255) NOT NULL,
    `amount` DECIMAL NOT NULL,
    `unitPrice` DECIMAL NOT NULL,
    `slug` VARCHAR(255) NOT NULL,
    PRIMARY KEY (`id`),
    INDEX `one_piece_order_fi_16b283` (`order_id`),
    CONSTRAINT `one_piece_order_fk_16b283`
        FOREIGN KEY (`order_id`)
        REFERENCES `baobab_order` (`id`)
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------
-- countries_shipping_prices
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `countries_shipping_prices`;

CREATE TABLE `countries_shipping_prices`
(
    `id` INTEGER(255) NOT NULL AUTO_INCREMENT,
    `country_name` VARCHAR(255) NOT NULL,
    `price` INTEGER(255) NOT NULL,
    `currency` VARCHAR(255) NOT NULL,
    PRIMARY KEY (`id`)
) ENGINE=InnoDB;

# This restores the fkey checks, after having unset them earlier
SET FOREIGN_KEY_CHECKS = 1;
