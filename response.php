<?php /** This will be a quick page showing the user what (s)he bought and redirecting to the shop */

use Baobab\app\concerns\BaobabShop;
use BaobabModels\BaobabOrderQuery;
use Velocity\Authentication\Input;

include 'vendor/autoload.php';

ini_set('display_errors',1);
error_reporting(E_ALL);

$order_id = Input::get('referenceCode');
$order_status = intval(Input::get('transactionState'));

if($order_status === 4) {
//    $baobab_shop = new BaobabShop();
//    $baobab_shop->empty_cart();
}

$order = BaobabOrderQuery::create()->findOneById($order_id); ?>

<!DOCTYPE html>
<html>
<head>
    <title>Baobab | Estado de tu pedido</title>
    <link rel="stylesheet" type="text/css" href="/public/css/style.min.css"/>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/js-cookie/2.1.3/js.cookie.min.js"></script>
    <script type="text/javascript" src="/public/js/script.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/nprogress/0.2.0/nprogress.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/nprogress/0.2.0/nprogress.min.css" />
</head>


<body><script id="__bs_script__">//<![CDATA[
    document.write("<script async src='/browser-sync/browser-sync-client.js?v=2.17.6'><\/script>".replace("HOST", location.hostname));
    //]]></script>

<header class="scrolled">
    <div class="grid-6-m">
        <div class="left-menu-container">
            <a onclick="openSearch();"><i class="icon-search"></i></a>
        </div>
    </div>
    <div class="grid-6-m last">
        <div class="right-menu-container">
            <div id="currency-selector" onclick="toggleCurrencyMenu();">
                $<p id="currency-display"><?php echo $_COOKIE['currency'] ?></p> <span class="icon-arrow-down"></span>

                <div style="display: none;" id="currency-selection-list">
                    <ul>
                        <li onclick="changeCurrency('COP');">$COP</li>
                        <hr>
                        <li onclick="changeCurrency('USD');">$USD</li>
                    </ul>
                </div>
            </div>
            <a href="/user"><i class="icon-user "></i></a>
            <a href="/user/favorites"><i id="icon-favorites" class="icon-heart-outline "></i></a>
            <a onclick="toggleCartBox();">
                <i class="icon-shopping-bag  active "></i>
            </a>
        </div>
    </div>
    <nav class="grid-12-m last">
        <div id="nav-title" class="nav-title">
            <h2>BAOBAB</h2>
        </div>
        <div id="nav-menu" class="nav-menu">
            <ul>
                <li id="nav-item-home" class=""><a href="/">HOME</a>
                </li>
                <li id="nav-item-shop" class=""><a href="/shop">SHOP</a>
                </li>
                <li id="nav-item-give" class=""><a href="/how-we-give">HOW WE
                        GIVE</a></li>
                <li id="nav-item-sizing" class=""><a href="/sizing">SIZING</a>
                </li>
                <li id="nav-item-video" class=""><a href="">VIDEO</a>
                </li>
                <li id="nav-item-lookbook" class=""><a href="">LOOKBOOK</a>
                </li>
                <li id="nav-item-customercare" class=""><a href="/customer-care">CUSTOMER CARE</a>
                </li>
            </ul>
        </div>
    </nav>
</header>
<div id="search-box" class="transparent send-to-back">
    <div class="cont95 search-box-content">
        <i onclick="openSearch();" class="icon-times"></i>
        <div class="grid-12-m last">
            <input type="text" placeholder="INGRESA TU B&Uacute;SQUEDA AQU&Iacute;" id="search-input">
        </div>
        <div class="grid-12-m last">
            <div id="search-results">
                SEARCH RESULTS
            </div>
        </div>
    </div>
</div>
<div id="cart-box" class="transparent send-to-back"></div>
<div id="whatwegive">
    <div class="cont60">
        <div class="cont95 buyonecont text-center">
            <div class="grid-12-xs text-right">
                <i class="icon-times" id="cerrarwhatwegive"></i>
            </div>
            <h3 class="montserrat-thin">#BuyOneGrowOne</h3>
            <img src="/assets/img/buyone.png" id="whatwegivehimg">
            <p class="montserrat-thin">Gracias a tu compra sembraremos un</p>
            <h2 id="whatwegiveh2" class="text-uppercase monsterrat">ACACIO</h2>
        </div>
    </div>
</div>

<div id="cart" class="content-wrapper">
    <div class="top-filler"></div>
    <div class="grid-12-m last">
        <div class="cont90">
            <?php if($order_status === 4): ?>
                <h3>YOUR ORDER IS PAID</h3>
            <?php elseif ($order_status === 7): ?>
                <h3>YOUR ORDER IN QUEUE</h3>
            <?php elseif($order_status === 6): ?>
                <h3>WE'RE SORRY! YOUR ORDER WAS CANCELLED!</h3>
            <?php endif; ?>
            <hr>
<!--            <div class="cart-item">-->
<!--                <table>-->
<!--                    <tr>-->
<!--                        <td>-->
<!--                            <img src="/assets/img/swimwear-placeholder.jpg">-->
<!--                        </td>-->
<!--                        <td>-->
<!--                            <h3>AKIRA</h3>-->
<!--                            <p> TALLA-->
<!--                                <span>M</span></p>-->
<!--                        </td>-->
<!--                        <td>-->
<!--                            <div class="grid-6-xs quantity">-->
<!--                                <div class="grid-4-xs" onclick="decreaseAmountInCart('akira::OnePiece::C02');">-->
<!--                                    <span>-</span>-->
<!--                                </div>-->
<!--                                <div class="grid-4-xs">-->
<!--                                    <span class="cart-item-amount">2</span>-->
<!--                                </div>-->
<!--                                <div class="grid-4-xs last"-->
<!--                                     onclick="increaseAmountInCart('akira::OnePiece::C02');">-->
<!--                                    <span>+</span>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                        </td>-->
<!--                        <td class="price-display">-->
<!--                            $132</td>-->
<!--                        <td></td>-->
<!--                    </tr>-->
<!--                </table>-->
<!--            </div>-->
            <hr>
        </div>
    </div>
    <div class="grid-12-m last"></div>
    <div class="grid-12-m last">
        <div class="cont90">
            <a href="/shop">VOLVER A LA TIENDA</a>
        </div>
    </div>
</div>

<footer>
    <div class="footer-container">
        <div class="grid-5-m">
            <div class="container">
                <div class="grid-12-s">
                    ¿TE GUSTA BAOBAB?
                    <form>
                        <input type="email" placeholder="INGRESA TU EMAIL">
                        <button type="submit">SUSCRIBIRSE</button>
                    </form>
                </div>
                <div class="grid-12-s last">
                    S&Iacute;GUENOS Y COMPARTE
                    <p>
                        <i class="icon-instagram"></i>
                        <i class="icon-facebook2"></i>
                        <i class="icon-youtube"></i>
                        <i class="icon-pinterest"></i>
                    </p>
                </div>
            </div>
        </div>
        <div class="grid-7-m last">
            <div class="container">
                <div class="grid-4-s">
                    LINKS DE INTER&Eacute;S
                    <ul>
                        <li><a href="">INICIO</a></li>
                        <li><a href="">TIENDA</a></li>
                        <li><a href="">C&Oacute;MO APORTAMOS</a></li>
                        <li><a href="">VIDEO</a></li>
                        <li><a href="">LOOKBOOK</a></li>
                        <li><a href="">SIZING</a></li>
                        <li><a href="">CONTACTO</a></li>
                        <li><a href="">PRIVACIDAD</a></li>
                    </ul>
                </div>
                <div class="grid-4-s">
                    PRODUCTOS
                    <ul>
                        <li><a href="">NUEVA COLECCI&Oacute;N</a></li>
                        <li><a href="">SWIMWEAR</a></li>
                        <li><a href="">ENTERIZOS</a></li>
                        <li><a href="">BIKINIS</a></li>
                        <li><a href="">MIX</a></li>
                        <li><a href="">FAVORITOS</a></li>
                        <li><a href="">OFERTAS</a></li>
                    </ul>
                </div>
                <div class="grid-4-s last">
                    SERVICIO AL CLIENTE
                    <ul>
                        <li><a href="">CONTACTO</a></li>
                        <li><a href="">DEVOLUCI&Oacute;N</a></li>
                        <li><a href="">GU&Iacute;A DE TALLAS</a></li>
                        <li><a href="">INFO. DE ENV&Iacute;O</a></li>
                        <li><a href="">PERFIL CLIENTE</a></li>
                        <li><a href="">FAQ</a></li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="grid-12-m last">
            <div class="footer-contact-info">
                +(57) 313 6868 EXT 017 | CARRERA 8 #67-74 | BOGOTÁ, COLOMBIA<br>
                CREADO CON AMOR POR <a href="http://estudiocodesign.com" target="_blank">ESTUDIO CODESIGN</a> | TODOS LOS DERECHOS
                RESERVADOS 2016
            </div>

        </div>
    </div>
</footer>
<script type="text/javascript" src="/public/js/script.min.js"></script>
</body>
