'use strict';

var gulp = require('gulp');

// js uglifier and compressor
var uglify = require('gulp-uglify');
var pump = require('pump');

// css uglifier, compressor & minificator
var cleanCSS = require('gulp-clean-css');
var concatCss = require('gulp-concat-css');

// css preprocessor
var sass = require('gulp-sass');

//js concat
var concat = require('gulp-concat');

// run gulp tasks in sequence
var runSequence = require('run-sequence');

// auto reload browser on changes
var browserSync = require('browser-sync').create();
var reload = browserSync.reload;

// merge the streams
var merge = require('merge-stream');

var paths = {
    scripts: ['assets/js/**/*.js'],
    styles: 'assets/sass/**/*.scss',
    fonts: ['assets/fonts/**/*.css'],
    css: ['assets/css/**/*.css']
};

gulp.task('sass', function () {
    var sassStream = gulp.src(paths.styles)
        .pipe(sass.sync().on('error', sass.logError))
        .pipe(concatCss("sass.min.css"));

    var fontsStream = gulp.src(paths.fonts)
        .pipe(concatCss("fonts.min.css"));

    var cssStream = gulp.src(paths.css)
        .pipe(concatCss("styles.min.css"));

    return merge(sassStream, fontsStream, cssStream)
        .pipe(concatCss("style.min.css"))
        .pipe(cleanCSS({compatibility: 'ie8'}))
        .pipe(gulp.dest('./public/css/'))
        .pipe(browserSync.stream());
});

gulp.task('scripts', function() {
    return gulp.src(paths.scripts)
        // .pipe(uglify())
        .pipe(concat('script.min.js'))
        .pipe(gulp.dest('./public/js/'));
});

gulp.task('sync', function() {
    browserSync.init({
        injectChanges: true,
        proxy: 'localhost:8080'
    });
});

gulp.task('watch', function() {
    gulp.watch(paths.scripts, { maxListeners: 999 }, ['scripts']);
    gulp.watch(paths.styles, { maxListeners: 999 }, ['sass']);
    gulp.watch(['app/**/*.php', 'app/templates/**/*.twig', 'public/js/script.min.js'], { maxListeners: 999 }, reload);
});

gulp.task('default', function() {
    runSequence(
        ['sass', 'scripts', 'watch', 'sync']
    );
});

gulp.task('deploy', function() {
    runSequence(
        ['sass', 'scripts']
    );
});
