<?php
/**
 * Created by PhpStorm.
 * User: jsrolon
 * Date: 05-07-2016
 * Time: 11:08 AM
 */

namespace Baobab\App\Controllers;

use Baobab\app\concerns\BaobabController;
use Baobab\app\concerns\BaobabShop;
use BaobabModels\Base\CountriesShippingPricesQuery;
use BaobabModels\Bikini;
use BaobabModels\OnePiece;
use BaobabModels\ProductQuery;
use Swap\Builder;
use Velocity\Authentication\Input;
use Velocity\Db\Db;
use Velocity\Ecommerce\CartController;
use Velocity\Core\Controller;

class AJAXCtrl extends BaobabController {

    public $db;

    private $baobab_shop;

    public function init() {
        $this->db = Db::getInstance();
        $this->baobab_shop = new BaobabShop();
    }

    public function add_to_cart() {
        $this->is_add_to_cart = true;
        $ref = Input::get('ref');

        $this->baobab_shop->add_to_cart($ref, 1);
        list($this->cart, $this->cart_total) = $this->baobab_shop->get_cart();
        foreach ($this->cart as &$cart_item) {
            $ref_exploded = explode('::', $cart_item['ref']);
            $cart_item['image'] = ProductQuery::create()->findOneBySlug($ref_exploded[0])->getProductImagess()->getFirst()->getUrl();
        }
    }

    public function remove_from_cart() {
        $this->is_remove_from_cart = true;
        $ref = Input::get('ref');
        $this->baobab_shop->remove_from_cart($ref);
        list($this->cart, $this->cart_total) = $this->baobab_shop->get_cart();
        foreach ($this->cart as &$cart_item) {
            $ref_exploded = explode('::', $cart_item['ref']);
            $cart_item['image'] = ProductQuery::create()->findOneBySlug($ref_exploded[0])->getProductImagess()->getFirst()->getUrl();
        }
    }

    public function empty_cart() {
        $this->is_empty_cart = true;
        $this->baobab_shop->empty_cart();
        list($this->cart, $this->cart_total) = $this->baobab_shop->get_cart();
        foreach ($this->cart as &$cart_item) {
            $ref_exploded = explode('::', $cart_item['ref']);
            $cart_item['image'] = ProductQuery::create()->findOneBySlug($ref_exploded[0])->getProductImagess()->getFirst()->getUrl();
        }
    }

    public function modify_amount_in_cart() {
        $this->is_modify_amount_in_cart = true;
        $ref = Input::get('ref');

        $ref_exploded = explode('::', $ref);

        $amount = Input::get('amount');
        $this->baobab_shop->add_to_cart($ref, $amount);
        list($this->cart, $this->cart_total) = $this->baobab_shop->get_cart();
        foreach ($this->cart as $item) {
            if ($item['ref'] === $ref) {
                $this->modified_item = $item;
                $this->slug = $ref_exploded[0];
                break;
            }
        }
    }

    public function search() {
        $this->is_search = true;
        $query = Input::get('query');
        if ($query === 'featured') {
            $this->products = $this->shop->get_productos_featured(8);
        } else {
            $this->products = $this->baobab_shop->search_products($query);
        }
    }

    public function add_to_favorites() {
        $sku = Input::get('sku');
        $this->shop->add_to_favorites($sku);
    }

    public function remove_from_favorites() {
        $sku = Input::get('sku');
        $this->shop->remove_from_favorites($sku);
    }

    public function get_availability() {
        $this->is_get_availability = true;

        $slug = Input::get('slug');
        // get the product
        $product = ProductQuery::create()->findOneBySlug($slug)->getChildObject();

        // build associative array with the availabilites
        // IMPORTANT: 0 means top, 1 means bottom. In case there is no 1, it means it has a single size
        $availabilities = array();
        if ($product instanceof Bikini) {
            foreach ($product->getBikiniTops() as $bikini_top) {
                $availabilities[0][$bikini_top->getSize()] = array(($bikini_top->getStockAmount() >= 1),
                    $bikini_top->getPushupEnabled());
            }
            foreach ($product->getBikiniBottoms() as $bikini_bottom) {
                $availabilities[1][$bikini_bottom->getSize()] = array(($bikini_bottom->getStockAmount() >= 1));
            }
        } else if ($product instanceof OnePiece) {
            foreach ($product->getOnePieceStockables() as $one_piece_stockable) {
                $availabilities[0][$one_piece_stockable->getSize()] = array(($one_piece_stockable->getStockAmount() >= 1));
            }
        }

        $this->availability = json_encode($availabilities);
    }

    public function shipping_price_for() {
        $country_code = Input::get('countryCode');
        $country_object = CountriesShippingPricesQuery::create()->findOneById($country_code);
        $shipping_price = $country_object->getPrice();
        $country_currency = $country_object->getCurrency();

        if($_COOKIE['currency'] !== $country_currency) {
            $convert_to = $_COOKIE['currency'] === 'USD' ? 'COP' : 'USD';
            $swap = (new Builder())
                ->add('fixer')
                ->add('google')
                ->add('yahoo')
                ->build();
            $rate = $swap->latest("$convert_to/{$_COOKIE['currency']}");
            $rate_value = $rate->getValue();
            $shipping_price = intval($shipping_price * floatval($rate_value));
        }

        echo $shipping_price;
    }
}
