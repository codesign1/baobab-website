<?php

namespace Baobab\app\controllers;

use Baobab\app\concerns\BaobabController;
use BaobabModels\ProductQuery;
use Propel\Runtime\ActiveQuery\Criteria;
use Velocity\Authentication\Input;
use Velocity\Config\Config;
use Velocity\Db\Db;
use Velocity\Ecommerce\CartController;
use Velocity\Ecommerce\Shop;

class HomeCtrl extends BaobabController {
    public $pagination_size = 6;

	public function init() {
		$this->is_home = true;

        $this->db = Db::getInstance();
        $this->products = ProductQuery::create()->orderBy('show_order', Criteria::ASC)->find();
        $this->number_of_pages = count($this->products) / $this->pagination_size;
        $this->current_page = 1;
        $this->base_url = '/shop/';

        // filtering by sizes
        $this->selected_filter_sizes = Input::get('size');
        if (!$this->at_least_one_size_selected()) {
            $this->selected_filter_sizes = array(
                'xs' => false,
                's' => false,
                'm' => false,
                'l' => false,
                'xl' => false
            );
        }

        // range limits for the slider
        $this->min_price = $this->db->query("SELECT min(precio) AS min_price FROM v_productos")->results()[0]->min_price;
        $this->max_price = $this->db->query("SELECT max(precio) AS max_price FROM v_productos")->results()[0]->max_price;

        // get the price filtering info
        $this->min_selected_price = Input::get('precio-min');
        $this->max_selected_price = Input::get('precio-max');

        // default selected values for slider
        if ($this->min_selected_price === "") {
            $this->min_selected_price = $this->min_price;
        }

        if ($this->max_selected_price === "") {
            $this->max_selected_price = $this->max_price;
        }
	}

    private function at_least_one_size_selected() {
        foreach ($this->selected_filter_sizes as $filter_size => $filter_size_active) {
            if ($filter_size_active) {
                return true;
            }
        }
        return false;
    }
}
