<?php
/**
 * Created by PhpStorm.
 * User: jsrolon
 * Date: 30-06-2016
 * Time: 2:40 PM
 */

namespace Baobab\App\Controllers;

use Auth0\SDK\API\Authentication;
use Baobab\app\concerns\BaobabController;
use BaobabModels\BaobabOrderQuery;
use BaobabModels\BikiniOrderQuery;
use BaobabModels\OnePieceOrderQuery;
use BaobabModels\ProductQuery;
use Velocity\Authentication\Input;
use Velocity\Authentication\Session;
use Velocity\Authentication\Token;
use Velocity\Authentication\Validate;
use Velocity\Core\Controller;
use Velocity\Ecommerce\CartController;
use Velocity\Helpers\Redirect;
use Velocity\Security\HASH;
use Velocity\Users\User;
use \Exception;

class UserCtrl extends BaobabController {

    public function init() {
        // so that the header displays with the correct style
        $this->header_always_scrolled = true;
        $this->is_user_controller = true;
    }

    public function get_orders() {
        $this->is_orders = true;
        $this->pedidos = BaobabOrderQuery::create()->findByUserId($this->user_data->email);

        foreach ($this->pedidos as $pedido) {
            $bikini_orders = BikiniOrderQuery::create()->findByOrderId($pedido->getId());
            $one_piece_orders = OnePieceOrderQuery::create()->findByOrderId($pedido->getId());

            $merged = array_merge($bikini_orders->toArray(), $one_piece_orders->toArray());
            foreach ($merged as &$item) {
                $item_object = ProductQuery::create()->findOneBySlug($item['Slug']);
                $item['name'] = $item_object->getName();
            }

            $pedido->sub_pedidos = $merged;
        }
    }

    public function get_addresses() {
        $this->is_addresses = true;
    }

    public function get_promo_codes() {
        $this->is_promo_codes = true;
    }

    public function get_register_form() {
        $this->is_register_form = true;
    }

    public function login() {
        if (Input::exists()) {
            $validate = new Validate();
            $validation = $validate->check($_POST, array(
                'username' => array(
                    'required' => true
                ),
                'password' => array(
                    'required' => true
                )
            ));
            if ($validation->passed()) {
                $user = new User();
                $remember = (Input::get('remember') === 'on') ? true : false;
                $login = $user->login(Input::get('username'), Input::get('password'), $remember);
                if ($login) {
                    if($_COOKIE['checkout_login'] === 'true') {
                        unset($_COOKIE['checkout_login']);
                        Redirect::to('/cart');
                    } else {
                        Redirect::to('/user');
                    }
                } else {
                    echo 'Sorry, login failed.';
                }
            } else {
                foreach ($validation->errors() as $error) {
                    echo $error . '<br>';
                }
            }
        }
        if (Input::get('username') != "" && Input::get('activation_key') != "") {
            $user = new User();
            if ($user->activate_user(Input::get('username'), Input::get('activation_key'))) {
                $login = $user->login_after_activation(Input::get('username'));
                if ($login) {
                    Redirect::to('/user');
                } else {
                    echo 'Sorry, login failed.';
                }
            } else {

            }
        }
        if (Session::exists('login')) {
            echo '<p>' . Session::flash('login') . '</p>';
        }
    }

    public function register() {
        if (Input::exists()) {
            $validate = new Validate();
            $validation = $validate->check($_POST, array(
                'password' => array(
                    'required' => true,
                    'min' => 6
                ),
                'email' => array(
                    'required' => true,
                    'email' => 'valid'
                ),
                'name' => array(
                    'required' => true,
                    'min' => 2,
                    'max' => 50
                )
            ));
            if ($validation->passed()) {
                $user = new User();
                $salt = mcrypt_create_iv(32);
                try {
                    $user->create(array(
                        'username' => Input::get('username'),
                        'password' => hash('sha256', Input::get('password') . $salt),
                        'email' => Input::get('email'),
                        'salt' => $salt,
                        'name' => Input::get('name'),
                        'joined' => date('Y-m-d H:i:s'),
                        'group' => 1
                    ));

                    // because we don't have an infrastructure to show messages to users, we just log them in
                    $this->successful_login = true;
                } catch (\Exception $e) {
                    var_dump($e->getMessage());
                }
            } else {
                foreach ($validation->errors() as $error) {
                    echo $error . '<br>';
                }
            }
        }
    }

    public function get_favorites() {
        $this->is_get_favorites = true;
        $this->favorites = $this->shop->get_favorites();
    }

    public function logout() {
        $user = new User();
        $user->logout();

        Redirect::to('/');
    }

    public function modify_address() {
        $this->is_add_address = true;
        $action = Input::get('execute');

        if($action === 'create') {
            $address = Input::get('address');
            $hood = Input::get('neighborhood');
            $city = Input::get('city');
            $this->shop->crear_direccion($city, $hood, $address);
        } else {
            $this->shop->eliminar_direccion(Input::get('id'));
        }
        $this->direcciones = $this->shop->direcciones();
    }
}
