<?php
/**
 * Created by PhpStorm.
 * User: jsrolon
 * Date: 28-06-2016
 * Time: 2:35 PM
 */

namespace Baobab\App\Controllers;

use Baobab\app\concerns\BaobabController;
use BaobabModels\Base\ProductQuery;
use BaobabModels\Bikini;
use BaobabModels\OnePiece;
use BaobabModels\PricesCOPQuery;
use BaobabModels\Product;
use Velocity\Core\Controller;
use Velocity\Db\Db;
use Velocity\Ecommerce\CartController;
use Velocity\Ecommerce\Shop;

class ItemCtrl extends BaobabController {
    public $item, $item_is_favorite, $db, $availability, $available_colors;

    public function init() {
        $this->header_always_scrolled = true;
        $this->db = Db::getInstance();
        $this->availability = array();
    }

    public function by_slug($slug) {
        $basic_pic_url = ProductQuery::create()->findPK($slug)->getProductImagess()->getFirst()->getUrl();
        $this->pic = str_replace('.png', '-item.png', $basic_pic_url);
        $this->back_pic = str_replace('-small-item.png', '-back-small-item.png', $this->pic);
        $this->pic = $basic_pic_url;
        $this->item = ProductQuery::create()->findPK($slug)->getChildObject();
        $this->item_price = $this->item->getPrice();

        $reflection = new \ReflectionClass($this->item);
        $this->item_category = $reflection->getShortName();

        if($this->item instanceof Bikini) {
            $this->tops = $this->item->getBikiniTops();
            $this->bottoms = $this->item->getBikiniBottoms();
        } else if ($this->item instanceof OnePiece) {
            $this->stockables = $this->item->getOnePieceStockables();
        }

        $this->item_class = explode("\\", get_class($this->item))[1];
        $this->item_is_favorite = $this->shop->is_user_favorite($slug);
    }
}
