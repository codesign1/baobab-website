<?php
/**
 * Created by PhpStorm.
 * User: jsrolon
 * Date: 29-06-2016
 * Time: 1:13 PM
 */

namespace Baobab\App\Controllers;

use Velocity\Core\Controller;

class SizingCtrl extends Controller {

    public function init() {
        // so that the header displays with the correct style
        $this->header_always_scrolled = true;
    }
}
