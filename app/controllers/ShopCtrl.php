<?php
/**
 * Created by PhpStorm.
 * User: jsrolon
 * Date: 29-06-2016
 * Time: 1:13 PM
 */

namespace Baobab\App\Controllers;

use Baobab\app\concerns\BaobabShop;
use BaobabModels;
use BaobabModels\PricesUSDQuery;
use BaobabModels\ProductQuery;
use Propel\Runtime\ActiveQuery\Criteria;
use Baobab\app\concerns\BaobabController;
use Velocity\Authentication\Input;
use Velocity\Core\Controller;
use Velocity\Db\Db;
use Velocity\Ecommerce\CartController;

class ShopCtrl extends BaobabController {

    private $db;
    public $number_of_pages,
        $current_page,
        $base_url, $pagination_size = 6,
        $selected_filter_sizes, $min_price, $max_price, $min_selected_price, $max_selected_price;

    private $baobab_shop;

    public function init() {
        // so that the header displays with the correct style
        $this->header_always_scrolled = true;
        $this->db = Db::getInstance();

        // filtering by sizes
        $this->selected_filter_sizes = Input::get('size');
        if (!$this->at_least_one_size_selected()) {
            $this->selected_filter_sizes = array(
//                'xs' => false,
                's' => false,
                'm' => false,
                'l' => false,
//                'xl' => false
            );
        }

        // range limits for the slider
        $currency_class_name = '\BaobabModels\Prices' . $_COOKIE['currency'] . 'Query';
        $highest_priced = call_user_func(array($currency_class_name, 'create'))->orderByPrice('desc')->findOne();
        $lowest_priced = call_user_func(array($currency_class_name, 'create'))->orderByPrice('asc')->findOne();

        $this->min_price = intval($lowest_priced->getPrice());
        $this->max_price = intval($highest_priced->getPrice());

        if ($this->min_price === $this->max_price) {
            $this->min_price -= 10000;
            $this->min_price = $this->min_price < 0 ? 0 : $this->min_price;

            $this->max_price += 10000;
        }

        // get the price filtering info
        $this->min_selected_price = Input::get('precio-min');
        $this->max_selected_price = Input::get('precio-max');

        // default selected values for slider
        if ($this->min_selected_price === "") {
            $this->min_selected_price = $this->min_price;
        }

        if ($this->max_selected_price === "") {
            $this->max_selected_price = $this->max_price;
        }

        // pagination (must be calculated for all queries)
        $all_products = ProductQuery::create()->orderBy('show_order', Criteria::ASC)->find();
        $this->number_of_pages = count($all_products) / $this->pagination_size;
    }

    public function all($page) {
        $this->is_all = true;
        $this->base_url = '/shop/';
        $this->current_page = $page == 0 ? 1 : $page;
        if (is_null($page)) {
            $page = 0;
        } else {
            --$page;
        }

        $capitalizer = function ($e) {
            return strtoupper($e);
        };

//        $actual_selected_sizes = array_map($capitalizer, array_keys($this->selected_filter_sizes));

        $currency_method_name = 'usePrices' . $_COOKIE['currency'] . 'Query';

        $this->products = ProductQuery::create()
            // filter by selected price
            ->$currency_method_name()
            ->filterByPrice(array('min' => $this->min_selected_price, 'max' => $this->max_selected_price))
            // filter by selected sizes
            ->endUse()
            ->orderBy('show_order', Criteria::ASC)->paginate($page, $this->pagination_size);
    }

    public function new_collection($page) {
        $this->is_new_collection = true;
        $this->current_page = $page == 0 ? 1 : $page;
        $this->base_url = '/new/';

        $this->execute_sql_query(null, $page);
    }

    public function sale($page) {
        $this->is_sale = true;
        $this->current_page = $page == 0 ? 1 : $page;
        $this->base_url = '/sale/';

        $this->execute_sql_query(null, $page);
    }

    public function swimwear($swimwear_category, $page) {
        $this->current_page = $page == 0 ? 1 : $page;
        if (is_null($page)) {
            $page = 0;
        } else {
            --$page;
        }

        $this->{'is_' . str_replace('-', '_', $swimwear_category)} = true;
        $this->base_url = "/shop/swimwear/{$swimwear_category}/";

        // we create the class name by switching depending on the incoming category
        // could be made programmatically, if we could find where to change the route stuff
        $query_category = '';
        switch ($swimwear_category) {
            case 'bikini':
                $query_category = 'Bikini';
                break;
            case 'one-piece':
                $query_category = 'OnePiece';
                break;
        }

        $class_name = '\\BaobabModels\\' . str_replace('-', '', ucwords($swimwear_category, '-')) . 'Query';
        $query = new $class_name;
        $currency_method_name = 'usePrices' . $_COOKIE['currency'] . 'Query';
        $category_query_name = 'use' . str_replace('-', '', ucwords($swimwear_category, '-')) . 'Query';

        $this->products = ProductQuery::create()// filter by selected price
            ->$category_query_name()->endUse()
            ->$currency_method_name()
            ->filterByPrice(array('min' => $this->min_selected_price, 'max' => $this->max_selected_price))
            // filter by selected sizes
            ->endUse()
            ->orderBy('show_order', Criteria::ASC)->paginate($page, $this->pagination_size);
    }

    private function execute_sql_query($category, $page) {
        $sql_string = "SELECT categoria.* FROM (" .
            $this->get_category_sql_string($category)
            . ") AS categoria INNER JOIN (" .
            $this->get_price_sql_string()
            . ") AS precio ON categoria.sku = precio.sku INNER JOIN (" .
            $this->get_size_sql_string()
            . ") AS talla ON precio.sku = talla.sku INNER JOIN (" .
            $this->get_on_sale_sql_string()
            . ") AS promocion ON talla.sku = promocion.sku INNER JOIN (" .
            $this->get_new_collection_sql_string()
            . ") AS nueva ON promocion.sku = nueva.sku ORDER BY categoria.precio ASC";

        $all_products = $this->db->query(
            $sql_string
        )->results();

        $this->number_of_pages = ceil(count($all_products) / $this->pagination_size);
        $this->products = array_slice($all_products, $this->pagination_size * $page, $this->pagination_size);
    }

    public function get_size_sql_string() {
        if (!$this->at_least_one_size_selected()) {
            $filter_string = "SELECT * FROM v_productos";
        } else {
            $filter_string = "SELECT productos.* FROM v_productos AS productos INNER JOIN v_modelos_producto AS modelos ON " .
                "modelos.sku = productos.sku WHERE ";

            // add the size filters
            foreach ($this->selected_filter_sizes as $filter_size => $filter_size_active) {
                if ($filter_size_active) {
                    $filter_string .= "modelos.size = '" . $filter_size . "' or ";
                }
            }

            // remove the last OR and add the grouping clause
            $filter_string = substr($filter_string, 0, -4);
            $filter_string .= " GROUP BY productos.sku";
        }
        return $filter_string;
    }

    public function get_price_sql_string() {
        $filter_string = "select * from v_productos where precio >= {$this->min_selected_price} and " .
            " precio <= {$this->max_selected_price}";
        return $filter_string;
    }

    public function get_category_sql_string($category) {
        if (is_null($category)) {
            return "SELECT * FROM v_productos";
        }
        return "select * from v_productos where categoria = '{$category}'";
    }

    public function get_new_collection_sql_string() {
        if (!$this->is_get_new_collection) {
            return "SELECT * FROM v_productos";
        }
        return "SELECT * FROM v_productos WHERE coleccion_nueva = TRUE";
    }

    public function get_on_sale_sql_string() {
        if (!$this->is_get_on_sale) {
            return "SELECT * FROM v_productos";
        }
        return "SELECT * FROM v_productos WHERE en_promocion = TRUE";
    }

    private function at_least_one_size_selected() {
        foreach ($this->selected_filter_sizes as $filter_size => $filter_size_active) {
            if ($filter_size_active) {
                return true;
            }
        }
        return false;
    }
}
