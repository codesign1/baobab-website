<?php
/**
 * Created by PhpStorm.
 * User: jsrolon
 * Date: 29-07-2016
 * Time: 12:10 PM
 */

namespace Baobab\App\Controllers;

use Baobab\app\concerns\BaobabController;
use Baobab\app\concerns\BaobabShop;
use BaobabModels\BaobabOrderQuery;
use BaobabModels\Base\BikiniTopQuery;
use BaobabModels\BikiniBottom;
use BaobabModels\BikiniBottomQuery;
use BaobabModels\OnePieceOrderQuery;
use BaobabModels\BikiniOrderQuery;
use BaobabModels\OnePieceQuery;
use BaobabModels\OnePieceStockableQuery;
use BaobabModels\ProductQuery;
use Velocity\Authentication\Input;
use Velocity\Core\Controller;
use Velocity\Db\Db;
use Velocity\Ecommerce\CartController;

class PaymentsCtrl extends BaobabController {

    private $db, $baobab_shop;

    public function init() {
        // so that the header displays with the correct style
        $this->header_always_scrolled = true;
        $this->db = Db::getInstance();
        $this->baobab_shop = new BaobabShop();
    }

    public function response() {
        // make sure the cart is empty
        $this->order_status = intval(Input::get('transactionState'));
        if($this->order_status === 4) {
            $this->baobab_shop->empty_cart();
            list($this->cart, $this->cart_total) = $this->baobab_shop->get_cart();
        }

        $this->pedido = BaobabOrderQuery::create()->findOneById(Input::get('referenceCode'));
        $this->pedido_currency = $this->pedido->getCurrency();
        $bikini_orders = BikiniOrderQuery::create()->findByOrderId($this->pedido->getId());
        $one_piece_orders = OnePieceOrderQuery::create()->findByOrderId($this->pedido->getId());

        $merged = array_merge($bikini_orders->toArray(), $one_piece_orders->toArray());
        foreach ($merged as &$item) {
            $item_object = ProductQuery::create()->findOneBySlug($item['Slug']); // this is because we converted into array

            if($item['TopSku'] && $item['BottomSku']) {
                $item['talla_top'] = BikiniTopQuery::create()->findOneBySku($item['TopSku'])->getSize();
                $item['talla_bottom'] = BikiniBottomQuery::create()->findOneBySku($item['BottomSku'])->getSize();
            } else {
                $item['talla_top'] = OnePieceStockableQuery::create()->findOneBySku($item['TopSku'])->getSize();
            }

            $item['name'] = $item_object->getName();

            $item['image'] = $item_object->getProductImagess()[0]->getUrl();
        }

        $this->sub_pedidos = $merged;
//        var_dump($this->sub_pedidos);
    }
}
