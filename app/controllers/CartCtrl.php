<?php
/**
 * Created by PhpStorm.
 * User: jsrolon
 * Date: 29-06-2016
 * Time: 1:52 PM
 */

namespace Baobab\App\Controllers;

use Baobab\app\concerns\BaobabController;
use Baobab\app\concerns\BaobabShop;
use BaobabModels\BaobabOrder;
use BaobabModels\BaobabOrderQuery;
use BaobabModels\BikiniBottom;
use BaobabModels\BikiniBottomQuery;
use BaobabModels\BikiniOrder;
use BaobabModels\BikiniOrderQuery;
use BaobabModels\BikiniQuery;
use BaobabModels\BikiniTop;
use BaobabModels\BikiniTopQuery;
use BaobabModels\CountriesShippingPrices;
use BaobabModels\CountriesShippingPricesQuery;
use BaobabModels\Map\ProductTableMap;
use BaobabModels\OnePieceOrder;
use BaobabModels\OnePieceOrderQuery;
use BaobabModels\OnePieceQuery;
use BaobabModels\OnePieceStockable;
use BaobabModels\OnePieceStockableQuery;
use BaobabModels\Order;
use BaobabModels\ProductQuery;
use PDO;
use Propel\Runtime\Propel;
use Swap\Builder;
use Velocity\Authentication\Input;
use Velocity\Ecommerce\CartController;
use Velocity\Core\Controller;
use Velocity\Helpers\HELPERS;
use Velocity\Helpers\Redirect;

class CartCtrl extends BaobabController {
    public $item;

    private $baobab_shop;

    public function init() {
        $this->baobab_shop = new BaobabShop();
        $this->header_always_scrolled = true;
        $this->is_cart_controller = true;
        list($this->cart, $this->cart_total) = $this->baobab_shop->get_cart();
        foreach ($this->cart as &$cart_item) {
            $ref_exploded = explode('::', $cart_item['ref']);
            $cart_item['image'] = ProductQuery::create()->findOneBySlug($ref_exploded[0])->getProductImagess()->getFirst()->getUrl();
        }
    }

    public function checkout() {
        $this->is_checkout = true;

        // get the country list
        $this->countries = CountriesShippingPricesQuery::create()->find();
        $first_country_currency = CountriesShippingPricesQuery::create()->findOneByCountryName('Colombia')->getCurrency();
        $this->shipping_price_to_country = CountriesShippingPricesQuery::create()->findOneByCountryName('Colombia')->getPrice();

        if ($_COOKIE['currency'] !== $first_country_currency) {
            $convert_to = $_COOKIE['currency'] === 'USD' ? 'COP' : 'USD';
            $swap = (new Builder())
                ->add('fixer')
                ->add('google')
                ->add('yahoo')
                ->build();
            $rate = $swap->latest("$convert_to/{$_COOKIE['currency']}");
            $rate_value = $rate->getValue();
            $this->shipping_price_to_country = intval($this->shipping_price_to_country * floatval($rate_value));
        }

        if ($this->isLoggedIn) {
            // We're gonna make a transaction. We'll make another one when payu confirms the payment (or not)
            $db_connection = Propel::getWriteConnection(ProductTableMap::DATABASE_NAME);
            $db_connection->beginTransaction();
            try {
                // we'll start by creating an order, hopefully it will be garbage collected
                $right_now = new \DateTime();
                $this->order_id = $this->user_data->id . '-' . $right_now->getTimestamp();

                $order = new BaobabOrder();
                $order->setOrderStatus('pending');
                $order->setTotalPrice(intval($this->cart_total));
                $order->setCurrency($_COOKIE['currency']);
                $order->setUserId($this->user_data->email);
                $order->setId($this->order_id);

                foreach ($this->cart as $cart_item) {
                    // we're gonna extract the skus so we can check their amounts
                    $ref_exploded = explode('::', $cart_item['ref']);
                    $slug = $ref_exploded[0];
                    $category = $ref_exploded[1];
                    $top_sku = $ref_exploded[2];

                    if ($category === 'Bikini') {
                        $bottom_sku = $ref_exploded[3];
                        $pushup = $ref_exploded[4];
                    }

                    // now we're gonna check if there are more stocks than the amount desired
                    if ($category === 'Bikini') {
                        $top_item = BikiniTopQuery::create()->findOneBySku($top_sku);
                        if ($top_item->getStockAmount() < intval($cart_item['amount'])) {
                            throw new \Exception($slug . '::' . 'bikini_top'); // we'll send the class as the exception parameter
                            // so that we know where was the lack of stock
                        }

                        // remove the stocks
                        $top_item->setStockAmount($top_item->getStockAmount() - intval($cart_item['cantidad']));
                        $top_item->save();

                        $bottom_item = BikiniBottomQuery::create()->findOneBySku($bottom_sku);
                        if ($bottom_item->getStockAmount() < intval($cart_item['cantidad'])) {
                            throw new \Exception($slug . '::' . 'bikini_bottom');
                        }

                        // remove the stocks
                        $bottom_item->setStockAmount($bottom_item->getStockAmount() - intval($cart_item['cantidad']));
                        $bottom_item->save();

                        // now that everything's fine, we can create the bikini order component
                        $bikini_order = new BikiniOrder();
                        $bikini_order->setTopSku($top_sku);
                        $bikini_order->setBottomSku($bottom_sku);
                        $bikini_order->setPushup($pushup ? true : false);
                        $bikini_order->setAmount(intval($cart_item['cantidad']));
                        $bikini_order->setSlug($slug);

                        $bikini_object = BikiniQuery::create()->findOneBySlug($slug);
                        $bikini_order->setUnitprice($bikini_object->getPrice($pushup));
                        $order->addBikiniOrder($bikini_order);
                    } else {
                        $stockable = OnePieceStockableQuery::create()->findOneBySku($top_sku);
                        if ($stockable->getStockAmount() < intval($cart_item['cantidad'])) {
                            throw new \Exception($slug . '::' . 'onepiece_top');
                        }
                        $stockable->setStockAmount($stockable->getStockAmount() - intval($cart_item['cantidad']));
                        $stockable->save();

                        // now that everything's fine, we can create the one piece order component
                        $one_piece_order = new OnePieceOrder();
                        $one_piece_order->setTopSku($top_sku);
                        $one_piece_order->setAmount(intval($cart_item['cantidad']));
                        $one_piece_order->setSlug($slug);

                        $one_piece_object = OnePieceQuery::create()->findOneBySlug($slug);
                        $one_piece_order->setUnitprice($one_piece_object->getPrice($pushup));

                        $order->addOnePieceOrder($one_piece_order);
                    }
                }

                $order->save();
                $db_connection->commit();

                // in this moment after commiting we know the order exists, so we create the check event
                $event_create_string = <<<EOSTR
SET GLOBAL event_scheduler = ON;
CREATE EVENT `check_order_{$order->getId()}` ON SCHEDULE AT current_timestamp() + INTERVAL 2 HOUR DO
  BEGIN
    START TRANSACTION;
    SET @order_id = '{$order->getId()}';
    SET @order_status = (SELECT order_status FROM baobab_order WHERE baobab_order.`id` = @order_id);
    IF(@order_status = 'pending') THEN
      UPDATE baobab_order SET baobab_order.`order_status` = 1 WHERE baobab_order.`id` = @order_id;

      UPDATE bikini_top SET bikini_top.`stock_amount` =
      bikini_top.`stock_amount` + 
      ( SELECT amount FROM bikini_order 
            WHERE bikini_order.`top_sku` = bikini_top.`sku` AND
            bikini_order.`order_id` = @order_id) 
            where  bikini_top.`sku` = 
            (SELECT top_sku FROM bikini_order 
            WHERE bikini_order.`top_sku` = bikini_top.`sku` AND 
            bikini_order.`order_id` = @order_id);

      UPDATE bikini_bottom SET bikini_bottom.`stock_amount` =
      bikini_bottom.`stock_amount` + 
      (SELECT amount FROM bikini_order WHERE bikini_order.`bottom_sku` = 
      bikini_bottom.`sku` AND bikini_order.`order_id` = @order_id)
      where bikini_bottom.`sku`= 
      (SELECT bottom_sku FROM bikini_order WHERE bikini_order.`bottom_sku` = 
      bikini_bottom.`sku` AND bikini_order.`order_id` = @order_id);

      UPDATE one_piece_stockable SET one_piece_stockable.`stock_amount` =
      one_piece_stockable.`stock_amount` + 
      (SELECT amount FROM one_piece_order WHERE one_piece_order.`top_sku` =
       one_piece_stockable.`sku` AND one_piece_order.`order_id` = @order_id)
      where one_piece_stockable.`sku` =
      (SELECT top_sku FROM one_piece_order WHERE one_piece_order.`top_sku` =
       one_piece_stockable.`sku` AND one_piece_order.`order_id` = @order_id);
    END IF;
    COMMIT;
END;
EOSTR;
                $db_conn = new PDO("mysql:host=127.0.0.1;dbname=baobab", 'baobab_admin', "E6h2nt4UhYUu+C#C!XJH");
                $db_conn->query($event_create_string);
//                var_dump($db_conn->errorInfo());
                $db_conn = null;
            } catch (\Exception $e) {
                $db_connection->rollBack();
                $this->error_stock = $e->getMessage();
//                var_dump($e->getMessage());
            }
        } else {
            // remember that the user wanted to check out
            setcookie('checkout_login', 'true', 0, '/');
            Redirect::to('/user');
        }
    }

    public function check_order() {
        $this->is_check_order = true;

        $this->email = $this->user_data->email;
        $this->name = Input::get('name');
        $this->address = Input::get('address');
        $this->phone = Input::get('phone');
        $this->city = Input::get('city');

        $country_object = CountriesShippingPricesQuery::create()->findOneById(Input::get('country'));
        $this->shipping_cost = $country_object->getPrice();

        if ($_COOKIE['currency'] !== $country_object->getCurrency()) {
            $convert_to = $_COOKIE['currency'] === 'USD' ? 'COP' : 'USD';
            $swap = (new Builder())
                ->add('fixer')
                ->add('google')
                ->add('yahoo')
                ->build();
            $rate = $swap->latest("$convert_to/{$_COOKIE['currency']}");
            $rate_value = $rate->getValue();
            $this->shipping_cost = intval($this->shipping_cost * floatval($rate_value));
        }

        $this->country = $country_object->getCountryName();

        // update the order created before
        $order_id = Input::get('order-id');
        $order = BaobabOrderQuery::create()->findOneById($order_id);
        $order->setAddress($this->address);
        $order->setPhone($this->phone);
        $order->setCity($this->city);
        $order->setNeighborhood($this->neighborhood);
        $order->setPostalCode(Input::get('postal_code'));
        $order->save();

        $this->payu_api_key = '4BATHi06uospS9DxLGj9Au4hV7';

        $this->payu_url = 'https://gateway.payulatam.com/ppp-web-gateway';
        $this->merchantId = '552092';
        $this->accountId = '554410';
        $this->language = 'en';

        $description_string = '';
        foreach ($this->cart as $cart_item) {
            $ref_exploded = explode('::', $cart_item['ref']);
            $product = ProductQuery::create()->findOneBySlug($ref_exploded[0]);
            if($cart_item['category'] == 'Bikini'){
                $description_string .= $cart_item['cantidad'] . ' x ' . $product->getName() . ' Talla Top: ' . $cart_item['talla_top'] . ' Talla Bottom: ' . $cart_item['talla_bottom'] . ' Push Up: ' . $cart_item['pushup'] . "\n";
            }else{
                $description_string .= $cart_item['cantidad'] . ' x ' . $product->getName() . ' Talla Top: ' . $cart_item['talla_top'] . "\n";
            }

        }

        $this->description = $description_string;

        $date = new \DateTime();
        $this->referenceCode = $order_id;



        $this->amount = intval($this->cart_total + $this->shipping_cost);
        $this->tax = 0;
        $this->taxReturnBase = 0;
        $this->currency = $_COOKIE['currency'];
        $this->signature = md5($this->payu_api_key . '~' . $this->merchantId . '~' . $this->referenceCode . '~' . $this->amount . '~' . $this->currency);
        $this->test = 0;
        $this->responseUrl = "https://{$_SERVER['HTTP_HOST']}/response";
        $this->confirmationUrl = "https://{$_SERVER['HTTP_HOST']}/confirmation.php";
        $this->shippingCountry = 'CO';
        $this->shippingCity = $this->city;
        $this->shippingAddress = $this->address;
        $this->zipCode = Input::get('postal_code');
        $this->buyerFullName = Input::get('buyerFullName');
        $this->payerPhone = Input::get('payerPhone');
        $this->buyerEmail = Input::get('buyerEmail');
        $this->payerMobilePhone = Input::get('payerMobilePhone');
        $this->extra1 = (INPUT::get('extra1')) ? INPUT::get('extra1') : 'None';
        $this->extra2 = (INPUT::get('extra2')) ? INPUT::get('extra2') : 'None';
        $this->envio = floatval(Input::get('envio'));

        // stuff to create the order
//        if($this->extra1 != 'None') {
//            $this->descuento = $this->shop->check_codigo($this->extra1);
//        } else {
//            $this->descuento = 1;
//        }
//
//        if($this->extra2 != 'None') {
//            $this->bono = $this->shop->check_bono($this->extra2);
//        } else {
//            $this->bono = 0;
//        }

//        $datos = array(
//            'nombre' => $this->name,
//            'email' => $this->email,
//            'celular' => $this->phone,
//            'direccion' => $this->address + ", " + $this->neighborhood,
//            'ciudad' => $this->city
//        );
//
//        $this->shop->hacer_pedido($this->referenceCode, $datos, $this->envio, $this->descuento, $this->bono);
    }

    public function cancel_order() {
        $order_id = Input::get('referenceCode');
        $order = BaobabOrderQuery::create()->findOneById($order_id);

        $db_connection = Propel::getWriteConnection(ProductTableMap::DATABASE_NAME);
        $db_connection->beginTransaction();

        $order->setOrderStatus('cancelled');

        // we need to find all skus related to this order and return the stocks
        // this will be exhaustive
        $bikini_orders = BikiniOrderQuery::create()->findByOrderId($order_id);
        foreach ($bikini_orders as $bikini_order) {
            $bikini_top = BikiniTopQuery::create()->findOneBySku($bikini_order->getTopSku());
            $bikini_top->setStockAmount(intval($bikini_top->getStockAmount()) + intval($bikini_order->getAmount()));
            $bikini_top->save();

            $bikini_bottom = BikiniBottomQuery::create()->findOneBySku($bikini_order->getBottomSku());
            $bikini_bottom->setStockAmount(intval($bikini_bottom->getStockAmount()) + intval($bikini_order->getAmount()));
            $bikini_bottom->save();
        }

        $one_piece_orders = OnePieceOrderQuery::create()->findByOrderId($order_id);
        foreach ($one_piece_orders as $one_piece_order) {
            $stockable = OnePieceStockableQuery::create()->findOneBySku($one_piece_order->getTopSku());
            $stockable->setStockAmount(intval($stockable->getStockAmount()) + intval($one_piece_order->getAmount()));
            $stockable->save();
        }

        $order->save();
        $db_connection->commit();

        Redirect::to('/cart');
    }
}
