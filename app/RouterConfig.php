<?php

namespace Baobab\App;

use Velocity\Core\Router;

class RouterConfig {
	public static function execute() {
		Router::get('/', 'home');
		Router::get('/item/(?<slug>[A-Za-z0-9]+(-[A-Za-z0-9]+)*)', 'item#by_slug');
        Router::get('/sizing(.*)', 'sizing');
        Router::get('/cart', 'cart');
        Router::get('/lookbook', 'lookbook');
        Router::get('/customer-care', 'customer');
        Router::get('/how-we-give', 'give');

        /**
         * Shop routes
         */
        // make each one of the filters a different route
        Router::get('/shop(/(?<page>[0-9]+))?', 'shop#all');
        Router::get('/shop/new(/(?<page>[0-9]+))?', 'shop#new_collection');
        Router::get('/shop/sale(/(?<page>[0-9]+))?', 'shop#sale');
        // swimwear categories
        Router::get('/shop/swimwear(/(?<page>[0-9]+))?', 'shop#all');
        Router::get('/shop/swimwear/(?<swimwear_category>(bikini|one-piece))(/(?<page>[0-9]+))?', 'shop#swimwear');

        // for the ones with filters
        Router::post('/shop(/(?<page>[0-9]+))?', 'shop#all');
        Router::post('/shop/swimwear(/(?<page>[0-9]+))?', 'shop#all');
        Router::post('/shop/swimwear/(?<swimwear_category>(bikinis|mix|one-piece))(/(?<page>[0-9]+))?', 'shop#swimwear');

		/**
		 * User routes
		 */
		Router::get('/user', 'user#get_orders');
        Router::get('/user/orders', 'user#get_orders');
        Router::get('/user/addresses', 'user#get_addresses');
        Router::get('/user/promo_codes', 'user#get_promo_codes');
        Router::get('/user/favorites', 'user#get_favorites');

        // user auth and crud
        Router::post('/user/login', 'user#login');
        Router::post('/user/logout', 'user#logout');
        Router::post('/user/register', 'user#register');

        // User addresses
        Router::post('/user/addresses', 'user#modify_address');

        /**
         * Ecommerce routes
         */
        // add to cart etc
        Router::post('/api/add_to_cart', 'ajax#add_to_cart');
        Router::post('/api/remove_from_cart', 'ajax#remove_from_cart');
        Router::post('/api/modify_amount_in_cart', 'ajax#modify_amount_in_cart');
        Router::post('/api/empty_cart', 'ajax#empty_cart');
        Router::post('/api/search', 'ajax#search');
        Router::post('/api/get_availability', 'ajax#get_availability');
        Router::post('/api/shipping_price_for', 'ajax#shipping_price_for');

        // add to favorites etc
        Router::post('/api/add_to_favorites', 'ajax#add_to_favorites');
        Router::post('/api/remove_from_favorites', 'ajax#remove_from_favorites');

        // payments etc
        Router::post('/cart/checkout', 'cart#checkout');
        Router::post('/cart/check_order', 'cart#check_order');
        Router::post('/cart/cancel_order', 'cart#cancel_order');

        // payment system responses etc
        Router::get('/response(.*)', 'payments#response');
	}
}
