<?php

namespace BaobabModels;

use BaobabModels\PricesCOPQuery;
use BaobabModels\Base\Product as BaseProduct;

/**
 * Skeleton subclass for representing a row from the 'product' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class Product extends BaseProduct
{
    function getPrice($pushup = null) {
        $currency = $_COOKIE['currency'];
        $query_class_name = '\\BaobabModels\\Prices' . $currency . 'Query';
        $query = new $query_class_name;
        $product_object = $query->create()->findOneByProductSlug($this->slug);
        $normal_price = $product_object->getPrice();

        if($this->getChildObject() instanceof Bikini) {
            if($pushup) {
                $normal_price += $this->get_pushup_surcharge();
            }
        }

        return $normal_price;
    }

    private function get_pushup_surcharge() {
        if($_COOKIE['currency'] === 'COP') {
            return 2000;
        } else {
            return 2;
        }
    }
}
