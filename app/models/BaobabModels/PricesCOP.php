<?php

namespace BaobabModels;

use BaobabModels\Base\PricesCOP as BasePricesCOP;

/**
 * Skeleton subclass for representing a row from the 'prices_cop' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class PricesCOP extends BasePricesCOP
{

}
