<?php

namespace BaobabModels;

use BaobabModels\Base\BikiniQuery as BaseBikiniQuery;

/**
 * Skeleton subclass for performing query and update operations on the 'bikini' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class BikiniQuery extends BaseBikiniQuery
{

}
