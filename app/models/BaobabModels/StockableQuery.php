<?php

namespace BaobabModels;

use BaobabModels\Base\StockableQuery as BaseStockableQuery;

/**
 * Skeleton subclass for performing query and update operations on the 'stockable' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class StockableQuery extends BaseStockableQuery
{

}
