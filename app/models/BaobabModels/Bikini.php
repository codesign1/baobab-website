<?php

namespace BaobabModels;

use BaobabModels\Base\Bikini as BaseBikini;

/**
 * Skeleton subclass for representing a row from the 'bikini' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class Bikini extends BaseBikini
{
    function getPrice() {
        $currency = $_COOKIE['currency'];
        $query_class_name = '\\BaobabModels\\Prices' . $currency . 'Query';
        $query = new $query_class_name;
        return $query->create()->findOneByProductSlug($this->slug)->getPrice();
    }
}
