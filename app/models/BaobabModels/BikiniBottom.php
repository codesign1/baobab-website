<?php

namespace BaobabModels;

use BaobabModels\Base\BikiniBottom as BaseBikiniBottom;

/**
 * Skeleton subclass for representing a row from the 'bikini_bottom' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class BikiniBottom extends BaseBikiniBottom
{

}
