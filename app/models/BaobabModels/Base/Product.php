<?php

namespace BaobabModels\Base;

use \Exception;
use \PDO;
use BaobabModels\Bikini as ChildBikini;
use BaobabModels\BikiniQuery as ChildBikiniQuery;
use BaobabModels\OnePiece as ChildOnePiece;
use BaobabModels\OnePieceQuery as ChildOnePieceQuery;
use BaobabModels\PricesCOP as ChildPricesCOP;
use BaobabModels\PricesCOPQuery as ChildPricesCOPQuery;
use BaobabModels\PricesUSD as ChildPricesUSD;
use BaobabModels\PricesUSDQuery as ChildPricesUSDQuery;
use BaobabModels\Product as ChildProduct;
use BaobabModels\ProductImages as ChildProductImages;
use BaobabModels\ProductImagesQuery as ChildProductImagesQuery;
use BaobabModels\ProductQuery as ChildProductQuery;
use BaobabModels\Map\PricesCOPTableMap;
use BaobabModels\Map\PricesUSDTableMap;
use BaobabModels\Map\ProductImagesTableMap;
use BaobabModels\Map\ProductTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\PropelQuery;
use Propel\Runtime\ActiveRecord\ActiveRecordInterface;
use Propel\Runtime\Collection\Collection;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\BadMethodCallException;
use Propel\Runtime\Exception\LogicException;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Parser\AbstractParser;

/**
 * Base class that represents a row from the 'product' table.
 *
 *
 *
 * @package    propel.generator.BaobabModels.Base
 */
abstract class Product implements ActiveRecordInterface
{
    /**
     * TableMap class name
     */
    const TABLE_MAP = '\\BaobabModels\\Map\\ProductTableMap';


    /**
     * attribute to determine if this object has previously been saved.
     * @var boolean
     */
    protected $new = true;

    /**
     * attribute to determine whether this object has been deleted.
     * @var boolean
     */
    protected $deleted = false;

    /**
     * The columns that have been modified in current object.
     * Tracking modified columns allows us to only update modified columns.
     * @var array
     */
    protected $modifiedColumns = array();

    /**
     * The (virtual) columns that are added at runtime
     * The formatters can add supplementary columns based on a resultset
     * @var array
     */
    protected $virtualColumns = array();

    /**
     * The value for the slug field.
     *
     * @var        string
     */
    protected $slug;

    /**
     * The value for the name field.
     *
     * @var        string
     */
    protected $name;

    /**
     * The value for the show_order field.
     *
     * @var        int
     */
    protected $show_order;

    /**
     * The value for the description field.
     *
     * @var        string
     */
    protected $description;

    /**
     * The value for the descendant_class field.
     *
     * @var        string
     */
    protected $descendant_class;

    /**
     * @var        ObjectCollection|ChildProductImages[] Collection to store aggregation of ChildProductImages objects.
     */
    protected $collProductImagess;
    protected $collProductImagessPartial;

    /**
     * @var        ObjectCollection|ChildPricesCOP[] Collection to store aggregation of ChildPricesCOP objects.
     */
    protected $collPricesCOPs;
    protected $collPricesCOPsPartial;

    /**
     * @var        ObjectCollection|ChildPricesUSD[] Collection to store aggregation of ChildPricesUSD objects.
     */
    protected $collPricesUSDs;
    protected $collPricesUSDsPartial;

    /**
     * @var        ChildBikini one-to-one related ChildBikini object
     */
    protected $singleBikini;

    /**
     * @var        ChildOnePiece one-to-one related ChildOnePiece object
     */
    protected $singleOnePiece;

    /**
     * Flag to prevent endless save loop, if this object is referenced
     * by another object which falls in this transaction.
     *
     * @var boolean
     */
    protected $alreadyInSave = false;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildProductImages[]
     */
    protected $productImagessScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildPricesCOP[]
     */
    protected $pricesCOPsScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildPricesUSD[]
     */
    protected $pricesUSDsScheduledForDeletion = null;

    /**
     * Initializes internal state of BaobabModels\Base\Product object.
     */
    public function __construct()
    {
    }

    /**
     * Returns whether the object has been modified.
     *
     * @return boolean True if the object has been modified.
     */
    public function isModified()
    {
        return !!$this->modifiedColumns;
    }

    /**
     * Has specified column been modified?
     *
     * @param  string  $col column fully qualified name (TableMap::TYPE_COLNAME), e.g. Book::AUTHOR_ID
     * @return boolean True if $col has been modified.
     */
    public function isColumnModified($col)
    {
        return $this->modifiedColumns && isset($this->modifiedColumns[$col]);
    }

    /**
     * Get the columns that have been modified in this object.
     * @return array A unique list of the modified column names for this object.
     */
    public function getModifiedColumns()
    {
        return $this->modifiedColumns ? array_keys($this->modifiedColumns) : [];
    }

    /**
     * Returns whether the object has ever been saved.  This will
     * be false, if the object was retrieved from storage or was created
     * and then saved.
     *
     * @return boolean true, if the object has never been persisted.
     */
    public function isNew()
    {
        return $this->new;
    }

    /**
     * Setter for the isNew attribute.  This method will be called
     * by Propel-generated children and objects.
     *
     * @param boolean $b the state of the object.
     */
    public function setNew($b)
    {
        $this->new = (boolean) $b;
    }

    /**
     * Whether this object has been deleted.
     * @return boolean The deleted state of this object.
     */
    public function isDeleted()
    {
        return $this->deleted;
    }

    /**
     * Specify whether this object has been deleted.
     * @param  boolean $b The deleted state of this object.
     * @return void
     */
    public function setDeleted($b)
    {
        $this->deleted = (boolean) $b;
    }

    /**
     * Sets the modified state for the object to be false.
     * @param  string $col If supplied, only the specified column is reset.
     * @return void
     */
    public function resetModified($col = null)
    {
        if (null !== $col) {
            if (isset($this->modifiedColumns[$col])) {
                unset($this->modifiedColumns[$col]);
            }
        } else {
            $this->modifiedColumns = array();
        }
    }

    /**
     * Compares this with another <code>Product</code> instance.  If
     * <code>obj</code> is an instance of <code>Product</code>, delegates to
     * <code>equals(Product)</code>.  Otherwise, returns <code>false</code>.
     *
     * @param  mixed   $obj The object to compare to.
     * @return boolean Whether equal to the object specified.
     */
    public function equals($obj)
    {
        if (!$obj instanceof static) {
            return false;
        }

        if ($this === $obj) {
            return true;
        }

        if (null === $this->getPrimaryKey() || null === $obj->getPrimaryKey()) {
            return false;
        }

        return $this->getPrimaryKey() === $obj->getPrimaryKey();
    }

    /**
     * Get the associative array of the virtual columns in this object
     *
     * @return array
     */
    public function getVirtualColumns()
    {
        return $this->virtualColumns;
    }

    /**
     * Checks the existence of a virtual column in this object
     *
     * @param  string  $name The virtual column name
     * @return boolean
     */
    public function hasVirtualColumn($name)
    {
        return array_key_exists($name, $this->virtualColumns);
    }

    /**
     * Get the value of a virtual column in this object
     *
     * @param  string $name The virtual column name
     * @return mixed
     *
     * @throws PropelException
     */
    public function getVirtualColumn($name)
    {
        if (!$this->hasVirtualColumn($name)) {
            throw new PropelException(sprintf('Cannot get value of inexistent virtual column %s.', $name));
        }

        return $this->virtualColumns[$name];
    }

    /**
     * Set the value of a virtual column in this object
     *
     * @param string $name  The virtual column name
     * @param mixed  $value The value to give to the virtual column
     *
     * @return $this|Product The current object, for fluid interface
     */
    public function setVirtualColumn($name, $value)
    {
        $this->virtualColumns[$name] = $value;

        return $this;
    }

    /**
     * Logs a message using Propel::log().
     *
     * @param  string  $msg
     * @param  int     $priority One of the Propel::LOG_* logging levels
     * @return boolean
     */
    protected function log($msg, $priority = Propel::LOG_INFO)
    {
        return Propel::log(get_class($this) . ': ' . $msg, $priority);
    }

    /**
     * Export the current object properties to a string, using a given parser format
     * <code>
     * $book = BookQuery::create()->findPk(9012);
     * echo $book->exportTo('JSON');
     *  => {"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * @param  mixed   $parser                 A AbstractParser instance, or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param  boolean $includeLazyLoadColumns (optional) Whether to include lazy load(ed) columns. Defaults to TRUE.
     * @return string  The exported data
     */
    public function exportTo($parser, $includeLazyLoadColumns = true)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        return $parser->fromArray($this->toArray(TableMap::TYPE_PHPNAME, $includeLazyLoadColumns, array(), true));
    }

    /**
     * Clean up internal collections prior to serializing
     * Avoids recursive loops that turn into segmentation faults when serializing
     */
    public function __sleep()
    {
        $this->clearAllReferences();

        $cls = new \ReflectionClass($this);
        $propertyNames = [];
        $serializableProperties = array_diff($cls->getProperties(), $cls->getProperties(\ReflectionProperty::IS_STATIC));

        foreach($serializableProperties as $property) {
            $propertyNames[] = $property->getName();
        }

        return $propertyNames;
    }

    /**
     * Get the [slug] column value.
     *
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * Get the [name] column value.
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Get the [show_order] column value.
     *
     * @return int
     */
    public function getShowOrder()
    {
        return $this->show_order;
    }

    /**
     * Get the [description] column value.
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Get the [descendant_class] column value.
     *
     * @return string
     */
    public function getDescendantClass()
    {
        return $this->descendant_class;
    }

    /**
     * Set the value of [slug] column.
     *
     * @param string $v new value
     * @return $this|\BaobabModels\Product The current object (for fluent API support)
     */
    public function setSlug($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->slug !== $v) {
            $this->slug = $v;
            $this->modifiedColumns[ProductTableMap::COL_SLUG] = true;
        }

        return $this;
    } // setSlug()

    /**
     * Set the value of [name] column.
     *
     * @param string $v new value
     * @return $this|\BaobabModels\Product The current object (for fluent API support)
     */
    public function setName($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->name !== $v) {
            $this->name = $v;
            $this->modifiedColumns[ProductTableMap::COL_NAME] = true;
        }

        return $this;
    } // setName()

    /**
     * Set the value of [show_order] column.
     *
     * @param int $v new value
     * @return $this|\BaobabModels\Product The current object (for fluent API support)
     */
    public function setShowOrder($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->show_order !== $v) {
            $this->show_order = $v;
            $this->modifiedColumns[ProductTableMap::COL_SHOW_ORDER] = true;
        }

        return $this;
    } // setShowOrder()

    /**
     * Set the value of [description] column.
     *
     * @param string $v new value
     * @return $this|\BaobabModels\Product The current object (for fluent API support)
     */
    public function setDescription($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->description !== $v) {
            $this->description = $v;
            $this->modifiedColumns[ProductTableMap::COL_DESCRIPTION] = true;
        }

        return $this;
    } // setDescription()

    /**
     * Set the value of [descendant_class] column.
     *
     * @param string $v new value
     * @return $this|\BaobabModels\Product The current object (for fluent API support)
     */
    public function setDescendantClass($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->descendant_class !== $v) {
            $this->descendant_class = $v;
            $this->modifiedColumns[ProductTableMap::COL_DESCENDANT_CLASS] = true;
        }

        return $this;
    } // setDescendantClass()

    /**
     * Indicates whether the columns in this object are only set to default values.
     *
     * This method can be used in conjunction with isModified() to indicate whether an object is both
     * modified _and_ has some values set which are non-default.
     *
     * @return boolean Whether the columns in this object are only been set with default values.
     */
    public function hasOnlyDefaultValues()
    {
        // otherwise, everything was equal, so return TRUE
        return true;
    } // hasOnlyDefaultValues()

    /**
     * Hydrates (populates) the object variables with values from the database resultset.
     *
     * An offset (0-based "start column") is specified so that objects can be hydrated
     * with a subset of the columns in the resultset rows.  This is needed, for example,
     * for results of JOIN queries where the resultset row includes columns from two or
     * more tables.
     *
     * @param array   $row       The row returned by DataFetcher->fetch().
     * @param int     $startcol  0-based offset column which indicates which restultset column to start with.
     * @param boolean $rehydrate Whether this object is being re-hydrated from the database.
     * @param string  $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                  One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                            TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @return int             next starting column
     * @throws PropelException - Any caught Exception will be rewrapped as a PropelException.
     */
    public function hydrate($row, $startcol = 0, $rehydrate = false, $indexType = TableMap::TYPE_NUM)
    {
        try {

            $col = $row[TableMap::TYPE_NUM == $indexType ? 0 + $startcol : ProductTableMap::translateFieldName('Slug', TableMap::TYPE_PHPNAME, $indexType)];
            $this->slug = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 1 + $startcol : ProductTableMap::translateFieldName('Name', TableMap::TYPE_PHPNAME, $indexType)];
            $this->name = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 2 + $startcol : ProductTableMap::translateFieldName('ShowOrder', TableMap::TYPE_PHPNAME, $indexType)];
            $this->show_order = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 3 + $startcol : ProductTableMap::translateFieldName('Description', TableMap::TYPE_PHPNAME, $indexType)];
            $this->description = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 4 + $startcol : ProductTableMap::translateFieldName('DescendantClass', TableMap::TYPE_PHPNAME, $indexType)];
            $this->descendant_class = (null !== $col) ? (string) $col : null;
            $this->resetModified();

            $this->setNew(false);

            if ($rehydrate) {
                $this->ensureConsistency();
            }

            return $startcol + 5; // 5 = ProductTableMap::NUM_HYDRATE_COLUMNS.

        } catch (Exception $e) {
            throw new PropelException(sprintf('Error populating %s object', '\\BaobabModels\\Product'), 0, $e);
        }
    }

    /**
     * Checks and repairs the internal consistency of the object.
     *
     * This method is executed after an already-instantiated object is re-hydrated
     * from the database.  It exists to check any foreign keys to make sure that
     * the objects related to the current object are correct based on foreign key.
     *
     * You can override this method in the stub class, but you should always invoke
     * the base method from the overridden method (i.e. parent::ensureConsistency()),
     * in case your model changes.
     *
     * @throws PropelException
     */
    public function ensureConsistency()
    {
    } // ensureConsistency

    /**
     * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
     *
     * This will only work if the object has been saved and has a valid primary key set.
     *
     * @param      boolean $deep (optional) Whether to also de-associated any related objects.
     * @param      ConnectionInterface $con (optional) The ConnectionInterface connection to use.
     * @return void
     * @throws PropelException - if this object is deleted, unsaved or doesn't have pk match in db
     */
    public function reload($deep = false, ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("Cannot reload a deleted object.");
        }

        if ($this->isNew()) {
            throw new PropelException("Cannot reload an unsaved object.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(ProductTableMap::DATABASE_NAME);
        }

        // We don't need to alter the object instance pool; we're just modifying this instance
        // already in the pool.

        $dataFetcher = ChildProductQuery::create(null, $this->buildPkeyCriteria())->setFormatter(ModelCriteria::FORMAT_STATEMENT)->find($con);
        $row = $dataFetcher->fetch();
        $dataFetcher->close();
        if (!$row) {
            throw new PropelException('Cannot find matching row in the database to reload object values.');
        }
        $this->hydrate($row, 0, true, $dataFetcher->getIndexType()); // rehydrate

        if ($deep) {  // also de-associate any related objects?

            $this->collProductImagess = null;

            $this->collPricesCOPs = null;

            $this->collPricesUSDs = null;

            $this->singleBikini = null;

            $this->singleOnePiece = null;

        } // if (deep)
    }

    /**
     * Removes this object from datastore and sets delete attribute.
     *
     * @param      ConnectionInterface $con
     * @return void
     * @throws PropelException
     * @see Product::setDeleted()
     * @see Product::isDeleted()
     */
    public function delete(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("This object has already been deleted.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(ProductTableMap::DATABASE_NAME);
        }

        $con->transaction(function () use ($con) {
            $deleteQuery = ChildProductQuery::create()
                ->filterByPrimaryKey($this->getPrimaryKey());
            $ret = $this->preDelete($con);
            if ($ret) {
                $deleteQuery->delete($con);
                $this->postDelete($con);
                $this->setDeleted(true);
            }
        });
    }

    /**
     * Persists this object to the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All modified related objects will also be persisted in the doSave()
     * method.  This method wraps all precipitate database operations in a
     * single transaction.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see doSave()
     */
    public function save(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("You cannot save an object that has been deleted.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(ProductTableMap::DATABASE_NAME);
        }

        return $con->transaction(function () use ($con) {
            $ret = $this->preSave($con);
            $isInsert = $this->isNew();
            if ($isInsert) {
                $ret = $ret && $this->preInsert($con);
            } else {
                $ret = $ret && $this->preUpdate($con);
            }
            if ($ret) {
                $affectedRows = $this->doSave($con);
                if ($isInsert) {
                    $this->postInsert($con);
                } else {
                    $this->postUpdate($con);
                }
                $this->postSave($con);
                ProductTableMap::addInstanceToPool($this);
            } else {
                $affectedRows = 0;
            }

            return $affectedRows;
        });
    }

    /**
     * Performs the work of inserting or updating the row in the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All related objects are also updated in this method.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see save()
     */
    protected function doSave(ConnectionInterface $con)
    {
        $affectedRows = 0; // initialize var to track total num of affected rows
        if (!$this->alreadyInSave) {
            $this->alreadyInSave = true;

            if ($this->isNew() || $this->isModified()) {
                // persist changes
                if ($this->isNew()) {
                    $this->doInsert($con);
                    $affectedRows += 1;
                } else {
                    $affectedRows += $this->doUpdate($con);
                }
                $this->resetModified();
            }

            if ($this->productImagessScheduledForDeletion !== null) {
                if (!$this->productImagessScheduledForDeletion->isEmpty()) {
                    \BaobabModels\ProductImagesQuery::create()
                        ->filterByPrimaryKeys($this->productImagessScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->productImagessScheduledForDeletion = null;
                }
            }

            if ($this->collProductImagess !== null) {
                foreach ($this->collProductImagess as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->pricesCOPsScheduledForDeletion !== null) {
                if (!$this->pricesCOPsScheduledForDeletion->isEmpty()) {
                    \BaobabModels\PricesCOPQuery::create()
                        ->filterByPrimaryKeys($this->pricesCOPsScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->pricesCOPsScheduledForDeletion = null;
                }
            }

            if ($this->collPricesCOPs !== null) {
                foreach ($this->collPricesCOPs as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->pricesUSDsScheduledForDeletion !== null) {
                if (!$this->pricesUSDsScheduledForDeletion->isEmpty()) {
                    \BaobabModels\PricesUSDQuery::create()
                        ->filterByPrimaryKeys($this->pricesUSDsScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->pricesUSDsScheduledForDeletion = null;
                }
            }

            if ($this->collPricesUSDs !== null) {
                foreach ($this->collPricesUSDs as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->singleBikini !== null) {
                if (!$this->singleBikini->isDeleted() && ($this->singleBikini->isNew() || $this->singleBikini->isModified())) {
                    $affectedRows += $this->singleBikini->save($con);
                }
            }

            if ($this->singleOnePiece !== null) {
                if (!$this->singleOnePiece->isDeleted() && ($this->singleOnePiece->isNew() || $this->singleOnePiece->isModified())) {
                    $affectedRows += $this->singleOnePiece->save($con);
                }
            }

            $this->alreadyInSave = false;

        }

        return $affectedRows;
    } // doSave()

    /**
     * Insert the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @throws PropelException
     * @see doSave()
     */
    protected function doInsert(ConnectionInterface $con)
    {
        $modifiedColumns = array();
        $index = 0;


         // check the columns in natural order for more readable SQL queries
        if ($this->isColumnModified(ProductTableMap::COL_SLUG)) {
            $modifiedColumns[':p' . $index++]  = 'slug';
        }
        if ($this->isColumnModified(ProductTableMap::COL_NAME)) {
            $modifiedColumns[':p' . $index++]  = 'name';
        }
        if ($this->isColumnModified(ProductTableMap::COL_SHOW_ORDER)) {
            $modifiedColumns[':p' . $index++]  = 'show_order';
        }
        if ($this->isColumnModified(ProductTableMap::COL_DESCRIPTION)) {
            $modifiedColumns[':p' . $index++]  = 'description';
        }
        if ($this->isColumnModified(ProductTableMap::COL_DESCENDANT_CLASS)) {
            $modifiedColumns[':p' . $index++]  = 'descendant_class';
        }

        $sql = sprintf(
            'INSERT INTO product (%s) VALUES (%s)',
            implode(', ', $modifiedColumns),
            implode(', ', array_keys($modifiedColumns))
        );

        try {
            $stmt = $con->prepare($sql);
            foreach ($modifiedColumns as $identifier => $columnName) {
                switch ($columnName) {
                    case 'slug':
                        $stmt->bindValue($identifier, $this->slug, PDO::PARAM_STR);
                        break;
                    case 'name':
                        $stmt->bindValue($identifier, $this->name, PDO::PARAM_STR);
                        break;
                    case 'show_order':
                        $stmt->bindValue($identifier, $this->show_order, PDO::PARAM_INT);
                        break;
                    case 'description':
                        $stmt->bindValue($identifier, $this->description, PDO::PARAM_STR);
                        break;
                    case 'descendant_class':
                        $stmt->bindValue($identifier, $this->descendant_class, PDO::PARAM_STR);
                        break;
                }
            }
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute INSERT statement [%s]', $sql), 0, $e);
        }

        $this->setNew(false);
    }

    /**
     * Update the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @return Integer Number of updated rows
     * @see doSave()
     */
    protected function doUpdate(ConnectionInterface $con)
    {
        $selectCriteria = $this->buildPkeyCriteria();
        $valuesCriteria = $this->buildCriteria();

        return $selectCriteria->doUpdate($valuesCriteria, $con);
    }

    /**
     * Retrieves a field from the object by name passed in as a string.
     *
     * @param      string $name name
     * @param      string $type The type of fieldname the $name is of:
     *                     one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                     TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                     Defaults to TableMap::TYPE_PHPNAME.
     * @return mixed Value of field.
     */
    public function getByName($name, $type = TableMap::TYPE_PHPNAME)
    {
        $pos = ProductTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);
        $field = $this->getByPosition($pos);

        return $field;
    }

    /**
     * Retrieves a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param      int $pos position in xml schema
     * @return mixed Value of field at $pos
     */
    public function getByPosition($pos)
    {
        switch ($pos) {
            case 0:
                return $this->getSlug();
                break;
            case 1:
                return $this->getName();
                break;
            case 2:
                return $this->getShowOrder();
                break;
            case 3:
                return $this->getDescription();
                break;
            case 4:
                return $this->getDescendantClass();
                break;
            default:
                return null;
                break;
        } // switch()
    }

    /**
     * Exports the object as an array.
     *
     * You can specify the key type of the array by passing one of the class
     * type constants.
     *
     * @param     string  $keyType (optional) One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     *                    TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                    Defaults to TableMap::TYPE_PHPNAME.
     * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to TRUE.
     * @param     array $alreadyDumpedObjects List of objects to skip to avoid recursion
     * @param     boolean $includeForeignObjects (optional) Whether to include hydrated related objects. Default to FALSE.
     *
     * @return array an associative array containing the field names (as keys) and field values
     */
    public function toArray($keyType = TableMap::TYPE_PHPNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array(), $includeForeignObjects = false)
    {

        if (isset($alreadyDumpedObjects['Product'][$this->hashCode()])) {
            return '*RECURSION*';
        }
        $alreadyDumpedObjects['Product'][$this->hashCode()] = true;
        $keys = ProductTableMap::getFieldNames($keyType);
        $result = array(
            $keys[0] => $this->getSlug(),
            $keys[1] => $this->getName(),
            $keys[2] => $this->getShowOrder(),
            $keys[3] => $this->getDescription(),
            $keys[4] => $this->getDescendantClass(),
        );
        $virtualColumns = $this->virtualColumns;
        foreach ($virtualColumns as $key => $virtualColumn) {
            $result[$key] = $virtualColumn;
        }

        if ($includeForeignObjects) {
            if (null !== $this->collProductImagess) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'productImagess';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'product_imagess';
                        break;
                    default:
                        $key = 'ProductImagess';
                }

                $result[$key] = $this->collProductImagess->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collPricesCOPs) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'pricesCOPs';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'prices_cops';
                        break;
                    default:
                        $key = 'PricesCOPs';
                }

                $result[$key] = $this->collPricesCOPs->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collPricesUSDs) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'pricesUSDs';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'prices_usds';
                        break;
                    default:
                        $key = 'PricesUSDs';
                }

                $result[$key] = $this->collPricesUSDs->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->singleBikini) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'bikini';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'bikini';
                        break;
                    default:
                        $key = 'Bikini';
                }

                $result[$key] = $this->singleBikini->toArray($keyType, $includeLazyLoadColumns, $alreadyDumpedObjects, true);
            }
            if (null !== $this->singleOnePiece) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'onePiece';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'one_piece';
                        break;
                    default:
                        $key = 'OnePiece';
                }

                $result[$key] = $this->singleOnePiece->toArray($keyType, $includeLazyLoadColumns, $alreadyDumpedObjects, true);
            }
        }

        return $result;
    }

    /**
     * Sets a field from the object by name passed in as a string.
     *
     * @param  string $name
     * @param  mixed  $value field value
     * @param  string $type The type of fieldname the $name is of:
     *                one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                Defaults to TableMap::TYPE_PHPNAME.
     * @return $this|\BaobabModels\Product
     */
    public function setByName($name, $value, $type = TableMap::TYPE_PHPNAME)
    {
        $pos = ProductTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);

        return $this->setByPosition($pos, $value);
    }

    /**
     * Sets a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param  int $pos position in xml schema
     * @param  mixed $value field value
     * @return $this|\BaobabModels\Product
     */
    public function setByPosition($pos, $value)
    {
        switch ($pos) {
            case 0:
                $this->setSlug($value);
                break;
            case 1:
                $this->setName($value);
                break;
            case 2:
                $this->setShowOrder($value);
                break;
            case 3:
                $this->setDescription($value);
                break;
            case 4:
                $this->setDescendantClass($value);
                break;
        } // switch()

        return $this;
    }

    /**
     * Populates the object using an array.
     *
     * This is particularly useful when populating an object from one of the
     * request arrays (e.g. $_POST).  This method goes through the column
     * names, checking to see whether a matching key exists in populated
     * array. If so the setByName() method is called for that column.
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_PHPNAME.
     *
     * @param      array  $arr     An array to populate the object from.
     * @param      string $keyType The type of keys the array uses.
     * @return void
     */
    public function fromArray($arr, $keyType = TableMap::TYPE_PHPNAME)
    {
        $keys = ProductTableMap::getFieldNames($keyType);

        if (array_key_exists($keys[0], $arr)) {
            $this->setSlug($arr[$keys[0]]);
        }
        if (array_key_exists($keys[1], $arr)) {
            $this->setName($arr[$keys[1]]);
        }
        if (array_key_exists($keys[2], $arr)) {
            $this->setShowOrder($arr[$keys[2]]);
        }
        if (array_key_exists($keys[3], $arr)) {
            $this->setDescription($arr[$keys[3]]);
        }
        if (array_key_exists($keys[4], $arr)) {
            $this->setDescendantClass($arr[$keys[4]]);
        }
    }

     /**
     * Populate the current object from a string, using a given parser format
     * <code>
     * $book = new Book();
     * $book->importFrom('JSON', '{"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_PHPNAME.
     *
     * @param mixed $parser A AbstractParser instance,
     *                       or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param string $data The source data to import from
     * @param string $keyType The type of keys the array uses.
     *
     * @return $this|\BaobabModels\Product The current object, for fluid interface
     */
    public function importFrom($parser, $data, $keyType = TableMap::TYPE_PHPNAME)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        $this->fromArray($parser->toArray($data), $keyType);

        return $this;
    }

    /**
     * Build a Criteria object containing the values of all modified columns in this object.
     *
     * @return Criteria The Criteria object containing all modified values.
     */
    public function buildCriteria()
    {
        $criteria = new Criteria(ProductTableMap::DATABASE_NAME);

        if ($this->isColumnModified(ProductTableMap::COL_SLUG)) {
            $criteria->add(ProductTableMap::COL_SLUG, $this->slug);
        }
        if ($this->isColumnModified(ProductTableMap::COL_NAME)) {
            $criteria->add(ProductTableMap::COL_NAME, $this->name);
        }
        if ($this->isColumnModified(ProductTableMap::COL_SHOW_ORDER)) {
            $criteria->add(ProductTableMap::COL_SHOW_ORDER, $this->show_order);
        }
        if ($this->isColumnModified(ProductTableMap::COL_DESCRIPTION)) {
            $criteria->add(ProductTableMap::COL_DESCRIPTION, $this->description);
        }
        if ($this->isColumnModified(ProductTableMap::COL_DESCENDANT_CLASS)) {
            $criteria->add(ProductTableMap::COL_DESCENDANT_CLASS, $this->descendant_class);
        }

        return $criteria;
    }

    /**
     * Builds a Criteria object containing the primary key for this object.
     *
     * Unlike buildCriteria() this method includes the primary key values regardless
     * of whether or not they have been modified.
     *
     * @throws LogicException if no primary key is defined
     *
     * @return Criteria The Criteria object containing value(s) for primary key(s).
     */
    public function buildPkeyCriteria()
    {
        $criteria = ChildProductQuery::create();
        $criteria->add(ProductTableMap::COL_SLUG, $this->slug);

        return $criteria;
    }

    /**
     * If the primary key is not null, return the hashcode of the
     * primary key. Otherwise, return the hash code of the object.
     *
     * @return int Hashcode
     */
    public function hashCode()
    {
        $validPk = null !== $this->getSlug();

        $validPrimaryKeyFKs = 0;
        $primaryKeyFKs = [];

        if ($validPk) {
            return crc32(json_encode($this->getPrimaryKey(), JSON_UNESCAPED_UNICODE));
        } elseif ($validPrimaryKeyFKs) {
            return crc32(json_encode($primaryKeyFKs, JSON_UNESCAPED_UNICODE));
        }

        return spl_object_hash($this);
    }

    /**
     * Returns the primary key for this object (row).
     * @return string
     */
    public function getPrimaryKey()
    {
        return $this->getSlug();
    }

    /**
     * Generic method to set the primary key (slug column).
     *
     * @param       string $key Primary key.
     * @return void
     */
    public function setPrimaryKey($key)
    {
        $this->setSlug($key);
    }

    /**
     * Returns true if the primary key for this object is null.
     * @return boolean
     */
    public function isPrimaryKeyNull()
    {
        return null === $this->getSlug();
    }

    /**
     * Sets contents of passed object to values from current object.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param      object $copyObj An object of \BaobabModels\Product (or compatible) type.
     * @param      boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @param      boolean $makeNew Whether to reset autoincrement PKs and make the object new.
     * @throws PropelException
     */
    public function copyInto($copyObj, $deepCopy = false, $makeNew = true)
    {
        $copyObj->setSlug($this->getSlug());
        $copyObj->setName($this->getName());
        $copyObj->setShowOrder($this->getShowOrder());
        $copyObj->setDescription($this->getDescription());
        $copyObj->setDescendantClass($this->getDescendantClass());

        if ($deepCopy) {
            // important: temporarily setNew(false) because this affects the behavior of
            // the getter/setter methods for fkey referrer objects.
            $copyObj->setNew(false);

            foreach ($this->getProductImagess() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addProductImages($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getPricesCOPs() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addPricesCOP($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getPricesUSDs() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addPricesUSD($relObj->copy($deepCopy));
                }
            }

            $relObj = $this->getBikini();
            if ($relObj) {
                $copyObj->setBikini($relObj->copy($deepCopy));
            }

            $relObj = $this->getOnePiece();
            if ($relObj) {
                $copyObj->setOnePiece($relObj->copy($deepCopy));
            }

        } // if ($deepCopy)

        if ($makeNew) {
            $copyObj->setNew(true);
        }
    }

    /**
     * Makes a copy of this object that will be inserted as a new row in table when saved.
     * It creates a new object filling in the simple attributes, but skipping any primary
     * keys that are defined for the table.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param  boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @return \BaobabModels\Product Clone of current object.
     * @throws PropelException
     */
    public function copy($deepCopy = false)
    {
        // we use get_class(), because this might be a subclass
        $clazz = get_class($this);
        $copyObj = new $clazz();
        $this->copyInto($copyObj, $deepCopy);

        return $copyObj;
    }


    /**
     * Initializes a collection based on the name of a relation.
     * Avoids crafting an 'init[$relationName]s' method name
     * that wouldn't work when StandardEnglishPluralizer is used.
     *
     * @param      string $relationName The name of the relation to initialize
     * @return void
     */
    public function initRelation($relationName)
    {
        if ('ProductImages' == $relationName) {
            return $this->initProductImagess();
        }
        if ('PricesCOP' == $relationName) {
            return $this->initPricesCOPs();
        }
        if ('PricesUSD' == $relationName) {
            return $this->initPricesUSDs();
        }
    }

    /**
     * Clears out the collProductImagess collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addProductImagess()
     */
    public function clearProductImagess()
    {
        $this->collProductImagess = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collProductImagess collection loaded partially.
     */
    public function resetPartialProductImagess($v = true)
    {
        $this->collProductImagessPartial = $v;
    }

    /**
     * Initializes the collProductImagess collection.
     *
     * By default this just sets the collProductImagess collection to an empty array (like clearcollProductImagess());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initProductImagess($overrideExisting = true)
    {
        if (null !== $this->collProductImagess && !$overrideExisting) {
            return;
        }

        $collectionClassName = ProductImagesTableMap::getTableMap()->getCollectionClassName();

        $this->collProductImagess = new $collectionClassName;
        $this->collProductImagess->setModel('\BaobabModels\ProductImages');
    }

    /**
     * Gets an array of ChildProductImages objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildProduct is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildProductImages[] List of ChildProductImages objects
     * @throws PropelException
     */
    public function getProductImagess(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collProductImagessPartial && !$this->isNew();
        if (null === $this->collProductImagess || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collProductImagess) {
                // return empty collection
                $this->initProductImagess();
            } else {
                $collProductImagess = ChildProductImagesQuery::create(null, $criteria)
                    ->filterByProduct($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collProductImagessPartial && count($collProductImagess)) {
                        $this->initProductImagess(false);

                        foreach ($collProductImagess as $obj) {
                            if (false == $this->collProductImagess->contains($obj)) {
                                $this->collProductImagess->append($obj);
                            }
                        }

                        $this->collProductImagessPartial = true;
                    }

                    return $collProductImagess;
                }

                if ($partial && $this->collProductImagess) {
                    foreach ($this->collProductImagess as $obj) {
                        if ($obj->isNew()) {
                            $collProductImagess[] = $obj;
                        }
                    }
                }

                $this->collProductImagess = $collProductImagess;
                $this->collProductImagessPartial = false;
            }
        }

        return $this->collProductImagess;
    }

    /**
     * Sets a collection of ChildProductImages objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $productImagess A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildProduct The current object (for fluent API support)
     */
    public function setProductImagess(Collection $productImagess, ConnectionInterface $con = null)
    {
        /** @var ChildProductImages[] $productImagessToDelete */
        $productImagessToDelete = $this->getProductImagess(new Criteria(), $con)->diff($productImagess);


        $this->productImagessScheduledForDeletion = $productImagessToDelete;

        foreach ($productImagessToDelete as $productImagesRemoved) {
            $productImagesRemoved->setProduct(null);
        }

        $this->collProductImagess = null;
        foreach ($productImagess as $productImages) {
            $this->addProductImages($productImages);
        }

        $this->collProductImagess = $productImagess;
        $this->collProductImagessPartial = false;

        return $this;
    }

    /**
     * Returns the number of related ProductImages objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related ProductImages objects.
     * @throws PropelException
     */
    public function countProductImagess(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collProductImagessPartial && !$this->isNew();
        if (null === $this->collProductImagess || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collProductImagess) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getProductImagess());
            }

            $query = ChildProductImagesQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByProduct($this)
                ->count($con);
        }

        return count($this->collProductImagess);
    }

    /**
     * Method called to associate a ChildProductImages object to this object
     * through the ChildProductImages foreign key attribute.
     *
     * @param  ChildProductImages $l ChildProductImages
     * @return $this|\BaobabModels\Product The current object (for fluent API support)
     */
    public function addProductImages(ChildProductImages $l)
    {
        if ($this->collProductImagess === null) {
            $this->initProductImagess();
            $this->collProductImagessPartial = true;
        }

        if (!$this->collProductImagess->contains($l)) {
            $this->doAddProductImages($l);

            if ($this->productImagessScheduledForDeletion and $this->productImagessScheduledForDeletion->contains($l)) {
                $this->productImagessScheduledForDeletion->remove($this->productImagessScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildProductImages $productImages The ChildProductImages object to add.
     */
    protected function doAddProductImages(ChildProductImages $productImages)
    {
        $this->collProductImagess[]= $productImages;
        $productImages->setProduct($this);
    }

    /**
     * @param  ChildProductImages $productImages The ChildProductImages object to remove.
     * @return $this|ChildProduct The current object (for fluent API support)
     */
    public function removeProductImages(ChildProductImages $productImages)
    {
        if ($this->getProductImagess()->contains($productImages)) {
            $pos = $this->collProductImagess->search($productImages);
            $this->collProductImagess->remove($pos);
            if (null === $this->productImagessScheduledForDeletion) {
                $this->productImagessScheduledForDeletion = clone $this->collProductImagess;
                $this->productImagessScheduledForDeletion->clear();
            }
            $this->productImagessScheduledForDeletion[]= clone $productImages;
            $productImages->setProduct(null);
        }

        return $this;
    }

    /**
     * Clears out the collPricesCOPs collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addPricesCOPs()
     */
    public function clearPricesCOPs()
    {
        $this->collPricesCOPs = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collPricesCOPs collection loaded partially.
     */
    public function resetPartialPricesCOPs($v = true)
    {
        $this->collPricesCOPsPartial = $v;
    }

    /**
     * Initializes the collPricesCOPs collection.
     *
     * By default this just sets the collPricesCOPs collection to an empty array (like clearcollPricesCOPs());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initPricesCOPs($overrideExisting = true)
    {
        if (null !== $this->collPricesCOPs && !$overrideExisting) {
            return;
        }

        $collectionClassName = PricesCOPTableMap::getTableMap()->getCollectionClassName();

        $this->collPricesCOPs = new $collectionClassName;
        $this->collPricesCOPs->setModel('\BaobabModels\PricesCOP');
    }

    /**
     * Gets an array of ChildPricesCOP objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildProduct is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildPricesCOP[] List of ChildPricesCOP objects
     * @throws PropelException
     */
    public function getPricesCOPs(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collPricesCOPsPartial && !$this->isNew();
        if (null === $this->collPricesCOPs || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collPricesCOPs) {
                // return empty collection
                $this->initPricesCOPs();
            } else {
                $collPricesCOPs = ChildPricesCOPQuery::create(null, $criteria)
                    ->filterByProduct($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collPricesCOPsPartial && count($collPricesCOPs)) {
                        $this->initPricesCOPs(false);

                        foreach ($collPricesCOPs as $obj) {
                            if (false == $this->collPricesCOPs->contains($obj)) {
                                $this->collPricesCOPs->append($obj);
                            }
                        }

                        $this->collPricesCOPsPartial = true;
                    }

                    return $collPricesCOPs;
                }

                if ($partial && $this->collPricesCOPs) {
                    foreach ($this->collPricesCOPs as $obj) {
                        if ($obj->isNew()) {
                            $collPricesCOPs[] = $obj;
                        }
                    }
                }

                $this->collPricesCOPs = $collPricesCOPs;
                $this->collPricesCOPsPartial = false;
            }
        }

        return $this->collPricesCOPs;
    }

    /**
     * Sets a collection of ChildPricesCOP objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $pricesCOPs A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildProduct The current object (for fluent API support)
     */
    public function setPricesCOPs(Collection $pricesCOPs, ConnectionInterface $con = null)
    {
        /** @var ChildPricesCOP[] $pricesCOPsToDelete */
        $pricesCOPsToDelete = $this->getPricesCOPs(new Criteria(), $con)->diff($pricesCOPs);


        $this->pricesCOPsScheduledForDeletion = $pricesCOPsToDelete;

        foreach ($pricesCOPsToDelete as $pricesCOPRemoved) {
            $pricesCOPRemoved->setProduct(null);
        }

        $this->collPricesCOPs = null;
        foreach ($pricesCOPs as $pricesCOP) {
            $this->addPricesCOP($pricesCOP);
        }

        $this->collPricesCOPs = $pricesCOPs;
        $this->collPricesCOPsPartial = false;

        return $this;
    }

    /**
     * Returns the number of related PricesCOP objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related PricesCOP objects.
     * @throws PropelException
     */
    public function countPricesCOPs(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collPricesCOPsPartial && !$this->isNew();
        if (null === $this->collPricesCOPs || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collPricesCOPs) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getPricesCOPs());
            }

            $query = ChildPricesCOPQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByProduct($this)
                ->count($con);
        }

        return count($this->collPricesCOPs);
    }

    /**
     * Method called to associate a ChildPricesCOP object to this object
     * through the ChildPricesCOP foreign key attribute.
     *
     * @param  ChildPricesCOP $l ChildPricesCOP
     * @return $this|\BaobabModels\Product The current object (for fluent API support)
     */
    public function addPricesCOP(ChildPricesCOP $l)
    {
        if ($this->collPricesCOPs === null) {
            $this->initPricesCOPs();
            $this->collPricesCOPsPartial = true;
        }

        if (!$this->collPricesCOPs->contains($l)) {
            $this->doAddPricesCOP($l);

            if ($this->pricesCOPsScheduledForDeletion and $this->pricesCOPsScheduledForDeletion->contains($l)) {
                $this->pricesCOPsScheduledForDeletion->remove($this->pricesCOPsScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildPricesCOP $pricesCOP The ChildPricesCOP object to add.
     */
    protected function doAddPricesCOP(ChildPricesCOP $pricesCOP)
    {
        $this->collPricesCOPs[]= $pricesCOP;
        $pricesCOP->setProduct($this);
    }

    /**
     * @param  ChildPricesCOP $pricesCOP The ChildPricesCOP object to remove.
     * @return $this|ChildProduct The current object (for fluent API support)
     */
    public function removePricesCOP(ChildPricesCOP $pricesCOP)
    {
        if ($this->getPricesCOPs()->contains($pricesCOP)) {
            $pos = $this->collPricesCOPs->search($pricesCOP);
            $this->collPricesCOPs->remove($pos);
            if (null === $this->pricesCOPsScheduledForDeletion) {
                $this->pricesCOPsScheduledForDeletion = clone $this->collPricesCOPs;
                $this->pricesCOPsScheduledForDeletion->clear();
            }
            $this->pricesCOPsScheduledForDeletion[]= clone $pricesCOP;
            $pricesCOP->setProduct(null);
        }

        return $this;
    }

    /**
     * Clears out the collPricesUSDs collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addPricesUSDs()
     */
    public function clearPricesUSDs()
    {
        $this->collPricesUSDs = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collPricesUSDs collection loaded partially.
     */
    public function resetPartialPricesUSDs($v = true)
    {
        $this->collPricesUSDsPartial = $v;
    }

    /**
     * Initializes the collPricesUSDs collection.
     *
     * By default this just sets the collPricesUSDs collection to an empty array (like clearcollPricesUSDs());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initPricesUSDs($overrideExisting = true)
    {
        if (null !== $this->collPricesUSDs && !$overrideExisting) {
            return;
        }

        $collectionClassName = PricesUSDTableMap::getTableMap()->getCollectionClassName();

        $this->collPricesUSDs = new $collectionClassName;
        $this->collPricesUSDs->setModel('\BaobabModels\PricesUSD');
    }

    /**
     * Gets an array of ChildPricesUSD objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildProduct is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildPricesUSD[] List of ChildPricesUSD objects
     * @throws PropelException
     */
    public function getPricesUSDs(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collPricesUSDsPartial && !$this->isNew();
        if (null === $this->collPricesUSDs || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collPricesUSDs) {
                // return empty collection
                $this->initPricesUSDs();
            } else {
                $collPricesUSDs = ChildPricesUSDQuery::create(null, $criteria)
                    ->filterByProduct($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collPricesUSDsPartial && count($collPricesUSDs)) {
                        $this->initPricesUSDs(false);

                        foreach ($collPricesUSDs as $obj) {
                            if (false == $this->collPricesUSDs->contains($obj)) {
                                $this->collPricesUSDs->append($obj);
                            }
                        }

                        $this->collPricesUSDsPartial = true;
                    }

                    return $collPricesUSDs;
                }

                if ($partial && $this->collPricesUSDs) {
                    foreach ($this->collPricesUSDs as $obj) {
                        if ($obj->isNew()) {
                            $collPricesUSDs[] = $obj;
                        }
                    }
                }

                $this->collPricesUSDs = $collPricesUSDs;
                $this->collPricesUSDsPartial = false;
            }
        }

        return $this->collPricesUSDs;
    }

    /**
     * Sets a collection of ChildPricesUSD objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $pricesUSDs A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildProduct The current object (for fluent API support)
     */
    public function setPricesUSDs(Collection $pricesUSDs, ConnectionInterface $con = null)
    {
        /** @var ChildPricesUSD[] $pricesUSDsToDelete */
        $pricesUSDsToDelete = $this->getPricesUSDs(new Criteria(), $con)->diff($pricesUSDs);


        $this->pricesUSDsScheduledForDeletion = $pricesUSDsToDelete;

        foreach ($pricesUSDsToDelete as $pricesUSDRemoved) {
            $pricesUSDRemoved->setProduct(null);
        }

        $this->collPricesUSDs = null;
        foreach ($pricesUSDs as $pricesUSD) {
            $this->addPricesUSD($pricesUSD);
        }

        $this->collPricesUSDs = $pricesUSDs;
        $this->collPricesUSDsPartial = false;

        return $this;
    }

    /**
     * Returns the number of related PricesUSD objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related PricesUSD objects.
     * @throws PropelException
     */
    public function countPricesUSDs(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collPricesUSDsPartial && !$this->isNew();
        if (null === $this->collPricesUSDs || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collPricesUSDs) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getPricesUSDs());
            }

            $query = ChildPricesUSDQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByProduct($this)
                ->count($con);
        }

        return count($this->collPricesUSDs);
    }

    /**
     * Method called to associate a ChildPricesUSD object to this object
     * through the ChildPricesUSD foreign key attribute.
     *
     * @param  ChildPricesUSD $l ChildPricesUSD
     * @return $this|\BaobabModels\Product The current object (for fluent API support)
     */
    public function addPricesUSD(ChildPricesUSD $l)
    {
        if ($this->collPricesUSDs === null) {
            $this->initPricesUSDs();
            $this->collPricesUSDsPartial = true;
        }

        if (!$this->collPricesUSDs->contains($l)) {
            $this->doAddPricesUSD($l);

            if ($this->pricesUSDsScheduledForDeletion and $this->pricesUSDsScheduledForDeletion->contains($l)) {
                $this->pricesUSDsScheduledForDeletion->remove($this->pricesUSDsScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildPricesUSD $pricesUSD The ChildPricesUSD object to add.
     */
    protected function doAddPricesUSD(ChildPricesUSD $pricesUSD)
    {
        $this->collPricesUSDs[]= $pricesUSD;
        $pricesUSD->setProduct($this);
    }

    /**
     * @param  ChildPricesUSD $pricesUSD The ChildPricesUSD object to remove.
     * @return $this|ChildProduct The current object (for fluent API support)
     */
    public function removePricesUSD(ChildPricesUSD $pricesUSD)
    {
        if ($this->getPricesUSDs()->contains($pricesUSD)) {
            $pos = $this->collPricesUSDs->search($pricesUSD);
            $this->collPricesUSDs->remove($pos);
            if (null === $this->pricesUSDsScheduledForDeletion) {
                $this->pricesUSDsScheduledForDeletion = clone $this->collPricesUSDs;
                $this->pricesUSDsScheduledForDeletion->clear();
            }
            $this->pricesUSDsScheduledForDeletion[]= clone $pricesUSD;
            $pricesUSD->setProduct(null);
        }

        return $this;
    }

    /**
     * Gets a single ChildBikini object, which is related to this object by a one-to-one relationship.
     *
     * @param  ConnectionInterface $con optional connection object
     * @return ChildBikini
     * @throws PropelException
     */
    public function getBikini(ConnectionInterface $con = null)
    {

        if ($this->singleBikini === null && !$this->isNew()) {
            $this->singleBikini = ChildBikiniQuery::create()->findPk($this->getPrimaryKey(), $con);
        }

        return $this->singleBikini;
    }

    /**
     * Sets a single ChildBikini object as related to this object by a one-to-one relationship.
     *
     * @param  ChildBikini $v ChildBikini
     * @return $this|\BaobabModels\Product The current object (for fluent API support)
     * @throws PropelException
     */
    public function setBikini(ChildBikini $v = null)
    {
        $this->singleBikini = $v;

        // Make sure that that the passed-in ChildBikini isn't already associated with this object
        if ($v !== null && $v->getProduct(null, false) === null) {
            $v->setProduct($this);
        }

        return $this;
    }

    /**
     * Gets a single ChildOnePiece object, which is related to this object by a one-to-one relationship.
     *
     * @param  ConnectionInterface $con optional connection object
     * @return ChildOnePiece
     * @throws PropelException
     */
    public function getOnePiece(ConnectionInterface $con = null)
    {

        if ($this->singleOnePiece === null && !$this->isNew()) {
            $this->singleOnePiece = ChildOnePieceQuery::create()->findPk($this->getPrimaryKey(), $con);
        }

        return $this->singleOnePiece;
    }

    /**
     * Sets a single ChildOnePiece object as related to this object by a one-to-one relationship.
     *
     * @param  ChildOnePiece $v ChildOnePiece
     * @return $this|\BaobabModels\Product The current object (for fluent API support)
     * @throws PropelException
     */
    public function setOnePiece(ChildOnePiece $v = null)
    {
        $this->singleOnePiece = $v;

        // Make sure that that the passed-in ChildOnePiece isn't already associated with this object
        if ($v !== null && $v->getProduct(null, false) === null) {
            $v->setProduct($this);
        }

        return $this;
    }

    /**
     * Clears the current object, sets all attributes to their default values and removes
     * outgoing references as well as back-references (from other objects to this one. Results probably in a database
     * change of those foreign objects when you call `save` there).
     */
    public function clear()
    {
        $this->slug = null;
        $this->name = null;
        $this->show_order = null;
        $this->description = null;
        $this->descendant_class = null;
        $this->alreadyInSave = false;
        $this->clearAllReferences();
        $this->resetModified();
        $this->setNew(true);
        $this->setDeleted(false);
    }

    /**
     * Resets all references and back-references to other model objects or collections of model objects.
     *
     * This method is used to reset all php object references (not the actual reference in the database).
     * Necessary for object serialisation.
     *
     * @param      boolean $deep Whether to also clear the references on all referrer objects.
     */
    public function clearAllReferences($deep = false)
    {
        if ($deep) {
            if ($this->collProductImagess) {
                foreach ($this->collProductImagess as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collPricesCOPs) {
                foreach ($this->collPricesCOPs as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collPricesUSDs) {
                foreach ($this->collPricesUSDs as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->singleBikini) {
                $this->singleBikini->clearAllReferences($deep);
            }
            if ($this->singleOnePiece) {
                $this->singleOnePiece->clearAllReferences($deep);
            }
        } // if ($deep)

        $this->collProductImagess = null;
        $this->collPricesCOPs = null;
        $this->collPricesUSDs = null;
        $this->singleBikini = null;
        $this->singleOnePiece = null;
    }

    /**
     * Return the string representation of this object
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->exportTo(ProductTableMap::DEFAULT_STRING_FORMAT);
    }

    // concrete_inheritance_parent behavior

    /**
     * Whether or not this object is the parent of a child object
     *
     * @return    bool
     */
    public function hasChildObject()
    {
        return $this->getDescendantClass() !== null;
    }

    /**
     * Get the child object of this object
     *
     * @return    mixed
     */
    public function getChildObject()
    {
        if (!$this->hasChildObject()) {
            return null;
        }
        $childObjectClass = $this->getDescendantClass();
        $childObject = PropelQuery::from($childObjectClass)->findPk($this->getPrimaryKey());

        return $childObject->hasChildObject() ? $childObject->getChildObject() : $childObject;
    }

    /**
     * Code to be run before persisting the object
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preSave(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preSave')) {
            return parent::preSave($con);
        }
        return true;
    }

    /**
     * Code to be run after persisting the object
     * @param ConnectionInterface $con
     */
    public function postSave(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postSave')) {
            parent::postSave($con);
        }
    }

    /**
     * Code to be run before inserting to database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preInsert(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preInsert')) {
            return parent::preInsert($con);
        }
        return true;
    }

    /**
     * Code to be run after inserting to database
     * @param ConnectionInterface $con
     */
    public function postInsert(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postInsert')) {
            parent::postInsert($con);
        }
    }

    /**
     * Code to be run before updating the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preUpdate(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preUpdate')) {
            return parent::preUpdate($con);
        }
        return true;
    }

    /**
     * Code to be run after updating the object in database
     * @param ConnectionInterface $con
     */
    public function postUpdate(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postUpdate')) {
            parent::postUpdate($con);
        }
    }

    /**
     * Code to be run before deleting the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preDelete(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preDelete')) {
            return parent::preDelete($con);
        }
        return true;
    }

    /**
     * Code to be run after deleting the object in database
     * @param ConnectionInterface $con
     */
    public function postDelete(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postDelete')) {
            parent::postDelete($con);
        }
    }


    /**
     * Derived method to catches calls to undefined methods.
     *
     * Provides magic import/export method support (fromXML()/toXML(), fromYAML()/toYAML(), etc.).
     * Allows to define default __call() behavior if you overwrite __call()
     *
     * @param string $name
     * @param mixed  $params
     *
     * @return array|string
     */
    public function __call($name, $params)
    {
        if (0 === strpos($name, 'get')) {
            $virtualColumn = substr($name, 3);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }

            $virtualColumn = lcfirst($virtualColumn);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }
        }

        if (0 === strpos($name, 'from')) {
            $format = substr($name, 4);

            return $this->importFrom($format, reset($params));
        }

        if (0 === strpos($name, 'to')) {
            $format = substr($name, 2);
            $includeLazyLoadColumns = isset($params[0]) ? $params[0] : true;

            return $this->exportTo($format, $includeLazyLoadColumns);
        }

        throw new BadMethodCallException(sprintf('Call to undefined method: %s.', $name));
    }

}
