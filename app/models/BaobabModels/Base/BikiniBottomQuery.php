<?php

namespace BaobabModels\Base;

use \Exception;
use \PDO;
use BaobabModels\BikiniBottom as ChildBikiniBottom;
use BaobabModels\BikiniBottomQuery as ChildBikiniBottomQuery;
use BaobabModels\StockableQuery as ChildStockableQuery;
use BaobabModels\Map\BikiniBottomTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'bikini_bottom' table.
 *
 *
 *
 * @method     ChildBikiniBottomQuery orderBySize($order = Criteria::ASC) Order by the size column
 * @method     ChildBikiniBottomQuery orderByBikiniSlug($order = Criteria::ASC) Order by the bikini_slug column
 * @method     ChildBikiniBottomQuery orderBySku($order = Criteria::ASC) Order by the sku column
 * @method     ChildBikiniBottomQuery orderByStockAmount($order = Criteria::ASC) Order by the stock_amount column
 *
 * @method     ChildBikiniBottomQuery groupBySize() Group by the size column
 * @method     ChildBikiniBottomQuery groupByBikiniSlug() Group by the bikini_slug column
 * @method     ChildBikiniBottomQuery groupBySku() Group by the sku column
 * @method     ChildBikiniBottomQuery groupByStockAmount() Group by the stock_amount column
 *
 * @method     ChildBikiniBottomQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildBikiniBottomQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildBikiniBottomQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildBikiniBottomQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildBikiniBottomQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildBikiniBottomQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildBikiniBottomQuery leftJoinBikini($relationAlias = null) Adds a LEFT JOIN clause to the query using the Bikini relation
 * @method     ChildBikiniBottomQuery rightJoinBikini($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Bikini relation
 * @method     ChildBikiniBottomQuery innerJoinBikini($relationAlias = null) Adds a INNER JOIN clause to the query using the Bikini relation
 *
 * @method     ChildBikiniBottomQuery joinWithBikini($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Bikini relation
 *
 * @method     ChildBikiniBottomQuery leftJoinWithBikini() Adds a LEFT JOIN clause and with to the query using the Bikini relation
 * @method     ChildBikiniBottomQuery rightJoinWithBikini() Adds a RIGHT JOIN clause and with to the query using the Bikini relation
 * @method     ChildBikiniBottomQuery innerJoinWithBikini() Adds a INNER JOIN clause and with to the query using the Bikini relation
 *
 * @method     ChildBikiniBottomQuery leftJoinStockable($relationAlias = null) Adds a LEFT JOIN clause to the query using the Stockable relation
 * @method     ChildBikiniBottomQuery rightJoinStockable($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Stockable relation
 * @method     ChildBikiniBottomQuery innerJoinStockable($relationAlias = null) Adds a INNER JOIN clause to the query using the Stockable relation
 *
 * @method     ChildBikiniBottomQuery joinWithStockable($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Stockable relation
 *
 * @method     ChildBikiniBottomQuery leftJoinWithStockable() Adds a LEFT JOIN clause and with to the query using the Stockable relation
 * @method     ChildBikiniBottomQuery rightJoinWithStockable() Adds a RIGHT JOIN clause and with to the query using the Stockable relation
 * @method     ChildBikiniBottomQuery innerJoinWithStockable() Adds a INNER JOIN clause and with to the query using the Stockable relation
 *
 * @method     \BaobabModels\BikiniQuery|\BaobabModels\StockableQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildBikiniBottom findOne(ConnectionInterface $con = null) Return the first ChildBikiniBottom matching the query
 * @method     ChildBikiniBottom findOneOrCreate(ConnectionInterface $con = null) Return the first ChildBikiniBottom matching the query, or a new ChildBikiniBottom object populated from the query conditions when no match is found
 *
 * @method     ChildBikiniBottom findOneBySize(string $size) Return the first ChildBikiniBottom filtered by the size column
 * @method     ChildBikiniBottom findOneByBikiniSlug(string $bikini_slug) Return the first ChildBikiniBottom filtered by the bikini_slug column
 * @method     ChildBikiniBottom findOneBySku(string $sku) Return the first ChildBikiniBottom filtered by the sku column
 * @method     ChildBikiniBottom findOneByStockAmount(int $stock_amount) Return the first ChildBikiniBottom filtered by the stock_amount column *

 * @method     ChildBikiniBottom requirePk($key, ConnectionInterface $con = null) Return the ChildBikiniBottom by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildBikiniBottom requireOne(ConnectionInterface $con = null) Return the first ChildBikiniBottom matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildBikiniBottom requireOneBySize(string $size) Return the first ChildBikiniBottom filtered by the size column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildBikiniBottom requireOneByBikiniSlug(string $bikini_slug) Return the first ChildBikiniBottom filtered by the bikini_slug column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildBikiniBottom requireOneBySku(string $sku) Return the first ChildBikiniBottom filtered by the sku column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildBikiniBottom requireOneByStockAmount(int $stock_amount) Return the first ChildBikiniBottom filtered by the stock_amount column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildBikiniBottom[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildBikiniBottom objects based on current ModelCriteria
 * @method     ChildBikiniBottom[]|ObjectCollection findBySize(string $size) Return ChildBikiniBottom objects filtered by the size column
 * @method     ChildBikiniBottom[]|ObjectCollection findByBikiniSlug(string $bikini_slug) Return ChildBikiniBottom objects filtered by the bikini_slug column
 * @method     ChildBikiniBottom[]|ObjectCollection findBySku(string $sku) Return ChildBikiniBottom objects filtered by the sku column
 * @method     ChildBikiniBottom[]|ObjectCollection findByStockAmount(int $stock_amount) Return ChildBikiniBottom objects filtered by the stock_amount column
 * @method     ChildBikiniBottom[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class BikiniBottomQuery extends ChildStockableQuery
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \BaobabModels\Base\BikiniBottomQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'baobab', $modelName = '\\BaobabModels\\BikiniBottom', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildBikiniBottomQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildBikiniBottomQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildBikiniBottomQuery) {
            return $criteria;
        }
        $query = new ChildBikiniBottomQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildBikiniBottom|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(BikiniBottomTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = BikiniBottomTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildBikiniBottom A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT size, bikini_slug, sku, stock_amount FROM bikini_bottom WHERE sku = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_STR);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildBikiniBottom $obj */
            $obj = new ChildBikiniBottom();
            $obj->hydrate($row);
            BikiniBottomTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildBikiniBottom|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildBikiniBottomQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(BikiniBottomTableMap::COL_SKU, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildBikiniBottomQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(BikiniBottomTableMap::COL_SKU, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the size column
     *
     * Example usage:
     * <code>
     * $query->filterBySize('fooValue');   // WHERE size = 'fooValue'
     * $query->filterBySize('%fooValue%', Criteria::LIKE); // WHERE size LIKE '%fooValue%'
     * </code>
     *
     * @param     string $size The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildBikiniBottomQuery The current query, for fluid interface
     */
    public function filterBySize($size = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($size)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(BikiniBottomTableMap::COL_SIZE, $size, $comparison);
    }

    /**
     * Filter the query on the bikini_slug column
     *
     * Example usage:
     * <code>
     * $query->filterByBikiniSlug('fooValue');   // WHERE bikini_slug = 'fooValue'
     * $query->filterByBikiniSlug('%fooValue%', Criteria::LIKE); // WHERE bikini_slug LIKE '%fooValue%'
     * </code>
     *
     * @param     string $bikiniSlug The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildBikiniBottomQuery The current query, for fluid interface
     */
    public function filterByBikiniSlug($bikiniSlug = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($bikiniSlug)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(BikiniBottomTableMap::COL_BIKINI_SLUG, $bikiniSlug, $comparison);
    }

    /**
     * Filter the query on the sku column
     *
     * Example usage:
     * <code>
     * $query->filterBySku('fooValue');   // WHERE sku = 'fooValue'
     * $query->filterBySku('%fooValue%', Criteria::LIKE); // WHERE sku LIKE '%fooValue%'
     * </code>
     *
     * @param     string $sku The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildBikiniBottomQuery The current query, for fluid interface
     */
    public function filterBySku($sku = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($sku)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(BikiniBottomTableMap::COL_SKU, $sku, $comparison);
    }

    /**
     * Filter the query on the stock_amount column
     *
     * Example usage:
     * <code>
     * $query->filterByStockAmount(1234); // WHERE stock_amount = 1234
     * $query->filterByStockAmount(array(12, 34)); // WHERE stock_amount IN (12, 34)
     * $query->filterByStockAmount(array('min' => 12)); // WHERE stock_amount > 12
     * </code>
     *
     * @param     mixed $stockAmount The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildBikiniBottomQuery The current query, for fluid interface
     */
    public function filterByStockAmount($stockAmount = null, $comparison = null)
    {
        if (is_array($stockAmount)) {
            $useMinMax = false;
            if (isset($stockAmount['min'])) {
                $this->addUsingAlias(BikiniBottomTableMap::COL_STOCK_AMOUNT, $stockAmount['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($stockAmount['max'])) {
                $this->addUsingAlias(BikiniBottomTableMap::COL_STOCK_AMOUNT, $stockAmount['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(BikiniBottomTableMap::COL_STOCK_AMOUNT, $stockAmount, $comparison);
    }

    /**
     * Filter the query by a related \BaobabModels\Bikini object
     *
     * @param \BaobabModels\Bikini|ObjectCollection $bikini The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildBikiniBottomQuery The current query, for fluid interface
     */
    public function filterByBikini($bikini, $comparison = null)
    {
        if ($bikini instanceof \BaobabModels\Bikini) {
            return $this
                ->addUsingAlias(BikiniBottomTableMap::COL_BIKINI_SLUG, $bikini->getSlug(), $comparison);
        } elseif ($bikini instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(BikiniBottomTableMap::COL_BIKINI_SLUG, $bikini->toKeyValue('PrimaryKey', 'Slug'), $comparison);
        } else {
            throw new PropelException('filterByBikini() only accepts arguments of type \BaobabModels\Bikini or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Bikini relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildBikiniBottomQuery The current query, for fluid interface
     */
    public function joinBikini($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Bikini');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Bikini');
        }

        return $this;
    }

    /**
     * Use the Bikini relation Bikini object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \BaobabModels\BikiniQuery A secondary query class using the current class as primary query
     */
    public function useBikiniQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinBikini($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Bikini', '\BaobabModels\BikiniQuery');
    }

    /**
     * Filter the query by a related \BaobabModels\Stockable object
     *
     * @param \BaobabModels\Stockable|ObjectCollection $stockable The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildBikiniBottomQuery The current query, for fluid interface
     */
    public function filterByStockable($stockable, $comparison = null)
    {
        if ($stockable instanceof \BaobabModels\Stockable) {
            return $this
                ->addUsingAlias(BikiniBottomTableMap::COL_SKU, $stockable->getSku(), $comparison);
        } elseif ($stockable instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(BikiniBottomTableMap::COL_SKU, $stockable->toKeyValue('PrimaryKey', 'Sku'), $comparison);
        } else {
            throw new PropelException('filterByStockable() only accepts arguments of type \BaobabModels\Stockable or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Stockable relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildBikiniBottomQuery The current query, for fluid interface
     */
    public function joinStockable($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Stockable');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Stockable');
        }

        return $this;
    }

    /**
     * Use the Stockable relation Stockable object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \BaobabModels\StockableQuery A secondary query class using the current class as primary query
     */
    public function useStockableQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinStockable($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Stockable', '\BaobabModels\StockableQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildBikiniBottom $bikiniBottom Object to remove from the list of results
     *
     * @return $this|ChildBikiniBottomQuery The current query, for fluid interface
     */
    public function prune($bikiniBottom = null)
    {
        if ($bikiniBottom) {
            $this->addUsingAlias(BikiniBottomTableMap::COL_SKU, $bikiniBottom->getSku(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the bikini_bottom table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(BikiniBottomTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            BikiniBottomTableMap::clearInstancePool();
            BikiniBottomTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(BikiniBottomTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(BikiniBottomTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            BikiniBottomTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            BikiniBottomTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // BikiniBottomQuery
