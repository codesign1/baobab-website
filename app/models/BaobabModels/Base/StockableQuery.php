<?php

namespace BaobabModels\Base;

use \Exception;
use \PDO;
use BaobabModels\Stockable as ChildStockable;
use BaobabModels\StockableQuery as ChildStockableQuery;
use BaobabModels\Map\StockableTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'stockable' table.
 *
 *
 *
 * @method     ChildStockableQuery orderBySku($order = Criteria::ASC) Order by the sku column
 * @method     ChildStockableQuery orderByStockAmount($order = Criteria::ASC) Order by the stock_amount column
 * @method     ChildStockableQuery orderByDescendantClass($order = Criteria::ASC) Order by the descendant_class column
 *
 * @method     ChildStockableQuery groupBySku() Group by the sku column
 * @method     ChildStockableQuery groupByStockAmount() Group by the stock_amount column
 * @method     ChildStockableQuery groupByDescendantClass() Group by the descendant_class column
 *
 * @method     ChildStockableQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildStockableQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildStockableQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildStockableQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildStockableQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildStockableQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildStockableQuery leftJoinBikiniTop($relationAlias = null) Adds a LEFT JOIN clause to the query using the BikiniTop relation
 * @method     ChildStockableQuery rightJoinBikiniTop($relationAlias = null) Adds a RIGHT JOIN clause to the query using the BikiniTop relation
 * @method     ChildStockableQuery innerJoinBikiniTop($relationAlias = null) Adds a INNER JOIN clause to the query using the BikiniTop relation
 *
 * @method     ChildStockableQuery joinWithBikiniTop($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the BikiniTop relation
 *
 * @method     ChildStockableQuery leftJoinWithBikiniTop() Adds a LEFT JOIN clause and with to the query using the BikiniTop relation
 * @method     ChildStockableQuery rightJoinWithBikiniTop() Adds a RIGHT JOIN clause and with to the query using the BikiniTop relation
 * @method     ChildStockableQuery innerJoinWithBikiniTop() Adds a INNER JOIN clause and with to the query using the BikiniTop relation
 *
 * @method     ChildStockableQuery leftJoinBikiniBottom($relationAlias = null) Adds a LEFT JOIN clause to the query using the BikiniBottom relation
 * @method     ChildStockableQuery rightJoinBikiniBottom($relationAlias = null) Adds a RIGHT JOIN clause to the query using the BikiniBottom relation
 * @method     ChildStockableQuery innerJoinBikiniBottom($relationAlias = null) Adds a INNER JOIN clause to the query using the BikiniBottom relation
 *
 * @method     ChildStockableQuery joinWithBikiniBottom($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the BikiniBottom relation
 *
 * @method     ChildStockableQuery leftJoinWithBikiniBottom() Adds a LEFT JOIN clause and with to the query using the BikiniBottom relation
 * @method     ChildStockableQuery rightJoinWithBikiniBottom() Adds a RIGHT JOIN clause and with to the query using the BikiniBottom relation
 * @method     ChildStockableQuery innerJoinWithBikiniBottom() Adds a INNER JOIN clause and with to the query using the BikiniBottom relation
 *
 * @method     ChildStockableQuery leftJoinOnePieceStockable($relationAlias = null) Adds a LEFT JOIN clause to the query using the OnePieceStockable relation
 * @method     ChildStockableQuery rightJoinOnePieceStockable($relationAlias = null) Adds a RIGHT JOIN clause to the query using the OnePieceStockable relation
 * @method     ChildStockableQuery innerJoinOnePieceStockable($relationAlias = null) Adds a INNER JOIN clause to the query using the OnePieceStockable relation
 *
 * @method     ChildStockableQuery joinWithOnePieceStockable($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the OnePieceStockable relation
 *
 * @method     ChildStockableQuery leftJoinWithOnePieceStockable() Adds a LEFT JOIN clause and with to the query using the OnePieceStockable relation
 * @method     ChildStockableQuery rightJoinWithOnePieceStockable() Adds a RIGHT JOIN clause and with to the query using the OnePieceStockable relation
 * @method     ChildStockableQuery innerJoinWithOnePieceStockable() Adds a INNER JOIN clause and with to the query using the OnePieceStockable relation
 *
 * @method     \BaobabModels\BikiniTopQuery|\BaobabModels\BikiniBottomQuery|\BaobabModels\OnePieceStockableQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildStockable findOne(ConnectionInterface $con = null) Return the first ChildStockable matching the query
 * @method     ChildStockable findOneOrCreate(ConnectionInterface $con = null) Return the first ChildStockable matching the query, or a new ChildStockable object populated from the query conditions when no match is found
 *
 * @method     ChildStockable findOneBySku(string $sku) Return the first ChildStockable filtered by the sku column
 * @method     ChildStockable findOneByStockAmount(int $stock_amount) Return the first ChildStockable filtered by the stock_amount column
 * @method     ChildStockable findOneByDescendantClass(string $descendant_class) Return the first ChildStockable filtered by the descendant_class column *

 * @method     ChildStockable requirePk($key, ConnectionInterface $con = null) Return the ChildStockable by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildStockable requireOne(ConnectionInterface $con = null) Return the first ChildStockable matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildStockable requireOneBySku(string $sku) Return the first ChildStockable filtered by the sku column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildStockable requireOneByStockAmount(int $stock_amount) Return the first ChildStockable filtered by the stock_amount column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildStockable requireOneByDescendantClass(string $descendant_class) Return the first ChildStockable filtered by the descendant_class column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildStockable[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildStockable objects based on current ModelCriteria
 * @method     ChildStockable[]|ObjectCollection findBySku(string $sku) Return ChildStockable objects filtered by the sku column
 * @method     ChildStockable[]|ObjectCollection findByStockAmount(int $stock_amount) Return ChildStockable objects filtered by the stock_amount column
 * @method     ChildStockable[]|ObjectCollection findByDescendantClass(string $descendant_class) Return ChildStockable objects filtered by the descendant_class column
 * @method     ChildStockable[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class StockableQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \BaobabModels\Base\StockableQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'baobab', $modelName = '\\BaobabModels\\Stockable', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildStockableQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildStockableQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildStockableQuery) {
            return $criteria;
        }
        $query = new ChildStockableQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildStockable|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(StockableTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = StockableTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildStockable A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT sku, stock_amount, descendant_class FROM stockable WHERE sku = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_STR);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildStockable $obj */
            $obj = new ChildStockable();
            $obj->hydrate($row);
            StockableTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildStockable|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildStockableQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(StockableTableMap::COL_SKU, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildStockableQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(StockableTableMap::COL_SKU, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the sku column
     *
     * Example usage:
     * <code>
     * $query->filterBySku('fooValue');   // WHERE sku = 'fooValue'
     * $query->filterBySku('%fooValue%', Criteria::LIKE); // WHERE sku LIKE '%fooValue%'
     * </code>
     *
     * @param     string $sku The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildStockableQuery The current query, for fluid interface
     */
    public function filterBySku($sku = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($sku)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(StockableTableMap::COL_SKU, $sku, $comparison);
    }

    /**
     * Filter the query on the stock_amount column
     *
     * Example usage:
     * <code>
     * $query->filterByStockAmount(1234); // WHERE stock_amount = 1234
     * $query->filterByStockAmount(array(12, 34)); // WHERE stock_amount IN (12, 34)
     * $query->filterByStockAmount(array('min' => 12)); // WHERE stock_amount > 12
     * </code>
     *
     * @param     mixed $stockAmount The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildStockableQuery The current query, for fluid interface
     */
    public function filterByStockAmount($stockAmount = null, $comparison = null)
    {
        if (is_array($stockAmount)) {
            $useMinMax = false;
            if (isset($stockAmount['min'])) {
                $this->addUsingAlias(StockableTableMap::COL_STOCK_AMOUNT, $stockAmount['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($stockAmount['max'])) {
                $this->addUsingAlias(StockableTableMap::COL_STOCK_AMOUNT, $stockAmount['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(StockableTableMap::COL_STOCK_AMOUNT, $stockAmount, $comparison);
    }

    /**
     * Filter the query on the descendant_class column
     *
     * Example usage:
     * <code>
     * $query->filterByDescendantClass('fooValue');   // WHERE descendant_class = 'fooValue'
     * $query->filterByDescendantClass('%fooValue%', Criteria::LIKE); // WHERE descendant_class LIKE '%fooValue%'
     * </code>
     *
     * @param     string $descendantClass The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildStockableQuery The current query, for fluid interface
     */
    public function filterByDescendantClass($descendantClass = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($descendantClass)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(StockableTableMap::COL_DESCENDANT_CLASS, $descendantClass, $comparison);
    }

    /**
     * Filter the query by a related \BaobabModels\BikiniTop object
     *
     * @param \BaobabModels\BikiniTop|ObjectCollection $bikiniTop the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildStockableQuery The current query, for fluid interface
     */
    public function filterByBikiniTop($bikiniTop, $comparison = null)
    {
        if ($bikiniTop instanceof \BaobabModels\BikiniTop) {
            return $this
                ->addUsingAlias(StockableTableMap::COL_SKU, $bikiniTop->getSku(), $comparison);
        } elseif ($bikiniTop instanceof ObjectCollection) {
            return $this
                ->useBikiniTopQuery()
                ->filterByPrimaryKeys($bikiniTop->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByBikiniTop() only accepts arguments of type \BaobabModels\BikiniTop or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the BikiniTop relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildStockableQuery The current query, for fluid interface
     */
    public function joinBikiniTop($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('BikiniTop');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'BikiniTop');
        }

        return $this;
    }

    /**
     * Use the BikiniTop relation BikiniTop object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \BaobabModels\BikiniTopQuery A secondary query class using the current class as primary query
     */
    public function useBikiniTopQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinBikiniTop($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'BikiniTop', '\BaobabModels\BikiniTopQuery');
    }

    /**
     * Filter the query by a related \BaobabModels\BikiniBottom object
     *
     * @param \BaobabModels\BikiniBottom|ObjectCollection $bikiniBottom the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildStockableQuery The current query, for fluid interface
     */
    public function filterByBikiniBottom($bikiniBottom, $comparison = null)
    {
        if ($bikiniBottom instanceof \BaobabModels\BikiniBottom) {
            return $this
                ->addUsingAlias(StockableTableMap::COL_SKU, $bikiniBottom->getSku(), $comparison);
        } elseif ($bikiniBottom instanceof ObjectCollection) {
            return $this
                ->useBikiniBottomQuery()
                ->filterByPrimaryKeys($bikiniBottom->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByBikiniBottom() only accepts arguments of type \BaobabModels\BikiniBottom or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the BikiniBottom relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildStockableQuery The current query, for fluid interface
     */
    public function joinBikiniBottom($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('BikiniBottom');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'BikiniBottom');
        }

        return $this;
    }

    /**
     * Use the BikiniBottom relation BikiniBottom object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \BaobabModels\BikiniBottomQuery A secondary query class using the current class as primary query
     */
    public function useBikiniBottomQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinBikiniBottom($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'BikiniBottom', '\BaobabModels\BikiniBottomQuery');
    }

    /**
     * Filter the query by a related \BaobabModels\OnePieceStockable object
     *
     * @param \BaobabModels\OnePieceStockable|ObjectCollection $onePieceStockable the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildStockableQuery The current query, for fluid interface
     */
    public function filterByOnePieceStockable($onePieceStockable, $comparison = null)
    {
        if ($onePieceStockable instanceof \BaobabModels\OnePieceStockable) {
            return $this
                ->addUsingAlias(StockableTableMap::COL_SKU, $onePieceStockable->getSku(), $comparison);
        } elseif ($onePieceStockable instanceof ObjectCollection) {
            return $this
                ->useOnePieceStockableQuery()
                ->filterByPrimaryKeys($onePieceStockable->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByOnePieceStockable() only accepts arguments of type \BaobabModels\OnePieceStockable or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the OnePieceStockable relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildStockableQuery The current query, for fluid interface
     */
    public function joinOnePieceStockable($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('OnePieceStockable');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'OnePieceStockable');
        }

        return $this;
    }

    /**
     * Use the OnePieceStockable relation OnePieceStockable object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \BaobabModels\OnePieceStockableQuery A secondary query class using the current class as primary query
     */
    public function useOnePieceStockableQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinOnePieceStockable($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'OnePieceStockable', '\BaobabModels\OnePieceStockableQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildStockable $stockable Object to remove from the list of results
     *
     * @return $this|ChildStockableQuery The current query, for fluid interface
     */
    public function prune($stockable = null)
    {
        if ($stockable) {
            $this->addUsingAlias(StockableTableMap::COL_SKU, $stockable->getSku(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the stockable table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(StockableTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            StockableTableMap::clearInstancePool();
            StockableTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(StockableTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(StockableTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            StockableTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            StockableTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // StockableQuery
