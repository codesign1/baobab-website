<?php

namespace BaobabModels\Base;

use \Exception;
use \PDO;
use BaobabModels\Product as ChildProduct;
use BaobabModels\ProductQuery as ChildProductQuery;
use BaobabModels\Map\ProductTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'product' table.
 *
 *
 *
 * @method     ChildProductQuery orderBySlug($order = Criteria::ASC) Order by the slug column
 * @method     ChildProductQuery orderByName($order = Criteria::ASC) Order by the name column
 * @method     ChildProductQuery orderByShowOrder($order = Criteria::ASC) Order by the show_order column
 * @method     ChildProductQuery orderByDescription($order = Criteria::ASC) Order by the description column
 * @method     ChildProductQuery orderByDescendantClass($order = Criteria::ASC) Order by the descendant_class column
 *
 * @method     ChildProductQuery groupBySlug() Group by the slug column
 * @method     ChildProductQuery groupByName() Group by the name column
 * @method     ChildProductQuery groupByShowOrder() Group by the show_order column
 * @method     ChildProductQuery groupByDescription() Group by the description column
 * @method     ChildProductQuery groupByDescendantClass() Group by the descendant_class column
 *
 * @method     ChildProductQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildProductQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildProductQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildProductQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildProductQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildProductQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildProductQuery leftJoinProductImages($relationAlias = null) Adds a LEFT JOIN clause to the query using the ProductImages relation
 * @method     ChildProductQuery rightJoinProductImages($relationAlias = null) Adds a RIGHT JOIN clause to the query using the ProductImages relation
 * @method     ChildProductQuery innerJoinProductImages($relationAlias = null) Adds a INNER JOIN clause to the query using the ProductImages relation
 *
 * @method     ChildProductQuery joinWithProductImages($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the ProductImages relation
 *
 * @method     ChildProductQuery leftJoinWithProductImages() Adds a LEFT JOIN clause and with to the query using the ProductImages relation
 * @method     ChildProductQuery rightJoinWithProductImages() Adds a RIGHT JOIN clause and with to the query using the ProductImages relation
 * @method     ChildProductQuery innerJoinWithProductImages() Adds a INNER JOIN clause and with to the query using the ProductImages relation
 *
 * @method     ChildProductQuery leftJoinPricesCOP($relationAlias = null) Adds a LEFT JOIN clause to the query using the PricesCOP relation
 * @method     ChildProductQuery rightJoinPricesCOP($relationAlias = null) Adds a RIGHT JOIN clause to the query using the PricesCOP relation
 * @method     ChildProductQuery innerJoinPricesCOP($relationAlias = null) Adds a INNER JOIN clause to the query using the PricesCOP relation
 *
 * @method     ChildProductQuery joinWithPricesCOP($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the PricesCOP relation
 *
 * @method     ChildProductQuery leftJoinWithPricesCOP() Adds a LEFT JOIN clause and with to the query using the PricesCOP relation
 * @method     ChildProductQuery rightJoinWithPricesCOP() Adds a RIGHT JOIN clause and with to the query using the PricesCOP relation
 * @method     ChildProductQuery innerJoinWithPricesCOP() Adds a INNER JOIN clause and with to the query using the PricesCOP relation
 *
 * @method     ChildProductQuery leftJoinPricesUSD($relationAlias = null) Adds a LEFT JOIN clause to the query using the PricesUSD relation
 * @method     ChildProductQuery rightJoinPricesUSD($relationAlias = null) Adds a RIGHT JOIN clause to the query using the PricesUSD relation
 * @method     ChildProductQuery innerJoinPricesUSD($relationAlias = null) Adds a INNER JOIN clause to the query using the PricesUSD relation
 *
 * @method     ChildProductQuery joinWithPricesUSD($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the PricesUSD relation
 *
 * @method     ChildProductQuery leftJoinWithPricesUSD() Adds a LEFT JOIN clause and with to the query using the PricesUSD relation
 * @method     ChildProductQuery rightJoinWithPricesUSD() Adds a RIGHT JOIN clause and with to the query using the PricesUSD relation
 * @method     ChildProductQuery innerJoinWithPricesUSD() Adds a INNER JOIN clause and with to the query using the PricesUSD relation
 *
 * @method     ChildProductQuery leftJoinBikini($relationAlias = null) Adds a LEFT JOIN clause to the query using the Bikini relation
 * @method     ChildProductQuery rightJoinBikini($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Bikini relation
 * @method     ChildProductQuery innerJoinBikini($relationAlias = null) Adds a INNER JOIN clause to the query using the Bikini relation
 *
 * @method     ChildProductQuery joinWithBikini($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Bikini relation
 *
 * @method     ChildProductQuery leftJoinWithBikini() Adds a LEFT JOIN clause and with to the query using the Bikini relation
 * @method     ChildProductQuery rightJoinWithBikini() Adds a RIGHT JOIN clause and with to the query using the Bikini relation
 * @method     ChildProductQuery innerJoinWithBikini() Adds a INNER JOIN clause and with to the query using the Bikini relation
 *
 * @method     ChildProductQuery leftJoinOnePiece($relationAlias = null) Adds a LEFT JOIN clause to the query using the OnePiece relation
 * @method     ChildProductQuery rightJoinOnePiece($relationAlias = null) Adds a RIGHT JOIN clause to the query using the OnePiece relation
 * @method     ChildProductQuery innerJoinOnePiece($relationAlias = null) Adds a INNER JOIN clause to the query using the OnePiece relation
 *
 * @method     ChildProductQuery joinWithOnePiece($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the OnePiece relation
 *
 * @method     ChildProductQuery leftJoinWithOnePiece() Adds a LEFT JOIN clause and with to the query using the OnePiece relation
 * @method     ChildProductQuery rightJoinWithOnePiece() Adds a RIGHT JOIN clause and with to the query using the OnePiece relation
 * @method     ChildProductQuery innerJoinWithOnePiece() Adds a INNER JOIN clause and with to the query using the OnePiece relation
 *
 * @method     \BaobabModels\ProductImagesQuery|\BaobabModels\PricesCOPQuery|\BaobabModels\PricesUSDQuery|\BaobabModels\BikiniQuery|\BaobabModels\OnePieceQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildProduct findOne(ConnectionInterface $con = null) Return the first ChildProduct matching the query
 * @method     ChildProduct findOneOrCreate(ConnectionInterface $con = null) Return the first ChildProduct matching the query, or a new ChildProduct object populated from the query conditions when no match is found
 *
 * @method     ChildProduct findOneBySlug(string $slug) Return the first ChildProduct filtered by the slug column
 * @method     ChildProduct findOneByName(string $name) Return the first ChildProduct filtered by the name column
 * @method     ChildProduct findOneByShowOrder(int $show_order) Return the first ChildProduct filtered by the show_order column
 * @method     ChildProduct findOneByDescription(string $description) Return the first ChildProduct filtered by the description column
 * @method     ChildProduct findOneByDescendantClass(string $descendant_class) Return the first ChildProduct filtered by the descendant_class column *

 * @method     ChildProduct requirePk($key, ConnectionInterface $con = null) Return the ChildProduct by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildProduct requireOne(ConnectionInterface $con = null) Return the first ChildProduct matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildProduct requireOneBySlug(string $slug) Return the first ChildProduct filtered by the slug column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildProduct requireOneByName(string $name) Return the first ChildProduct filtered by the name column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildProduct requireOneByShowOrder(int $show_order) Return the first ChildProduct filtered by the show_order column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildProduct requireOneByDescription(string $description) Return the first ChildProduct filtered by the description column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildProduct requireOneByDescendantClass(string $descendant_class) Return the first ChildProduct filtered by the descendant_class column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildProduct[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildProduct objects based on current ModelCriteria
 * @method     ChildProduct[]|ObjectCollection findBySlug(string $slug) Return ChildProduct objects filtered by the slug column
 * @method     ChildProduct[]|ObjectCollection findByName(string $name) Return ChildProduct objects filtered by the name column
 * @method     ChildProduct[]|ObjectCollection findByShowOrder(int $show_order) Return ChildProduct objects filtered by the show_order column
 * @method     ChildProduct[]|ObjectCollection findByDescription(string $description) Return ChildProduct objects filtered by the description column
 * @method     ChildProduct[]|ObjectCollection findByDescendantClass(string $descendant_class) Return ChildProduct objects filtered by the descendant_class column
 * @method     ChildProduct[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class ProductQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \BaobabModels\Base\ProductQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'baobab', $modelName = '\\BaobabModels\\Product', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildProductQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildProductQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildProductQuery) {
            return $criteria;
        }
        $query = new ChildProductQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildProduct|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(ProductTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = ProductTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildProduct A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT slug, name, show_order, description, descendant_class FROM product WHERE slug = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_STR);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildProduct $obj */
            $obj = new ChildProduct();
            $obj->hydrate($row);
            ProductTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildProduct|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildProductQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(ProductTableMap::COL_SLUG, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildProductQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(ProductTableMap::COL_SLUG, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the slug column
     *
     * Example usage:
     * <code>
     * $query->filterBySlug('fooValue');   // WHERE slug = 'fooValue'
     * $query->filterBySlug('%fooValue%', Criteria::LIKE); // WHERE slug LIKE '%fooValue%'
     * </code>
     *
     * @param     string $slug The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildProductQuery The current query, for fluid interface
     */
    public function filterBySlug($slug = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($slug)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ProductTableMap::COL_SLUG, $slug, $comparison);
    }

    /**
     * Filter the query on the name column
     *
     * Example usage:
     * <code>
     * $query->filterByName('fooValue');   // WHERE name = 'fooValue'
     * $query->filterByName('%fooValue%', Criteria::LIKE); // WHERE name LIKE '%fooValue%'
     * </code>
     *
     * @param     string $name The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildProductQuery The current query, for fluid interface
     */
    public function filterByName($name = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($name)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ProductTableMap::COL_NAME, $name, $comparison);
    }

    /**
     * Filter the query on the show_order column
     *
     * Example usage:
     * <code>
     * $query->filterByShowOrder(1234); // WHERE show_order = 1234
     * $query->filterByShowOrder(array(12, 34)); // WHERE show_order IN (12, 34)
     * $query->filterByShowOrder(array('min' => 12)); // WHERE show_order > 12
     * </code>
     *
     * @param     mixed $showOrder The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildProductQuery The current query, for fluid interface
     */
    public function filterByShowOrder($showOrder = null, $comparison = null)
    {
        if (is_array($showOrder)) {
            $useMinMax = false;
            if (isset($showOrder['min'])) {
                $this->addUsingAlias(ProductTableMap::COL_SHOW_ORDER, $showOrder['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($showOrder['max'])) {
                $this->addUsingAlias(ProductTableMap::COL_SHOW_ORDER, $showOrder['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ProductTableMap::COL_SHOW_ORDER, $showOrder, $comparison);
    }

    /**
     * Filter the query on the description column
     *
     * Example usage:
     * <code>
     * $query->filterByDescription('fooValue');   // WHERE description = 'fooValue'
     * $query->filterByDescription('%fooValue%', Criteria::LIKE); // WHERE description LIKE '%fooValue%'
     * </code>
     *
     * @param     string $description The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildProductQuery The current query, for fluid interface
     */
    public function filterByDescription($description = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($description)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ProductTableMap::COL_DESCRIPTION, $description, $comparison);
    }

    /**
     * Filter the query on the descendant_class column
     *
     * Example usage:
     * <code>
     * $query->filterByDescendantClass('fooValue');   // WHERE descendant_class = 'fooValue'
     * $query->filterByDescendantClass('%fooValue%', Criteria::LIKE); // WHERE descendant_class LIKE '%fooValue%'
     * </code>
     *
     * @param     string $descendantClass The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildProductQuery The current query, for fluid interface
     */
    public function filterByDescendantClass($descendantClass = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($descendantClass)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ProductTableMap::COL_DESCENDANT_CLASS, $descendantClass, $comparison);
    }

    /**
     * Filter the query by a related \BaobabModels\ProductImages object
     *
     * @param \BaobabModels\ProductImages|ObjectCollection $productImages the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildProductQuery The current query, for fluid interface
     */
    public function filterByProductImages($productImages, $comparison = null)
    {
        if ($productImages instanceof \BaobabModels\ProductImages) {
            return $this
                ->addUsingAlias(ProductTableMap::COL_SLUG, $productImages->getProductSlug(), $comparison);
        } elseif ($productImages instanceof ObjectCollection) {
            return $this
                ->useProductImagesQuery()
                ->filterByPrimaryKeys($productImages->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByProductImages() only accepts arguments of type \BaobabModels\ProductImages or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the ProductImages relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildProductQuery The current query, for fluid interface
     */
    public function joinProductImages($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('ProductImages');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'ProductImages');
        }

        return $this;
    }

    /**
     * Use the ProductImages relation ProductImages object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \BaobabModels\ProductImagesQuery A secondary query class using the current class as primary query
     */
    public function useProductImagesQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinProductImages($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'ProductImages', '\BaobabModels\ProductImagesQuery');
    }

    /**
     * Filter the query by a related \BaobabModels\PricesCOP object
     *
     * @param \BaobabModels\PricesCOP|ObjectCollection $pricesCOP the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildProductQuery The current query, for fluid interface
     */
    public function filterByPricesCOP($pricesCOP, $comparison = null)
    {
        if ($pricesCOP instanceof \BaobabModels\PricesCOP) {
            return $this
                ->addUsingAlias(ProductTableMap::COL_SLUG, $pricesCOP->getProductSlug(), $comparison);
        } elseif ($pricesCOP instanceof ObjectCollection) {
            return $this
                ->usePricesCOPQuery()
                ->filterByPrimaryKeys($pricesCOP->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByPricesCOP() only accepts arguments of type \BaobabModels\PricesCOP or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the PricesCOP relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildProductQuery The current query, for fluid interface
     */
    public function joinPricesCOP($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('PricesCOP');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'PricesCOP');
        }

        return $this;
    }

    /**
     * Use the PricesCOP relation PricesCOP object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \BaobabModels\PricesCOPQuery A secondary query class using the current class as primary query
     */
    public function usePricesCOPQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinPricesCOP($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'PricesCOP', '\BaobabModels\PricesCOPQuery');
    }

    /**
     * Filter the query by a related \BaobabModels\PricesUSD object
     *
     * @param \BaobabModels\PricesUSD|ObjectCollection $pricesUSD the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildProductQuery The current query, for fluid interface
     */
    public function filterByPricesUSD($pricesUSD, $comparison = null)
    {
        if ($pricesUSD instanceof \BaobabModels\PricesUSD) {
            return $this
                ->addUsingAlias(ProductTableMap::COL_SLUG, $pricesUSD->getProductSlug(), $comparison);
        } elseif ($pricesUSD instanceof ObjectCollection) {
            return $this
                ->usePricesUSDQuery()
                ->filterByPrimaryKeys($pricesUSD->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByPricesUSD() only accepts arguments of type \BaobabModels\PricesUSD or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the PricesUSD relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildProductQuery The current query, for fluid interface
     */
    public function joinPricesUSD($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('PricesUSD');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'PricesUSD');
        }

        return $this;
    }

    /**
     * Use the PricesUSD relation PricesUSD object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \BaobabModels\PricesUSDQuery A secondary query class using the current class as primary query
     */
    public function usePricesUSDQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinPricesUSD($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'PricesUSD', '\BaobabModels\PricesUSDQuery');
    }

    /**
     * Filter the query by a related \BaobabModels\Bikini object
     *
     * @param \BaobabModels\Bikini|ObjectCollection $bikini the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildProductQuery The current query, for fluid interface
     */
    public function filterByBikini($bikini, $comparison = null)
    {
        if ($bikini instanceof \BaobabModels\Bikini) {
            return $this
                ->addUsingAlias(ProductTableMap::COL_SLUG, $bikini->getSlug(), $comparison);
        } elseif ($bikini instanceof ObjectCollection) {
            return $this
                ->useBikiniQuery()
                ->filterByPrimaryKeys($bikini->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByBikini() only accepts arguments of type \BaobabModels\Bikini or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Bikini relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildProductQuery The current query, for fluid interface
     */
    public function joinBikini($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Bikini');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Bikini');
        }

        return $this;
    }

    /**
     * Use the Bikini relation Bikini object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \BaobabModels\BikiniQuery A secondary query class using the current class as primary query
     */
    public function useBikiniQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinBikini($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Bikini', '\BaobabModels\BikiniQuery');
    }

    /**
     * Filter the query by a related \BaobabModels\OnePiece object
     *
     * @param \BaobabModels\OnePiece|ObjectCollection $onePiece the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildProductQuery The current query, for fluid interface
     */
    public function filterByOnePiece($onePiece, $comparison = null)
    {
        if ($onePiece instanceof \BaobabModels\OnePiece) {
            return $this
                ->addUsingAlias(ProductTableMap::COL_SLUG, $onePiece->getSlug(), $comparison);
        } elseif ($onePiece instanceof ObjectCollection) {
            return $this
                ->useOnePieceQuery()
                ->filterByPrimaryKeys($onePiece->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByOnePiece() only accepts arguments of type \BaobabModels\OnePiece or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the OnePiece relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildProductQuery The current query, for fluid interface
     */
    public function joinOnePiece($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('OnePiece');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'OnePiece');
        }

        return $this;
    }

    /**
     * Use the OnePiece relation OnePiece object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \BaobabModels\OnePieceQuery A secondary query class using the current class as primary query
     */
    public function useOnePieceQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinOnePiece($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'OnePiece', '\BaobabModels\OnePieceQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildProduct $product Object to remove from the list of results
     *
     * @return $this|ChildProductQuery The current query, for fluid interface
     */
    public function prune($product = null)
    {
        if ($product) {
            $this->addUsingAlias(ProductTableMap::COL_SLUG, $product->getSlug(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the product table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(ProductTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            ProductTableMap::clearInstancePool();
            ProductTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(ProductTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(ProductTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            ProductTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            ProductTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // ProductQuery
