<?php

namespace BaobabModels\Base;

use \Exception;
use \PDO;
use BaobabModels\CountriesShippingPrices as ChildCountriesShippingPrices;
use BaobabModels\CountriesShippingPricesQuery as ChildCountriesShippingPricesQuery;
use BaobabModels\Map\CountriesShippingPricesTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'countries_shipping_prices' table.
 *
 *
 *
 * @method     ChildCountriesShippingPricesQuery orderById($order = Criteria::ASC) Order by the id column
 * @method     ChildCountriesShippingPricesQuery orderByCountryName($order = Criteria::ASC) Order by the country_name column
 * @method     ChildCountriesShippingPricesQuery orderByPrice($order = Criteria::ASC) Order by the price column
 * @method     ChildCountriesShippingPricesQuery orderByCurrency($order = Criteria::ASC) Order by the currency column
 *
 * @method     ChildCountriesShippingPricesQuery groupById() Group by the id column
 * @method     ChildCountriesShippingPricesQuery groupByCountryName() Group by the country_name column
 * @method     ChildCountriesShippingPricesQuery groupByPrice() Group by the price column
 * @method     ChildCountriesShippingPricesQuery groupByCurrency() Group by the currency column
 *
 * @method     ChildCountriesShippingPricesQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildCountriesShippingPricesQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildCountriesShippingPricesQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildCountriesShippingPricesQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildCountriesShippingPricesQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildCountriesShippingPricesQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildCountriesShippingPrices findOne(ConnectionInterface $con = null) Return the first ChildCountriesShippingPrices matching the query
 * @method     ChildCountriesShippingPrices findOneOrCreate(ConnectionInterface $con = null) Return the first ChildCountriesShippingPrices matching the query, or a new ChildCountriesShippingPrices object populated from the query conditions when no match is found
 *
 * @method     ChildCountriesShippingPrices findOneById(int $id) Return the first ChildCountriesShippingPrices filtered by the id column
 * @method     ChildCountriesShippingPrices findOneByCountryName(string $country_name) Return the first ChildCountriesShippingPrices filtered by the country_name column
 * @method     ChildCountriesShippingPrices findOneByPrice(int $price) Return the first ChildCountriesShippingPrices filtered by the price column
 * @method     ChildCountriesShippingPrices findOneByCurrency(string $currency) Return the first ChildCountriesShippingPrices filtered by the currency column *

 * @method     ChildCountriesShippingPrices requirePk($key, ConnectionInterface $con = null) Return the ChildCountriesShippingPrices by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCountriesShippingPrices requireOne(ConnectionInterface $con = null) Return the first ChildCountriesShippingPrices matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildCountriesShippingPrices requireOneById(int $id) Return the first ChildCountriesShippingPrices filtered by the id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCountriesShippingPrices requireOneByCountryName(string $country_name) Return the first ChildCountriesShippingPrices filtered by the country_name column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCountriesShippingPrices requireOneByPrice(int $price) Return the first ChildCountriesShippingPrices filtered by the price column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCountriesShippingPrices requireOneByCurrency(string $currency) Return the first ChildCountriesShippingPrices filtered by the currency column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildCountriesShippingPrices[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildCountriesShippingPrices objects based on current ModelCriteria
 * @method     ChildCountriesShippingPrices[]|ObjectCollection findById(int $id) Return ChildCountriesShippingPrices objects filtered by the id column
 * @method     ChildCountriesShippingPrices[]|ObjectCollection findByCountryName(string $country_name) Return ChildCountriesShippingPrices objects filtered by the country_name column
 * @method     ChildCountriesShippingPrices[]|ObjectCollection findByPrice(int $price) Return ChildCountriesShippingPrices objects filtered by the price column
 * @method     ChildCountriesShippingPrices[]|ObjectCollection findByCurrency(string $currency) Return ChildCountriesShippingPrices objects filtered by the currency column
 * @method     ChildCountriesShippingPrices[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class CountriesShippingPricesQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \BaobabModels\Base\CountriesShippingPricesQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'baobab', $modelName = '\\BaobabModels\\CountriesShippingPrices', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildCountriesShippingPricesQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildCountriesShippingPricesQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildCountriesShippingPricesQuery) {
            return $criteria;
        }
        $query = new ChildCountriesShippingPricesQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildCountriesShippingPrices|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(CountriesShippingPricesTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = CountriesShippingPricesTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildCountriesShippingPrices A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT id, country_name, price, currency FROM countries_shipping_prices WHERE id = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildCountriesShippingPrices $obj */
            $obj = new ChildCountriesShippingPrices();
            $obj->hydrate($row);
            CountriesShippingPricesTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildCountriesShippingPrices|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildCountriesShippingPricesQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(CountriesShippingPricesTableMap::COL_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildCountriesShippingPricesQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(CountriesShippingPricesTableMap::COL_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id > 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCountriesShippingPricesQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(CountriesShippingPricesTableMap::COL_ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(CountriesShippingPricesTableMap::COL_ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CountriesShippingPricesTableMap::COL_ID, $id, $comparison);
    }

    /**
     * Filter the query on the country_name column
     *
     * Example usage:
     * <code>
     * $query->filterByCountryName('fooValue');   // WHERE country_name = 'fooValue'
     * $query->filterByCountryName('%fooValue%', Criteria::LIKE); // WHERE country_name LIKE '%fooValue%'
     * </code>
     *
     * @param     string $countryName The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCountriesShippingPricesQuery The current query, for fluid interface
     */
    public function filterByCountryName($countryName = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($countryName)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CountriesShippingPricesTableMap::COL_COUNTRY_NAME, $countryName, $comparison);
    }

    /**
     * Filter the query on the price column
     *
     * Example usage:
     * <code>
     * $query->filterByPrice(1234); // WHERE price = 1234
     * $query->filterByPrice(array(12, 34)); // WHERE price IN (12, 34)
     * $query->filterByPrice(array('min' => 12)); // WHERE price > 12
     * </code>
     *
     * @param     mixed $price The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCountriesShippingPricesQuery The current query, for fluid interface
     */
    public function filterByPrice($price = null, $comparison = null)
    {
        if (is_array($price)) {
            $useMinMax = false;
            if (isset($price['min'])) {
                $this->addUsingAlias(CountriesShippingPricesTableMap::COL_PRICE, $price['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($price['max'])) {
                $this->addUsingAlias(CountriesShippingPricesTableMap::COL_PRICE, $price['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CountriesShippingPricesTableMap::COL_PRICE, $price, $comparison);
    }

    /**
     * Filter the query on the currency column
     *
     * Example usage:
     * <code>
     * $query->filterByCurrency('fooValue');   // WHERE currency = 'fooValue'
     * $query->filterByCurrency('%fooValue%', Criteria::LIKE); // WHERE currency LIKE '%fooValue%'
     * </code>
     *
     * @param     string $currency The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCountriesShippingPricesQuery The current query, for fluid interface
     */
    public function filterByCurrency($currency = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($currency)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CountriesShippingPricesTableMap::COL_CURRENCY, $currency, $comparison);
    }

    /**
     * Exclude object from result
     *
     * @param   ChildCountriesShippingPrices $countriesShippingPrices Object to remove from the list of results
     *
     * @return $this|ChildCountriesShippingPricesQuery The current query, for fluid interface
     */
    public function prune($countriesShippingPrices = null)
    {
        if ($countriesShippingPrices) {
            $this->addUsingAlias(CountriesShippingPricesTableMap::COL_ID, $countriesShippingPrices->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the countries_shipping_prices table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(CountriesShippingPricesTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            CountriesShippingPricesTableMap::clearInstancePool();
            CountriesShippingPricesTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(CountriesShippingPricesTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(CountriesShippingPricesTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            CountriesShippingPricesTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            CountriesShippingPricesTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // CountriesShippingPricesQuery
