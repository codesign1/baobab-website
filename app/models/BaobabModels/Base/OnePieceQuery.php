<?php

namespace BaobabModels\Base;

use \Exception;
use \PDO;
use BaobabModels\OnePiece as ChildOnePiece;
use BaobabModels\OnePieceQuery as ChildOnePieceQuery;
use BaobabModels\ProductQuery as ChildProductQuery;
use BaobabModels\Map\OnePieceTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'one_piece' table.
 *
 *
 *
 * @method     ChildOnePieceQuery orderBySlug($order = Criteria::ASC) Order by the slug column
 * @method     ChildOnePieceQuery orderByName($order = Criteria::ASC) Order by the name column
 * @method     ChildOnePieceQuery orderByShowOrder($order = Criteria::ASC) Order by the show_order column
 * @method     ChildOnePieceQuery orderByDescription($order = Criteria::ASC) Order by the description column
 *
 * @method     ChildOnePieceQuery groupBySlug() Group by the slug column
 * @method     ChildOnePieceQuery groupByName() Group by the name column
 * @method     ChildOnePieceQuery groupByShowOrder() Group by the show_order column
 * @method     ChildOnePieceQuery groupByDescription() Group by the description column
 *
 * @method     ChildOnePieceQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildOnePieceQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildOnePieceQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildOnePieceQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildOnePieceQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildOnePieceQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildOnePieceQuery leftJoinProduct($relationAlias = null) Adds a LEFT JOIN clause to the query using the Product relation
 * @method     ChildOnePieceQuery rightJoinProduct($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Product relation
 * @method     ChildOnePieceQuery innerJoinProduct($relationAlias = null) Adds a INNER JOIN clause to the query using the Product relation
 *
 * @method     ChildOnePieceQuery joinWithProduct($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Product relation
 *
 * @method     ChildOnePieceQuery leftJoinWithProduct() Adds a LEFT JOIN clause and with to the query using the Product relation
 * @method     ChildOnePieceQuery rightJoinWithProduct() Adds a RIGHT JOIN clause and with to the query using the Product relation
 * @method     ChildOnePieceQuery innerJoinWithProduct() Adds a INNER JOIN clause and with to the query using the Product relation
 *
 * @method     ChildOnePieceQuery leftJoinOnePieceStockable($relationAlias = null) Adds a LEFT JOIN clause to the query using the OnePieceStockable relation
 * @method     ChildOnePieceQuery rightJoinOnePieceStockable($relationAlias = null) Adds a RIGHT JOIN clause to the query using the OnePieceStockable relation
 * @method     ChildOnePieceQuery innerJoinOnePieceStockable($relationAlias = null) Adds a INNER JOIN clause to the query using the OnePieceStockable relation
 *
 * @method     ChildOnePieceQuery joinWithOnePieceStockable($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the OnePieceStockable relation
 *
 * @method     ChildOnePieceQuery leftJoinWithOnePieceStockable() Adds a LEFT JOIN clause and with to the query using the OnePieceStockable relation
 * @method     ChildOnePieceQuery rightJoinWithOnePieceStockable() Adds a RIGHT JOIN clause and with to the query using the OnePieceStockable relation
 * @method     ChildOnePieceQuery innerJoinWithOnePieceStockable() Adds a INNER JOIN clause and with to the query using the OnePieceStockable relation
 *
 * @method     \BaobabModels\ProductQuery|\BaobabModels\OnePieceStockableQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildOnePiece findOne(ConnectionInterface $con = null) Return the first ChildOnePiece matching the query
 * @method     ChildOnePiece findOneOrCreate(ConnectionInterface $con = null) Return the first ChildOnePiece matching the query, or a new ChildOnePiece object populated from the query conditions when no match is found
 *
 * @method     ChildOnePiece findOneBySlug(string $slug) Return the first ChildOnePiece filtered by the slug column
 * @method     ChildOnePiece findOneByName(string $name) Return the first ChildOnePiece filtered by the name column
 * @method     ChildOnePiece findOneByShowOrder(int $show_order) Return the first ChildOnePiece filtered by the show_order column
 * @method     ChildOnePiece findOneByDescription(string $description) Return the first ChildOnePiece filtered by the description column *

 * @method     ChildOnePiece requirePk($key, ConnectionInterface $con = null) Return the ChildOnePiece by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildOnePiece requireOne(ConnectionInterface $con = null) Return the first ChildOnePiece matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildOnePiece requireOneBySlug(string $slug) Return the first ChildOnePiece filtered by the slug column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildOnePiece requireOneByName(string $name) Return the first ChildOnePiece filtered by the name column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildOnePiece requireOneByShowOrder(int $show_order) Return the first ChildOnePiece filtered by the show_order column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildOnePiece requireOneByDescription(string $description) Return the first ChildOnePiece filtered by the description column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildOnePiece[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildOnePiece objects based on current ModelCriteria
 * @method     ChildOnePiece[]|ObjectCollection findBySlug(string $slug) Return ChildOnePiece objects filtered by the slug column
 * @method     ChildOnePiece[]|ObjectCollection findByName(string $name) Return ChildOnePiece objects filtered by the name column
 * @method     ChildOnePiece[]|ObjectCollection findByShowOrder(int $show_order) Return ChildOnePiece objects filtered by the show_order column
 * @method     ChildOnePiece[]|ObjectCollection findByDescription(string $description) Return ChildOnePiece objects filtered by the description column
 * @method     ChildOnePiece[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class OnePieceQuery extends ChildProductQuery
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \BaobabModels\Base\OnePieceQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'baobab', $modelName = '\\BaobabModels\\OnePiece', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildOnePieceQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildOnePieceQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildOnePieceQuery) {
            return $criteria;
        }
        $query = new ChildOnePieceQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildOnePiece|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(OnePieceTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = OnePieceTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildOnePiece A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT slug, name, show_order, description FROM one_piece WHERE slug = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_STR);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildOnePiece $obj */
            $obj = new ChildOnePiece();
            $obj->hydrate($row);
            OnePieceTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildOnePiece|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildOnePieceQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(OnePieceTableMap::COL_SLUG, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildOnePieceQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(OnePieceTableMap::COL_SLUG, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the slug column
     *
     * Example usage:
     * <code>
     * $query->filterBySlug('fooValue');   // WHERE slug = 'fooValue'
     * $query->filterBySlug('%fooValue%', Criteria::LIKE); // WHERE slug LIKE '%fooValue%'
     * </code>
     *
     * @param     string $slug The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildOnePieceQuery The current query, for fluid interface
     */
    public function filterBySlug($slug = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($slug)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(OnePieceTableMap::COL_SLUG, $slug, $comparison);
    }

    /**
     * Filter the query on the name column
     *
     * Example usage:
     * <code>
     * $query->filterByName('fooValue');   // WHERE name = 'fooValue'
     * $query->filterByName('%fooValue%', Criteria::LIKE); // WHERE name LIKE '%fooValue%'
     * </code>
     *
     * @param     string $name The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildOnePieceQuery The current query, for fluid interface
     */
    public function filterByName($name = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($name)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(OnePieceTableMap::COL_NAME, $name, $comparison);
    }

    /**
     * Filter the query on the show_order column
     *
     * Example usage:
     * <code>
     * $query->filterByShowOrder(1234); // WHERE show_order = 1234
     * $query->filterByShowOrder(array(12, 34)); // WHERE show_order IN (12, 34)
     * $query->filterByShowOrder(array('min' => 12)); // WHERE show_order > 12
     * </code>
     *
     * @param     mixed $showOrder The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildOnePieceQuery The current query, for fluid interface
     */
    public function filterByShowOrder($showOrder = null, $comparison = null)
    {
        if (is_array($showOrder)) {
            $useMinMax = false;
            if (isset($showOrder['min'])) {
                $this->addUsingAlias(OnePieceTableMap::COL_SHOW_ORDER, $showOrder['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($showOrder['max'])) {
                $this->addUsingAlias(OnePieceTableMap::COL_SHOW_ORDER, $showOrder['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(OnePieceTableMap::COL_SHOW_ORDER, $showOrder, $comparison);
    }

    /**
     * Filter the query on the description column
     *
     * Example usage:
     * <code>
     * $query->filterByDescription('fooValue');   // WHERE description = 'fooValue'
     * $query->filterByDescription('%fooValue%', Criteria::LIKE); // WHERE description LIKE '%fooValue%'
     * </code>
     *
     * @param     string $description The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildOnePieceQuery The current query, for fluid interface
     */
    public function filterByDescription($description = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($description)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(OnePieceTableMap::COL_DESCRIPTION, $description, $comparison);
    }

    /**
     * Filter the query by a related \BaobabModels\Product object
     *
     * @param \BaobabModels\Product|ObjectCollection $product The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildOnePieceQuery The current query, for fluid interface
     */
    public function filterByProduct($product, $comparison = null)
    {
        if ($product instanceof \BaobabModels\Product) {
            return $this
                ->addUsingAlias(OnePieceTableMap::COL_SLUG, $product->getSlug(), $comparison);
        } elseif ($product instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(OnePieceTableMap::COL_SLUG, $product->toKeyValue('PrimaryKey', 'Slug'), $comparison);
        } else {
            throw new PropelException('filterByProduct() only accepts arguments of type \BaobabModels\Product or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Product relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildOnePieceQuery The current query, for fluid interface
     */
    public function joinProduct($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Product');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Product');
        }

        return $this;
    }

    /**
     * Use the Product relation Product object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \BaobabModels\ProductQuery A secondary query class using the current class as primary query
     */
    public function useProductQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinProduct($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Product', '\BaobabModels\ProductQuery');
    }

    /**
     * Filter the query by a related \BaobabModels\OnePieceStockable object
     *
     * @param \BaobabModels\OnePieceStockable|ObjectCollection $onePieceStockable the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildOnePieceQuery The current query, for fluid interface
     */
    public function filterByOnePieceStockable($onePieceStockable, $comparison = null)
    {
        if ($onePieceStockable instanceof \BaobabModels\OnePieceStockable) {
            return $this
                ->addUsingAlias(OnePieceTableMap::COL_SLUG, $onePieceStockable->getOnePieceSlug(), $comparison);
        } elseif ($onePieceStockable instanceof ObjectCollection) {
            return $this
                ->useOnePieceStockableQuery()
                ->filterByPrimaryKeys($onePieceStockable->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByOnePieceStockable() only accepts arguments of type \BaobabModels\OnePieceStockable or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the OnePieceStockable relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildOnePieceQuery The current query, for fluid interface
     */
    public function joinOnePieceStockable($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('OnePieceStockable');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'OnePieceStockable');
        }

        return $this;
    }

    /**
     * Use the OnePieceStockable relation OnePieceStockable object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \BaobabModels\OnePieceStockableQuery A secondary query class using the current class as primary query
     */
    public function useOnePieceStockableQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinOnePieceStockable($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'OnePieceStockable', '\BaobabModels\OnePieceStockableQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildOnePiece $onePiece Object to remove from the list of results
     *
     * @return $this|ChildOnePieceQuery The current query, for fluid interface
     */
    public function prune($onePiece = null)
    {
        if ($onePiece) {
            $this->addUsingAlias(OnePieceTableMap::COL_SLUG, $onePiece->getSlug(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the one_piece table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(OnePieceTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            OnePieceTableMap::clearInstancePool();
            OnePieceTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(OnePieceTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(OnePieceTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            OnePieceTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            OnePieceTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // OnePieceQuery
