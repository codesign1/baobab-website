<?php

namespace BaobabModels\Base;

use \Exception;
use \PDO;
use BaobabModels\BaobabOrder as ChildBaobabOrder;
use BaobabModels\BaobabOrderQuery as ChildBaobabOrderQuery;
use BaobabModels\Map\BaobabOrderTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'baobab_order' table.
 *
 *
 *
 * @method     ChildBaobabOrderQuery orderById($order = Criteria::ASC) Order by the id column
 * @method     ChildBaobabOrderQuery orderByUserId($order = Criteria::ASC) Order by the user_id column
 * @method     ChildBaobabOrderQuery orderByOrderStatus($order = Criteria::ASC) Order by the order_status column
 * @method     ChildBaobabOrderQuery orderByTotalPrice($order = Criteria::ASC) Order by the total_price column
 * @method     ChildBaobabOrderQuery orderByCurrency($order = Criteria::ASC) Order by the currency column
 * @method     ChildBaobabOrderQuery orderByAddress($order = Criteria::ASC) Order by the address column
 * @method     ChildBaobabOrderQuery orderByPhone($order = Criteria::ASC) Order by the phone column
 * @method     ChildBaobabOrderQuery orderByCity($order = Criteria::ASC) Order by the city column
 * @method     ChildBaobabOrderQuery orderByNeighborhood($order = Criteria::ASC) Order by the neighborhood column
 * @method     ChildBaobabOrderQuery orderByPostalCode($order = Criteria::ASC) Order by the postal_code column
 * @method     ChildBaobabOrderQuery orderByCreatedAt($order = Criteria::ASC) Order by the created_at column
 * @method     ChildBaobabOrderQuery orderByUpdatedAt($order = Criteria::ASC) Order by the updated_at column
 *
 * @method     ChildBaobabOrderQuery groupById() Group by the id column
 * @method     ChildBaobabOrderQuery groupByUserId() Group by the user_id column
 * @method     ChildBaobabOrderQuery groupByOrderStatus() Group by the order_status column
 * @method     ChildBaobabOrderQuery groupByTotalPrice() Group by the total_price column
 * @method     ChildBaobabOrderQuery groupByCurrency() Group by the currency column
 * @method     ChildBaobabOrderQuery groupByAddress() Group by the address column
 * @method     ChildBaobabOrderQuery groupByPhone() Group by the phone column
 * @method     ChildBaobabOrderQuery groupByCity() Group by the city column
 * @method     ChildBaobabOrderQuery groupByNeighborhood() Group by the neighborhood column
 * @method     ChildBaobabOrderQuery groupByPostalCode() Group by the postal_code column
 * @method     ChildBaobabOrderQuery groupByCreatedAt() Group by the created_at column
 * @method     ChildBaobabOrderQuery groupByUpdatedAt() Group by the updated_at column
 *
 * @method     ChildBaobabOrderQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildBaobabOrderQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildBaobabOrderQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildBaobabOrderQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildBaobabOrderQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildBaobabOrderQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildBaobabOrderQuery leftJoinBikiniOrder($relationAlias = null) Adds a LEFT JOIN clause to the query using the BikiniOrder relation
 * @method     ChildBaobabOrderQuery rightJoinBikiniOrder($relationAlias = null) Adds a RIGHT JOIN clause to the query using the BikiniOrder relation
 * @method     ChildBaobabOrderQuery innerJoinBikiniOrder($relationAlias = null) Adds a INNER JOIN clause to the query using the BikiniOrder relation
 *
 * @method     ChildBaobabOrderQuery joinWithBikiniOrder($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the BikiniOrder relation
 *
 * @method     ChildBaobabOrderQuery leftJoinWithBikiniOrder() Adds a LEFT JOIN clause and with to the query using the BikiniOrder relation
 * @method     ChildBaobabOrderQuery rightJoinWithBikiniOrder() Adds a RIGHT JOIN clause and with to the query using the BikiniOrder relation
 * @method     ChildBaobabOrderQuery innerJoinWithBikiniOrder() Adds a INNER JOIN clause and with to the query using the BikiniOrder relation
 *
 * @method     ChildBaobabOrderQuery leftJoinOnePieceOrder($relationAlias = null) Adds a LEFT JOIN clause to the query using the OnePieceOrder relation
 * @method     ChildBaobabOrderQuery rightJoinOnePieceOrder($relationAlias = null) Adds a RIGHT JOIN clause to the query using the OnePieceOrder relation
 * @method     ChildBaobabOrderQuery innerJoinOnePieceOrder($relationAlias = null) Adds a INNER JOIN clause to the query using the OnePieceOrder relation
 *
 * @method     ChildBaobabOrderQuery joinWithOnePieceOrder($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the OnePieceOrder relation
 *
 * @method     ChildBaobabOrderQuery leftJoinWithOnePieceOrder() Adds a LEFT JOIN clause and with to the query using the OnePieceOrder relation
 * @method     ChildBaobabOrderQuery rightJoinWithOnePieceOrder() Adds a RIGHT JOIN clause and with to the query using the OnePieceOrder relation
 * @method     ChildBaobabOrderQuery innerJoinWithOnePieceOrder() Adds a INNER JOIN clause and with to the query using the OnePieceOrder relation
 *
 * @method     \BaobabModels\BikiniOrderQuery|\BaobabModels\OnePieceOrderQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildBaobabOrder findOne(ConnectionInterface $con = null) Return the first ChildBaobabOrder matching the query
 * @method     ChildBaobabOrder findOneOrCreate(ConnectionInterface $con = null) Return the first ChildBaobabOrder matching the query, or a new ChildBaobabOrder object populated from the query conditions when no match is found
 *
 * @method     ChildBaobabOrder findOneById(string $id) Return the first ChildBaobabOrder filtered by the id column
 * @method     ChildBaobabOrder findOneByUserId(string $user_id) Return the first ChildBaobabOrder filtered by the user_id column
 * @method     ChildBaobabOrder findOneByOrderStatus(int $order_status) Return the first ChildBaobabOrder filtered by the order_status column
 * @method     ChildBaobabOrder findOneByTotalPrice(string $total_price) Return the first ChildBaobabOrder filtered by the total_price column
 * @method     ChildBaobabOrder findOneByCurrency(string $currency) Return the first ChildBaobabOrder filtered by the currency column
 * @method     ChildBaobabOrder findOneByAddress(string $address) Return the first ChildBaobabOrder filtered by the address column
 * @method     ChildBaobabOrder findOneByPhone(string $phone) Return the first ChildBaobabOrder filtered by the phone column
 * @method     ChildBaobabOrder findOneByCity(string $city) Return the first ChildBaobabOrder filtered by the city column
 * @method     ChildBaobabOrder findOneByNeighborhood(string $neighborhood) Return the first ChildBaobabOrder filtered by the neighborhood column
 * @method     ChildBaobabOrder findOneByPostalCode(string $postal_code) Return the first ChildBaobabOrder filtered by the postal_code column
 * @method     ChildBaobabOrder findOneByCreatedAt(string $created_at) Return the first ChildBaobabOrder filtered by the created_at column
 * @method     ChildBaobabOrder findOneByUpdatedAt(string $updated_at) Return the first ChildBaobabOrder filtered by the updated_at column *

 * @method     ChildBaobabOrder requirePk($key, ConnectionInterface $con = null) Return the ChildBaobabOrder by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildBaobabOrder requireOne(ConnectionInterface $con = null) Return the first ChildBaobabOrder matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildBaobabOrder requireOneById(string $id) Return the first ChildBaobabOrder filtered by the id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildBaobabOrder requireOneByUserId(string $user_id) Return the first ChildBaobabOrder filtered by the user_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildBaobabOrder requireOneByOrderStatus(int $order_status) Return the first ChildBaobabOrder filtered by the order_status column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildBaobabOrder requireOneByTotalPrice(string $total_price) Return the first ChildBaobabOrder filtered by the total_price column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildBaobabOrder requireOneByCurrency(string $currency) Return the first ChildBaobabOrder filtered by the currency column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildBaobabOrder requireOneByAddress(string $address) Return the first ChildBaobabOrder filtered by the address column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildBaobabOrder requireOneByPhone(string $phone) Return the first ChildBaobabOrder filtered by the phone column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildBaobabOrder requireOneByCity(string $city) Return the first ChildBaobabOrder filtered by the city column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildBaobabOrder requireOneByNeighborhood(string $neighborhood) Return the first ChildBaobabOrder filtered by the neighborhood column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildBaobabOrder requireOneByPostalCode(string $postal_code) Return the first ChildBaobabOrder filtered by the postal_code column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildBaobabOrder requireOneByCreatedAt(string $created_at) Return the first ChildBaobabOrder filtered by the created_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildBaobabOrder requireOneByUpdatedAt(string $updated_at) Return the first ChildBaobabOrder filtered by the updated_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildBaobabOrder[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildBaobabOrder objects based on current ModelCriteria
 * @method     ChildBaobabOrder[]|ObjectCollection findById(string $id) Return ChildBaobabOrder objects filtered by the id column
 * @method     ChildBaobabOrder[]|ObjectCollection findByUserId(string $user_id) Return ChildBaobabOrder objects filtered by the user_id column
 * @method     ChildBaobabOrder[]|ObjectCollection findByOrderStatus(int $order_status) Return ChildBaobabOrder objects filtered by the order_status column
 * @method     ChildBaobabOrder[]|ObjectCollection findByTotalPrice(string $total_price) Return ChildBaobabOrder objects filtered by the total_price column
 * @method     ChildBaobabOrder[]|ObjectCollection findByCurrency(string $currency) Return ChildBaobabOrder objects filtered by the currency column
 * @method     ChildBaobabOrder[]|ObjectCollection findByAddress(string $address) Return ChildBaobabOrder objects filtered by the address column
 * @method     ChildBaobabOrder[]|ObjectCollection findByPhone(string $phone) Return ChildBaobabOrder objects filtered by the phone column
 * @method     ChildBaobabOrder[]|ObjectCollection findByCity(string $city) Return ChildBaobabOrder objects filtered by the city column
 * @method     ChildBaobabOrder[]|ObjectCollection findByNeighborhood(string $neighborhood) Return ChildBaobabOrder objects filtered by the neighborhood column
 * @method     ChildBaobabOrder[]|ObjectCollection findByPostalCode(string $postal_code) Return ChildBaobabOrder objects filtered by the postal_code column
 * @method     ChildBaobabOrder[]|ObjectCollection findByCreatedAt(string $created_at) Return ChildBaobabOrder objects filtered by the created_at column
 * @method     ChildBaobabOrder[]|ObjectCollection findByUpdatedAt(string $updated_at) Return ChildBaobabOrder objects filtered by the updated_at column
 * @method     ChildBaobabOrder[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class BaobabOrderQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \BaobabModels\Base\BaobabOrderQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'baobab', $modelName = '\\BaobabModels\\BaobabOrder', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildBaobabOrderQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildBaobabOrderQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildBaobabOrderQuery) {
            return $criteria;
        }
        $query = new ChildBaobabOrderQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildBaobabOrder|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(BaobabOrderTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = BaobabOrderTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildBaobabOrder A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT id, user_id, order_status, total_price, currency, address, phone, city, neighborhood, postal_code, created_at, updated_at FROM baobab_order WHERE id = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_STR);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildBaobabOrder $obj */
            $obj = new ChildBaobabOrder();
            $obj->hydrate($row);
            BaobabOrderTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildBaobabOrder|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildBaobabOrderQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(BaobabOrderTableMap::COL_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildBaobabOrderQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(BaobabOrderTableMap::COL_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById('fooValue');   // WHERE id = 'fooValue'
     * $query->filterById('%fooValue%', Criteria::LIKE); // WHERE id LIKE '%fooValue%'
     * </code>
     *
     * @param     string $id The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildBaobabOrderQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($id)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(BaobabOrderTableMap::COL_ID, $id, $comparison);
    }

    /**
     * Filter the query on the user_id column
     *
     * Example usage:
     * <code>
     * $query->filterByUserId('fooValue');   // WHERE user_id = 'fooValue'
     * $query->filterByUserId('%fooValue%', Criteria::LIKE); // WHERE user_id LIKE '%fooValue%'
     * </code>
     *
     * @param     string $userId The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildBaobabOrderQuery The current query, for fluid interface
     */
    public function filterByUserId($userId = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($userId)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(BaobabOrderTableMap::COL_USER_ID, $userId, $comparison);
    }

    /**
     * Filter the query on the order_status column
     *
     * @param     mixed $orderStatus The value to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildBaobabOrderQuery The current query, for fluid interface
     */
    public function filterByOrderStatus($orderStatus = null, $comparison = null)
    {
        $valueSet = BaobabOrderTableMap::getValueSet(BaobabOrderTableMap::COL_ORDER_STATUS);
        if (is_scalar($orderStatus)) {
            if (!in_array($orderStatus, $valueSet)) {
                throw new PropelException(sprintf('Value "%s" is not accepted in this enumerated column', $orderStatus));
            }
            $orderStatus = array_search($orderStatus, $valueSet);
        } elseif (is_array($orderStatus)) {
            $convertedValues = array();
            foreach ($orderStatus as $value) {
                if (!in_array($value, $valueSet)) {
                    throw new PropelException(sprintf('Value "%s" is not accepted in this enumerated column', $value));
                }
                $convertedValues []= array_search($value, $valueSet);
            }
            $orderStatus = $convertedValues;
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(BaobabOrderTableMap::COL_ORDER_STATUS, $orderStatus, $comparison);
    }

    /**
     * Filter the query on the total_price column
     *
     * Example usage:
     * <code>
     * $query->filterByTotalPrice(1234); // WHERE total_price = 1234
     * $query->filterByTotalPrice(array(12, 34)); // WHERE total_price IN (12, 34)
     * $query->filterByTotalPrice(array('min' => 12)); // WHERE total_price > 12
     * </code>
     *
     * @param     mixed $totalPrice The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildBaobabOrderQuery The current query, for fluid interface
     */
    public function filterByTotalPrice($totalPrice = null, $comparison = null)
    {
        if (is_array($totalPrice)) {
            $useMinMax = false;
            if (isset($totalPrice['min'])) {
                $this->addUsingAlias(BaobabOrderTableMap::COL_TOTAL_PRICE, $totalPrice['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($totalPrice['max'])) {
                $this->addUsingAlias(BaobabOrderTableMap::COL_TOTAL_PRICE, $totalPrice['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(BaobabOrderTableMap::COL_TOTAL_PRICE, $totalPrice, $comparison);
    }

    /**
     * Filter the query on the currency column
     *
     * Example usage:
     * <code>
     * $query->filterByCurrency('fooValue');   // WHERE currency = 'fooValue'
     * $query->filterByCurrency('%fooValue%', Criteria::LIKE); // WHERE currency LIKE '%fooValue%'
     * </code>
     *
     * @param     string $currency The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildBaobabOrderQuery The current query, for fluid interface
     */
    public function filterByCurrency($currency = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($currency)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(BaobabOrderTableMap::COL_CURRENCY, $currency, $comparison);
    }

    /**
     * Filter the query on the address column
     *
     * Example usage:
     * <code>
     * $query->filterByAddress('fooValue');   // WHERE address = 'fooValue'
     * $query->filterByAddress('%fooValue%', Criteria::LIKE); // WHERE address LIKE '%fooValue%'
     * </code>
     *
     * @param     string $address The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildBaobabOrderQuery The current query, for fluid interface
     */
    public function filterByAddress($address = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($address)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(BaobabOrderTableMap::COL_ADDRESS, $address, $comparison);
    }

    /**
     * Filter the query on the phone column
     *
     * Example usage:
     * <code>
     * $query->filterByPhone('fooValue');   // WHERE phone = 'fooValue'
     * $query->filterByPhone('%fooValue%', Criteria::LIKE); // WHERE phone LIKE '%fooValue%'
     * </code>
     *
     * @param     string $phone The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildBaobabOrderQuery The current query, for fluid interface
     */
    public function filterByPhone($phone = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($phone)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(BaobabOrderTableMap::COL_PHONE, $phone, $comparison);
    }

    /**
     * Filter the query on the city column
     *
     * Example usage:
     * <code>
     * $query->filterByCity('fooValue');   // WHERE city = 'fooValue'
     * $query->filterByCity('%fooValue%', Criteria::LIKE); // WHERE city LIKE '%fooValue%'
     * </code>
     *
     * @param     string $city The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildBaobabOrderQuery The current query, for fluid interface
     */
    public function filterByCity($city = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($city)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(BaobabOrderTableMap::COL_CITY, $city, $comparison);
    }

    /**
     * Filter the query on the neighborhood column
     *
     * Example usage:
     * <code>
     * $query->filterByNeighborhood('fooValue');   // WHERE neighborhood = 'fooValue'
     * $query->filterByNeighborhood('%fooValue%', Criteria::LIKE); // WHERE neighborhood LIKE '%fooValue%'
     * </code>
     *
     * @param     string $neighborhood The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildBaobabOrderQuery The current query, for fluid interface
     */
    public function filterByNeighborhood($neighborhood = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($neighborhood)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(BaobabOrderTableMap::COL_NEIGHBORHOOD, $neighborhood, $comparison);
    }

    /**
     * Filter the query on the postal_code column
     *
     * Example usage:
     * <code>
     * $query->filterByPostalCode('fooValue');   // WHERE postal_code = 'fooValue'
     * $query->filterByPostalCode('%fooValue%', Criteria::LIKE); // WHERE postal_code LIKE '%fooValue%'
     * </code>
     *
     * @param     string $postalCode The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildBaobabOrderQuery The current query, for fluid interface
     */
    public function filterByPostalCode($postalCode = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($postalCode)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(BaobabOrderTableMap::COL_POSTAL_CODE, $postalCode, $comparison);
    }

    /**
     * Filter the query on the created_at column
     *
     * Example usage:
     * <code>
     * $query->filterByCreatedAt('2011-03-14'); // WHERE created_at = '2011-03-14'
     * $query->filterByCreatedAt('now'); // WHERE created_at = '2011-03-14'
     * $query->filterByCreatedAt(array('max' => 'yesterday')); // WHERE created_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $createdAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildBaobabOrderQuery The current query, for fluid interface
     */
    public function filterByCreatedAt($createdAt = null, $comparison = null)
    {
        if (is_array($createdAt)) {
            $useMinMax = false;
            if (isset($createdAt['min'])) {
                $this->addUsingAlias(BaobabOrderTableMap::COL_CREATED_AT, $createdAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($createdAt['max'])) {
                $this->addUsingAlias(BaobabOrderTableMap::COL_CREATED_AT, $createdAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(BaobabOrderTableMap::COL_CREATED_AT, $createdAt, $comparison);
    }

    /**
     * Filter the query on the updated_at column
     *
     * Example usage:
     * <code>
     * $query->filterByUpdatedAt('2011-03-14'); // WHERE updated_at = '2011-03-14'
     * $query->filterByUpdatedAt('now'); // WHERE updated_at = '2011-03-14'
     * $query->filterByUpdatedAt(array('max' => 'yesterday')); // WHERE updated_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $updatedAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildBaobabOrderQuery The current query, for fluid interface
     */
    public function filterByUpdatedAt($updatedAt = null, $comparison = null)
    {
        if (is_array($updatedAt)) {
            $useMinMax = false;
            if (isset($updatedAt['min'])) {
                $this->addUsingAlias(BaobabOrderTableMap::COL_UPDATED_AT, $updatedAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($updatedAt['max'])) {
                $this->addUsingAlias(BaobabOrderTableMap::COL_UPDATED_AT, $updatedAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(BaobabOrderTableMap::COL_UPDATED_AT, $updatedAt, $comparison);
    }

    /**
     * Filter the query by a related \BaobabModels\BikiniOrder object
     *
     * @param \BaobabModels\BikiniOrder|ObjectCollection $bikiniOrder the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildBaobabOrderQuery The current query, for fluid interface
     */
    public function filterByBikiniOrder($bikiniOrder, $comparison = null)
    {
        if ($bikiniOrder instanceof \BaobabModels\BikiniOrder) {
            return $this
                ->addUsingAlias(BaobabOrderTableMap::COL_ID, $bikiniOrder->getOrderId(), $comparison);
        } elseif ($bikiniOrder instanceof ObjectCollection) {
            return $this
                ->useBikiniOrderQuery()
                ->filterByPrimaryKeys($bikiniOrder->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByBikiniOrder() only accepts arguments of type \BaobabModels\BikiniOrder or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the BikiniOrder relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildBaobabOrderQuery The current query, for fluid interface
     */
    public function joinBikiniOrder($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('BikiniOrder');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'BikiniOrder');
        }

        return $this;
    }

    /**
     * Use the BikiniOrder relation BikiniOrder object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \BaobabModels\BikiniOrderQuery A secondary query class using the current class as primary query
     */
    public function useBikiniOrderQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinBikiniOrder($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'BikiniOrder', '\BaobabModels\BikiniOrderQuery');
    }

    /**
     * Filter the query by a related \BaobabModels\OnePieceOrder object
     *
     * @param \BaobabModels\OnePieceOrder|ObjectCollection $onePieceOrder the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildBaobabOrderQuery The current query, for fluid interface
     */
    public function filterByOnePieceOrder($onePieceOrder, $comparison = null)
    {
        if ($onePieceOrder instanceof \BaobabModels\OnePieceOrder) {
            return $this
                ->addUsingAlias(BaobabOrderTableMap::COL_ID, $onePieceOrder->getOrderId(), $comparison);
        } elseif ($onePieceOrder instanceof ObjectCollection) {
            return $this
                ->useOnePieceOrderQuery()
                ->filterByPrimaryKeys($onePieceOrder->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByOnePieceOrder() only accepts arguments of type \BaobabModels\OnePieceOrder or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the OnePieceOrder relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildBaobabOrderQuery The current query, for fluid interface
     */
    public function joinOnePieceOrder($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('OnePieceOrder');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'OnePieceOrder');
        }

        return $this;
    }

    /**
     * Use the OnePieceOrder relation OnePieceOrder object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \BaobabModels\OnePieceOrderQuery A secondary query class using the current class as primary query
     */
    public function useOnePieceOrderQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinOnePieceOrder($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'OnePieceOrder', '\BaobabModels\OnePieceOrderQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildBaobabOrder $baobabOrder Object to remove from the list of results
     *
     * @return $this|ChildBaobabOrderQuery The current query, for fluid interface
     */
    public function prune($baobabOrder = null)
    {
        if ($baobabOrder) {
            $this->addUsingAlias(BaobabOrderTableMap::COL_ID, $baobabOrder->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the baobab_order table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(BaobabOrderTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            BaobabOrderTableMap::clearInstancePool();
            BaobabOrderTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(BaobabOrderTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(BaobabOrderTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            BaobabOrderTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            BaobabOrderTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    // timestampable behavior

    /**
     * Filter by the latest updated
     *
     * @param      int $nbDays Maximum age of the latest update in days
     *
     * @return     $this|ChildBaobabOrderQuery The current query, for fluid interface
     */
    public function recentlyUpdated($nbDays = 7)
    {
        return $this->addUsingAlias(BaobabOrderTableMap::COL_UPDATED_AT, time() - $nbDays * 24 * 60 * 60, Criteria::GREATER_EQUAL);
    }

    /**
     * Order by update date desc
     *
     * @return     $this|ChildBaobabOrderQuery The current query, for fluid interface
     */
    public function lastUpdatedFirst()
    {
        return $this->addDescendingOrderByColumn(BaobabOrderTableMap::COL_UPDATED_AT);
    }

    /**
     * Order by update date asc
     *
     * @return     $this|ChildBaobabOrderQuery The current query, for fluid interface
     */
    public function firstUpdatedFirst()
    {
        return $this->addAscendingOrderByColumn(BaobabOrderTableMap::COL_UPDATED_AT);
    }

    /**
     * Order by create date desc
     *
     * @return     $this|ChildBaobabOrderQuery The current query, for fluid interface
     */
    public function lastCreatedFirst()
    {
        return $this->addDescendingOrderByColumn(BaobabOrderTableMap::COL_CREATED_AT);
    }

    /**
     * Filter by the latest created
     *
     * @param      int $nbDays Maximum age of in days
     *
     * @return     $this|ChildBaobabOrderQuery The current query, for fluid interface
     */
    public function recentlyCreated($nbDays = 7)
    {
        return $this->addUsingAlias(BaobabOrderTableMap::COL_CREATED_AT, time() - $nbDays * 24 * 60 * 60, Criteria::GREATER_EQUAL);
    }

    /**
     * Order by create date asc
     *
     * @return     $this|ChildBaobabOrderQuery The current query, for fluid interface
     */
    public function firstCreatedFirst()
    {
        return $this->addAscendingOrderByColumn(BaobabOrderTableMap::COL_CREATED_AT);
    }

} // BaobabOrderQuery
