<?php

namespace BaobabModels\Base;

use \Exception;
use \PDO;
use BaobabModels\OnePieceOrder as ChildOnePieceOrder;
use BaobabModels\OnePieceOrderQuery as ChildOnePieceOrderQuery;
use BaobabModels\Map\OnePieceOrderTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'one_piece_order' table.
 *
 *
 *
 * @method     ChildOnePieceOrderQuery orderById($order = Criteria::ASC) Order by the id column
 * @method     ChildOnePieceOrderQuery orderByOrderId($order = Criteria::ASC) Order by the order_id column
 * @method     ChildOnePieceOrderQuery orderByTopSku($order = Criteria::ASC) Order by the top_sku column
 * @method     ChildOnePieceOrderQuery orderByAmount($order = Criteria::ASC) Order by the amount column
 * @method     ChildOnePieceOrderQuery orderByUnitprice($order = Criteria::ASC) Order by the unitPrice column
 * @method     ChildOnePieceOrderQuery orderBySlug($order = Criteria::ASC) Order by the slug column
 *
 * @method     ChildOnePieceOrderQuery groupById() Group by the id column
 * @method     ChildOnePieceOrderQuery groupByOrderId() Group by the order_id column
 * @method     ChildOnePieceOrderQuery groupByTopSku() Group by the top_sku column
 * @method     ChildOnePieceOrderQuery groupByAmount() Group by the amount column
 * @method     ChildOnePieceOrderQuery groupByUnitprice() Group by the unitPrice column
 * @method     ChildOnePieceOrderQuery groupBySlug() Group by the slug column
 *
 * @method     ChildOnePieceOrderQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildOnePieceOrderQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildOnePieceOrderQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildOnePieceOrderQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildOnePieceOrderQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildOnePieceOrderQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildOnePieceOrderQuery leftJoinBaobabOrder($relationAlias = null) Adds a LEFT JOIN clause to the query using the BaobabOrder relation
 * @method     ChildOnePieceOrderQuery rightJoinBaobabOrder($relationAlias = null) Adds a RIGHT JOIN clause to the query using the BaobabOrder relation
 * @method     ChildOnePieceOrderQuery innerJoinBaobabOrder($relationAlias = null) Adds a INNER JOIN clause to the query using the BaobabOrder relation
 *
 * @method     ChildOnePieceOrderQuery joinWithBaobabOrder($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the BaobabOrder relation
 *
 * @method     ChildOnePieceOrderQuery leftJoinWithBaobabOrder() Adds a LEFT JOIN clause and with to the query using the BaobabOrder relation
 * @method     ChildOnePieceOrderQuery rightJoinWithBaobabOrder() Adds a RIGHT JOIN clause and with to the query using the BaobabOrder relation
 * @method     ChildOnePieceOrderQuery innerJoinWithBaobabOrder() Adds a INNER JOIN clause and with to the query using the BaobabOrder relation
 *
 * @method     \BaobabModels\BaobabOrderQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildOnePieceOrder findOne(ConnectionInterface $con = null) Return the first ChildOnePieceOrder matching the query
 * @method     ChildOnePieceOrder findOneOrCreate(ConnectionInterface $con = null) Return the first ChildOnePieceOrder matching the query, or a new ChildOnePieceOrder object populated from the query conditions when no match is found
 *
 * @method     ChildOnePieceOrder findOneById(int $id) Return the first ChildOnePieceOrder filtered by the id column
 * @method     ChildOnePieceOrder findOneByOrderId(string $order_id) Return the first ChildOnePieceOrder filtered by the order_id column
 * @method     ChildOnePieceOrder findOneByTopSku(string $top_sku) Return the first ChildOnePieceOrder filtered by the top_sku column
 * @method     ChildOnePieceOrder findOneByAmount(string $amount) Return the first ChildOnePieceOrder filtered by the amount column
 * @method     ChildOnePieceOrder findOneByUnitprice(string $unitPrice) Return the first ChildOnePieceOrder filtered by the unitPrice column
 * @method     ChildOnePieceOrder findOneBySlug(string $slug) Return the first ChildOnePieceOrder filtered by the slug column *

 * @method     ChildOnePieceOrder requirePk($key, ConnectionInterface $con = null) Return the ChildOnePieceOrder by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildOnePieceOrder requireOne(ConnectionInterface $con = null) Return the first ChildOnePieceOrder matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildOnePieceOrder requireOneById(int $id) Return the first ChildOnePieceOrder filtered by the id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildOnePieceOrder requireOneByOrderId(string $order_id) Return the first ChildOnePieceOrder filtered by the order_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildOnePieceOrder requireOneByTopSku(string $top_sku) Return the first ChildOnePieceOrder filtered by the top_sku column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildOnePieceOrder requireOneByAmount(string $amount) Return the first ChildOnePieceOrder filtered by the amount column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildOnePieceOrder requireOneByUnitprice(string $unitPrice) Return the first ChildOnePieceOrder filtered by the unitPrice column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildOnePieceOrder requireOneBySlug(string $slug) Return the first ChildOnePieceOrder filtered by the slug column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildOnePieceOrder[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildOnePieceOrder objects based on current ModelCriteria
 * @method     ChildOnePieceOrder[]|ObjectCollection findById(int $id) Return ChildOnePieceOrder objects filtered by the id column
 * @method     ChildOnePieceOrder[]|ObjectCollection findByOrderId(string $order_id) Return ChildOnePieceOrder objects filtered by the order_id column
 * @method     ChildOnePieceOrder[]|ObjectCollection findByTopSku(string $top_sku) Return ChildOnePieceOrder objects filtered by the top_sku column
 * @method     ChildOnePieceOrder[]|ObjectCollection findByAmount(string $amount) Return ChildOnePieceOrder objects filtered by the amount column
 * @method     ChildOnePieceOrder[]|ObjectCollection findByUnitprice(string $unitPrice) Return ChildOnePieceOrder objects filtered by the unitPrice column
 * @method     ChildOnePieceOrder[]|ObjectCollection findBySlug(string $slug) Return ChildOnePieceOrder objects filtered by the slug column
 * @method     ChildOnePieceOrder[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class OnePieceOrderQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \BaobabModels\Base\OnePieceOrderQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'baobab', $modelName = '\\BaobabModels\\OnePieceOrder', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildOnePieceOrderQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildOnePieceOrderQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildOnePieceOrderQuery) {
            return $criteria;
        }
        $query = new ChildOnePieceOrderQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildOnePieceOrder|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(OnePieceOrderTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = OnePieceOrderTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildOnePieceOrder A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT id, order_id, top_sku, amount, unitPrice, slug FROM one_piece_order WHERE id = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildOnePieceOrder $obj */
            $obj = new ChildOnePieceOrder();
            $obj->hydrate($row);
            OnePieceOrderTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildOnePieceOrder|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildOnePieceOrderQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(OnePieceOrderTableMap::COL_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildOnePieceOrderQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(OnePieceOrderTableMap::COL_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id > 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildOnePieceOrderQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(OnePieceOrderTableMap::COL_ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(OnePieceOrderTableMap::COL_ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(OnePieceOrderTableMap::COL_ID, $id, $comparison);
    }

    /**
     * Filter the query on the order_id column
     *
     * Example usage:
     * <code>
     * $query->filterByOrderId('fooValue');   // WHERE order_id = 'fooValue'
     * $query->filterByOrderId('%fooValue%', Criteria::LIKE); // WHERE order_id LIKE '%fooValue%'
     * </code>
     *
     * @param     string $orderId The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildOnePieceOrderQuery The current query, for fluid interface
     */
    public function filterByOrderId($orderId = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($orderId)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(OnePieceOrderTableMap::COL_ORDER_ID, $orderId, $comparison);
    }

    /**
     * Filter the query on the top_sku column
     *
     * Example usage:
     * <code>
     * $query->filterByTopSku('fooValue');   // WHERE top_sku = 'fooValue'
     * $query->filterByTopSku('%fooValue%', Criteria::LIKE); // WHERE top_sku LIKE '%fooValue%'
     * </code>
     *
     * @param     string $topSku The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildOnePieceOrderQuery The current query, for fluid interface
     */
    public function filterByTopSku($topSku = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($topSku)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(OnePieceOrderTableMap::COL_TOP_SKU, $topSku, $comparison);
    }

    /**
     * Filter the query on the amount column
     *
     * Example usage:
     * <code>
     * $query->filterByAmount(1234); // WHERE amount = 1234
     * $query->filterByAmount(array(12, 34)); // WHERE amount IN (12, 34)
     * $query->filterByAmount(array('min' => 12)); // WHERE amount > 12
     * </code>
     *
     * @param     mixed $amount The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildOnePieceOrderQuery The current query, for fluid interface
     */
    public function filterByAmount($amount = null, $comparison = null)
    {
        if (is_array($amount)) {
            $useMinMax = false;
            if (isset($amount['min'])) {
                $this->addUsingAlias(OnePieceOrderTableMap::COL_AMOUNT, $amount['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($amount['max'])) {
                $this->addUsingAlias(OnePieceOrderTableMap::COL_AMOUNT, $amount['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(OnePieceOrderTableMap::COL_AMOUNT, $amount, $comparison);
    }

    /**
     * Filter the query on the unitPrice column
     *
     * Example usage:
     * <code>
     * $query->filterByUnitprice(1234); // WHERE unitPrice = 1234
     * $query->filterByUnitprice(array(12, 34)); // WHERE unitPrice IN (12, 34)
     * $query->filterByUnitprice(array('min' => 12)); // WHERE unitPrice > 12
     * </code>
     *
     * @param     mixed $unitprice The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildOnePieceOrderQuery The current query, for fluid interface
     */
    public function filterByUnitprice($unitprice = null, $comparison = null)
    {
        if (is_array($unitprice)) {
            $useMinMax = false;
            if (isset($unitprice['min'])) {
                $this->addUsingAlias(OnePieceOrderTableMap::COL_UNITPRICE, $unitprice['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($unitprice['max'])) {
                $this->addUsingAlias(OnePieceOrderTableMap::COL_UNITPRICE, $unitprice['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(OnePieceOrderTableMap::COL_UNITPRICE, $unitprice, $comparison);
    }

    /**
     * Filter the query on the slug column
     *
     * Example usage:
     * <code>
     * $query->filterBySlug('fooValue');   // WHERE slug = 'fooValue'
     * $query->filterBySlug('%fooValue%', Criteria::LIKE); // WHERE slug LIKE '%fooValue%'
     * </code>
     *
     * @param     string $slug The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildOnePieceOrderQuery The current query, for fluid interface
     */
    public function filterBySlug($slug = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($slug)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(OnePieceOrderTableMap::COL_SLUG, $slug, $comparison);
    }

    /**
     * Filter the query by a related \BaobabModels\BaobabOrder object
     *
     * @param \BaobabModels\BaobabOrder|ObjectCollection $baobabOrder The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildOnePieceOrderQuery The current query, for fluid interface
     */
    public function filterByBaobabOrder($baobabOrder, $comparison = null)
    {
        if ($baobabOrder instanceof \BaobabModels\BaobabOrder) {
            return $this
                ->addUsingAlias(OnePieceOrderTableMap::COL_ORDER_ID, $baobabOrder->getId(), $comparison);
        } elseif ($baobabOrder instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(OnePieceOrderTableMap::COL_ORDER_ID, $baobabOrder->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByBaobabOrder() only accepts arguments of type \BaobabModels\BaobabOrder or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the BaobabOrder relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildOnePieceOrderQuery The current query, for fluid interface
     */
    public function joinBaobabOrder($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('BaobabOrder');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'BaobabOrder');
        }

        return $this;
    }

    /**
     * Use the BaobabOrder relation BaobabOrder object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \BaobabModels\BaobabOrderQuery A secondary query class using the current class as primary query
     */
    public function useBaobabOrderQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinBaobabOrder($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'BaobabOrder', '\BaobabModels\BaobabOrderQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildOnePieceOrder $onePieceOrder Object to remove from the list of results
     *
     * @return $this|ChildOnePieceOrderQuery The current query, for fluid interface
     */
    public function prune($onePieceOrder = null)
    {
        if ($onePieceOrder) {
            $this->addUsingAlias(OnePieceOrderTableMap::COL_ID, $onePieceOrder->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the one_piece_order table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(OnePieceOrderTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            OnePieceOrderTableMap::clearInstancePool();
            OnePieceOrderTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(OnePieceOrderTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(OnePieceOrderTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            OnePieceOrderTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            OnePieceOrderTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // OnePieceOrderQuery
