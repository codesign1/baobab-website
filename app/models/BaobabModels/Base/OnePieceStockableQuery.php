<?php

namespace BaobabModels\Base;

use \Exception;
use \PDO;
use BaobabModels\OnePieceStockable as ChildOnePieceStockable;
use BaobabModels\OnePieceStockableQuery as ChildOnePieceStockableQuery;
use BaobabModels\StockableQuery as ChildStockableQuery;
use BaobabModels\Map\OnePieceStockableTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'one_piece_stockable' table.
 *
 *
 *
 * @method     ChildOnePieceStockableQuery orderBySize($order = Criteria::ASC) Order by the size column
 * @method     ChildOnePieceStockableQuery orderByOnePieceSlug($order = Criteria::ASC) Order by the one_piece_slug column
 * @method     ChildOnePieceStockableQuery orderBySku($order = Criteria::ASC) Order by the sku column
 * @method     ChildOnePieceStockableQuery orderByStockAmount($order = Criteria::ASC) Order by the stock_amount column
 *
 * @method     ChildOnePieceStockableQuery groupBySize() Group by the size column
 * @method     ChildOnePieceStockableQuery groupByOnePieceSlug() Group by the one_piece_slug column
 * @method     ChildOnePieceStockableQuery groupBySku() Group by the sku column
 * @method     ChildOnePieceStockableQuery groupByStockAmount() Group by the stock_amount column
 *
 * @method     ChildOnePieceStockableQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildOnePieceStockableQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildOnePieceStockableQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildOnePieceStockableQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildOnePieceStockableQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildOnePieceStockableQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildOnePieceStockableQuery leftJoinOnePiece($relationAlias = null) Adds a LEFT JOIN clause to the query using the OnePiece relation
 * @method     ChildOnePieceStockableQuery rightJoinOnePiece($relationAlias = null) Adds a RIGHT JOIN clause to the query using the OnePiece relation
 * @method     ChildOnePieceStockableQuery innerJoinOnePiece($relationAlias = null) Adds a INNER JOIN clause to the query using the OnePiece relation
 *
 * @method     ChildOnePieceStockableQuery joinWithOnePiece($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the OnePiece relation
 *
 * @method     ChildOnePieceStockableQuery leftJoinWithOnePiece() Adds a LEFT JOIN clause and with to the query using the OnePiece relation
 * @method     ChildOnePieceStockableQuery rightJoinWithOnePiece() Adds a RIGHT JOIN clause and with to the query using the OnePiece relation
 * @method     ChildOnePieceStockableQuery innerJoinWithOnePiece() Adds a INNER JOIN clause and with to the query using the OnePiece relation
 *
 * @method     ChildOnePieceStockableQuery leftJoinStockable($relationAlias = null) Adds a LEFT JOIN clause to the query using the Stockable relation
 * @method     ChildOnePieceStockableQuery rightJoinStockable($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Stockable relation
 * @method     ChildOnePieceStockableQuery innerJoinStockable($relationAlias = null) Adds a INNER JOIN clause to the query using the Stockable relation
 *
 * @method     ChildOnePieceStockableQuery joinWithStockable($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Stockable relation
 *
 * @method     ChildOnePieceStockableQuery leftJoinWithStockable() Adds a LEFT JOIN clause and with to the query using the Stockable relation
 * @method     ChildOnePieceStockableQuery rightJoinWithStockable() Adds a RIGHT JOIN clause and with to the query using the Stockable relation
 * @method     ChildOnePieceStockableQuery innerJoinWithStockable() Adds a INNER JOIN clause and with to the query using the Stockable relation
 *
 * @method     \BaobabModels\OnePieceQuery|\BaobabModels\StockableQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildOnePieceStockable findOne(ConnectionInterface $con = null) Return the first ChildOnePieceStockable matching the query
 * @method     ChildOnePieceStockable findOneOrCreate(ConnectionInterface $con = null) Return the first ChildOnePieceStockable matching the query, or a new ChildOnePieceStockable object populated from the query conditions when no match is found
 *
 * @method     ChildOnePieceStockable findOneBySize(string $size) Return the first ChildOnePieceStockable filtered by the size column
 * @method     ChildOnePieceStockable findOneByOnePieceSlug(string $one_piece_slug) Return the first ChildOnePieceStockable filtered by the one_piece_slug column
 * @method     ChildOnePieceStockable findOneBySku(string $sku) Return the first ChildOnePieceStockable filtered by the sku column
 * @method     ChildOnePieceStockable findOneByStockAmount(int $stock_amount) Return the first ChildOnePieceStockable filtered by the stock_amount column *

 * @method     ChildOnePieceStockable requirePk($key, ConnectionInterface $con = null) Return the ChildOnePieceStockable by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildOnePieceStockable requireOne(ConnectionInterface $con = null) Return the first ChildOnePieceStockable matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildOnePieceStockable requireOneBySize(string $size) Return the first ChildOnePieceStockable filtered by the size column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildOnePieceStockable requireOneByOnePieceSlug(string $one_piece_slug) Return the first ChildOnePieceStockable filtered by the one_piece_slug column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildOnePieceStockable requireOneBySku(string $sku) Return the first ChildOnePieceStockable filtered by the sku column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildOnePieceStockable requireOneByStockAmount(int $stock_amount) Return the first ChildOnePieceStockable filtered by the stock_amount column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildOnePieceStockable[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildOnePieceStockable objects based on current ModelCriteria
 * @method     ChildOnePieceStockable[]|ObjectCollection findBySize(string $size) Return ChildOnePieceStockable objects filtered by the size column
 * @method     ChildOnePieceStockable[]|ObjectCollection findByOnePieceSlug(string $one_piece_slug) Return ChildOnePieceStockable objects filtered by the one_piece_slug column
 * @method     ChildOnePieceStockable[]|ObjectCollection findBySku(string $sku) Return ChildOnePieceStockable objects filtered by the sku column
 * @method     ChildOnePieceStockable[]|ObjectCollection findByStockAmount(int $stock_amount) Return ChildOnePieceStockable objects filtered by the stock_amount column
 * @method     ChildOnePieceStockable[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class OnePieceStockableQuery extends ChildStockableQuery
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \BaobabModels\Base\OnePieceStockableQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'baobab', $modelName = '\\BaobabModels\\OnePieceStockable', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildOnePieceStockableQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildOnePieceStockableQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildOnePieceStockableQuery) {
            return $criteria;
        }
        $query = new ChildOnePieceStockableQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildOnePieceStockable|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(OnePieceStockableTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = OnePieceStockableTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildOnePieceStockable A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT size, one_piece_slug, sku, stock_amount FROM one_piece_stockable WHERE sku = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_STR);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildOnePieceStockable $obj */
            $obj = new ChildOnePieceStockable();
            $obj->hydrate($row);
            OnePieceStockableTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildOnePieceStockable|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildOnePieceStockableQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(OnePieceStockableTableMap::COL_SKU, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildOnePieceStockableQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(OnePieceStockableTableMap::COL_SKU, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the size column
     *
     * Example usage:
     * <code>
     * $query->filterBySize('fooValue');   // WHERE size = 'fooValue'
     * $query->filterBySize('%fooValue%', Criteria::LIKE); // WHERE size LIKE '%fooValue%'
     * </code>
     *
     * @param     string $size The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildOnePieceStockableQuery The current query, for fluid interface
     */
    public function filterBySize($size = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($size)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(OnePieceStockableTableMap::COL_SIZE, $size, $comparison);
    }

    /**
     * Filter the query on the one_piece_slug column
     *
     * Example usage:
     * <code>
     * $query->filterByOnePieceSlug('fooValue');   // WHERE one_piece_slug = 'fooValue'
     * $query->filterByOnePieceSlug('%fooValue%', Criteria::LIKE); // WHERE one_piece_slug LIKE '%fooValue%'
     * </code>
     *
     * @param     string $onePieceSlug The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildOnePieceStockableQuery The current query, for fluid interface
     */
    public function filterByOnePieceSlug($onePieceSlug = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($onePieceSlug)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(OnePieceStockableTableMap::COL_ONE_PIECE_SLUG, $onePieceSlug, $comparison);
    }

    /**
     * Filter the query on the sku column
     *
     * Example usage:
     * <code>
     * $query->filterBySku('fooValue');   // WHERE sku = 'fooValue'
     * $query->filterBySku('%fooValue%', Criteria::LIKE); // WHERE sku LIKE '%fooValue%'
     * </code>
     *
     * @param     string $sku The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildOnePieceStockableQuery The current query, for fluid interface
     */
    public function filterBySku($sku = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($sku)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(OnePieceStockableTableMap::COL_SKU, $sku, $comparison);
    }

    /**
     * Filter the query on the stock_amount column
     *
     * Example usage:
     * <code>
     * $query->filterByStockAmount(1234); // WHERE stock_amount = 1234
     * $query->filterByStockAmount(array(12, 34)); // WHERE stock_amount IN (12, 34)
     * $query->filterByStockAmount(array('min' => 12)); // WHERE stock_amount > 12
     * </code>
     *
     * @param     mixed $stockAmount The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildOnePieceStockableQuery The current query, for fluid interface
     */
    public function filterByStockAmount($stockAmount = null, $comparison = null)
    {
        if (is_array($stockAmount)) {
            $useMinMax = false;
            if (isset($stockAmount['min'])) {
                $this->addUsingAlias(OnePieceStockableTableMap::COL_STOCK_AMOUNT, $stockAmount['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($stockAmount['max'])) {
                $this->addUsingAlias(OnePieceStockableTableMap::COL_STOCK_AMOUNT, $stockAmount['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(OnePieceStockableTableMap::COL_STOCK_AMOUNT, $stockAmount, $comparison);
    }

    /**
     * Filter the query by a related \BaobabModels\OnePiece object
     *
     * @param \BaobabModels\OnePiece|ObjectCollection $onePiece The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildOnePieceStockableQuery The current query, for fluid interface
     */
    public function filterByOnePiece($onePiece, $comparison = null)
    {
        if ($onePiece instanceof \BaobabModels\OnePiece) {
            return $this
                ->addUsingAlias(OnePieceStockableTableMap::COL_ONE_PIECE_SLUG, $onePiece->getSlug(), $comparison);
        } elseif ($onePiece instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(OnePieceStockableTableMap::COL_ONE_PIECE_SLUG, $onePiece->toKeyValue('PrimaryKey', 'Slug'), $comparison);
        } else {
            throw new PropelException('filterByOnePiece() only accepts arguments of type \BaobabModels\OnePiece or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the OnePiece relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildOnePieceStockableQuery The current query, for fluid interface
     */
    public function joinOnePiece($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('OnePiece');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'OnePiece');
        }

        return $this;
    }

    /**
     * Use the OnePiece relation OnePiece object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \BaobabModels\OnePieceQuery A secondary query class using the current class as primary query
     */
    public function useOnePieceQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinOnePiece($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'OnePiece', '\BaobabModels\OnePieceQuery');
    }

    /**
     * Filter the query by a related \BaobabModels\Stockable object
     *
     * @param \BaobabModels\Stockable|ObjectCollection $stockable The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildOnePieceStockableQuery The current query, for fluid interface
     */
    public function filterByStockable($stockable, $comparison = null)
    {
        if ($stockable instanceof \BaobabModels\Stockable) {
            return $this
                ->addUsingAlias(OnePieceStockableTableMap::COL_SKU, $stockable->getSku(), $comparison);
        } elseif ($stockable instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(OnePieceStockableTableMap::COL_SKU, $stockable->toKeyValue('PrimaryKey', 'Sku'), $comparison);
        } else {
            throw new PropelException('filterByStockable() only accepts arguments of type \BaobabModels\Stockable or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Stockable relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildOnePieceStockableQuery The current query, for fluid interface
     */
    public function joinStockable($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Stockable');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Stockable');
        }

        return $this;
    }

    /**
     * Use the Stockable relation Stockable object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \BaobabModels\StockableQuery A secondary query class using the current class as primary query
     */
    public function useStockableQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinStockable($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Stockable', '\BaobabModels\StockableQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildOnePieceStockable $onePieceStockable Object to remove from the list of results
     *
     * @return $this|ChildOnePieceStockableQuery The current query, for fluid interface
     */
    public function prune($onePieceStockable = null)
    {
        if ($onePieceStockable) {
            $this->addUsingAlias(OnePieceStockableTableMap::COL_SKU, $onePieceStockable->getSku(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the one_piece_stockable table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(OnePieceStockableTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            OnePieceStockableTableMap::clearInstancePool();
            OnePieceStockableTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(OnePieceStockableTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(OnePieceStockableTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            OnePieceStockableTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            OnePieceStockableTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // OnePieceStockableQuery
