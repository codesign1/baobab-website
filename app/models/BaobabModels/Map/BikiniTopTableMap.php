<?php

namespace BaobabModels\Map;

use BaobabModels\BikiniTop;
use BaobabModels\BikiniTopQuery;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\InstancePoolTrait;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\DataFetcher\DataFetcherInterface;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\RelationMap;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Map\TableMapTrait;


/**
 * This class defines the structure of the 'bikini_top' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 */
class BikiniTopTableMap extends TableMap
{
    use InstancePoolTrait;
    use TableMapTrait;

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'BaobabModels.Map.BikiniTopTableMap';

    /**
     * The default database name for this class
     */
    const DATABASE_NAME = 'baobab';

    /**
     * The table name for this class
     */
    const TABLE_NAME = 'bikini_top';

    /**
     * The related Propel class for this table
     */
    const OM_CLASS = '\\BaobabModels\\BikiniTop';

    /**
     * A class that can be returned by this tableMap
     */
    const CLASS_DEFAULT = 'BaobabModels.BikiniTop';

    /**
     * The total number of columns
     */
    const NUM_COLUMNS = 5;

    /**
     * The number of lazy-loaded columns
     */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /**
     * The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS)
     */
    const NUM_HYDRATE_COLUMNS = 5;

    /**
     * the column name for the size field
     */
    const COL_SIZE = 'bikini_top.size';

    /**
     * the column name for the pushup_enabled field
     */
    const COL_PUSHUP_ENABLED = 'bikini_top.pushup_enabled';

    /**
     * the column name for the bikini_slug field
     */
    const COL_BIKINI_SLUG = 'bikini_top.bikini_slug';

    /**
     * the column name for the sku field
     */
    const COL_SKU = 'bikini_top.sku';

    /**
     * the column name for the stock_amount field
     */
    const COL_STOCK_AMOUNT = 'bikini_top.stock_amount';

    /**
     * The default string format for model objects of the related table
     */
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldNames[self::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        self::TYPE_PHPNAME       => array('Size', 'PushupEnabled', 'BikiniSlug', 'Sku', 'StockAmount', ),
        self::TYPE_CAMELNAME     => array('size', 'pushupEnabled', 'bikiniSlug', 'sku', 'stockAmount', ),
        self::TYPE_COLNAME       => array(BikiniTopTableMap::COL_SIZE, BikiniTopTableMap::COL_PUSHUP_ENABLED, BikiniTopTableMap::COL_BIKINI_SLUG, BikiniTopTableMap::COL_SKU, BikiniTopTableMap::COL_STOCK_AMOUNT, ),
        self::TYPE_FIELDNAME     => array('size', 'pushup_enabled', 'bikini_slug', 'sku', 'stock_amount', ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldKeys[self::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        self::TYPE_PHPNAME       => array('Size' => 0, 'PushupEnabled' => 1, 'BikiniSlug' => 2, 'Sku' => 3, 'StockAmount' => 4, ),
        self::TYPE_CAMELNAME     => array('size' => 0, 'pushupEnabled' => 1, 'bikiniSlug' => 2, 'sku' => 3, 'stockAmount' => 4, ),
        self::TYPE_COLNAME       => array(BikiniTopTableMap::COL_SIZE => 0, BikiniTopTableMap::COL_PUSHUP_ENABLED => 1, BikiniTopTableMap::COL_BIKINI_SLUG => 2, BikiniTopTableMap::COL_SKU => 3, BikiniTopTableMap::COL_STOCK_AMOUNT => 4, ),
        self::TYPE_FIELDNAME     => array('size' => 0, 'pushup_enabled' => 1, 'bikini_slug' => 2, 'sku' => 3, 'stock_amount' => 4, ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, )
    );

    /**
     * Initialize the table attributes and columns
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('bikini_top');
        $this->setPhpName('BikiniTop');
        $this->setIdentifierQuoting(false);
        $this->setClassName('\\BaobabModels\\BikiniTop');
        $this->setPackage('BaobabModels');
        $this->setUseIdGenerator(false);
        // columns
        $this->addColumn('size', 'Size', 'CHAR', true, 10, null);
        $this->addColumn('pushup_enabled', 'PushupEnabled', 'BOOLEAN', true, 1, null);
        $this->addForeignKey('bikini_slug', 'BikiniSlug', 'VARCHAR', 'bikini', 'slug', true, 255, null);
        $this->addForeignPrimaryKey('sku', 'Sku', 'VARCHAR' , 'stockable', 'sku', true, 255, null);
        $this->addColumn('stock_amount', 'StockAmount', 'INTEGER', true, null, null);
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('Bikini', '\\BaobabModels\\Bikini', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':bikini_slug',
    1 => ':slug',
  ),
), null, null, null, false);
        $this->addRelation('Stockable', '\\BaobabModels\\Stockable', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':sku',
    1 => ':sku',
  ),
), 'CASCADE', null, null, false);
    } // buildRelations()

    /**
     *
     * Gets the list of behaviors registered for this table
     *
     * @return array Associative array (name => parameters) of behaviors
     */
    public function getBehaviors()
    {
        return array(
            'concrete_inheritance' => array('extends' => 'stockable', 'descendant_column' => 'descendant_class', 'copy_data_to_parent' => 'true', 'copy_data_to_child' => 'false', 'schema' => '', 'exclude_behaviors' => '', ),
        );
    } // getBehaviors()

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return string The primary key hash of the row
     */
    public static function getPrimaryKeyHashFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        // If the PK cannot be derived from the row, return NULL.
        if ($row[TableMap::TYPE_NUM == $indexType ? 3 + $offset : static::translateFieldName('Sku', TableMap::TYPE_PHPNAME, $indexType)] === null) {
            return null;
        }

        return null === $row[TableMap::TYPE_NUM == $indexType ? 3 + $offset : static::translateFieldName('Sku', TableMap::TYPE_PHPNAME, $indexType)] || is_scalar($row[TableMap::TYPE_NUM == $indexType ? 3 + $offset : static::translateFieldName('Sku', TableMap::TYPE_PHPNAME, $indexType)]) || is_callable([$row[TableMap::TYPE_NUM == $indexType ? 3 + $offset : static::translateFieldName('Sku', TableMap::TYPE_PHPNAME, $indexType)], '__toString']) ? (string) $row[TableMap::TYPE_NUM == $indexType ? 3 + $offset : static::translateFieldName('Sku', TableMap::TYPE_PHPNAME, $indexType)] : $row[TableMap::TYPE_NUM == $indexType ? 3 + $offset : static::translateFieldName('Sku', TableMap::TYPE_PHPNAME, $indexType)];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        return (string) $row[
            $indexType == TableMap::TYPE_NUM
                ? 3 + $offset
                : self::translateFieldName('Sku', TableMap::TYPE_PHPNAME, $indexType)
        ];
    }

    /**
     * The class that the tableMap will make instances of.
     *
     * If $withPrefix is true, the returned path
     * uses a dot-path notation which is translated into a path
     * relative to a location on the PHP include_path.
     * (e.g. path.to.MyClass -> 'path/to/MyClass.php')
     *
     * @param boolean $withPrefix Whether or not to return the path with the class name
     * @return string path.to.ClassName
     */
    public static function getOMClass($withPrefix = true)
    {
        return $withPrefix ? BikiniTopTableMap::CLASS_DEFAULT : BikiniTopTableMap::OM_CLASS;
    }

    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param array  $row       row returned by DataFetcher->fetch().
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                 One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     * @return array           (BikiniTop object, last column rank)
     */
    public static function populateObject($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        $key = BikiniTopTableMap::getPrimaryKeyHashFromRow($row, $offset, $indexType);
        if (null !== ($obj = BikiniTopTableMap::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $offset, true); // rehydrate
            $col = $offset + BikiniTopTableMap::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = BikiniTopTableMap::OM_CLASS;
            /** @var BikiniTop $obj */
            $obj = new $cls();
            $col = $obj->hydrate($row, $offset, false, $indexType);
            BikiniTopTableMap::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @param DataFetcherInterface $dataFetcher
     * @return array
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function populateObjects(DataFetcherInterface $dataFetcher)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = static::getOMClass(false);
        // populate the object(s)
        while ($row = $dataFetcher->fetch()) {
            $key = BikiniTopTableMap::getPrimaryKeyHashFromRow($row, 0, $dataFetcher->getIndexType());
            if (null !== ($obj = BikiniTopTableMap::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                /** @var BikiniTop $obj */
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                BikiniTopTableMap::addInstanceToPool($obj, $key);
            } // if key exists
        }

        return $results;
    }
    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param Criteria $criteria object containing the columns to add.
     * @param string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(BikiniTopTableMap::COL_SIZE);
            $criteria->addSelectColumn(BikiniTopTableMap::COL_PUSHUP_ENABLED);
            $criteria->addSelectColumn(BikiniTopTableMap::COL_BIKINI_SLUG);
            $criteria->addSelectColumn(BikiniTopTableMap::COL_SKU);
            $criteria->addSelectColumn(BikiniTopTableMap::COL_STOCK_AMOUNT);
        } else {
            $criteria->addSelectColumn($alias . '.size');
            $criteria->addSelectColumn($alias . '.pushup_enabled');
            $criteria->addSelectColumn($alias . '.bikini_slug');
            $criteria->addSelectColumn($alias . '.sku');
            $criteria->addSelectColumn($alias . '.stock_amount');
        }
    }

    /**
     * Returns the TableMap related to this object.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getServiceContainer()->getDatabaseMap(BikiniTopTableMap::DATABASE_NAME)->getTable(BikiniTopTableMap::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this tableMap class.
     */
    public static function buildTableMap()
    {
        $dbMap = Propel::getServiceContainer()->getDatabaseMap(BikiniTopTableMap::DATABASE_NAME);
        if (!$dbMap->hasTable(BikiniTopTableMap::TABLE_NAME)) {
            $dbMap->addTableObject(new BikiniTopTableMap());
        }
    }

    /**
     * Performs a DELETE on the database, given a BikiniTop or Criteria object OR a primary key value.
     *
     * @param mixed               $values Criteria or BikiniTop object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param  ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, ConnectionInterface $con = null)
     {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(BikiniTopTableMap::DATABASE_NAME);
        }

        if ($values instanceof Criteria) {
            // rename for clarity
            $criteria = $values;
        } elseif ($values instanceof \BaobabModels\BikiniTop) { // it's a model object
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(BikiniTopTableMap::DATABASE_NAME);
            $criteria->add(BikiniTopTableMap::COL_SKU, (array) $values, Criteria::IN);
        }

        $query = BikiniTopQuery::create()->mergeWith($criteria);

        if ($values instanceof Criteria) {
            BikiniTopTableMap::clearInstancePool();
        } elseif (!is_object($values)) { // it's a primary key, or an array of pks
            foreach ((array) $values as $singleval) {
                BikiniTopTableMap::removeInstanceFromPool($singleval);
            }
        }

        return $query->delete($con);
    }

    /**
     * Deletes all rows from the bikini_top table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public static function doDeleteAll(ConnectionInterface $con = null)
    {
        return BikiniTopQuery::create()->doDeleteAll($con);
    }

    /**
     * Performs an INSERT on the database, given a BikiniTop or Criteria object.
     *
     * @param mixed               $criteria Criteria or BikiniTop object containing data that is used to create the INSERT statement.
     * @param ConnectionInterface $con the ConnectionInterface connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function doInsert($criteria, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(BikiniTopTableMap::DATABASE_NAME);
        }

        if ($criteria instanceof Criteria) {
            $criteria = clone $criteria; // rename for clarity
        } else {
            $criteria = $criteria->buildCriteria(); // build Criteria from BikiniTop object
        }


        // Set the correct dbName
        $query = BikiniTopQuery::create()->mergeWith($criteria);

        // use transaction because $criteria could contain info
        // for more than one table (I guess, conceivably)
        return $con->transaction(function () use ($con, $query) {
            return $query->doInsert($con);
        });
    }

} // BikiniTopTableMap
// This is the static code needed to register the TableMap for this table with the main Propel class.
//
BikiniTopTableMap::buildTableMap();
