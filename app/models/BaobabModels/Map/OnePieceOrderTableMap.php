<?php

namespace BaobabModels\Map;

use BaobabModels\OnePieceOrder;
use BaobabModels\OnePieceOrderQuery;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\InstancePoolTrait;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\DataFetcher\DataFetcherInterface;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\RelationMap;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Map\TableMapTrait;


/**
 * This class defines the structure of the 'one_piece_order' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 */
class OnePieceOrderTableMap extends TableMap
{
    use InstancePoolTrait;
    use TableMapTrait;

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'BaobabModels.Map.OnePieceOrderTableMap';

    /**
     * The default database name for this class
     */
    const DATABASE_NAME = 'baobab';

    /**
     * The table name for this class
     */
    const TABLE_NAME = 'one_piece_order';

    /**
     * The related Propel class for this table
     */
    const OM_CLASS = '\\BaobabModels\\OnePieceOrder';

    /**
     * A class that can be returned by this tableMap
     */
    const CLASS_DEFAULT = 'BaobabModels.OnePieceOrder';

    /**
     * The total number of columns
     */
    const NUM_COLUMNS = 6;

    /**
     * The number of lazy-loaded columns
     */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /**
     * The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS)
     */
    const NUM_HYDRATE_COLUMNS = 6;

    /**
     * the column name for the id field
     */
    const COL_ID = 'one_piece_order.id';

    /**
     * the column name for the order_id field
     */
    const COL_ORDER_ID = 'one_piece_order.order_id';

    /**
     * the column name for the top_sku field
     */
    const COL_TOP_SKU = 'one_piece_order.top_sku';

    /**
     * the column name for the amount field
     */
    const COL_AMOUNT = 'one_piece_order.amount';

    /**
     * the column name for the unitPrice field
     */
    const COL_UNITPRICE = 'one_piece_order.unitPrice';

    /**
     * the column name for the slug field
     */
    const COL_SLUG = 'one_piece_order.slug';

    /**
     * The default string format for model objects of the related table
     */
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldNames[self::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        self::TYPE_PHPNAME       => array('Id', 'OrderId', 'TopSku', 'Amount', 'Unitprice', 'Slug', ),
        self::TYPE_CAMELNAME     => array('id', 'orderId', 'topSku', 'amount', 'unitprice', 'slug', ),
        self::TYPE_COLNAME       => array(OnePieceOrderTableMap::COL_ID, OnePieceOrderTableMap::COL_ORDER_ID, OnePieceOrderTableMap::COL_TOP_SKU, OnePieceOrderTableMap::COL_AMOUNT, OnePieceOrderTableMap::COL_UNITPRICE, OnePieceOrderTableMap::COL_SLUG, ),
        self::TYPE_FIELDNAME     => array('id', 'order_id', 'top_sku', 'amount', 'unitPrice', 'slug', ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldKeys[self::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        self::TYPE_PHPNAME       => array('Id' => 0, 'OrderId' => 1, 'TopSku' => 2, 'Amount' => 3, 'Unitprice' => 4, 'Slug' => 5, ),
        self::TYPE_CAMELNAME     => array('id' => 0, 'orderId' => 1, 'topSku' => 2, 'amount' => 3, 'unitprice' => 4, 'slug' => 5, ),
        self::TYPE_COLNAME       => array(OnePieceOrderTableMap::COL_ID => 0, OnePieceOrderTableMap::COL_ORDER_ID => 1, OnePieceOrderTableMap::COL_TOP_SKU => 2, OnePieceOrderTableMap::COL_AMOUNT => 3, OnePieceOrderTableMap::COL_UNITPRICE => 4, OnePieceOrderTableMap::COL_SLUG => 5, ),
        self::TYPE_FIELDNAME     => array('id' => 0, 'order_id' => 1, 'top_sku' => 2, 'amount' => 3, 'unitPrice' => 4, 'slug' => 5, ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, )
    );

    /**
     * Initialize the table attributes and columns
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('one_piece_order');
        $this->setPhpName('OnePieceOrder');
        $this->setIdentifierQuoting(false);
        $this->setClassName('\\BaobabModels\\OnePieceOrder');
        $this->setPackage('BaobabModels');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('id', 'Id', 'INTEGER', true, 255, null);
        $this->addForeignKey('order_id', 'OrderId', 'VARCHAR', 'baobab_order', 'id', true, 255, null);
        $this->addColumn('top_sku', 'TopSku', 'VARCHAR', true, 255, null);
        $this->addColumn('amount', 'Amount', 'NUMERIC', true, null, null);
        $this->addColumn('unitPrice', 'Unitprice', 'NUMERIC', true, null, null);
        $this->addColumn('slug', 'Slug', 'VARCHAR', true, 255, null);
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('BaobabOrder', '\\BaobabModels\\BaobabOrder', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':order_id',
    1 => ':id',
  ),
), null, null, null, false);
    } // buildRelations()

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return string The primary key hash of the row
     */
    public static function getPrimaryKeyHashFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        // If the PK cannot be derived from the row, return NULL.
        if ($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] === null) {
            return null;
        }

        return null === $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] || is_scalar($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)]) || is_callable([$row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)], '__toString']) ? (string) $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] : $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        return (int) $row[
            $indexType == TableMap::TYPE_NUM
                ? 0 + $offset
                : self::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)
        ];
    }

    /**
     * The class that the tableMap will make instances of.
     *
     * If $withPrefix is true, the returned path
     * uses a dot-path notation which is translated into a path
     * relative to a location on the PHP include_path.
     * (e.g. path.to.MyClass -> 'path/to/MyClass.php')
     *
     * @param boolean $withPrefix Whether or not to return the path with the class name
     * @return string path.to.ClassName
     */
    public static function getOMClass($withPrefix = true)
    {
        return $withPrefix ? OnePieceOrderTableMap::CLASS_DEFAULT : OnePieceOrderTableMap::OM_CLASS;
    }

    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param array  $row       row returned by DataFetcher->fetch().
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                 One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     * @return array           (OnePieceOrder object, last column rank)
     */
    public static function populateObject($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        $key = OnePieceOrderTableMap::getPrimaryKeyHashFromRow($row, $offset, $indexType);
        if (null !== ($obj = OnePieceOrderTableMap::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $offset, true); // rehydrate
            $col = $offset + OnePieceOrderTableMap::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = OnePieceOrderTableMap::OM_CLASS;
            /** @var OnePieceOrder $obj */
            $obj = new $cls();
            $col = $obj->hydrate($row, $offset, false, $indexType);
            OnePieceOrderTableMap::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @param DataFetcherInterface $dataFetcher
     * @return array
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function populateObjects(DataFetcherInterface $dataFetcher)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = static::getOMClass(false);
        // populate the object(s)
        while ($row = $dataFetcher->fetch()) {
            $key = OnePieceOrderTableMap::getPrimaryKeyHashFromRow($row, 0, $dataFetcher->getIndexType());
            if (null !== ($obj = OnePieceOrderTableMap::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                /** @var OnePieceOrder $obj */
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                OnePieceOrderTableMap::addInstanceToPool($obj, $key);
            } // if key exists
        }

        return $results;
    }
    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param Criteria $criteria object containing the columns to add.
     * @param string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(OnePieceOrderTableMap::COL_ID);
            $criteria->addSelectColumn(OnePieceOrderTableMap::COL_ORDER_ID);
            $criteria->addSelectColumn(OnePieceOrderTableMap::COL_TOP_SKU);
            $criteria->addSelectColumn(OnePieceOrderTableMap::COL_AMOUNT);
            $criteria->addSelectColumn(OnePieceOrderTableMap::COL_UNITPRICE);
            $criteria->addSelectColumn(OnePieceOrderTableMap::COL_SLUG);
        } else {
            $criteria->addSelectColumn($alias . '.id');
            $criteria->addSelectColumn($alias . '.order_id');
            $criteria->addSelectColumn($alias . '.top_sku');
            $criteria->addSelectColumn($alias . '.amount');
            $criteria->addSelectColumn($alias . '.unitPrice');
            $criteria->addSelectColumn($alias . '.slug');
        }
    }

    /**
     * Returns the TableMap related to this object.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getServiceContainer()->getDatabaseMap(OnePieceOrderTableMap::DATABASE_NAME)->getTable(OnePieceOrderTableMap::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this tableMap class.
     */
    public static function buildTableMap()
    {
        $dbMap = Propel::getServiceContainer()->getDatabaseMap(OnePieceOrderTableMap::DATABASE_NAME);
        if (!$dbMap->hasTable(OnePieceOrderTableMap::TABLE_NAME)) {
            $dbMap->addTableObject(new OnePieceOrderTableMap());
        }
    }

    /**
     * Performs a DELETE on the database, given a OnePieceOrder or Criteria object OR a primary key value.
     *
     * @param mixed               $values Criteria or OnePieceOrder object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param  ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, ConnectionInterface $con = null)
     {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(OnePieceOrderTableMap::DATABASE_NAME);
        }

        if ($values instanceof Criteria) {
            // rename for clarity
            $criteria = $values;
        } elseif ($values instanceof \BaobabModels\OnePieceOrder) { // it's a model object
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(OnePieceOrderTableMap::DATABASE_NAME);
            $criteria->add(OnePieceOrderTableMap::COL_ID, (array) $values, Criteria::IN);
        }

        $query = OnePieceOrderQuery::create()->mergeWith($criteria);

        if ($values instanceof Criteria) {
            OnePieceOrderTableMap::clearInstancePool();
        } elseif (!is_object($values)) { // it's a primary key, or an array of pks
            foreach ((array) $values as $singleval) {
                OnePieceOrderTableMap::removeInstanceFromPool($singleval);
            }
        }

        return $query->delete($con);
    }

    /**
     * Deletes all rows from the one_piece_order table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public static function doDeleteAll(ConnectionInterface $con = null)
    {
        return OnePieceOrderQuery::create()->doDeleteAll($con);
    }

    /**
     * Performs an INSERT on the database, given a OnePieceOrder or Criteria object.
     *
     * @param mixed               $criteria Criteria or OnePieceOrder object containing data that is used to create the INSERT statement.
     * @param ConnectionInterface $con the ConnectionInterface connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function doInsert($criteria, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(OnePieceOrderTableMap::DATABASE_NAME);
        }

        if ($criteria instanceof Criteria) {
            $criteria = clone $criteria; // rename for clarity
        } else {
            $criteria = $criteria->buildCriteria(); // build Criteria from OnePieceOrder object
        }

        if ($criteria->containsKey(OnePieceOrderTableMap::COL_ID) && $criteria->keyContainsValue(OnePieceOrderTableMap::COL_ID) ) {
            throw new PropelException('Cannot insert a value for auto-increment primary key ('.OnePieceOrderTableMap::COL_ID.')');
        }


        // Set the correct dbName
        $query = OnePieceOrderQuery::create()->mergeWith($criteria);

        // use transaction because $criteria could contain info
        // for more than one table (I guess, conceivably)
        return $con->transaction(function () use ($con, $query) {
            return $query->doInsert($con);
        });
    }

} // OnePieceOrderTableMap
// This is the static code needed to register the TableMap for this table with the main Propel class.
//
OnePieceOrderTableMap::buildTableMap();
