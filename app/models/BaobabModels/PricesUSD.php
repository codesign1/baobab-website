<?php

namespace BaobabModels;

use BaobabModels\Base\PricesUSD as BasePricesUSD;

/**
 * Skeleton subclass for representing a row from the 'prices_usd' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class PricesUSD extends BasePricesUSD
{

}
