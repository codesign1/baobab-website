<?php
/**
 * Created by PhpStorm.
 * User: jsrolon
 * Date: 28-07-2016
 * Time: 12:15 PM
 */

namespace Baobab\app\concerns;


use BaobabModels\BikiniBottomQuery;
use BaobabModels\BikiniTop;
use BaobabModels\BikiniTopQuery;
use BaobabModels\OnePieceStockableQuery;
use BaobabModels\Product;
use BaobabModels\ProductQuery;
use Velocity\Db\Db;
use Velocity\Ecommerce\Shop;

class BaobabShop extends Shop {

    public function check_if_in_cart($ref) {
        $user_id = $this->user_id;
        return $this->db->query("SELECT * FROM v_cart WHERE user_id = '$user_id' AND ref = '$ref' ORDER BY id DESC")->results();
    }

    public function add_to_cart($ref, $cant = 1) {
        // create the array and set each one of the properties as its own variable
        // we are using the reference to contain the data relating to the selected product in the cart
        // depending on the product type, the reference has different sections
        // it always contains the product slug first. then it contains the product type. then it contains the
        // skus, starting from the top, and in the case of bikinis, ending with whether it has a pushup cup or not
        $ref_data = explode('::', $ref);
        if (count($ref_data) === 3) {
            list($slug, $type, $sku_top) = $ref_data;
        } else {
            list($slug, $type, $sku_top, $sku_bottom, $pushup) = $ref_data;
        }

        $product = ProductQuery::create()->findOneBySlug($slug);

        if ($this->logged_in && !isset($_SESSION['cart'])) {

            $exists = $this->check_if_in_cart($ref);
            if (count($exists)) {

                $update_id = $exists[0]->id;
                $cant_nueva = $exists[0]->cantidad += $cant;

                $this->db->update('v_cart', $update_id, array(
                    'cantidad' => $cant_nueva
                ));

            } else {

                $this->db->insert('v_cart', array(
                    'added' => Timedate::get_mysql_format(),
                    'user_id' => $this->user_id,
                    'sku' => '',
                    'nombre' => $product->getName(),
                    'precio' => $product->getPrice($pushup),
                    'cantidad' => $cant,
                    'ref' => $ref,
                    'color' => '',
                    'push_up' => $pushup,
                    'talla_top' => $sku_top,
                    'talla_bottom' => $sku_bottom
                ));

            }

        } else {
            if (!isset($_SESSION['cart'])) {
                $_SESSION['cart'][$ref] = $cant;
            } else {
                if (isset($_SESSION['cart'][$ref])) {
                    if (($cant < 0 && $_SESSION['cart'][$ref] > 1) // don't subtract if amount 1 or less
                        || $cant >= 0
                    ) {
                        $_SESSION['cart'][$ref] += $cant;
                    }
                } else {
                    $_SESSION['cart'][$ref] = $cant;
                }
            }
        }
//        $this->decrease_availability($ref, $cant);
    }

    public function get_cart() {
        if ($this->logged_in && !isset($_SESSION['cart'])) {
            $cart_array = array();
            $total = 0;
            $cart_user = $this->get_cart_user();
            if (count($cart_user)) {
                foreach ($cart_user as $cart_u) {
                    if (stripos($cart_u->sku, 'combo') !== false) {
                        $prod = $this->get_combo($cart_u->sku, 'sku');
                    } else {
                        $prod = $this->get_product($cart_u->sku, 'sku');
                    }
                    $total += $cart_u->precio * $cart_u->cantidad;

                    $cart_array[] = array(
                        'nombre' => $prod->nombre,
                        'sku' => $prod->sku,
                        'precio' => $prod->precio,
                        'cantidad' => $cart_u->cantidad,
                        'ref' => $cart_u->ref
                    );
                }
                return array($cart_array, $total);
            } else {
                return array(array(), 0);
            }
        } else {
            if (!isset($_SESSION['cart'])) {

                return array(array(), 0);

            } else {
                if (count($_SESSION['cart']) > 0) {

                    $cart_array = array();
                    $total = 0;

                    foreach ($_SESSION['cart'] as $ref => $amount) {
                        list($slug, $type, $sku_top, $sku_bottom, $pushup) = explode('::', $ref);
                        $prod = ProductQuery::create()->findOneBySlug($slug);

                        // to find the top and bottom sizes, we need to know what the type of the
                        // product is, so this will be messy code
                        if ($type === 'Bikini') {
                            $bikini_top = BikiniTopQuery::create()->findOneBySku($sku_top);
                            $top_size = $bikini_top->getSize();

                            $bikini_bottom = BikiniBottomQuery::create()->findOneBySku($sku_bottom);
                            $bottom_size = $bikini_bottom->getSize();
                        } else {
                            $model = OnePieceStockableQuery::create()->findOneBySku($sku_top);
                            $top_size = $model->getSize();
                        }

                        $total += $prod->getPrice($pushup) * $amount;
                        $cart_array[] = array(
                            'nombre' => $prod->getName(),
                            'sku' => '',
                            'precio' => $prod->getPrice($pushup),
                            'cantidad' => $amount,
                            'ref' => $ref,
                            'pushup' => $pushup === 'true' ? 'si' : 'no',
                            'talla_top' => $top_size,
                            'talla_bottom' => $bottom_size,
                            'color' => '',
                            'category' => $type
                        );
                    }

                    return array($cart_array, $total);

                } else {
                    return array(array(), 0);
                }
            }
        }
    }

    public function search_products($query) {
        return ProductQuery::create()->findByName($query);
    }

    public function remove_from_cart($ref) {
        if ($this->logged_in) {
            $user_id = $this->user_id;
            $this->db->query("DELETE FROM v_cart WHERE user_id = '$user_id' AND ref = '$ref'");
            // TODO decrease the availability
        } else {
            if (isset($_SESSION['cart'])) {
                $cant = $_SESSION['cart'][$ref];
                $this->decrease_availability($ref, -$cant);
                unset($_SESSION['cart'][$ref]);
            }
        }
    }

    public function decrease_availability($ref, $amount) {
        $this->db = Db::getInstance();
        $ref_data = explode('::', $ref);
        if (count($ref_data) === 4) {
            list($sku, $pushup, $size, $color) = $ref_data;
            $this->db->query("UPDATE v_modelos SET disponibilidad = LEAST(stock, disponibilidad - $amount) where sku = '$sku' and "
                . "size='$size' and "
                . "color = '$color' and "
                . "pushup = 0");
        } else {
            list($sku, $pushup, $top_size, $bottom_size, $color) = $ref_data;
            // decrease top
            $str = "UPDATE v_modelos SET disponibilidad = LEAST(stock, disponibilidad - $amount) where sku = '$sku' and "
                . "size='$top_size' and "
                . "color = '$color' and "
                . "pushup = $pushup and "
                . "pieza = 'top'";
            $this->db->query($str);

            // decrease bottom
            $this->db->query("UPDATE v_modelos SET disponibilidad = LEAST(stock, disponibilidad - $amount) where sku = '$sku' and "
                . "size='$bottom_size' and "
                . "color = '$color' and "
                . "pushup = 0 and "
                . "pieza = 'bottom'");
        }
    }

    public function empty_cart() {
        if ($this->logged_in) {
            $user_id = $this->user_id;

            $cart_records = $this->db->query("SELECT ref, cantidad from v_cart where user_id = $user_id")->results();
            foreach ($cart_records as $record) {
                $this->decrease_availability($record->ref, -$record->cantidad); // if the cart is emptied, availability rises
            }

            $this->db->delete('v_cart', array('user_id', '=', $user_id));
        } else {
            if (isset($_SESSION['cart'])) {
                // change all the required availabilities
                foreach ($_SESSION['cart'] as $ref => $amount) {
                    $this->decrease_availability($ref, $amount);
                }
                unset($_SESSION['cart']);
            }
        }
    }
}
