<?php
/**
 * Created by PhpStorm.
 * User: jsrolon
 * Date: 25-11-2016
 * Time: 4:50 PM
 */

namespace Baobab\app\concerns;


use Baobab\app\concerns\BaobabShop;
use BaobabModels\ProductQuery;
use Velocity\Ecommerce\CartController;

class BaobabController extends CartController {
    private $baobab_shop;

    public function __construct($name, $description, $keywords, $author) {
        parent::__construct($name, $description, $keywords, $author);

        $this->baobab_shop = new BaobabShop();
        list($this->cart, $this->cart_total) = $this->baobab_shop->get_cart();
        foreach ($this->cart as &$cart_item) {
            $ref_exploded = explode('::', $cart_item['ref']);
            $cart_item['image'] = ProductQuery::create()->findOneBySlug($ref_exploded[0])->getProductImagess()->getFirst()->getUrl();
        }
    }
}