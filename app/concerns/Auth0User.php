<?php
/**
 * Created by PhpStorm.
 * User: jsrolon
 * Date: 20-11-2016
 * Time: 9:04 PM
 */

namespace Baobab\app\concerns;


class Auth0User {
    private static $instance;

    public static function getInstance() {
        if(!static::$instance) {
            static::$instance = new Auth0User();
        }
        return static::$instance;
    }

    protected function __construct() {}
}
