<?php
/**
 * Created by PhpStorm.
 * User: jsrolon
 * Date: 11-07-2016
 * Time: 2:20 PM
 */

$image_selection = rand(1, 2);
$selected_image = '/assets/img/404_img_' . $image_selection . '.svg';
if ($image_selection === 1) {
    $error_text = 'Parece que algo anda mal, regresa a la p&aacute;gina de incio para arreglarlo';
} else {
    $error_text = '¡Esta combinaci&oacute;n de bikinis no est&aacute; bien! Regresa a la p&aacute;gina de incio para arreglarlo';
}
?>

<head>
    <title>Bodegario | Error 404 - ¡Página no encontrada!</title>
    <link rel="stylesheet" type="text/css" href="/public/css/style.min.css"/>
    <link rel="stylesheet" type="text/css" href="/assets/fonts/icomoon/style.css"/>
    <script type="text/javascript" src="/public/js/script.min.js"></script>
</head>
<body>
<div id="error-page">
    <div class="grid-12-m last">
        <div class="cont30">
            <h2 class="error-page-title <?php if ($image_selection === 2) {
                echo 'error-page-color-2';
            } ?>">Oops!</h2>
            <h1 class="error-page-subtitle <?php if ($image_selection === 1) {
                echo 'error-page-color-1';
            } ?>">Página no encontrada</h1>
            <p class="error-page-text text-center <?php if ($image_selection === 1) {
                echo 'error-page-color-1';
            } ?>"><a href="/">
                    <?php echo $error_text ?>
                </a>
            </p>
        </div>
    </div>
    <div class="grid-12-m last text-center">
        <a href="/"><img class="<?php if ($image_selection === 1) {
                echo 'error-img-big';
            } else {
                echo 'error-img-small';
            } ?>" src="<?php echo $selected_image ?>" alt="<?php echo $error_text ?>">
        </a>
    </div>
</div>
</body>
