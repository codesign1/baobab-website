<?php

namespace Baobab;

## get all the composer dependencies
include 'vendor/autoload.php';

use BaobabModels\Base\ProductQuery;
use BaobabModels\Bikini;
use BaobabModels\BikiniBottom;
use BaobabModels\BikiniQuery;
use BaobabModels\BikiniTop;
use BaobabModels\OnePiece;
use BaobabModels\OnePieceOrderQuery;
use BaobabModels\OnePieceQuery;
use BaobabModels\OnePieceStockable;
use BaobabModels\PricesCOP;
use BaobabModels\PricesUSD;
use GeoIp2\Database\Reader;
use Mailgun\Mailgun;
use Propel\Runtime\Collection\Collection;
use Velocity\Db\Db;
use Velocity\Velocity;
use Auth0\SDK\Auth0;

// enable error reporting quickly
// ini_set('display_errors',1);
// error_reporting(E_ALL);

//create_test_data();

## set up the web framework, giving it the name of the
## application

if(!$_COOKIE['currency']) {
    setcookie('currency', 'USD', 0, '/'); // set the default cookie
}

$velocity = Velocity::getInstance('Baobab');
$velocity->add_twig_global('_BAOBAB_CURRENCY', $_COOKIE['currency']);
$velocity->execute();

function create_test_data() {
//    $one_piece = OnePieceQuery::create()->findOneBySlug('akira');
//    $one_piece->setSlug('sarabi');
//    $one_piece->setName('Sarabi');
//    $one_piece->setDescription('One piece cutout swimsuit<br>Model wears size M');

//    $skus = array('C01', 'C03');
//    $sizes = array('S', 'L');
//    $amounts = array(12, 12, 6);
//    for($i = 0; $i < 3; $i++) {
//        $one_piece_stockable = new OnePieceStockable();
//        $one_piece_stockable->setSku($skus[$i]);
//        $one_piece_stockable->setSize($sizes[$i]);
//        $one_piece_stockable->setStockAmount($amounts[$i]);
//        $one_piece->addOnePieceStockable($one_piece_stockable);
//    }

//    $prices_cop_one_piece = new PricesCOP();
//    $prices_cop_one_piece->setPrice(150000);
//
//    $prices_usd_one_piece = new PricesUSD();
//    $prices_usd_one_piece->setPrice(69);
//
//    $one_piece->addPricesCOP($prices_cop_one_piece);
//    $one_piece->addPricesUSD($prices_usd_one_piece);

//    $one_piece->save();

//    $bikini = BikiniQuery::create()->findOneBySlug('green-mawi');
//    $bikini->setSlug('grey-dakari');
//    $bikini->setName('Grey Dakari');
//    $bikini->setDescription("One shoulder top<br>ligth removable padding<br>model wears size M");

//    $skus = array('A02T', 'A03T');
//    $sizes = array('M', 'L');
//    $amounts = array(21, 11);
//    for($i = 0; $i < 2; $i++) {
//        $bikini_top = new BikiniTop();
//        $bikini_top->setSku($skus[$i]);
//        $bikini_top->setStockAmount($amounts[$i]);
//        $bikini_top->setSize($sizes[$i]);
//        $bikini_top->setPushupEnabled(true);
//        $bikini->addBikiniTop($bikini_top);
//    }
//
//    $skus = array('A02P', 'A03P');
//    $sizes = array('M', 'L');
//    $amounts = array(21, 11);
//    for($i = 0; $i < 2; $i++) {
//        $bikini_bottom = new BikiniBottom();
//        $bikini_bottom->setSku($skus[$i]);
//        $bikini_bottom->setStockAmount($amounts[$i]);
//        $bikini_bottom->setSize($sizes[$i]);
//        $bikini->addBikiniBottom($bikini_bottom);
//    }
//
//    $bikini->save();
}

