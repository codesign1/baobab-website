<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'baobab');

/** MySQL database username */
define('DB_USER', 'baobablog');

/** MySQL database password */
define('DB_PASSWORD', '1Y5sMBAJC2YfCZBo');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'pd1(Aj]AvB=|(QKHLN?r9.Q8;[mVuDv@E#3Z!&8lM&w?|h/`[B<3Yi.KZqQD@ux}');
define('SECURE_AUTH_KEY',  'RVK} 6X-5CA!YH<,H<$aOuJeolpBQ/?tE_[[.gzt47i#hU)hX*^S}WBD^$,kua/w');
define('LOGGED_IN_KEY',    'qpef;B7qHZ^R^%cr6sS8)[$HJu1>+hV|0ENT-&jv8hcEasw |V;`wz`5Br(7srr_');
define('NONCE_KEY',        'O{8XaCZSt<OjVSUQDP.I]L`r FAYZ}_w:u<wacCS_Pr$B! nEe~C~,EAuZ|&oIuA');
define('AUTH_SALT',        'w@P!{c2s~tkYM4#6,ccLzI#+W03*TJ7ogDWXkg-g,7G,W;@@N-`|?qJRbsMDY_`~');
define('SECURE_AUTH_SALT', 'VHDx4Cg;}4o|31#1-N-JL!f+.)m(&//|[[~*8<hKZqXkeVAUFv[1N#5xadz-|QcH');
define('LOGGED_IN_SALT',   '%0d+F~POLAO47{(,9M1/>!HubkGq mPD]33{=Rbe2,2L:|,raUIpz9P9.>DSK#_{');
define('NONCE_SALT',       'pEy(OaWICz2;+H{YO(4Z[R:h3 Mbv@ob@Ol@M?WhKR4tz%TTXYtQVO9E0edCe+U5');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
