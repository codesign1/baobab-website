-- phpMyAdmin SQL Dump
-- version 4.4.10
-- http://www.phpmyadmin.net
--
-- Host: localhost:3306
-- Generation Time: Nov 30, 2016 at 11:49 PM
-- Server version: 5.5.42
-- PHP Version: 5.6.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Database: `baobab`
--

-- --------------------------------------------------------

--
-- Table structure for table `baobab_order`
--

CREATE TABLE `baobab_order` (
  `id` varchar(255) NOT NULL,
  `user_id` varchar(255) NOT NULL,
  `order_status` tinyint(4) NOT NULL,
  `total_price` decimal(10,0) NOT NULL,
  `currency` varchar(255) NOT NULL,
  `address` varchar(255) DEFAULT NULL,
  `phone` varchar(255) DEFAULT NULL,
  `city` varchar(255) DEFAULT NULL,
  `neighborhood` varchar(255) DEFAULT NULL,
  `postal_code` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `baobab_order`
--

INSERT INTO `baobab_order` (`id`, `user_id`, `order_status`, `total_price`, `currency`, `address`, `phone`, `city`, `neighborhood`, `postal_code`, `created_at`, `updated_at`) VALUES
  ('2-1480109288', 'a@a.com', 1, '152000', 'COP', NULL, NULL, NULL, NULL, NULL, '2016-11-25 16:28:08', '2016-11-25 16:29:01'),
  ('2-1480109518', 'a@a.com', 0, '152000', 'COP', NULL, NULL, NULL, NULL, NULL, '2016-11-25 16:31:59', '2016-11-25 16:31:59'),
  ('2-1480109574', 'a@a.com', 0, '152000', 'COP', NULL, NULL, NULL, NULL, NULL, '2016-11-25 16:32:54', '2016-11-25 16:32:54'),
  ('2-1480109601', 'a@a.com', 0, '152000', 'COP', NULL, NULL, NULL, NULL, NULL, '2016-11-25 16:33:21', '2016-11-25 16:33:21'),
  ('2-1480109608', 'a@a.com', 0, '152000', 'COP', NULL, NULL, NULL, NULL, NULL, '2016-11-25 16:33:28', '2016-11-25 16:33:28'),
  ('2-1480109640', 'a@a.com', 0, '152000', 'COP', NULL, NULL, NULL, NULL, NULL, '2016-11-25 16:34:00', '2016-11-25 16:34:00'),
  ('2-1480109672', 'a@a.com', 0, '152000', 'COP', NULL, NULL, NULL, NULL, NULL, '2016-11-25 16:34:32', '2016-11-25 16:34:32'),
  ('2-1480109825', 'a@a.com', 0, '152000', 'COP', NULL, NULL, NULL, NULL, NULL, '2016-11-25 16:37:05', '2016-11-25 16:37:05'),
  ('2-1480109841', 'a@a.com', 0, '152000', 'COP', NULL, NULL, NULL, NULL, NULL, '2016-11-25 16:37:21', '2016-11-25 16:37:21'),
  ('2-1480109929', 'a@a.com', 0, '152000', 'COP', NULL, NULL, NULL, NULL, NULL, '2016-11-25 16:38:49', '2016-11-25 16:38:49'),
  ('2-1480110029', 'a@a.com', 0, '152000', 'COP', NULL, NULL, NULL, NULL, NULL, '2016-11-25 16:40:29', '2016-11-25 16:40:29'),
  ('2-1480110047', 'a@a.com', 0, '152000', 'COP', NULL, NULL, NULL, NULL, NULL, '2016-11-25 16:40:47', '2016-11-25 16:40:47'),
  ('2-1480110092', 'a@a.com', 1, '152000', 'COP', NULL, NULL, NULL, NULL, NULL, '2016-11-25 16:41:32', '2016-11-25 16:42:02'),
  ('2-1480366045', 'a@a.com', 0, '71', 'USD', NULL, NULL, NULL, NULL, NULL, '2016-11-28 15:47:25', '2016-11-28 15:47:25'),
  ('2-1480366265', 'a@a.com', 0, '71', 'USD', NULL, NULL, NULL, NULL, NULL, '2016-11-28 15:51:05', '2016-11-28 15:51:05'),
  ('2-1480366287', 'a@a.com', 0, '71', 'USD', NULL, NULL, NULL, NULL, NULL, '2016-11-28 15:51:27', '2016-11-28 15:51:27'),
  ('2-1480366394', 'a@a.com', 0, '71', 'USD', NULL, NULL, NULL, NULL, NULL, '2016-11-28 15:53:14', '2016-11-28 15:53:14'),
  ('2-1480366434', 'a@a.com', 0, '71', 'USD', NULL, NULL, NULL, NULL, NULL, '2016-11-28 15:53:55', '2016-11-28 15:53:55'),
  ('2-1480366486', 'a@a.com', 0, '71', 'USD', NULL, NULL, NULL, NULL, NULL, '2016-11-28 15:54:46', '2016-11-28 15:54:46'),
  ('2-1480367210', 'a@a.com', 0, '71', 'USD', NULL, NULL, NULL, NULL, NULL, '2016-11-28 16:06:50', '2016-11-28 16:06:50'),
  ('2-1480367283', 'a@a.com', 0, '71', 'USD', NULL, NULL, NULL, NULL, NULL, '2016-11-28 16:08:03', '2016-11-28 16:08:03'),
  ('2-1480367399', 'a@a.com', 0, '71', 'USD', NULL, NULL, NULL, NULL, NULL, '2016-11-28 16:09:59', '2016-11-28 16:09:59'),
  ('2-1480367448', 'a@a.com', 0, '71', 'USD', NULL, NULL, NULL, NULL, NULL, '2016-11-28 16:10:49', '2016-11-28 16:10:49'),
  ('2-1480367531', 'a@a.com', 0, '71', 'USD', NULL, NULL, NULL, NULL, NULL, '2016-11-28 16:12:11', '2016-11-28 16:12:11'),
  ('2-1480367628', 'a@a.com', 0, '71', 'USD', NULL, NULL, NULL, NULL, NULL, '2016-11-28 16:13:48', '2016-11-28 16:13:48'),
  ('2-1480367663', 'a@a.com', 0, '71', 'USD', NULL, NULL, NULL, NULL, NULL, '2016-11-28 16:14:23', '2016-11-28 16:14:23'),
  ('2-1480367723', 'a@a.com', 0, '71', 'USD', NULL, NULL, NULL, NULL, NULL, '2016-11-28 16:15:23', '2016-11-28 16:15:23'),
  ('2-1480367853', 'a@a.com', 0, '71', 'USD', NULL, NULL, NULL, NULL, NULL, '2016-11-28 16:17:33', '2016-11-28 16:17:33'),
  ('2-1480367934', 'a@a.com', 0, '71', 'USD', NULL, NULL, NULL, NULL, NULL, '2016-11-28 16:18:54', '2016-11-28 16:18:54'),
  ('2-1480368037', 'a@a.com', 0, '71', 'USD', NULL, NULL, NULL, NULL, NULL, '2016-11-28 16:20:37', '2016-11-28 16:20:37'),
  ('2-1480368441', 'a@a.com', 1, '71', 'USD', NULL, NULL, NULL, NULL, NULL, '2016-11-28 16:27:21', '2016-11-28 16:27:21'),
  ('2-1480369229', 'a@a.com', 0, '71', 'USD', NULL, NULL, NULL, NULL, NULL, '2016-11-28 16:40:29', '2016-11-28 16:40:29'),
  ('2-1480369268', 'a@a.com', 0, '71', 'USD', NULL, NULL, NULL, NULL, NULL, '2016-11-28 16:41:08', '2016-11-28 16:41:08'),
  ('2-1480369291', 'a@a.com', 0, '71', 'USD', NULL, NULL, NULL, NULL, NULL, '2016-11-28 16:41:31', '2016-11-28 16:41:31'),
  ('2-1480369317', 'a@a.com', 0, '71', 'USD', NULL, NULL, NULL, NULL, NULL, '2016-11-28 16:41:57', '2016-11-28 16:41:57'),
  ('2-1480369397', 'a@a.com', 0, '71', 'USD', NULL, NULL, NULL, NULL, NULL, '2016-11-28 16:43:17', '2016-11-28 16:43:17'),
  ('2-1480369456', 'a@a.com', 0, '71', 'USD', NULL, NULL, NULL, NULL, NULL, '2016-11-28 16:44:16', '2016-11-28 16:44:16'),
  ('2-1480369670', 'a@a.com', 0, '71', 'USD', NULL, NULL, NULL, NULL, NULL, '2016-11-28 16:47:50', '2016-11-28 16:47:50'),
  ('2-1480369687', 'a@a.com', 0, '71', 'USD', NULL, NULL, NULL, NULL, NULL, '2016-11-28 16:48:07', '2016-11-28 16:48:07'),
  ('2-1480369784', 'a@a.com', 1, '71', 'USD', NULL, NULL, NULL, NULL, NULL, '2016-11-28 16:49:44', '2016-11-28 17:10:35'),
  ('2-1480371088', 'a@a.com', 1, '205', 'USD', NULL, NULL, NULL, NULL, NULL, '2016-11-28 17:11:28', '2016-11-28 17:12:48'),
  ('2-1480371182', 'a@a.com', 1, '205', 'USD', NULL, NULL, NULL, NULL, NULL, '2016-11-28 17:13:02', '2016-11-28 17:13:02'),
  ('2-1480371308', 'a@a.com', 1, '205', 'USD', 'Cll 2', '3158370813', 'Bogota', NULL, NULL, '2016-11-28 17:15:09', '2016-11-28 17:16:30'),
  ('2-1480371401', 'a@a.com', 0, '205', 'USD', 'Cll 2', '3158370813', 'Bogota', NULL, NULL, '2016-11-28 17:16:41', '2016-11-28 17:17:14'),
  ('2-1480372039', 'a@a.com', 0, '205', 'USD', 'Cll 2', '3158370813', 'Bogota', NULL, NULL, '2016-11-28 17:27:19', '2016-11-28 17:27:34');

-- --------------------------------------------------------

--
-- Table structure for table `bikini`
--

CREATE TABLE `bikini` (
  `slug` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `show_order` int(11) DEFAULT NULL,
  `description` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bikini`
--

INSERT INTO `bikini` (`slug`, `name`, `show_order`, `description`) VALUES
  ('african-eyes', 'African Eyes', 7, 'handmade crochet high neck top<br>adjustable straps<br>ruched wide side bottom<br>model wears size M'),
  ('green-mawi', 'Green Mawï', 1, '-Halter top\n-Adjustable straps\n-Internal elastic band for extra support'),
  ('grey-dakari', 'Grey Dakari', 4, 'One shoulder top<br>ligth removable padding<br>model wears size M'),
  ('ivory-dakari', 'Ivory Dakari', 3, 'One shoulder top<br>ligth removable padding<br>model wears size M'),
  ('nude-mawi', 'Nude Mawï', 2, 'halter top<br>adjustable straps<br>internal elastic at base for extra support<br>seamless sewing<br>light removable padding<br>model wears size M'),
  ('trival-nandu', 'Trival Nandu', 5, 'High neck top<br>Adjustable center strap at back<br>model wears size M'),
  ('zulu-crochet', 'Zulu Crochet', 6, 'handmade crochet bikini<br>high neck top<br>adjustable straps<br>adjustable side ties<br>String low rise Bottom with adjustable side ties<br>model wears size M');

-- --------------------------------------------------------

--
-- Table structure for table `bikini_bottom`
--

CREATE TABLE `bikini_bottom` (
  `size` char(10) NOT NULL,
  `bikini_slug` varchar(255) NOT NULL,
  `sku` varchar(255) NOT NULL,
  `stock_amount` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bikini_bottom`
--

INSERT INTO `bikini_bottom` (`size`, `bikini_slug`, `sku`, `stock_amount`) VALUES
  ('S', 'green-mawi', 'A01P', 0),
  ('M', 'green-mawi', 'A02P', 0),
  ('L', 'green-mawi', 'A03P', 0),
  ('S', 'nude-mawi', 'B01P', 0),
  ('M', 'nude-mawi', 'B02P', 0),
  ('L', 'nude-mawi', 'B03P', 1),
  ('S', 'trival-nandu', 'E01P', 0),
  ('M', 'trival-nandu', 'E02P', 0),
  ('L', 'trival-nandu', 'E03P', 0),
  ('S', 'african-eyes', 'G01P', 0),
  ('M', 'african-eyes', 'G02P', 0),
  ('L', 'african-eyes', 'G03P', 0),
  ('S', 'zulu-crochet', 'H01P', 0),
  ('M', 'zulu-crochet', 'H02P', 0),
  ('L', 'zulu-crochet', 'H03P', 0),
  ('S', 'ivory-dakari', 'I01P', 0),
  ('M', 'ivory-dakari', 'I02P', 0),
  ('L', 'ivory-dakari', 'I03P', 0),
  ('S', 'grey-dakari', 'J01P', 0),
  ('M', 'grey-dakari', 'J02P', 0),
  ('L', 'grey-dakari', 'J03P', 0);

-- --------------------------------------------------------

--
-- Table structure for table `bikini_order`
--

CREATE TABLE `bikini_order` (
  `id` int(255) NOT NULL,
  `order_id` varchar(255) NOT NULL,
  `slug` varchar(255) NOT NULL,
  `top_sku` varchar(255) NOT NULL,
  `bottom_sku` varchar(255) NOT NULL,
  `pushup` tinyint(1) NOT NULL,
  `amount` decimal(10,0) NOT NULL,
  `unitPrice` decimal(10,0) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=51 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bikini_order`
--

INSERT INTO `bikini_order` (`id`, `order_id`, `slug`, `top_sku`, `bottom_sku`, `pushup`, `amount`, `unitPrice`) VALUES
  (1, '2-1480109288', 'green-mawi', 'A02T', 'A02P', 1, '1', '150000'),
  (2, '2-1480109518', 'green-mawi', 'A02T', 'A02P', 1, '1', '150000'),
  (3, '2-1480109574', 'green-mawi', 'A02T', 'A02P', 1, '1', '150000'),
  (4, '2-1480109601', 'green-mawi', 'A02T', 'A02P', 1, '1', '150000'),
  (5, '2-1480109608', 'green-mawi', 'A02T', 'A02P', 1, '1', '150000'),
  (6, '2-1480109640', 'green-mawi', 'A02T', 'A02P', 1, '1', '150000'),
  (7, '2-1480109672', 'green-mawi', 'A02T', 'A02P', 1, '1', '150000'),
  (8, '2-1480109825', 'green-mawi', 'A02T', 'A02P', 1, '1', '150000'),
  (9, '2-1480109841', 'green-mawi', 'A02T', 'A02P', 1, '1', '150000'),
  (10, '2-1480109929', 'green-mawi', 'A02T', 'A02P', 1, '1', '150000'),
  (11, '2-1480110029', 'green-mawi', 'A02T', 'A02P', 1, '1', '150000'),
  (12, '2-1480110047', 'green-mawi', 'A02T', 'A02P', 1, '1', '150000'),
  (13, '2-1480110092', 'green-mawi', 'A02T', 'A02P', 1, '1', '150000'),
  (14, '2-1480366045', 'green-mawi', 'A02T', 'A02P', 1, '1', '69'),
  (15, '2-1480366265', 'green-mawi', 'A02T', 'A02P', 1, '1', '69'),
  (16, '2-1480366287', 'green-mawi', 'A02T', 'A02P', 1, '1', '69'),
  (17, '2-1480366394', 'green-mawi', 'A02T', 'A02P', 1, '1', '69'),
  (18, '2-1480366434', 'green-mawi', 'A02T', 'A02P', 1, '1', '69'),
  (19, '2-1480366486', 'green-mawi', 'A02T', 'A02P', 1, '1', '69'),
  (20, '2-1480367210', 'green-mawi', 'A02T', 'A02P', 1, '1', '69'),
  (21, '2-1480367283', 'green-mawi', 'A02T', 'A02P', 1, '1', '69'),
  (22, '2-1480367399', 'green-mawi', 'A02T', 'A02P', 1, '1', '69'),
  (23, '2-1480367448', 'green-mawi', 'A02T', 'A02P', 1, '1', '69'),
  (24, '2-1480367531', 'green-mawi', 'A02T', 'A02P', 1, '1', '69'),
  (25, '2-1480367628', 'green-mawi', 'A02T', 'A02P', 1, '1', '69'),
  (26, '2-1480367663', 'green-mawi', 'A02T', 'A02P', 1, '1', '69'),
  (27, '2-1480367723', 'green-mawi', 'A02T', 'A02P', 1, '1', '69'),
  (28, '2-1480367853', 'green-mawi', 'A02T', 'A02P', 1, '1', '69'),
  (29, '2-1480367934', 'green-mawi', 'A02T', 'A02P', 1, '1', '69'),
  (30, '2-1480368037', 'green-mawi', 'A02T', 'A02P', 1, '1', '69'),
  (31, '2-1480368441', 'green-mawi', 'A02T', 'A02P', 1, '1', '69'),
  (32, '2-1480369229', 'green-mawi', 'A02T', 'A02P', 1, '1', '69'),
  (33, '2-1480369268', 'green-mawi', 'A02T', 'A02P', 1, '1', '69'),
  (34, '2-1480369291', 'green-mawi', 'A02T', 'A02P', 1, '1', '69'),
  (35, '2-1480369317', 'green-mawi', 'A02T', 'A02P', 1, '1', '69'),
  (36, '2-1480369397', 'green-mawi', 'A02T', 'A02P', 1, '1', '69'),
  (37, '2-1480369456', 'green-mawi', 'A02T', 'A02P', 1, '1', '69'),
  (38, '2-1480369670', 'green-mawi', 'A02T', 'A02P', 1, '1', '69'),
  (39, '2-1480369687', 'green-mawi', 'A02T', 'A02P', 1, '1', '69'),
  (40, '2-1480369784', 'green-mawi', 'A02T', 'A02P', 1, '1', '69'),
  (41, '2-1480371088', 'green-mawi', 'A02T', 'A02P', 1, '1', '69'),
  (42, '2-1480371088', 'nude-mawi', 'B03T', 'B03P', 1, '1', '66'),
  (43, '2-1480371182', 'green-mawi', 'A02T', 'A02P', 1, '1', '69'),
  (44, '2-1480371182', 'nude-mawi', 'B03T', 'B03P', 1, '1', '66'),
  (45, '2-1480371308', 'green-mawi', 'A02T', 'A02P', 1, '1', '69'),
  (46, '2-1480371308', 'nude-mawi', 'B03T', 'B03P', 1, '1', '66'),
  (47, '2-1480371401', 'green-mawi', 'A02T', 'A02P', 1, '1', '69'),
  (48, '2-1480371401', 'nude-mawi', 'B03T', 'B03P', 1, '1', '66'),
  (49, '2-1480372039', 'green-mawi', 'A02T', 'A02P', 1, '1', '69'),
  (50, '2-1480372039', 'nude-mawi', 'B03T', 'B03P', 1, '1', '66');

-- --------------------------------------------------------

--
-- Table structure for table `bikini_top`
--

CREATE TABLE `bikini_top` (
  `size` char(10) NOT NULL,
  `pushup_enabled` tinyint(1) NOT NULL,
  `bikini_slug` varchar(255) NOT NULL,
  `sku` varchar(255) NOT NULL,
  `stock_amount` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bikini_top`
--

INSERT INTO `bikini_top` (`size`, `pushup_enabled`, `bikini_slug`, `sku`, `stock_amount`) VALUES
  ('S', 1, 'green-mawi', 'A01T', 0),
  ('M', 1, 'green-mawi', 'A02T', 0),
  ('L', 1, 'green-mawi', 'A03T', 0),
  ('S', 1, 'nude-mawi', 'B01T', 1),
  ('M', 1, 'nude-mawi', 'B02T', 0),
  ('L', 1, 'nude-mawi', 'B03T', 0),
  ('S', 1, 'trival-nandu', 'E01T', 0),
  ('M', 1, 'trival-nandu', 'E02T', 0),
  ('L', 1, 'trival-nandu', 'E03T', 0),
  ('S', 1, 'african-eyes', 'G01T', 0),
  ('M', 1, 'african-eyes', 'G02T', 0),
  ('L', 1, 'african-eyes', 'G03T', 0),
  ('S', 1, 'zulu-crochet', 'H01T', 0),
  ('M', 1, 'zulu-crochet', 'H02T', 0),
  ('L', 1, 'zulu-crochet', 'H03T', 0),
  ('S', 1, 'ivory-dakari', 'I01T', 0),
  ('M', 1, 'ivory-dakari', 'I02T', 0),
  ('L', 1, 'ivory-dakari', 'I03T', 0),
  ('S', 1, 'grey-dakari', 'J01T', 0),
  ('M', 1, 'grey-dakari', 'J02T', 0),
  ('L', 1, 'grey-dakari', 'J03T', 0);

-- --------------------------------------------------------

--
-- Table structure for table `countries_shipping_prices`
--

CREATE TABLE `countries_shipping_prices` (
  `id` int(255) NOT NULL,
  `country_name` varchar(255) NOT NULL,
  `price` int(255) NOT NULL,
  `currency` varchar(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=457 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `countries_shipping_prices`
--

INSERT INTO `countries_shipping_prices` (`id`, `country_name`, `price`, `currency`) VALUES
  (229, 'Afghanistan ', 31, 'USD'),
  (230, 'Albania ', 31, 'USD'),
  (231, 'Algeria ', 31, 'USD'),
  (232, 'American Samoa ', 31, 'USD'),
  (233, 'Andorra ', 31, 'USD'),
  (234, 'Angola ', 31, 'USD'),
  (235, 'Anguilla ', 31, 'USD'),
  (236, 'Antigua & Barbuda ', 31, 'USD'),
  (237, 'Argentina ', 21, 'USD'),
  (238, 'Armenia ', 31, 'USD'),
  (239, 'Aruba ', 21, 'USD'),
  (240, 'Australia ', 31, 'USD'),
  (241, 'Austria ', 31, 'USD'),
  (242, 'Azerbaijan ', 35, 'USD'),
  (243, 'Bahamas, The ', 31, 'USD'),
  (244, 'Bahrain ', 31, 'USD'),
  (245, 'Bangladesh ', 31, 'USD'),
  (246, 'Barbados ', 31, 'USD'),
  (247, 'Belarus ', 31, 'USD'),
  (248, 'Belgium ', 21, 'USD'),
  (249, 'Belize ', 31, 'USD'),
  (250, 'Benin ', 31, 'USD'),
  (251, 'Bermuda ', 31, 'USD'),
  (252, 'Bhutan ', 31, 'USD'),
  (253, 'Bolivia ', 21, 'USD'),
  (254, 'Bosnia & Herzegovina ', 31, 'USD'),
  (255, 'Botswana ', 31, 'USD'),
  (256, 'Brazil ', 21, 'USD'),
  (257, 'British Virgin Is. ', 31, 'USD'),
  (258, 'Brunei ', 31, 'USD'),
  (259, 'Bulgaria ', 31, 'USD'),
  (260, 'Burkina Faso ', 31, 'USD'),
  (261, 'Burma ', 31, 'USD'),
  (262, 'Burundi ', 31, 'USD'),
  (263, 'Cambodia ', 31, 'USD'),
  (264, 'Cameroon ', 31, 'USD'),
  (265, 'Canada ', 19, 'USD'),
  (266, 'Cape Verde ', 31, 'USD'),
  (267, 'Cayman Islands ', 31, 'USD'),
  (268, 'Central African Rep. ', 31, 'USD'),
  (269, 'Chad ', 31, 'USD'),
  (270, 'Chile ', 15, 'USD'),
  (271, 'China ', 21, 'USD'),
  (272, 'Colombia ', 8000, 'COP'),
  (273, 'Comoros ', 31, 'USD'),
  (274, 'Congo, Dem. Rep. ', 31, 'USD'),
  (275, 'Congo, Repub. of the ', 31, 'USD'),
  (276, 'Cook Islands ', 31, 'USD'),
  (277, 'Costa Rica ', 15, 'USD'),
  (278, 'Cote d''Ivoire ', 31, 'USD'),
  (279, 'Croatia ', 31, 'USD'),
  (280, 'Cuba ', 21, 'USD'),
  (281, 'Cyprus ', 31, 'USD'),
  (282, 'Czech Republic ', 31, 'USD'),
  (283, 'Denmark ', 31, 'USD'),
  (284, 'Djibouti ', 31, 'USD'),
  (285, 'Dominica ', 31, 'USD'),
  (286, 'Dominican Republic ', 21, 'USD'),
  (287, 'East Timor ', 31, 'USD'),
  (288, 'Ecuador ', 15, 'USD'),
  (289, 'Egypt ', 31, 'USD'),
  (290, 'El Salvador ', 21, 'USD'),
  (291, 'Equatorial Guinea ', 31, 'USD'),
  (292, 'Eritrea ', 31, 'USD'),
  (293, 'Estonia ', 31, 'USD'),
  (294, 'Ethiopia ', 31, 'USD'),
  (295, 'Faroe Islands ', 31, 'USD'),
  (296, 'Fiji ', 31, 'USD'),
  (297, 'Finland ', 31, 'USD'),
  (298, 'France ', 31, 'USD'),
  (299, 'French Guiana ', 31, 'USD'),
  (300, 'French Polynesia ', 31, 'USD'),
  (301, 'Gabon ', 31, 'USD'),
  (302, 'Gambia, The ', 31, 'USD'),
  (303, 'Gaza Strip ', 31, 'USD'),
  (304, 'Georgia ', 31, 'USD'),
  (305, 'Germany ', 31, 'USD'),
  (306, 'Ghana ', 31, 'USD'),
  (307, 'Gibraltar ', 31, 'USD'),
  (308, 'Greece ', 31, 'USD'),
  (309, 'Greenland ', 31, 'USD'),
  (310, 'Grenada ', 31, 'USD'),
  (311, 'Guadeloupe ', 31, 'USD'),
  (312, 'Guam ', 31, 'USD'),
  (313, 'Guatemala ', 31, 'USD'),
  (314, 'Guernsey ', 31, 'USD'),
  (315, 'Guinea ', 31, 'USD'),
  (316, 'Guinea-Bissau ', 31, 'USD'),
  (317, 'Guyana ', 31, 'USD'),
  (318, 'Haiti ', 21, 'USD'),
  (319, 'Honduras ', 21, 'USD'),
  (320, 'Hong Kong ', 31, 'USD'),
  (321, 'Hungary ', 31, 'USD'),
  (322, 'Iceland ', 31, 'USD'),
  (323, 'India ', 31, 'USD'),
  (324, 'Indonesia ', 21, 'USD'),
  (325, 'Iran ', 31, 'USD'),
  (326, 'Iraq ', 31, 'USD'),
  (327, 'Ireland ', 31, 'USD'),
  (328, 'Isle of Man ', 31, 'USD'),
  (329, 'Israel ', 31, 'USD'),
  (330, 'Italy ', 21, 'USD'),
  (331, 'Jamaica ', 31, 'USD'),
  (332, 'Japan ', 21, 'USD'),
  (333, 'Jersey ', 31, 'USD'),
  (334, 'Jordan ', 31, 'USD'),
  (335, 'Kazakhstan ', 31, 'USD'),
  (336, 'Kenya ', 31, 'USD'),
  (337, 'Kiribati ', 31, 'USD'),
  (338, 'Korea, North ', 31, 'USD'),
  (339, 'Korea, South ', 31, 'USD'),
  (340, 'Kuwait ', 31, 'USD'),
  (341, 'Kyrgyzstan ', 31, 'USD'),
  (342, 'Laos ', 31, 'USD'),
  (343, 'Latvia ', 31, 'USD'),
  (344, 'Lebanon ', 31, 'USD'),
  (345, 'Lesotho ', 31, 'USD'),
  (346, 'Liberia ', 31, 'USD'),
  (347, 'Libya ', 31, 'USD'),
  (348, 'Liechtenstein ', 31, 'USD'),
  (349, 'Lithuania ', 31, 'USD'),
  (350, 'Luxembourg ', 21, 'USD'),
  (351, 'Macau ', 31, 'USD'),
  (352, 'Macedonia ', 31, 'USD'),
  (353, 'Madagascar ', 31, 'USD'),
  (354, 'Malawi ', 31, 'USD'),
  (355, 'Malaysia ', 21, 'USD'),
  (356, 'Maldives ', 31, 'USD'),
  (357, 'Mali ', 31, 'USD'),
  (358, 'Malta ', 31, 'USD'),
  (359, 'Marshall Islands ', 31, 'USD'),
  (360, 'Martinique ', 31, 'USD'),
  (361, 'Mauritania ', 31, 'USD'),
  (362, 'Mauritius ', 31, 'USD'),
  (363, 'Mayotte ', 31, 'USD'),
  (364, 'Mexico ', 19, 'USD'),
  (365, 'Micronesia, Fed. St. ', 31, 'USD'),
  (366, 'Moldova ', 31, 'USD'),
  (367, 'Monaco ', 31, 'USD'),
  (368, 'Mongolia ', 31, 'USD'),
  (369, 'Montserrat ', 31, 'USD'),
  (370, 'Morocco ', 31, 'USD'),
  (371, 'Mozambique ', 31, 'USD'),
  (372, 'Namibia ', 31, 'USD'),
  (373, 'Nauru ', 31, 'USD'),
  (374, 'Nepal ', 31, 'USD'),
  (375, 'Netherlands ', 21, 'USD'),
  (376, 'Netherlands Antilles ', 21, 'USD'),
  (377, 'New Caledonia ', 31, 'USD'),
  (378, 'New Zealand ', 31, 'USD'),
  (379, 'Nicaragua ', 21, 'USD'),
  (380, 'Niger ', 31, 'USD'),
  (381, 'Nigeria ', 31, 'USD'),
  (382, 'North Korea', 21, 'USD'),
  (383, 'N. Mariana Islands ', 31, 'USD'),
  (384, 'Norway ', 31, 'USD'),
  (385, 'Oman ', 31, 'USD'),
  (386, 'Pakistan ', 31, 'USD'),
  (387, 'Palau ', 31, 'USD'),
  (388, 'Panama ', 15, 'USD'),
  (389, 'Papua New Guinea ', 31, 'USD'),
  (390, 'Paraguay ', 21, 'USD'),
  (391, 'Peru ', 15, 'USD'),
  (392, 'Philippines ', 21, 'USD'),
  (393, 'Poland ', 31, 'USD'),
  (394, 'Portugal ', 21, 'USD'),
  (395, 'Puerto Rico ', 19, 'USD'),
  (396, 'Qatar ', 31, 'USD'),
  (397, 'Reunion ', 31, 'USD'),
  (398, 'Romania ', 31, 'USD'),
  (399, 'Russia ', 31, 'USD'),
  (400, 'Rwanda ', 31, 'USD'),
  (401, 'Saint Helena ', 31, 'USD'),
  (402, 'Saint Kitts & Nevis ', 31, 'USD'),
  (403, 'Saint Lucia ', 31, 'USD'),
  (404, 'St Pierre & Miquelon ', 31, 'USD'),
  (405, 'Saint Vincent and the Grenadines ', 31, 'USD'),
  (406, 'Samoa ', 31, 'USD'),
  (407, 'San Marino ', 31, 'USD'),
  (408, 'Sao Tome & Principe ', 31, 'USD'),
  (409, 'Saudi Arabia ', 31, 'USD'),
  (410, 'Senegal ', 31, 'USD'),
  (411, 'Serbia ', 31, 'USD'),
  (412, 'Seychelles ', 31, 'USD'),
  (413, 'Sierra Leone ', 31, 'USD'),
  (414, 'Singapore ', 31, 'USD'),
  (415, 'Slovakia ', 31, 'USD'),
  (416, 'Slovenia ', 31, 'USD'),
  (417, 'Solomon Islands ', 31, 'USD'),
  (418, 'Somalia ', 31, 'USD'),
  (419, 'South Africa ', 31, 'USD'),
  (420, 'Spain ', 21, 'USD'),
  (421, 'Sri Lanka ', 31, 'USD'),
  (422, 'Sudan ', 31, 'USD'),
  (423, 'Suriname ', 21, 'USD'),
  (424, 'Swaziland ', 31, 'USD'),
  (425, 'Sweden ', 21, 'USD'),
  (426, 'Switzerland ', 21, 'USD'),
  (427, 'Syria ', 31, 'USD'),
  (428, 'Taiwan ', 21, 'USD'),
  (429, 'Tajikistan ', 31, 'USD'),
  (430, 'Tanzania ', 31, 'USD'),
  (431, 'Thailand ', 21, 'USD'),
  (432, 'Togo ', 31, 'USD'),
  (433, 'Tonga ', 31, 'USD'),
  (434, 'Trinidad & Tobago ', 31, 'USD'),
  (435, 'Tunisia ', 31, 'USD'),
  (436, 'Turkey ', 31, 'USD'),
  (437, 'Turkmenistan ', 31, 'USD'),
  (438, 'Turks & Caicos Is ', 31, 'USD'),
  (439, 'Tuvalu ', 31, 'USD'),
  (440, 'Uganda ', 31, 'USD'),
  (441, 'Ukraine ', 35, 'USD'),
  (442, 'United Arab Emirates ', 35, 'USD'),
  (443, 'United Kingdom ', 21, 'USD'),
  (444, 'United States ', 15, 'USD'),
  (445, 'Uruguay ', 21, 'USD'),
  (446, 'Uzbekistan ', 31, 'USD'),
  (447, 'Vanuatu ', 31, 'USD'),
  (448, 'Venezuela ', 15, 'USD'),
  (449, 'Vietnam ', 31, 'USD'),
  (450, 'Virgin Islands ', 31, 'USD'),
  (451, 'Wallis and Futuna ', 31, 'USD'),
  (452, 'West Bank ', 31, 'USD'),
  (453, 'Western Sahara ', 31, 'USD'),
  (454, 'Yemen ', 31, 'USD'),
  (455, 'Zambia ', 31, 'USD'),
  (456, 'Zimbabwe ', 31, 'USD');

-- --------------------------------------------------------

--
-- Table structure for table `one_piece`
--

CREATE TABLE `one_piece` (
  `slug` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `show_order` int(11) DEFAULT NULL,
  `description` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `one_piece`
--

INSERT INTO `one_piece` (`slug`, `name`, `show_order`, `description`) VALUES
  ('akira', 'Akira', 8, 'Neoprene one piece low back swimsuit<br>model wears size M'),
  ('sarabi', 'Sarabi', 9, 'One piece cutout swimsuit<br>Model wears size M'),
  ('zadeh', 'Zadeh', 10, 'Choker strap<br>Wide cleavage<br>Model wears size M');

-- --------------------------------------------------------

--
-- Table structure for table `one_piece_order`
--

CREATE TABLE `one_piece_order` (
  `id` int(255) NOT NULL,
  `order_id` varchar(255) NOT NULL,
  `top_sku` varchar(255) NOT NULL,
  `amount` decimal(10,0) NOT NULL,
  `unitPrice` decimal(10,0) NOT NULL,
  `slug` varchar(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `one_piece_order`
--

INSERT INTO `one_piece_order` (`id`, `order_id`, `top_sku`, `amount`, `unitPrice`, `slug`) VALUES
  (1, '2-1480371088', 'C01', '1', '66', 'akira'),
  (2, '2-1480371182', 'C01', '1', '66', 'akira'),
  (3, '2-1480371308', 'C01', '1', '66', 'akira'),
  (4, '2-1480371401', 'C01', '1', '66', 'akira'),
  (5, '2-1480372039', 'C01', '1', '66', 'akira');

-- --------------------------------------------------------

--
-- Table structure for table `one_piece_stockable`
--

CREATE TABLE `one_piece_stockable` (
  `size` char(10) NOT NULL,
  `one_piece_slug` varchar(255) NOT NULL,
  `sku` varchar(255) NOT NULL,
  `stock_amount` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `one_piece_stockable`
--

INSERT INTO `one_piece_stockable` (`size`, `one_piece_slug`, `sku`, `stock_amount`) VALUES
  ('S', 'akira', 'C01', 10),
  ('M', 'akira', 'C02', 0),
  ('L', 'akira', 'C03', 0),
  ('S', 'zadeh', 'D01', 0),
  ('M', 'zadeh', 'D02', 0),
  ('L', 'zadeh', 'D03', 0),
  ('S', 'sarabi', 'F01', 0),
  ('M', 'sarabi', 'F02', 0),
  ('L', 'sarabi', 'F03', 0);

-- --------------------------------------------------------

--
-- Table structure for table `order`
--

CREATE TABLE `order` (
  `id` varchar(255) NOT NULL,
  `user_id` varchar(255) NOT NULL,
  `order_status` tinyint(4) NOT NULL,
  `total_price` decimal(10,0) NOT NULL,
  `currency` varchar(255) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `prices_cop`
--

CREATE TABLE `prices_cop` (
  `id` int(11) NOT NULL,
  `product_slug` varchar(255) NOT NULL,
  `price` decimal(10,0) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `prices_cop`
--

INSERT INTO `prices_cop` (`id`, `product_slug`, `price`) VALUES
  (1, 'akira', '150000'),
  (2, 'green-mawi', '150000'),
  (3, 'zadeh', '150000'),
  (4, 'nude-mawi', '150000'),
  (5, 'trival-nandu', '150000'),
  (6, 'sarabi', '150000'),
  (7, 'african-eyes', '160000'),
  (8, 'zulu-crochet', '180000'),
  (9, 'ivory-dakari', '150000'),
  (10, 'grey-dakari', '150000');

-- --------------------------------------------------------

--
-- Table structure for table `prices_usd`
--

CREATE TABLE `prices_usd` (
  `id` int(11) NOT NULL,
  `product_slug` varchar(255) NOT NULL,
  `price` decimal(10,0) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `prices_usd`
--

INSERT INTO `prices_usd` (`id`, `product_slug`, `price`) VALUES
  (1, 'akira', '66'),
  (2, 'green-mawi', '69'),
  (3, 'zadeh', '69'),
  (4, 'nude-mawi', '66'),
  (5, 'trival-nandu', '66'),
  (6, 'sarabi', '69'),
  (7, 'african-eyes', '69'),
  (8, 'zulu-crochet', '72'),
  (9, 'ivory-dakari', '66'),
  (10, 'grey-dakari', '66');

-- --------------------------------------------------------

--
-- Table structure for table `product`
--

CREATE TABLE `product` (
  `slug` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `show_order` int(11) DEFAULT NULL,
  `description` text,
  `descendant_class` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `product`
--

INSERT INTO `product` (`slug`, `name`, `show_order`, `description`, `descendant_class`) VALUES
  ('african-eyes', 'African Eyes', 7, 'handmade crochet high neck top<br>adjustable straps<br>ruched wide side bottom<br>model wears size M', 'BaobabModels\\Bikini'),
  ('akira', 'Akira', 8, 'Akira description', 'BaobabModels\\OnePiece'),
  ('green-mawi', 'Green Mawï', 1, '-Halter top\n-Adjustable straps\n-Internal elastic band for extra support', 'BaobabModels\\Bikini'),
  ('grey-dakari', 'Grey Dakari', 4, 'One shoulder top<br>ligth removable padding<br>model wears size M', 'BaobabModels\\Bikini'),
  ('ivory-dakari', 'Ivory Dakari', 3, 'One shoulder top<br>ligth removable padding<br>model wears size M', 'BaobabModels\\Bikini'),
  ('nude-mawi', 'Nude Mawï', 2, 'halter top<br>adjustable straps<br>internal elastic at base for extra support<br>seamless sewing<br>light removable padding<br>model wears size M', 'BaobabModels\\Bikini'),
  ('sarabi', 'Sarabi', 9, 'One piece cutout swimsuit<br>Model wears size M', 'BaobabModels\\OnePiece'),
  ('trival-nandu', 'Trival Nandu', 5, 'High neck top<br>Adjustable center strap at back<br>model wears size M', 'BaobabModels\\Bikini'),
  ('zadeh', 'Zadeh', 10, 'Choker strap<br>Wide cleavage<br>Model wears size M', 'BaobabModels\\OnePiece'),
  ('zulu-crochet', 'Zulu Crochet', 6, 'handmade crochet bikini<br>high neck top<br>adjustable straps<br>adjustable side ties<br>String low rise Bottom with adjustable side ties<br>model wears size M', 'BaobabModels\\Bikini');

-- --------------------------------------------------------

--
-- Table structure for table `product_images`
--

CREATE TABLE `product_images` (
  `id` int(11) NOT NULL,
  `featured` tinyint(1) DEFAULT NULL,
  `url` varchar(255) NOT NULL,
  `product_slug` varchar(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `product_images`
--

INSERT INTO `product_images` (`id`, `featured`, `url`, `product_slug`) VALUES
  (4, NULL, '/assets/photography/akira-small.png', 'akira'),
  (5, NULL, '/assets/photography/green-mawi-small.png', 'green-mawi'),
  (6, NULL, '/assets/photography/zadeh-small.png', 'zadeh'),
  (7, NULL, '/assets/photography/nude-mawi-small.png', 'nude-mawi'),
  (8, NULL, '/assets/photography/trival-nandu-small.png', 'trival-nandu'),
  (9, NULL, '/assets/photography/sarabi-small.png', 'sarabi'),
  (10, NULL, '/assets/photography/african-eyes-small.png', 'african-eyes'),
  (11, NULL, '/assets/photography/zulu-crochet-small.png', 'zulu-crochet'),
  (12, NULL, '/assets/photography/ivory-dakari-small.png', 'ivory-dakari'),
  (13, NULL, '/assets/photography/grey-dakari-small.png', 'grey-dakari');

-- --------------------------------------------------------

--
-- Table structure for table `stockable`
--

CREATE TABLE `stockable` (
  `sku` varchar(255) NOT NULL,
  `stock_amount` int(11) NOT NULL,
  `descendant_class` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `stockable`
--

INSERT INTO `stockable` (`sku`, `stock_amount`, `descendant_class`) VALUES
  ('', 6, 'BaobabModels\\OnePieceStockable'),
  ('A01P', 99937, 'BaobabModels\\BikiniBottom'),
  ('A01T', 9937, 'BaobabModels\\BikiniTop'),
  ('A02P', 90349303, 'BaobabModels\\BikiniBottom'),
  ('A02T', 39923454, 'BaobabModels\\BikiniTop'),
  ('A03P', 11, 'BaobabModels\\BikiniBottom'),
  ('A03T', 11, 'BaobabModels\\BikiniTop'),
  ('B01P', 19, 'BaobabModels\\BikiniBottom'),
  ('B01T', 19, 'BaobabModels\\BikiniTop'),
  ('B02P', 20, 'BaobabModels\\BikiniBottom'),
  ('B02T', 19, 'BaobabModels\\BikiniTop'),
  ('B03P', 54325233, 'BaobabModels\\BikiniBottom'),
  ('B03T', 2147483646, 'BaobabModels\\BikiniTop'),
  ('C01', 10, 'BaobabModels\\OnePieceStockable'),
  ('C02', 12, 'BaobabModels\\OnePieceStockable'),
  ('C03', 12, 'BaobabModels\\OnePieceStockable'),
  ('D01', 9, 'BaobabModels\\OnePieceStockable'),
  ('D02', 8, 'BaobabModels\\OnePieceStockable'),
  ('D03', 3, 'BaobabModels\\OnePieceStockable'),
  ('E01P', 15, 'BaobabModels\\BikiniBottom'),
  ('E01T', 16, 'BaobabModels\\BikiniTop'),
  ('E02P', 16, 'BaobabModels\\BikiniBottom'),
  ('E02T', 16, 'BaobabModels\\BikiniTop'),
  ('E03P', 8, 'BaobabModels\\BikiniBottom'),
  ('E03T', 8, 'BaobabModels\\BikiniTop'),
  ('F01', 12, 'BaobabModels\\OnePieceStockable'),
  ('F02', 12, 'BaobabModels\\OnePieceStockable'),
  ('F03', 6, 'BaobabModels\\OnePieceStockable'),
  ('G01P', 22, 'BaobabModels\\BikiniBottom'),
  ('G01T', 2, 'BaobabModels\\BikiniTop'),
  ('G02P', 22, 'BaobabModels\\BikiniBottom'),
  ('G02T', 0, 'BaobabModels\\BikiniTop'),
  ('G03P', 10, 'BaobabModels\\BikiniBottom'),
  ('G03T', 0, 'BaobabModels\\BikiniTop'),
  ('H01P', 3, 'BaobabModels\\BikiniBottom'),
  ('H01T', 3, 'BaobabModels\\BikiniTop'),
  ('H02P', 0, 'BaobabModels\\BikiniBottom'),
  ('H02T', 0, 'BaobabModels\\BikiniTop'),
  ('H03P', 0, 'BaobabModels\\BikiniBottom'),
  ('H03T', 0, 'BaobabModels\\BikiniTop'),
  ('I01P', 0, 'BaobabModels\\BikiniBottom'),
  ('I01T', 0, 'BaobabModels\\BikiniTop'),
  ('I02P', 0, 'BaobabModels\\BikiniBottom'),
  ('I02T', 0, 'BaobabModels\\BikiniTop'),
  ('I03P', 0, 'BaobabModels\\BikiniBottom'),
  ('I03T', 0, 'BaobabModels\\BikiniTop'),
  ('J01P', 0, 'BaobabModels\\BikiniBottom'),
  ('J01T', 0, 'BaobabModels\\BikiniTop'),
  ('J02P', 0, 'BaobabModels\\BikiniBottom'),
  ('J02T', 0, 'BaobabModels\\BikiniTop'),
  ('J03P', 0, 'BaobabModels\\BikiniBottom'),
  ('J03T', 0, 'BaobabModels\\BikiniTop');

-- --------------------------------------------------------

--
-- Table structure for table `v_activity`
--

CREATE TABLE `v_activity` (
  `id` int(11) NOT NULL,
  `logged` datetime NOT NULL,
  `activity` varchar(535) COLLATE utf8_unicode_ci NOT NULL,
  `category` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `user_from` int(11) NOT NULL,
  `user_to` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `v_album`
--

CREATE TABLE `v_album` (
  `id` int(11) NOT NULL,
  `titulo` varchar(255) NOT NULL,
  `created` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `v_balance`
--

CREATE TABLE `v_balance` (
  `id` int(11) NOT NULL,
  `user_id` varchar(535) COLLATE utf8_unicode_ci NOT NULL,
  `amount` double NOT NULL,
  `profit` double NOT NULL,
  `taxes` double NOT NULL,
  `logged` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `v_bids`
--

CREATE TABLE `v_bids` (
  `id` int(11) NOT NULL,
  `favor_id` varchar(55) COLLATE utf8_unicode_ci NOT NULL,
  `user_id` varchar(55) COLLATE utf8_unicode_ci NOT NULL,
  `amount` double NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `v_blog`
--

CREATE TABLE `v_blog` (
  `id` int(11) NOT NULL,
  `titulo` varchar(255) NOT NULL,
  `fecha` datetime NOT NULL,
  `url` varchar(255) NOT NULL,
  `sumario` varchar(535) NOT NULL,
  `texto` text NOT NULL,
  `img` varchar(535) NOT NULL DEFAULT 'None',
  `meta_autor` varchar(535) NOT NULL DEFAULT 'Vitasnacks. Hecho por Estudio Codesign.',
  `meta_keywords` varchar(535) NOT NULL DEFAULT 'Estudio, Codesign, Vitasnacks, Noticias, Snacks Saludables',
  `meta_description` text,
  `vistas` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `v_calendario`
--

CREATE TABLE `v_calendario` (
  `id` int(11) NOT NULL,
  `logged` datetime NOT NULL,
  `user_id` int(11) NOT NULL,
  `nombre` varchar(535) NOT NULL,
  `project_id` int(11) NOT NULL,
  `day` varchar(11) NOT NULL,
  `month` varchar(11) NOT NULL,
  `year` varchar(11) NOT NULL,
  `start_time` varchar(535) NOT NULL,
  `end_time` varchar(535) NOT NULL,
  `lugar` varchar(535) NOT NULL,
  `integrantes` varchar(535) NOT NULL DEFAULT 'None',
  `desc` varchar(535) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `v_cart`
--

CREATE TABLE `v_cart` (
  `id` int(11) NOT NULL,
  `added` datetime NOT NULL,
  `user_id` int(11) NOT NULL,
  `sku` varchar(535) NOT NULL DEFAULT 'None',
  `nombre` varchar(535) NOT NULL DEFAULT 'None',
  `precio` float NOT NULL DEFAULT '0',
  `cantidad` int(11) NOT NULL DEFAULT '0',
  `color` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `talla_top` enum('XS','S','M','L','XL') NOT NULL,
  `talla_bottom` enum('XS','S','M','L','XL') NOT NULL,
  `push_up` tinyint(1) NOT NULL,
  `ref` varchar(255) CHARACTER SET utf8 NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `v_categories`
--

CREATE TABLE `v_categories` (
  `id` int(11) NOT NULL,
  `category` varchar(535) COLLATE utf8_unicode_ci NOT NULL,
  `color` varchar(535) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `v_chats`
--

CREATE TABLE `v_chats` (
  `id` int(11) NOT NULL,
  `logged` datetime NOT NULL,
  `message` text COLLATE utf8_unicode_ci NOT NULL,
  `id_from` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `id_to` varchar(50) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `v_clicks`
--

CREATE TABLE `v_clicks` (
  `id` int(11) NOT NULL,
  `logged` datetime NOT NULL,
  `page` varchar(535) COLLATE utf8_unicode_ci NOT NULL,
  `ip` varchar(535) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `v_clientes`
--

CREATE TABLE `v_clientes` (
  `id` int(11) NOT NULL,
  `ano` varchar(50) NOT NULL DEFAULT '2015',
  `mes` varchar(50) NOT NULL DEFAULT '07',
  `dia` varchar(50) NOT NULL DEFAULT '16',
  `hora` varchar(50) NOT NULL DEFAULT '13:00',
  `nombre` varchar(255) NOT NULL,
  `cedula` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `fecha` varchar(255) NOT NULL,
  `creado_por` int(11) NOT NULL,
  `compras` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `v_colores_producto`
--

CREATE TABLE `v_colores_producto` (
  `id` int(11) NOT NULL,
  `sku` varchar(255) CHARACTER SET utf8 NOT NULL,
  `color` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `v_colors`
--

CREATE TABLE `v_colors` (
  `id` int(11) NOT NULL,
  `primary` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `secondary` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(25) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `v_colors`
--

INSERT INTO `v_colors` (`id`, `primary`, `secondary`, `name`) VALUES
  (1, '#3fb770', '#1a8445', 'green'),
  (2, '#f2ee8a', '#e5d839', 'yellow'),
  (3, '#65cced', '#179bba', 'blue'),
  (4, '#ea4b6f', '#c4224c', 'pink'),
  (5, '#ee974d', '#dd7222', 'orange'),
  (6, '#b07ab3', '#95499b', 'purple');

-- --------------------------------------------------------

--
-- Table structure for table `v_companias`
--

CREATE TABLE `v_companias` (
  `id` int(11) NOT NULL,
  `unique_id` varchar(535) NOT NULL DEFAULT '',
  `name` varchar(535) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `v_configuracion`
--

CREATE TABLE `v_configuracion` (
  `id` int(11) NOT NULL,
  `direccion` varchar(255) NOT NULL DEFAULT 'None',
  `telefono` varchar(255) NOT NULL DEFAULT 'None',
  `celular` varchar(255) NOT NULL DEFAULT 'None',
  `email_contacto` varchar(255) NOT NULL DEFAULT 'None',
  `facebook` varchar(255) NOT NULL DEFAULT 'http://facebook.com',
  `twitter` varchar(255) NOT NULL DEFAULT 'http://twitter.com',
  `youtube` varchar(255) NOT NULL DEFAULT 'http://youtube.com',
  `instagram` varchar(535) NOT NULL DEFAULT 'http://instagram.com',
  `vimeo` varchar(255) NOT NULL DEFAULT 'http://vimeo.com',
  `linkedin` varchar(255) NOT NULL DEFAULT 'http://linkedin.com',
  `flickr` varchar(255) NOT NULL DEFAULT 'http://flickr.com',
  `googleplus` varchar(255) NOT NULL DEFAULT 'http://plus.google.com',
  `pinterest` varchar(255) NOT NULL DEFAULT 'http://pinterest.com',
  `skype` varchar(255) NOT NULL DEFAULT 'http://skype.com',
  `behance` varchar(535) NOT NULL DEFAULT 'https://www.behance.net/',
  `evernote` varchar(535) NOT NULL DEFAULT 'https://evernote.com/',
  `paypal` varchar(535) NOT NULL DEFAULT 'https://www.paypal.com/',
  `soundcloud` varchar(535) NOT NULL DEFAULT 'https://soundcloud.com/',
  `spotify` varchar(535) NOT NULL DEFAULT 'https://www.spotify.com/',
  `dribbble` varchar(535) NOT NULL DEFAULT 'https://dribbble.com/',
  `github` varchar(535) NOT NULL DEFAULT 'https://github.com/'
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `v_configuracion`
--

INSERT INTO `v_configuracion` (`id`, `direccion`, `telefono`, `celular`, `email_contacto`, `facebook`, `twitter`, `youtube`, `instagram`, `vimeo`, `linkedin`, `flickr`, `googleplus`, `pinterest`, `skype`, `behance`, `evernote`, `paypal`, `soundcloud`, `spotify`, `dribbble`, `github`) VALUES
  (1, 'Carrera 8 #67-74', '3136868', '3214690562', 'contacto@grupoinspiro.com', 'http://facebook.com', 'http://twitter.com', 'http://youtube.com', 'http://instagram.com', 'http://vimeo.com', 'http://linkedin.com', 'http://flickr.com', 'http://plus.google.com', 'http://pinterest.com', 'http://skype.com', 'https://www.behance.net/', 'https://evernote.com/', 'https://www.paypal.com/', 'https://soundcloud.com/', 'https://www.spotify.com/', 'https://dribbble.com/', 'https://github.com/');

-- --------------------------------------------------------

--
-- Table structure for table `v_contacto`
--

CREATE TABLE `v_contacto` (
  `id` int(11) NOT NULL,
  `logged` datetime NOT NULL,
  `name` varchar(535) NOT NULL DEFAULT 'None',
  `email` varchar(535) NOT NULL DEFAULT 'None',
  `number` varchar(535) NOT NULL DEFAULT 'None',
  `mensaje` text,
  `motivo` varchar(535) NOT NULL DEFAULT 'None'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `v_contenido`
--

CREATE TABLE `v_contenido` (
  `id` int(11) NOT NULL,
  `tabla` varchar(535) NOT NULL,
  `nombre` varchar(535) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `v_counter`
--

CREATE TABLE `v_counter` (
  `id` int(11) NOT NULL,
  `logged` datetime NOT NULL,
  `page` varchar(535) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `v_currency`
--

CREATE TABLE `v_currency` (
  `id` int(11) NOT NULL,
  `currency` varchar(11) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `v_direcciones`
--

CREATE TABLE `v_direcciones` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `direccion` varchar(64) CHARACTER SET utf8 NOT NULL,
  `barrio` varchar(64) CHARACTER SET utf8 NOT NULL,
  `ciudad` varchar(64) CHARACTER SET utf8 NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `v_equipos`
--

CREATE TABLE `v_equipos` (
  `id` int(11) NOT NULL,
  `torneo_id` varchar(11) NOT NULL,
  `nombre` varchar(255) NOT NULL,
  `color` varchar(255) NOT NULL,
  `url` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `v_facturas`
--

CREATE TABLE `v_facturas` (
  `id` int(11) NOT NULL,
  `numero_factura` int(1) NOT NULL,
  `ano` varchar(50) NOT NULL DEFAULT '2015',
  `mes` varchar(50) NOT NULL DEFAULT '07',
  `dia` varchar(50) NOT NULL DEFAULT '12',
  `hora` varchar(50) NOT NULL DEFAULT '12:00',
  `producto` int(11) NOT NULL,
  `tipo` varchar(50) NOT NULL DEFAULT 'sencillo',
  `precio` float NOT NULL,
  `cantidad` int(11) NOT NULL,
  `total` float NOT NULL,
  `imp` float NOT NULL,
  `total_mas_imp` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `v_facturas_master`
--

CREATE TABLE `v_facturas_master` (
  `id` int(11) NOT NULL,
  `numero` int(11) NOT NULL DEFAULT '0',
  `ano` varchar(50) NOT NULL DEFAULT '0',
  `mes` varchar(50) NOT NULL DEFAULT '0',
  `dia` varchar(50) NOT NULL DEFAULT '0',
  `hora` varchar(50) NOT NULL DEFAULT '0',
  `cliente` int(11) NOT NULL DEFAULT '0',
  `productos` int(11) NOT NULL DEFAULT '0',
  `total` float NOT NULL DEFAULT '0',
  `impuesto` float NOT NULL DEFAULT '0',
  `total_mas_impuesto` float NOT NULL DEFAULT '0',
  `hecha_por` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `v_favorites`
--

CREATE TABLE `v_favorites` (
  `id` int(11) NOT NULL,
  `sku` varchar(255) CHARACTER SET utf8 NOT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `v_favors`
--

CREATE TABLE `v_favors` (
  `id` int(11) NOT NULL,
  `category` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(535) COLLATE utf8_unicode_ci NOT NULL,
  `required_skills` varchar(535) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `files` text COLLATE utf8_unicode_ci NOT NULL,
  `budget` double NOT NULL,
  `featured` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'no',
  `urgent` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'no',
  `user_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `bids` text COLLATE utf8_unicode_ci NOT NULL,
  `logged` datetime NOT NULL,
  `finishes` varchar(535) COLLATE utf8_unicode_ci NOT NULL,
  `candidates` text COLLATE utf8_unicode_ci NOT NULL,
  `status` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'incomplete',
  `hired` varchar(55) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'None'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `v_files`
--

CREATE TABLE `v_files` (
  `id` int(11) NOT NULL,
  `name` varchar(535) COLLATE utf8_unicode_ci NOT NULL,
  `file` varchar(535) COLLATE utf8_unicode_ci NOT NULL,
  `unique` varchar(535) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'None',
  `owner` varchar(535) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'None',
  `status` varchar(535) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'own'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `v_following`
--

CREATE TABLE `v_following` (
  `id` int(11) NOT NULL,
  `id_from` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `id_to` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `logged` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `v_fotos`
--

CREATE TABLE `v_fotos` (
  `id` int(11) NOT NULL,
  `nombre` varchar(535) NOT NULL,
  `uploaded` datetime NOT NULL,
  `album` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `v_friendships`
--

CREATE TABLE `v_friendships` (
  `id` int(11) NOT NULL,
  `logged` datetime NOT NULL,
  `user_to` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `user_id` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `status` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'request'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `v_groups`
--

CREATE TABLE `v_groups` (
  `id` int(11) NOT NULL,
  `permissions` varchar(535) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `v_imagenes`
--

CREATE TABLE `v_imagenes` (
  `id` int(11) NOT NULL,
  `torneo_id` varchar(255) NOT NULL,
  `url` varchar(255) NOT NULL,
  `equipos` varchar(535) NOT NULL DEFAULT 'No'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `v_images`
--

CREATE TABLE `v_images` (
  `id` int(11) NOT NULL,
  `user_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `img` varchar(535) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `v_info`
--

CREATE TABLE `v_info` (
  `id` int(11) NOT NULL,
  `info_name` varchar(535) COLLATE utf8_unicode_ci NOT NULL,
  `info_value` varchar(535) COLLATE utf8_unicode_ci NOT NULL,
  `category` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `price` float NOT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `v_invites`
--

CREATE TABLE `v_invites` (
  `id` int(11) NOT NULL,
  `email` varchar(535) COLLATE utf8_unicode_ci NOT NULL,
  `user_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Not Accepted'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `v_invoices`
--

CREATE TABLE `v_invoices` (
  `id` int(11) NOT NULL,
  `category` varchar(535) COLLATE utf8_unicode_ci NOT NULL,
  `reference` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `buy_from` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `buyer` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `price` float NOT NULL,
  `logged` datetime NOT NULL,
  `status` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'pending'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `v_menu`
--

CREATE TABLE `v_menu` (
  `id` int(11) NOT NULL,
  `user_id` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `menu_style` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'normal'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `v_modelos`
--

CREATE TABLE `v_modelos` (
  `id` int(11) NOT NULL,
  `sku` varchar(255) CHARACTER SET utf8 NOT NULL DEFAULT 'None',
  `size` enum('XS','S','M','L','XL') NOT NULL,
  `referencia` varchar(255) NOT NULL,
  `color` varchar(255) NOT NULL,
  `pushup` tinyint(1) DEFAULT NULL,
  `stock` int(11) NOT NULL,
  `disponibilidad` int(11) NOT NULL,
  `pieza` enum('top','bottom','onepiece') NOT NULL,
  `color_hex` varchar(7) CHARACTER SET utf8 NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `v_modelos`
--

INSERT INTO `v_modelos` (`id`, `sku`, `size`, `referencia`, `color`, `pushup`, `stock`, `disponibilidad`, `pieza`, `color_hex`) VALUES
  (4, '0078-0434', 'M', '0078-0434::0::top::m::negro', 'negro', 0, 5, 2, 'top', '#000000'),
  (5, '0078-0434', 'M', '0078-0434::1::top::m::negro', 'negro', 1, 2, 1, 'top', '#000000'),
  (6, '0019-9887', 'S', '0019-9887::0::bottom::s::negro', 'negro', 0, 5, 4, 'bottom', '#000000'),
  (7, '0078-0434', 'S', '', 'negro', 0, 5, 2, 'bottom', '#000000'),
  (8, '0078-0434', 'S', '', 'azul', 0, 5, 2, 'bottom', '#0000ff');

-- --------------------------------------------------------

--
-- Table structure for table `v_newsletter`
--

CREATE TABLE `v_newsletter` (
  `id` int(11) NOT NULL,
  `joined` datetime NOT NULL,
  `email` varchar(535) NOT NULL,
  `nombre` varchar(255) NOT NULL,
  `edad` int(11) NOT NULL,
  `padre` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `v_notas`
--

CREATE TABLE `v_notas` (
  `id` int(11) NOT NULL,
  `torneo_id` varchar(50) NOT NULL,
  `titulo` varchar(255) NOT NULL,
  `url` varchar(255) NOT NULL,
  `sumario` varchar(535) NOT NULL,
  `fecha` datetime NOT NULL,
  `img` varchar(255) NOT NULL,
  `texto` text NOT NULL,
  `equipos` varchar(535) NOT NULL DEFAULT 'None'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `v_notifications`
--

CREATE TABLE `v_notifications` (
  `id` int(11) NOT NULL,
  `logged` datetime NOT NULL,
  `activity` varchar(535) COLLATE utf8_unicode_ci NOT NULL,
  `category` varchar(535) COLLATE utf8_unicode_ci NOT NULL,
  `user_from` int(11) NOT NULL,
  `user_to` int(11) NOT NULL DEFAULT '0',
  `flashed` varchar(11) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'no'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `v_pages`
--

CREATE TABLE `v_pages` (
  `id` int(11) NOT NULL,
  `title` varchar(535) NOT NULL,
  `slug` varchar(535) NOT NULL,
  `descripcion` text,
  `keywords` varchar(535) NOT NULL,
  `template` varchar(535) NOT NULL,
  `created` datetime NOT NULL,
  `updated` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `v_partidos`
--

CREATE TABLE `v_partidos` (
  `id` int(11) NOT NULL,
  `torneo_id` varchar(255) NOT NULL,
  `equipo_a` varchar(255) NOT NULL,
  `equipo_b` varchar(255) NOT NULL,
  `fecha` varchar(255) NOT NULL,
  `score_a` int(11) NOT NULL DEFAULT '0',
  `score_b` int(11) NOT NULL DEFAULT '0',
  `status` varchar(255) NOT NULL DEFAULT 'Sin Jugar'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `v_passwords`
--

CREATE TABLE `v_passwords` (
  `id` int(11) NOT NULL,
  `user_id` varchar(11) NOT NULL DEFAULT 'None',
  `referencia` varchar(535) NOT NULL DEFAULT 'None',
  `usuario` varchar(535) NOT NULL DEFAULT 'None',
  `password` varchar(535) NOT NULL DEFAULT 'None',
  `shared` varchar(535) NOT NULL DEFAULT 'No'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `v_pedidos`
--

CREATE TABLE `v_pedidos` (
  `id` int(11) NOT NULL,
  `unique_id` varchar(255) NOT NULL,
  `ano` varchar(50) NOT NULL,
  `mes` varchar(50) NOT NULL,
  `dia` varchar(50) NOT NULL,
  `hora` varchar(50) NOT NULL,
  `nombre` varchar(535) NOT NULL DEFAULT 'None',
  `sku` varchar(535) NOT NULL DEFAULT 'None',
  `precio` float NOT NULL DEFAULT '0',
  `cantidad` int(11) DEFAULT '0',
  `id_color` int(11) DEFAULT NULL,
  `talla_top` enum('XS','S','M','L','XL') NOT NULL,
  `talla_bottom` enum('XS','S','M','L','XL') NOT NULL,
  `push_up` tinyint(1) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `v_pedidos`
--

INSERT INTO `v_pedidos` (`id`, `unique_id`, `ano`, `mes`, `dia`, `hora`, `nombre`, `sku`, `precio`, `cantidad`, `id_color`, `talla_top`, `talla_bottom`, `push_up`) VALUES
  (1, 'XzpWpvDTbInnA8AFrlV2zw5n0Xib75', '2016', '07', '19', '13:53', 'Regrant', '0', 38, 2, 0, 'XS', 'XS', 0),
  (2, 'GXA269wZV5UjvczXq2ldUFo7XToeBb', '2016', '07', '19', '13:56', 'Regrant', '0', 38, 2, 0, 'XS', 'XS', 0),
  (3, 'ttDo6wcsPHSSiv42rV10PpP4WGiV5k', '2016', '07', '19', '13:58', 'Regrant', '0', 38, 2, 0, 'XS', 'XS', 0),
  (4, 'HHlXzjJySw20HK2tv2uz54IBaJpnHM', '2016', '07', '19', '13:59', 'Regrant', '0', 38, 2, 0, 'XS', 'XS', 0),
  (5, 'b10c2FJLaQWQAZhHOpiMUYFNdy5L2l', '2016', '07', '19', '13:59', 'Regrant', '0', 38, 2, 0, 'XS', 'XS', 0),
  (6, 'fxPJJAI1YzujKHlj3uoJcCdZkU7C6J', '2016', '07', '19', '15:18', 'Regrant', '0', 38, 1, 0, 'XS', 'XS', 0),
  (7, '8FURqeX8DG0Pu1HVdmix7NU0s0rIo0', '2016', '07', '19', '15:46', 'Regrant', '0', 38, 1, 0, 'XS', 'XS', 0),
  (8, 'gJ3QJcrKfGqi8M6HZoRNc7ARdBPdYM', '2016', '07', '19', '15:48', 'Regrant', '0', 38, 1, 0, 'XS', 'XS', 0),
  (9, 'cfwg5fsx0HeqZmd64cuV0H2Bzgcoub', '2016', '07', '19', '15:52', 'Regrant', '0', 38, 1, 0, 'XS', 'XS', 0),
  (10, 'kYnGZsV6f83AhIrpJeEdB82gXVzSRP', '2016', '07', '19', '15:56', 'Regrant', '0', 38, 1, 0, 'XS', 'XS', 0),
  (11, '95VDZLM7JzwzAlQYcn1t20uDznw7zO', '2016', '07', '19', '15:58', 'Regrant', '0', 38, 1, 0, 'XS', 'XS', 0),
  (12, 'v0BRtxhr0T40nzjV8MMWMHywEdXFNi', '2016', '07', '19', '15:58', 'Regrant', '0', 38, 1, 0, 'XS', 'XS', 0),
  (13, 'dk5H9FF6ixqkBUOb1zgJSmrmoy4z0d', '2016', '07', '19', '16:02', 'Regrant', '0', 38, 1, 0, 'XS', 'XS', 0),
  (14, 'FbO2RNuNTKWWkeFLDo0iBBqDSozrhq', '2016', '07', '19', '16:04', 'Regrant', '0', 38, 1, 0, 'XS', 'XS', 0),
  (15, 'uoGTUGyQveYLHRe6jgqyUDYZuMxKf9', '2016', '07', '19', '16:09', 'Regrant', '0', 38, 1, 0, 'XS', 'XS', 0),
  (16, 'xGOo72mfF2fuxhcsuzL2GzXH1X8Kl9', '2016', '07', '19', '16:10', 'Regrant', '0', 38, 1, 0, 'XS', 'XS', 0),
  (17, 'CIUxmTj803IxCjTti5QjyTk3xTq410', '2016', '07', '19', '16:14', 'Regrant', '0', 38, 1, 0, 'XS', 'XS', 0),
  (18, 'bGrIWxYp4Y6io6FCdJOIFOpIpYYCns', '2016', '07', '19', '16:15', 'Regrant', '0', 38, 1, 0, 'XS', 'XS', 0),
  (19, 'gdTwK9rda2sjLBWP98bfq8mXTxy6Hz', '2016', '07', '19', '16:17', 'Regrant', '0', 38, 1, 0, 'XS', 'XS', 0),
  (20, 'SDINb4HudIyVfbf8ExevQMobQV4gZ5', '2016', '07', '19', '16:17', 'Regrant', '0', 38, 1, 0, 'XS', 'XS', 0),
  (21, 'in9LUrdVkhVIQeDZ1qVO7tkMHhsuzd', '2016', '07', '19', '16:19', 'Regrant', '0', 38, 1, 0, 'XS', 'XS', 0),
  (22, 'TJxADrgbiLphw78Lds2E1XhZWLLtv1', '2016', '07', '19', '16:20', 'Regrant', '0', 38, 1, 0, 'XS', 'XS', 0),
  (23, 'MZ7jy20MCOiK29tbNol2Aea8zE5tcB', '2016', '07', '19', '16:23', 'Regrant', '0', 38, 1, 0, 'XS', 'XS', 0),
  (24, 'Oy9fh5MfvRdBaCIPeWy3EeR4Wh3UTq', '2016', '07', '19', '16:26', 'Regrant', '41190-763', 38, 1, 0, 'XS', 'XS', 0),
  (25, 'CpKaZoCgzU1Zby6kkkMmYNjfNf1zJw', '2016', '07', '19', '16:35', 'Regrant', '41190-763', 38, 1, 0, 'XS', 'XS', 0);

-- --------------------------------------------------------

--
-- Table structure for table `v_pedidos_master`
--

CREATE TABLE `v_pedidos_master` (
  `id` int(11) NOT NULL,
  `unique_id` varchar(255) NOT NULL,
  `ano` varchar(50) NOT NULL,
  `mes` varchar(50) NOT NULL,
  `dia` varchar(50) NOT NULL,
  `hora` varchar(50) NOT NULL,
  `num_productos` int(11) NOT NULL DEFAULT '0',
  `precio` float NOT NULL DEFAULT '0',
  `envio` float NOT NULL DEFAULT '0',
  `precio_mas_envio` float NOT NULL DEFAULT '0',
  `nombre` varchar(535) NOT NULL DEFAULT 'None',
  `email` varchar(535) NOT NULL DEFAULT 'None',
  `celular` varchar(535) NOT NULL DEFAULT 'None',
  `direccion` varchar(535) NOT NULL DEFAULT 'None',
  `ciudad` varchar(535) NOT NULL DEFAULT 'None',
  `suscripcion` tinyint(1) NOT NULL DEFAULT '0',
  `status` enum('Sin Pagar','Pagado') CHARACTER SET utf8 NOT NULL DEFAULT 'Sin Pagar',
  `entregado` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `v_pedidos_master`
--

INSERT INTO `v_pedidos_master` (`id`, `unique_id`, `ano`, `mes`, `dia`, `hora`, `num_productos`, `precio`, `envio`, `precio_mas_envio`, `nombre`, `email`, `celular`, `direccion`, `ciudad`, `suscripcion`, `status`, `entregado`) VALUES
  (1, 'Oy9fh5MfvRdBaCIPeWy3EeR4Wh3UTq', '2016', '07', '19', '16:26', 1, 38, 0, 38, 'fsad', 'jafsdjafds@a', '123', '0', 'bogota', 0, 'Sin Pagar', 0),
  (2, 'CpKaZoCgzU1Zby6kkkMmYNjfNf1zJw', '2016', '07', '19', '16:35', 1, 38, 0, 38, 'kjfdjkadsf', 'js.rolon@estudiocodesign.com', '12335246', '0', 'bogota', 0, 'Sin Pagar', 0),
  (3, '3zEEvGyDY7tX8bQ13HOPk9mbJBKD44', '2016', '07', '29', '11:40', 1, 0, 0, 0, 'jkadfs', 'a@a.a', '98123', '0', 'bogota', 0, 'Sin Pagar', 0),
  (4, 'q5pVfSZvyBp7UlITCzRJeA8sJwIjkl', '2016', '07', '29', '12:06', 1, 0, 0, 0, 'jfasd', 'a@a.a', '123', '0', 'bogota', 0, 'Sin Pagar', 0),
  (5, '-2016-11-19-01:32:21:000000', '2016', '11', '19', '1:32', 1, 0, 0, 0, 'js', 'js.rolon@estudiocodesign.com', '1234567890', '0', 'bogota', 0, 'Sin Pagar', 0),
  (6, '-2016-11-19-01:33:32:000000', '2016', '11', '19', '1:33', 1, 0, 0, 0, 'js', 'js.rolon@estudiocodesign.com', '1234567890', '0', 'bogota', 0, 'Sin Pagar', 0);

-- --------------------------------------------------------

--
-- Table structure for table `v_posts`
--

CREATE TABLE `v_posts` (
  `id` int(11) NOT NULL,
  `user_to` varchar(535) COLLATE utf8_unicode_ci NOT NULL,
  `user_from` varchar(535) COLLATE utf8_unicode_ci NOT NULL,
  `message` text COLLATE utf8_unicode_ci NOT NULL,
  `logged` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `v_premio`
--

CREATE TABLE `v_premio` (
  `id` int(11) NOT NULL,
  `month` varchar(535) NOT NULL,
  `year` varchar(535) NOT NULL,
  `premio` varchar(535) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `v_productos`
--

CREATE TABLE `v_productos` (
  `id` int(11) NOT NULL,
  `nombre` varchar(255) NOT NULL DEFAULT 'None',
  `sku` varchar(255) CHARACTER SET utf8 NOT NULL DEFAULT 'None',
  `unidades` int(11) NOT NULL DEFAULT '1',
  `precio` float NOT NULL DEFAULT '0',
  `descripcion` text,
  `img` varchar(535) NOT NULL DEFAULT 'None',
  `tipo` varchar(535) NOT NULL DEFAULT 'None',
  `categoria` enum('BIKINIS','MIX','ONEPIECE') NOT NULL,
  `proveedor` varchar(255) NOT NULL DEFAULT 'None',
  `medida` varchar(255) NOT NULL DEFAULT 'None',
  `peso` float NOT NULL DEFAULT '0',
  `ultimo_pedido` varchar(50) NOT NULL DEFAULT 'Nunca ha pedido',
  `inventario` int(11) NOT NULL DEFAULT '0',
  `fecha_modificado` varchar(255) DEFAULT 'Nunca',
  `persona_modificacion` int(11) NOT NULL DEFAULT '0',
  `limite_medio` int(11) NOT NULL DEFAULT '0',
  `limite_bajo` int(11) NOT NULL DEFAULT '0',
  `impuesto_nombre` varchar(255) NOT NULL DEFAULT 'IVA',
  `impuesto_valor` float NOT NULL DEFAULT '0.16',
  `coleccion_nueva` tinyint(1) NOT NULL DEFAULT '1',
  `en_promocion` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=104 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `v_productos`
--

INSERT INTO `v_productos` (`id`, `nombre`, `sku`, `unidades`, `precio`, `descripcion`, `img`, `tipo`, `categoria`, `proveedor`, `medida`, `peso`, `ultimo_pedido`, `inventario`, `fecha_modificado`, `persona_modificacion`, `limite_medio`, `limite_bajo`, `impuesto_nombre`, `impuesto_valor`, `coleccion_nueva`, `en_promocion`) VALUES
  (6, 'Regrant', '41190-763', 1, 38, NULL, 'None', 'None', 'ONEPIECE', 'None', 'None', 0, 'Nunca ha pedido', 0, 'Nunca', 0, 0, 0, 'IVA', 0.16, 1, 0),
  (8, 'Konklab', '54868-6009', 1, 5977, NULL, 'None', 'None', 'ONEPIECE', 'None', 'None', 0, 'Nunca ha pedido', 0, 'Nunca', 0, 0, 0, 'IVA', 0.16, 1, 0),
  (9, 'Veribet', '59262-263', 1, 4922, NULL, 'None', 'None', 'BIKINIS', 'None', 'None', 0, 'Nunca ha pedido', 0, 'Nunca', 0, 0, 0, 'IVA', 0.16, 1, 0),
  (11, 'Kanlam', '55711-066', 1, 2, NULL, 'None', 'None', 'ONEPIECE', 'None', 'None', 0, 'Nunca ha pedido', 0, 'Nunca', 0, 0, 0, 'IVA', 0.16, 1, 0),
  (13, 'Tempsoft', '54575-356', 1, 0, NULL, 'None', 'None', 'BIKINIS', 'None', 'None', 0, 'Nunca ha pedido', 0, 'Nunca', 0, 0, 0, 'IVA', 0.16, 1, 0),
  (14, 'Latlux', '0113-0467', 1, 3, NULL, 'None', 'None', 'BIKINIS', 'None', 'None', 0, 'Nunca ha pedido', 0, 'Nunca', 0, 0, 0, 'IVA', 0.16, 1, 0),
  (16, 'Toughjoyfax', '50227-2110', 1, 0, NULL, 'None', 'None', 'BIKINIS', 'None', 'None', 0, 'Nunca ha pedido', 0, 'Nunca', 0, 0, 0, 'IVA', 0.16, 1, 0),
  (17, 'Viva', '54868-0826', 1, 92, NULL, 'None', 'None', 'BIKINIS', 'None', 'None', 0, 'Nunca ha pedido', 0, 'Nunca', 0, 0, 0, 'IVA', 0.16, 1, 0),
  (18, 'Ronstring', '49288-0229', 1, 358, NULL, 'None', 'None', 'ONEPIECE', 'None', 'None', 0, 'Nunca ha pedido', 0, 'Nunca', 0, 0, 0, 'IVA', 0.16, 1, 0),
  (22, 'Biodex', '49349-709', 1, 22, NULL, 'None', 'None', 'BIKINIS', 'None', 'None', 0, 'Nunca ha pedido', 0, 'Nunca', 0, 0, 0, 'IVA', 0.16, 1, 0),
  (23, 'Viva', '36800-872', 1, 1, NULL, 'None', 'None', 'BIKINIS', 'None', 'None', 0, 'Nunca ha pedido', 0, 'Nunca', 0, 0, 0, 'IVA', 0.16, 1, 0),
  (25, 'Tres-Zap', '48951-8210', 1, 2, NULL, 'None', 'None', 'ONEPIECE', 'None', 'None', 0, 'Nunca ha pedido', 0, 'Nunca', 0, 0, 0, 'IVA', 0.16, 1, 0),
  (26, 'Cardguard', '0113-0851', 1, 27541, NULL, 'None', 'None', 'BIKINIS', 'None', 'None', 0, 'Nunca ha pedido', 0, 'Nunca', 0, 0, 0, 'IVA', 0.16, 1, 0),
  (27, 'Cardify', '0046-1103', 1, 9945, NULL, 'None', 'None', 'ONEPIECE', 'None', 'None', 0, 'Nunca ha pedido', 0, 'Nunca', 0, 0, 0, 'IVA', 0.16, 1, 0),
  (30, 'Opela', '36987-2129', 1, 16, NULL, 'None', 'None', 'ONEPIECE', 'None', 'None', 0, 'Nunca ha pedido', 0, 'Nunca', 0, 0, 0, 'IVA', 0.16, 1, 0),
  (31, 'Cardify', '63824-015', 1, 649, NULL, 'None', 'None', 'BIKINIS', 'None', 'None', 0, 'Nunca ha pedido', 0, 'Nunca', 0, 0, 0, 'IVA', 0.16, 1, 0),
  (32, 'Transcof', '41900-400', 1, 8, NULL, 'None', 'None', 'ONEPIECE', 'None', 'None', 0, 'Nunca ha pedido', 0, 'Nunca', 0, 0, 0, 'IVA', 0.16, 1, 0),
  (33, 'Y-find', '29500-9210', 1, 4870, NULL, 'None', 'None', 'ONEPIECE', 'None', 'None', 0, 'Nunca ha pedido', 0, 'Nunca', 0, 0, 0, 'IVA', 0.16, 1, 0),
  (34, 'Trippledex', '68151-0084', 1, 581, NULL, 'None', 'None', 'BIKINIS', 'None', 'None', 0, 'Nunca ha pedido', 0, 'Nunca', 0, 0, 0, 'IVA', 0.16, 1, 0),
  (36, 'Zaam-Dox', '76123-245', 1, 1, NULL, 'None', 'None', 'BIKINIS', 'None', 'None', 0, 'Nunca ha pedido', 0, 'Nunca', 0, 0, 0, 'IVA', 0.16, 1, 0),
  (39, 'Bitchip', '36987-2315', 1, 952, NULL, 'None', 'None', 'BIKINIS', 'None', 'None', 0, 'Nunca ha pedido', 0, 'Nunca', 0, 0, 0, 'IVA', 0.16, 1, 0),
  (40, 'Veribet', '49643-401', 1, 654, NULL, 'None', 'None', 'ONEPIECE', 'None', 'None', 0, 'Nunca ha pedido', 0, 'Nunca', 0, 0, 0, 'IVA', 0.16, 1, 0),
  (41, 'Voyatouch', '43269-847', 1, 0, NULL, 'None', 'None', 'BIKINIS', 'None', 'None', 0, 'Nunca ha pedido', 0, 'Nunca', 0, 0, 0, 'IVA', 0.16, 1, 0),
  (43, 'Ronstring', '21695-689', 1, 71992, NULL, 'None', 'None', 'BIKINIS', 'None', 'None', 0, 'Nunca ha pedido', 0, 'Nunca', 0, 0, 0, 'IVA', 0.16, 1, 0),
  (44, 'Opela', '63629-2626', 1, 68869, NULL, 'None', 'None', 'ONEPIECE', 'None', 'None', 0, 'Nunca ha pedido', 0, 'Nunca', 0, 0, 0, 'IVA', 0.16, 1, 0),
  (48, 'It', '51386-400', 1, 50, NULL, 'None', 'None', 'BIKINIS', 'None', 'None', 0, 'Nunca ha pedido', 0, 'Nunca', 0, 0, 0, 'IVA', 0.16, 1, 0),
  (49, 'Hatity', '59779-589', 1, 12, NULL, 'None', 'None', 'BIKINIS', 'None', 'None', 0, 'Nunca ha pedido', 0, 'Nunca', 0, 0, 0, 'IVA', 0.16, 1, 0),
  (50, 'Mat Lam Tam', '54868-4598', 1, 9281, NULL, 'None', 'None', 'BIKINIS', 'None', 'None', 0, 'Nunca ha pedido', 0, 'Nunca', 0, 0, 0, 'IVA', 0.16, 1, 0),
  (51, 'Prodder', '59779-143', 1, 21, NULL, 'None', 'None', 'BIKINIS', 'None', 'None', 0, 'Nunca ha pedido', 0, 'Nunca', 0, 0, 0, 'IVA', 0.16, 1, 0),
  (52, 'Trippledex', '51220-2001', 1, 6891, NULL, 'None', 'None', 'ONEPIECE', 'None', 'None', 0, 'Nunca ha pedido', 0, 'Nunca', 0, 0, 0, 'IVA', 0.16, 1, 0),
  (53, 'Cookley', '76174-100', 1, 9, NULL, 'None', 'None', 'BIKINIS', 'None', 'None', 0, 'Nunca ha pedido', 0, 'Nunca', 0, 0, 0, 'IVA', 0.16, 1, 0),
  (54, 'Andalax', '63545-142', 1, 996, NULL, 'None', 'None', 'BIKINIS', 'None', 'None', 0, 'Nunca ha pedido', 0, 'Nunca', 0, 0, 0, 'IVA', 0.16, 1, 0),
  (55, 'Redhold', '57525-023', 1, 920, NULL, 'None', 'None', 'BIKINIS', 'None', 'None', 0, 'Nunca ha pedido', 0, 'Nunca', 0, 0, 0, 'IVA', 0.16, 1, 0),
  (56, 'Fintone', '0378-0860', 1, 112, NULL, 'None', 'None', 'ONEPIECE', 'None', 'None', 0, 'Nunca ha pedido', 0, 'Nunca', 0, 0, 0, 'IVA', 0.16, 1, 0),
  (57, 'Konklab', '0078-0434', 1, 427, NULL, 'None', 'None', 'BIKINIS', 'None', 'None', 0, 'Nunca ha pedido', 0, 'Nunca', 0, 0, 0, 'IVA', 0.16, 1, 0),
  (59, 'Ventosanzap', '29991-505', 1, 9, NULL, 'None', 'None', 'ONEPIECE', 'None', 'None', 0, 'Nunca ha pedido', 0, 'Nunca', 0, 0, 0, 'IVA', 0.16, 1, 0),
  (60, 'Namfix', '21695-947', 1, 2, NULL, 'None', 'None', 'ONEPIECE', 'None', 'None', 0, 'Nunca ha pedido', 0, 'Nunca', 0, 0, 0, 'IVA', 0.16, 1, 0),
  (61, 'Rank', '52125-774', 1, 504, NULL, 'None', 'None', 'ONEPIECE', 'None', 'None', 0, 'Nunca ha pedido', 0, 'Nunca', 0, 0, 0, 'IVA', 0.16, 1, 0),
  (62, 'Aerified', '46123-040', 1, 50702, NULL, 'None', 'None', 'BIKINIS', 'None', 'None', 0, 'Nunca ha pedido', 0, 'Nunca', 0, 0, 0, 'IVA', 0.16, 1, 0),
  (64, 'Biodex', '52125-854', 1, 44, NULL, 'None', 'None', 'ONEPIECE', 'None', 'None', 0, 'Nunca ha pedido', 0, 'Nunca', 0, 0, 0, 'IVA', 0.16, 1, 0),
  (67, 'Bitwolf', '55312-660', 1, 4, NULL, 'None', 'None', 'ONEPIECE', 'None', 'None', 0, 'Nunca ha pedido', 0, 'Nunca', 0, 0, 0, 'IVA', 0.16, 1, 0),
  (68, 'Veribet', '59779-960', 1, 58, NULL, 'None', 'None', 'BIKINIS', 'None', 'None', 0, 'Nunca ha pedido', 0, 'Nunca', 0, 0, 0, 'IVA', 0.16, 1, 0),
  (69, 'Vagram', '49288-0881', 1, 3, NULL, 'None', 'None', 'ONEPIECE', 'None', 'None', 0, 'Nunca ha pedido', 0, 'Nunca', 0, 0, 0, 'IVA', 0.16, 1, 0),
  (70, 'Bigtax', '68788-9819', 1, 44852, NULL, 'None', 'None', 'ONEPIECE', 'None', 'None', 0, 'Nunca ha pedido', 0, 'Nunca', 0, 0, 0, 'IVA', 0.16, 1, 0),
  (71, 'Temp', '43742-0456', 1, 11261, NULL, 'None', 'None', 'ONEPIECE', 'None', 'None', 0, 'Nunca ha pedido', 0, 'Nunca', 0, 0, 0, 'IVA', 0.16, 1, 0),
  (72, 'Zontrax', '0944-2923', 1, 1454, NULL, 'None', 'None', 'BIKINIS', 'None', 'None', 0, 'Nunca ha pedido', 0, 'Nunca', 0, 0, 0, 'IVA', 0.16, 1, 0),
  (74, 'Span', '0019-9887', 1, 8139, NULL, 'None', 'None', 'BIKINIS', 'None', 'None', 0, 'Nunca ha pedido', 0, 'Nunca', 0, 0, 0, 'IVA', 0.16, 1, 0),
  (75, 'Voltsillam', '48951-6012', 1, 9, NULL, 'None', 'None', 'BIKINIS', 'None', 'None', 0, 'Nunca ha pedido', 0, 'Nunca', 0, 0, 0, 'IVA', 0.16, 1, 0),
  (76, 'Alpha', '11994-003', 1, 7, NULL, 'None', 'None', 'BIKINIS', 'None', 'None', 0, 'Nunca ha pedido', 0, 'Nunca', 0, 0, 0, 'IVA', 0.16, 1, 0),
  (77, 'Konklab', '0268-1129', 1, 7934, NULL, 'None', 'None', 'ONEPIECE', 'None', 'None', 0, 'Nunca ha pedido', 0, 'Nunca', 0, 0, 0, 'IVA', 0.16, 1, 0),
  (79, 'Otcom', '53633-100', 1, 15, NULL, 'None', 'None', 'ONEPIECE', 'None', 'None', 0, 'Nunca ha pedido', 0, 'Nunca', 0, 0, 0, 'IVA', 0.16, 1, 0),
  (80, 'Bamity', '67046-126', 1, 357, NULL, 'None', 'None', 'BIKINIS', 'None', 'None', 0, 'Nunca ha pedido', 0, 'Nunca', 0, 0, 0, 'IVA', 0.16, 1, 0),
  (82, 'Ronstring', '63824-390', 1, 68829, NULL, 'None', 'None', 'ONEPIECE', 'None', 'None', 0, 'Nunca ha pedido', 0, 'Nunca', 0, 0, 0, 'IVA', 0.16, 1, 0),
  (84, 'Tresom', '54868-2271', 1, 130, NULL, 'None', 'None', 'BIKINIS', 'None', 'None', 0, 'Nunca ha pedido', 0, 'Nunca', 0, 0, 0, 'IVA', 0.16, 1, 0),
  (85, 'Fix San', '50484-342', 1, 89205, NULL, 'None', 'None', 'BIKINIS', 'None', 'None', 0, 'Nunca ha pedido', 0, 'Nunca', 0, 0, 0, 'IVA', 0.16, 1, 0),
  (86, 'Fix San', '52533-120', 1, 685, NULL, 'None', 'None', 'ONEPIECE', 'None', 'None', 0, 'Nunca ha pedido', 0, 'Nunca', 0, 0, 0, 'IVA', 0.16, 1, 0),
  (88, 'Bytecard', '33261-053', 1, 9830, NULL, 'None', 'None', 'BIKINIS', 'None', 'None', 0, 'Nunca ha pedido', 0, 'Nunca', 0, 0, 0, 'IVA', 0.16, 1, 0),
  (90, 'Latlux', '55806-020', 1, 855, NULL, 'None', 'None', 'ONEPIECE', 'None', 'None', 0, 'Nunca ha pedido', 0, 'Nunca', 0, 0, 0, 'IVA', 0.16, 1, 0),
  (91, 'Cookley', '34690-7001', 1, 8180, NULL, 'None', 'None', 'ONEPIECE', 'None', 'None', 0, 'Nunca ha pedido', 0, 'Nunca', 0, 0, 0, 'IVA', 0.16, 1, 0),
  (95, 'Veribet', '54868-0028', 1, 3, NULL, 'None', 'None', 'ONEPIECE', 'None', 'None', 0, 'Nunca ha pedido', 0, 'Nunca', 0, 0, 0, 'IVA', 0.16, 1, 0),
  (96, 'Tampflex', '68405-037', 1, 31736, NULL, 'None', 'None', 'BIKINIS', 'None', 'None', 0, 'Nunca ha pedido', 0, 'Nunca', 0, 0, 0, 'IVA', 0.16, 1, 0),
  (97, 'Duobam', '54569-0158', 1, 9, NULL, 'None', 'None', 'ONEPIECE', 'None', 'None', 0, 'Nunca ha pedido', 0, 'Nunca', 0, 0, 0, 'IVA', 0.16, 1, 0),
  (98, 'Alphazap', '45802-919', 1, 882, NULL, 'None', 'None', 'ONEPIECE', 'None', 'None', 0, 'Nunca ha pedido', 0, 'Nunca', 0, 0, 0, 'IVA', 0.16, 1, 0),
  (101, 'Duobam', '54868-2996', 1, 728, NULL, 'None', 'None', 'BIKINIS', 'None', 'None', 0, 'Nunca ha pedido', 0, 'Nunca', 0, 0, 0, 'IVA', 0.16, 1, 0),
  (103, 'Zamit', '35356-867', 1, 7311, NULL, 'None', 'None', 'BIKINIS', 'None', 'None', 0, 'Nunca ha pedido', 0, 'Nunca', 0, 0, 0, 'IVA', 0.16, 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `v_productos_complejos`
--

CREATE TABLE `v_productos_complejos` (
  `id` int(11) NOT NULL,
  `nombre` varchar(255) NOT NULL,
  `sku` varchar(255) NOT NULL,
  `precio` float NOT NULL DEFAULT '0',
  `img` varchar(535) NOT NULL DEFAULT 'http://vitasnacks.co/cms/uploads/products/VM-U_s15.jpg',
  `impuesto_nombre` varchar(50) NOT NULL DEFAULT 'IVA',
  `impuesto_valor` double NOT NULL DEFAULT '0.16'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `v_productos_complejos_sencillos`
--

CREATE TABLE `v_productos_complejos_sencillos` (
  `id` int(11) NOT NULL,
  `parent_sku` varchar(255) NOT NULL DEFAULT 'None',
  `producto_sku` varchar(255) NOT NULL DEFAULT 'None',
  `categoria` varchar(255) NOT NULL DEFAULT 'None',
  `medida` varchar(255) NOT NULL DEFAULT 'None',
  `cantidad` float NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `v_profileviews`
--

CREATE TABLE `v_profileviews` (
  `id` int(11) NOT NULL,
  `user_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `logged` datetime NOT NULL,
  `datee` varchar(535) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `v_projects`
--

CREATE TABLE `v_projects` (
  `id` int(11) NOT NULL,
  `created` datetime NOT NULL,
  `nombre` varchar(255) NOT NULL,
  `descripcion` varchar(535) NOT NULL,
  `creator` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `v_projects_members`
--

CREATE TABLE `v_projects_members` (
  `id` int(11) NOT NULL,
  `logged` datetime NOT NULL,
  `project_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `permissions` int(11) NOT NULL DEFAULT '3'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `v_promociones`
--

CREATE TABLE `v_promociones` (
  `id` int(11) NOT NULL,
  `tipo` varchar(535) NOT NULL DEFAULT 'promocion',
  `codigo` varchar(535) NOT NULL,
  `descuento` float NOT NULL DEFAULT '0',
  `plata` float NOT NULL DEFAULT '0',
  `usado` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `v_proveedores`
--

CREATE TABLE `v_proveedores` (
  `id` int(11) NOT NULL,
  `nombre` varchar(255) NOT NULL,
  `telefono` varchar(255) NOT NULL,
  `direccion` varchar(255) NOT NULL,
  `contacto` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `ciudad` varchar(255) NOT NULL,
  `pedidos` int(11) NOT NULL DEFAULT '0',
  `ultimo_pedido` varchar(255) NOT NULL DEFAULT 'No Ha Hecho Pedidos'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `v_requests`
--

CREATE TABLE `v_requests` (
  `id` int(11) NOT NULL,
  `user_from` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `user_to` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(535) COLLATE utf8_unicode_ci NOT NULL,
  `charge` int(11) NOT NULL,
  `logged` datetime NOT NULL,
  `status` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Pending'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `v_search`
--

CREATE TABLE `v_search` (
  `id` int(11) NOT NULL,
  `logged` datetime NOT NULL,
  `search` varchar(535) COLLATE utf8_unicode_ci NOT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `v_stats`
--

CREATE TABLE `v_stats` (
  `id` int(11) NOT NULL,
  `logged` datetime NOT NULL,
  `visitor` varchar(535) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Not Recorded',
  `browser_info` varchar(535) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Not Recorded',
  `language` varchar(200) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Not Recorded',
  `referer` varchar(535) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Not Recorded',
  `url` varchar(535) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Not Recorded',
  `page_title` varchar(535) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Not Recorded',
  `time_spent` varchar(535) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Not Recorded',
  `country` varchar(535) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Not Recorded',
  `region` varchar(535) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Not Recorded',
  `city` varchar(535) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Not Recorded',
  `supplier` varchar(535) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Not Recorded',
  `location` varchar(535) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Not Recorded',
  `device` varchar(535) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Not Recorded',
  `browser_name` varchar(535) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Not Recorded',
  `search_engine` varchar(535) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Not Recorded',
  `keywords` varchar(535) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Not Recorded',
  `social_network` varchar(535) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Not Recorded',
  `platform` varchar(535) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Not Recorded'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `v_sucursales`
--

CREATE TABLE `v_sucursales` (
  `id` int(11) NOT NULL,
  `nombre` varchar(535) NOT NULL,
  `direccion` varchar(535) NOT NULL,
  `telefono` varchar(535) NOT NULL,
  `contacto` varchar(535) NOT NULL,
  `email` varchar(535) NOT NULL,
  `horario` varchar(535) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `v_tabla`
--

CREATE TABLE `v_tabla` (
  `id` int(11) NOT NULL,
  `torneo_id` varchar(50) NOT NULL,
  `equipo_id` varchar(50) NOT NULL,
  `posicion` int(11) NOT NULL DEFAULT '0',
  `pts` int(11) NOT NULL DEFAULT '0',
  `pj` int(11) NOT NULL DEFAULT '0',
  `g` int(11) NOT NULL DEFAULT '0',
  `e` int(11) NOT NULL DEFAULT '0',
  `p` int(11) NOT NULL DEFAULT '0',
  `gf` int(11) NOT NULL DEFAULT '0',
  `gc` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `v_tareas`
--

CREATE TABLE `v_tareas` (
  `id` int(11) NOT NULL,
  `logged` datetime NOT NULL,
  `creator` int(11) NOT NULL,
  `tarea` varchar(535) NOT NULL,
  `desc` text NOT NULL,
  `project_id` int(11) NOT NULL,
  `prioridad` tinyint(1) NOT NULL,
  `entrega` datetime NOT NULL,
  `asignados` varchar(535) NOT NULL DEFAULT 'None',
  `completada` varchar(50) NOT NULL DEFAULT 'no',
  `fecha_completada` varchar(255) NOT NULL DEFAULT 'None'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `v_tareas_archivos`
--

CREATE TABLE `v_tareas_archivos` (
  `id` int(11) NOT NULL,
  `logged` datetime NOT NULL,
  `tarea_id` int(11) NOT NULL,
  `name` varchar(535) NOT NULL,
  `url` varchar(535) NOT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `v_timeline`
--

CREATE TABLE `v_timeline` (
  `id` int(11) NOT NULL,
  `logged` datetime NOT NULL,
  `user_from` int(11) NOT NULL,
  `user_to` int(11) NOT NULL,
  `message` text NOT NULL,
  `img` varchar(535) NOT NULL DEFAULT 'None',
  `video` varchar(535) NOT NULL DEFAULT 'None',
  `location` varchar(535) NOT NULL DEFAULT 'None',
  `users` varchar(535) NOT NULL DEFAULT 'None'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `v_timeline_comments`
--

CREATE TABLE `v_timeline_comments` (
  `id` int(11) NOT NULL,
  `post_id` int(11) NOT NULL,
  `user_from` int(11) NOT NULL,
  `logged` datetime NOT NULL,
  `mensaje` text NOT NULL,
  `likes` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `v_time_tracking`
--

CREATE TABLE `v_time_tracking` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `year` varchar(535) NOT NULL,
  `month` varchar(535) NOT NULL,
  `day` varchar(535) NOT NULL,
  `nombre` varchar(535) NOT NULL,
  `project_id` int(11) NOT NULL,
  `tiempo` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `v_torneos`
--

CREATE TABLE `v_torneos` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL DEFAULT 'None',
  `frase` varchar(255) NOT NULL DEFAULT 'None',
  `url` varchar(255) NOT NULL DEFAULT 'None',
  `logo` varchar(255) NOT NULL DEFAULT 'None',
  `img` varchar(255) NOT NULL DEFAULT 'None',
  `color` varchar(50) NOT NULL DEFAULT 'None',
  `logged` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `v_users`
--

CREATE TABLE `v_users` (
  `id` int(11) NOT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password` varchar(535) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `salt` varchar(535) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `joined` datetime NOT NULL,
  `group` int(11) NOT NULL,
  `img` varchar(535) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'uploads/userimg/default.png',
  `cover` varchar(535) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'uploads/userimg/defaultcover.jpg',
  `occupation` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Occupation...',
  `address` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Address...',
  `tel` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Tel...',
  `cel` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Cel...',
  `theme` varchar(535) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'light'
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `v_users`
--

INSERT INTO `v_users` (`id`, `username`, `password`, `email`, `salt`, `name`, `joined`, `group`, `img`, `cover`, `occupation`, `address`, `tel`, `cel`, `theme`) VALUES
  (2, '', '938fc7823b5cd2120e93b18c6e0dd64aee4e528beac327902712b2b07486997e', 'a@a.com', '*–?®	¹M —\rkÑÍ†}¿füê}[''ê‡0­ß4»ˆ“x', 'aa', '2016-11-20 21:14:38', 1, 'uploads/userimg/default.png', 'uploads/userimg/defaultcover.jpg', 'Occupation...', 'Address...', 'Tel...', 'Cel...', 'light'),
  (3, '', '4c7b560305326fffeda9483f13badcdd247c35ebbab5a54bbb29b6d77c5de7a7', 'b@b.com', 'ºh·ªL2üŸq€	1!´Òs¯‚C?áÑ«vAQÄs', 'bb', '2016-11-30 16:46:15', 1, 'uploads/userimg/default.png', 'uploads/userimg/defaultcover.jpg', 'Occupation...', 'Address...', 'Tel...', 'Cel...', 'light'),
  (4, '', 'b1961ff924deee4c750a89cc9401ba983ab3de60ce59a592d833578d3ad5cd43', 'b@b.com', '¨ ¿Šx+ê\Z÷Ü<ûvåö!€ú÷ñ.3ß¡¯D0A´', 'bb', '2016-11-30 16:53:20', 1, 'uploads/userimg/default.png', 'uploads/userimg/defaultcover.jpg', 'Occupation...', 'Address...', 'Tel...', 'Cel...', 'light'),
  (5, '', '33c2f9e8593640bbfd08013ec733885e0f75e4728a260d12d81266f999439e18', 'b@b.com', 'ý;a Ø‡ÐsgO¸Ix½OY\n–â1ˆ±Ù®¤', 'bb', '2016-11-30 16:54:47', 1, 'uploads/userimg/default.png', 'uploads/userimg/defaultcover.jpg', 'Occupation...', 'Address...', 'Tel...', 'Cel...', 'light'),
  (6, '', '49a176763d581e46b27746de87b9541d58f205a258aa3e95854555c6dc67a2f8', 'b@b.com', '–µn.&9  Še\r)¿8Ó§I¥*n{Ï¯"\\‚-KÖã', 'bb', '2016-11-30 16:55:25', 1, 'uploads/userimg/default.png', 'uploads/userimg/defaultcover.jpg', 'Occupation...', 'Address...', 'Tel...', 'Cel...', 'light'),
  (7, '', 'dc195570c00dccad645c22dc42509c6f6cb5ae2d2f0231e16aeed67a2316d1d8', 'b@b.com', 'ßœ«¾ÇÔ	ÞN«Îc-†åK¶€¸PÀMÔ¦#©ŸóÈ²', 'bb', '2016-11-30 16:56:43', 1, 'uploads/userimg/default.png', 'uploads/userimg/defaultcover.jpg', 'Occupation...', 'Address...', 'Tel...', 'Cel...', 'light'),
  (8, '', '6a51bcdeef5520b6f526e29557fd3591076ef6cdac09d598557a070b296266b3', 'c@a.com', '˜ìÖu~0Ík7ºÚ‹^û,ØsWZÈÜÝ™·Q2‘', 'cc', '2016-11-30 16:56:57', 1, 'uploads/userimg/default.png', 'uploads/userimg/defaultcover.jpg', 'Occupation...', 'Address...', 'Tel...', 'Cel...', 'light'),
  (9, '', '79e942aa39ad6781204d75d9e7761a573d6bc5135fa9868266ca5e7a45ae76fd', 'c@a.com', 'Sp [Kób†\0*”ÔeùþýÏöXeïÙÎˆ•x®/', 'cc', '2016-11-30 16:57:30', 1, 'uploads/userimg/default.png', 'uploads/userimg/defaultcover.jpg', 'Occupation...', 'Address...', 'Tel...', 'Cel...', 'light'),
  (10, '', 'd78e8f4cf8ba67eb8794d5e029f07864e30a4e3303929c5b9abe30e6df9ea649', 'd@a.com', 'cèL5*WT}9—I‚Þ¦ÇÞC5Ø ÞŽÀ\ZÜgüg©~', 'dd', '2016-11-30 16:58:27', 1, 'uploads/userimg/default.png', 'uploads/userimg/defaultcover.jpg', 'Occupation...', 'Address...', 'Tel...', 'Cel...', 'light'),
  (11, '', 'a314a450387ed8361349f7f6d3f5613662ff620225865b28994868bfe38d54c3', 'd@a.com', 'Vx»4óô5½®ìÕ–Ney`6­áËÔ>j^ÖÖ', 'dd', '2016-11-30 17:00:09', 1, 'uploads/userimg/default.png', 'uploads/userimg/defaultcover.jpg', 'Occupation...', 'Address...', 'Tel...', 'Cel...', 'light'),
  (12, '', 'd3ef26023edfdfa854997beba39b868566f3771fea066ea2a131d54dfdbd915e', 'e@a.com', '»3uÆÜL2ÿÅ˜·¬àÞäOgà¤øPõwÏ¢ù"Áb', 'ee', '2016-11-30 17:01:05', 1, 'uploads/userimg/default.png', 'uploads/userimg/defaultcover.jpg', 'Occupation...', 'Address...', 'Tel...', 'Cel...', 'light'),
  (13, '', 'f0c37d53b71f198461ce336b09767b4e9fa1f801c726854f80efa426d7c494db', 'm@a.com', 'û&gƒKÖ2ÛÚ\nÍ<ì­È›A=w(ž|#üç²|(', 'mm', '2016-11-30 17:01:52', 1, 'uploads/userimg/default.png', 'uploads/userimg/defaultcover.jpg', 'Occupation...', 'Address...', 'Tel...', 'Cel...', 'light'),
  (14, '', '5faf08ce049e36d140ced88f8c98de91ecbf20812b9f04767e2bb4b3c9979804', 'l@a.com', '¸2QšªÌù[ò#7Rç —­¹úñóHw›¢\nY;pû', 'lll', '2016-11-30 17:04:40', 1, 'uploads/userimg/default.png', 'uploads/userimg/defaultcover.jpg', 'Occupation...', 'Address...', 'Tel...', 'Cel...', 'light'),
  (15, '', '390c17d87cd6b5ab57eb70b64320423b799131da930475eb2c357c9e06aa365f', 'n@a.com', 'jÑ†\rŽcqQu}Jˆ¶b ñ¦Ç»`/À\rÁ m¶', 'nnn', '2016-11-30 17:10:24', 1, 'uploads/userimg/default.png', 'uploads/userimg/defaultcover.jpg', 'Occupation...', 'Address...', 'Tel...', 'Cel...', 'light'),
  (16, '', '305992e2b3ea4858c09097418e37f5d67dfc867583d282ae55bd08503172b943', 'z@a.com', 'UW…\ZûÄðû¢è-„‘öZˆÎ¯M«5ÔV/¸ë¬	Û', 'zzz', '2016-11-30 17:16:55', 1, 'uploads/userimg/default.png', 'uploads/userimg/defaultcover.jpg', 'Occupation...', 'Address...', 'Tel...', 'Cel...', 'light');

-- --------------------------------------------------------

--
-- Table structure for table `v_users_admin`
--

CREATE TABLE `v_users_admin` (
  `id` int(11) NOT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(535) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `salt` varchar(535) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `joined` datetime NOT NULL,
  `group` int(11) NOT NULL,
  `img` varchar(535) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'uploads/userimg/default.png',
  `cover` varchar(535) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'uploads/userimg/defaultcover.jpg',
  `occupation` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Occupation...',
  `address` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Address...',
  `tel` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Tel...',
  `cel` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Cel...',
  `theme` varchar(535) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'light'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `v_users_admin_session`
--

CREATE TABLE `v_users_admin_session` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `hash` varchar(535) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `v_users_session`
--

CREATE TABLE `v_users_session` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `hash` varchar(535) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `v_videos`
--

CREATE TABLE `v_videos` (
  `id` int(11) NOT NULL,
  `torneo_id` varchar(255) NOT NULL,
  `url` varchar(255) NOT NULL,
  `status` varchar(255) NOT NULL DEFAULT 'normal',
  `equipos` varchar(535) NOT NULL DEFAULT 'None'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `baobab_order`
--
ALTER TABLE `baobab_order`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bikini`
--
ALTER TABLE `bikini`
  ADD PRIMARY KEY (`slug`);

--
-- Indexes for table `bikini_bottom`
--
ALTER TABLE `bikini_bottom`
  ADD PRIMARY KEY (`sku`),
  ADD KEY `bikini_bottom_fi_2f973f` (`bikini_slug`);

--
-- Indexes for table `bikini_order`
--
ALTER TABLE `bikini_order`
  ADD PRIMARY KEY (`id`),
  ADD KEY `bikini_order_fi_16b283` (`order_id`);

--
-- Indexes for table `bikini_top`
--
ALTER TABLE `bikini_top`
  ADD PRIMARY KEY (`sku`),
  ADD KEY `bikini_top_fi_2f973f` (`bikini_slug`);

--
-- Indexes for table `countries_shipping_prices`
--
ALTER TABLE `countries_shipping_prices`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `one_piece`
--
ALTER TABLE `one_piece`
  ADD PRIMARY KEY (`slug`);

--
-- Indexes for table `one_piece_order`
--
ALTER TABLE `one_piece_order`
  ADD PRIMARY KEY (`id`),
  ADD KEY `one_piece_order_fi_16b283` (`order_id`);

--
-- Indexes for table `one_piece_stockable`
--
ALTER TABLE `one_piece_stockable`
  ADD PRIMARY KEY (`sku`),
  ADD KEY `one_piece_stockable_fi_254894` (`one_piece_slug`);

--
-- Indexes for table `order`
--
ALTER TABLE `order`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `prices_cop`
--
ALTER TABLE `prices_cop`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `prices_cop_u_f8e02b` (`product_slug`);

--
-- Indexes for table `prices_usd`
--
ALTER TABLE `prices_usd`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `prices_usd_u_f8e02b` (`product_slug`);

--
-- Indexes for table `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`slug`);

--
-- Indexes for table `product_images`
--
ALTER TABLE `product_images`
  ADD PRIMARY KEY (`id`),
  ADD KEY `product_images_fi_3d486b` (`product_slug`);

--
-- Indexes for table `stockable`
--
ALTER TABLE `stockable`
  ADD PRIMARY KEY (`sku`);

--
-- Indexes for table `v_activity`
--
ALTER TABLE `v_activity`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `v_album`
--
ALTER TABLE `v_album`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `v_balance`
--
ALTER TABLE `v_balance`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `v_bids`
--
ALTER TABLE `v_bids`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `v_blog`
--
ALTER TABLE `v_blog`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `v_calendario`
--
ALTER TABLE `v_calendario`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `v_cart`
--
ALTER TABLE `v_cart`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_color` (`color`);

--
-- Indexes for table `v_categories`
--
ALTER TABLE `v_categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `v_chats`
--
ALTER TABLE `v_chats`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `v_clicks`
--
ALTER TABLE `v_clicks`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `v_clientes`
--
ALTER TABLE `v_clientes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `v_colores_producto`
--
ALTER TABLE `v_colores_producto`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id` (`id`),
  ADD KEY `sku` (`sku`),
  ADD KEY `sku_2` (`sku`);

--
-- Indexes for table `v_colors`
--
ALTER TABLE `v_colors`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `v_companias`
--
ALTER TABLE `v_companias`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `v_configuracion`
--
ALTER TABLE `v_configuracion`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `v_contacto`
--
ALTER TABLE `v_contacto`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `v_contenido`
--
ALTER TABLE `v_contenido`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `v_counter`
--
ALTER TABLE `v_counter`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `v_currency`
--
ALTER TABLE `v_currency`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `v_direcciones`
--
ALTER TABLE `v_direcciones`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `v_equipos`
--
ALTER TABLE `v_equipos`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `v_facturas`
--
ALTER TABLE `v_facturas`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `v_facturas_master`
--
ALTER TABLE `v_facturas_master`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `v_favorites`
--
ALTER TABLE `v_favorites`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `sku_2` (`sku`,`user_id`),
  ADD KEY `sku` (`sku`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `v_favors`
--
ALTER TABLE `v_favors`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `v_files`
--
ALTER TABLE `v_files`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `v_following`
--
ALTER TABLE `v_following`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `v_fotos`
--
ALTER TABLE `v_fotos`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `v_friendships`
--
ALTER TABLE `v_friendships`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `v_groups`
--
ALTER TABLE `v_groups`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `v_imagenes`
--
ALTER TABLE `v_imagenes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `v_images`
--
ALTER TABLE `v_images`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `v_info`
--
ALTER TABLE `v_info`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `v_invites`
--
ALTER TABLE `v_invites`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `v_invoices`
--
ALTER TABLE `v_invoices`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `v_menu`
--
ALTER TABLE `v_menu`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `v_modelos`
--
ALTER TABLE `v_modelos`
  ADD PRIMARY KEY (`id`),
  ADD KEY `sku` (`sku`);

--
-- Indexes for table `v_newsletter`
--
ALTER TABLE `v_newsletter`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `v_notas`
--
ALTER TABLE `v_notas`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `v_notifications`
--
ALTER TABLE `v_notifications`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `v_pages`
--
ALTER TABLE `v_pages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `v_partidos`
--
ALTER TABLE `v_partidos`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `v_passwords`
--
ALTER TABLE `v_passwords`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `v_pedidos`
--
ALTER TABLE `v_pedidos`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_color` (`id_color`);

--
-- Indexes for table `v_pedidos_master`
--
ALTER TABLE `v_pedidos_master`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `v_posts`
--
ALTER TABLE `v_posts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `v_premio`
--
ALTER TABLE `v_premio`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `v_productos`
--
ALTER TABLE `v_productos`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `sku` (`sku`);

--
-- Indexes for table `v_productos_complejos`
--
ALTER TABLE `v_productos_complejos`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `v_productos_complejos_sencillos`
--
ALTER TABLE `v_productos_complejos_sencillos`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `v_profileviews`
--
ALTER TABLE `v_profileviews`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `v_projects`
--
ALTER TABLE `v_projects`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `v_projects_members`
--
ALTER TABLE `v_projects_members`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `v_promociones`
--
ALTER TABLE `v_promociones`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `v_proveedores`
--
ALTER TABLE `v_proveedores`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `v_requests`
--
ALTER TABLE `v_requests`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `v_search`
--
ALTER TABLE `v_search`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `v_stats`
--
ALTER TABLE `v_stats`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `v_sucursales`
--
ALTER TABLE `v_sucursales`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `v_tabla`
--
ALTER TABLE `v_tabla`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `v_tareas`
--
ALTER TABLE `v_tareas`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `v_tareas_archivos`
--
ALTER TABLE `v_tareas_archivos`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `v_timeline`
--
ALTER TABLE `v_timeline`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `v_timeline_comments`
--
ALTER TABLE `v_timeline_comments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `v_time_tracking`
--
ALTER TABLE `v_time_tracking`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `v_torneos`
--
ALTER TABLE `v_torneos`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `v_users`
--
ALTER TABLE `v_users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `v_users_admin`
--
ALTER TABLE `v_users_admin`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `v_users_admin_session`
--
ALTER TABLE `v_users_admin_session`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `v_users_session`
--
ALTER TABLE `v_users_session`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `v_videos`
--
ALTER TABLE `v_videos`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `bikini_order`
--
ALTER TABLE `bikini_order`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=51;
--
-- AUTO_INCREMENT for table `countries_shipping_prices`
--
ALTER TABLE `countries_shipping_prices`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=457;
--
-- AUTO_INCREMENT for table `one_piece_order`
--
ALTER TABLE `one_piece_order`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `prices_cop`
--
ALTER TABLE `prices_cop`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `prices_usd`
--
ALTER TABLE `prices_usd`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `product_images`
--
ALTER TABLE `product_images`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `v_activity`
--
ALTER TABLE `v_activity`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `v_album`
--
ALTER TABLE `v_album`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `v_balance`
--
ALTER TABLE `v_balance`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `v_bids`
--
ALTER TABLE `v_bids`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `v_blog`
--
ALTER TABLE `v_blog`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `v_calendario`
--
ALTER TABLE `v_calendario`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `v_cart`
--
ALTER TABLE `v_cart`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `v_categories`
--
ALTER TABLE `v_categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `v_chats`
--
ALTER TABLE `v_chats`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `v_clicks`
--
ALTER TABLE `v_clicks`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `v_clientes`
--
ALTER TABLE `v_clientes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `v_colores_producto`
--
ALTER TABLE `v_colores_producto`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `v_colors`
--
ALTER TABLE `v_colors`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `v_companias`
--
ALTER TABLE `v_companias`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `v_configuracion`
--
ALTER TABLE `v_configuracion`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `v_contacto`
--
ALTER TABLE `v_contacto`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `v_contenido`
--
ALTER TABLE `v_contenido`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `v_counter`
--
ALTER TABLE `v_counter`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `v_currency`
--
ALTER TABLE `v_currency`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `v_direcciones`
--
ALTER TABLE `v_direcciones`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `v_equipos`
--
ALTER TABLE `v_equipos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `v_facturas`
--
ALTER TABLE `v_facturas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `v_facturas_master`
--
ALTER TABLE `v_facturas_master`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `v_favorites`
--
ALTER TABLE `v_favorites`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `v_favors`
--
ALTER TABLE `v_favors`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `v_files`
--
ALTER TABLE `v_files`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `v_following`
--
ALTER TABLE `v_following`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `v_fotos`
--
ALTER TABLE `v_fotos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `v_friendships`
--
ALTER TABLE `v_friendships`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `v_groups`
--
ALTER TABLE `v_groups`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `v_imagenes`
--
ALTER TABLE `v_imagenes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `v_images`
--
ALTER TABLE `v_images`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `v_info`
--
ALTER TABLE `v_info`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `v_invites`
--
ALTER TABLE `v_invites`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `v_invoices`
--
ALTER TABLE `v_invoices`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `v_menu`
--
ALTER TABLE `v_menu`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `v_modelos`
--
ALTER TABLE `v_modelos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `v_newsletter`
--
ALTER TABLE `v_newsletter`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `v_notas`
--
ALTER TABLE `v_notas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `v_notifications`
--
ALTER TABLE `v_notifications`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `v_pages`
--
ALTER TABLE `v_pages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `v_partidos`
--
ALTER TABLE `v_partidos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `v_passwords`
--
ALTER TABLE `v_passwords`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `v_pedidos`
--
ALTER TABLE `v_pedidos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=26;
--
-- AUTO_INCREMENT for table `v_pedidos_master`
--
ALTER TABLE `v_pedidos_master`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `v_posts`
--
ALTER TABLE `v_posts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `v_premio`
--
ALTER TABLE `v_premio`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `v_productos`
--
ALTER TABLE `v_productos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=104;
--
-- AUTO_INCREMENT for table `v_productos_complejos`
--
ALTER TABLE `v_productos_complejos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `v_productos_complejos_sencillos`
--
ALTER TABLE `v_productos_complejos_sencillos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `v_profileviews`
--
ALTER TABLE `v_profileviews`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `v_projects`
--
ALTER TABLE `v_projects`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `v_projects_members`
--
ALTER TABLE `v_projects_members`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `v_promociones`
--
ALTER TABLE `v_promociones`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `v_proveedores`
--
ALTER TABLE `v_proveedores`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `v_requests`
--
ALTER TABLE `v_requests`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `v_search`
--
ALTER TABLE `v_search`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `v_stats`
--
ALTER TABLE `v_stats`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `v_sucursales`
--
ALTER TABLE `v_sucursales`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `v_tabla`
--
ALTER TABLE `v_tabla`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `v_tareas`
--
ALTER TABLE `v_tareas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `v_tareas_archivos`
--
ALTER TABLE `v_tareas_archivos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `v_timeline`
--
ALTER TABLE `v_timeline`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `v_timeline_comments`
--
ALTER TABLE `v_timeline_comments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `v_time_tracking`
--
ALTER TABLE `v_time_tracking`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `v_torneos`
--
ALTER TABLE `v_torneos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `v_users`
--
ALTER TABLE `v_users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `v_users_admin`
--
ALTER TABLE `v_users_admin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `v_users_admin_session`
--
ALTER TABLE `v_users_admin_session`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `v_users_session`
--
ALTER TABLE `v_users_session`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `v_videos`
--
ALTER TABLE `v_videos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `bikini`
--
ALTER TABLE `bikini`
  ADD CONSTRAINT `bikini_fk_6857a4` FOREIGN KEY (`slug`) REFERENCES `product` (`slug`) ON DELETE CASCADE;

--
-- Constraints for table `bikini_bottom`
--
ALTER TABLE `bikini_bottom`
  ADD CONSTRAINT `bikini_bottom_fk_2f973f` FOREIGN KEY (`bikini_slug`) REFERENCES `bikini` (`slug`),
  ADD CONSTRAINT `bikini_bottom_fk_e3692b` FOREIGN KEY (`sku`) REFERENCES `stockable` (`sku`) ON DELETE CASCADE;

--
-- Constraints for table `bikini_order`
--
ALTER TABLE `bikini_order`
  ADD CONSTRAINT `bikini_order_fk_16b283` FOREIGN KEY (`order_id`) REFERENCES `baobab_order` (`id`);

--
-- Constraints for table `bikini_top`
--
ALTER TABLE `bikini_top`
  ADD CONSTRAINT `bikini_top_fk_2f973f` FOREIGN KEY (`bikini_slug`) REFERENCES `bikini` (`slug`),
  ADD CONSTRAINT `bikini_top_fk_e3692b` FOREIGN KEY (`sku`) REFERENCES `stockable` (`sku`) ON DELETE CASCADE;

--
-- Constraints for table `one_piece`
--
ALTER TABLE `one_piece`
  ADD CONSTRAINT `one_piece_fk_6857a4` FOREIGN KEY (`slug`) REFERENCES `product` (`slug`) ON DELETE CASCADE;

--
-- Constraints for table `one_piece_order`
--
ALTER TABLE `one_piece_order`
  ADD CONSTRAINT `one_piece_order_fk_16b283` FOREIGN KEY (`order_id`) REFERENCES `baobab_order` (`id`);

--
-- Constraints for table `one_piece_stockable`
--
ALTER TABLE `one_piece_stockable`
  ADD CONSTRAINT `one_piece_stockable_fk_254894` FOREIGN KEY (`one_piece_slug`) REFERENCES `one_piece` (`slug`),
  ADD CONSTRAINT `one_piece_stockable_fk_e3692b` FOREIGN KEY (`sku`) REFERENCES `stockable` (`sku`) ON DELETE CASCADE;

--
-- Constraints for table `prices_cop`
--
ALTER TABLE `prices_cop`
  ADD CONSTRAINT `prices_cop_fk_3d486b` FOREIGN KEY (`product_slug`) REFERENCES `product` (`slug`);

--
-- Constraints for table `prices_usd`
--
ALTER TABLE `prices_usd`
  ADD CONSTRAINT `prices_usd_fk_3d486b` FOREIGN KEY (`product_slug`) REFERENCES `product` (`slug`);

--
-- Constraints for table `product_images`
--
ALTER TABLE `product_images`
  ADD CONSTRAINT `product_images_fk_3d486b` FOREIGN KEY (`product_slug`) REFERENCES `product` (`slug`);

--
-- Constraints for table `v_colores_producto`
--
ALTER TABLE `v_colores_producto`
  ADD CONSTRAINT `id_colores_producto_sku` FOREIGN KEY (`sku`) REFERENCES `v_productos` (`sku`);

--
-- Constraints for table `v_direcciones`
--
ALTER TABLE `v_direcciones`
  ADD CONSTRAINT `v_direcciones_to_users` FOREIGN KEY (`user_id`) REFERENCES `v_users` (`id`);

--
-- Constraints for table `v_favorites`
--
ALTER TABLE `v_favorites`
  ADD CONSTRAINT `favorites_to_products` FOREIGN KEY (`sku`) REFERENCES `v_productos` (`sku`),
  ADD CONSTRAINT `favorites_to_user_ids` FOREIGN KEY (`user_id`) REFERENCES `v_users` (`id`);

--
-- Constraints for table `v_modelos`
--
ALTER TABLE `v_modelos`
  ADD CONSTRAINT `id_modelos_producto_productos` FOREIGN KEY (`sku`) REFERENCES `v_productos` (`sku`);

DELIMITER $$
--
-- Events
--
CREATE DEFINER=`baobab_admin`@`localhost` EVENT `check_order_2-1480372039` ON SCHEDULE AT '2016-11-28 19:27:19' ON COMPLETION NOT PRESERVE ENABLE DO BEGIN
  START TRANSACTION;
  SET @order_id = '2-1480372039';
  SET @order_status = (SELECT order_status FROM baobab_order WHERE baobab_order.`id` = @order_id);
  IF(@order_status = 'pending') THEN
    UPDATE baobab_order SET baobab_order.`order_status` = 1 WHERE baobab_order.`id` = @order_id;

    UPDATE bikini_top SET bikini_top.`stock_amount` =
    bikini_top.`stock_amount` + (SELECT amount FROM bikini_order WHERE bikini_order.`top_sku` = bikini_top.`sku` AND bikini_order.`order_id` = @order_id);

    UPDATE bikini_bottom SET bikini_bottom.`stock_amount` =
    bikini_bottom.`stock_amount` + (SELECT amount FROM bikini_order WHERE bikini_order.`bottom_sku` = bikini_bottom.`sku` AND bikini_order.`order_id` = @order_id);

    UPDATE one_piece_stockable SET one_piece_stockable.`stock_amount` =
    one_piece_stockable.`stock_amount` + (SELECT amount FROM one_piece_order WHERE one_piece_order.`top_sku` = one_piece_stockable.`sku` AND one_piece_order.`order_id` = @order_id);
  END IF;
  COMMIT;
END$$

CREATE DEFINER=`baobab_admin`@`localhost` EVENT `check_order_2-1480371401` ON SCHEDULE AT '2016-11-28 19:16:41' ON COMPLETION NOT PRESERVE ENABLE DO BEGIN
  START TRANSACTION;
  SET @order_id = '2-1480371401';
  SET @order_status = (SELECT order_status FROM baobab_order WHERE baobab_order.`id` = @order_id);
  IF(@order_status = 'pending') THEN
    UPDATE baobab_order SET baobab_order.`order_status` = 1 WHERE baobab_order.`id` = @order_id;

    UPDATE bikini_top SET bikini_top.`stock_amount` =
    bikini_top.`stock_amount` + (SELECT amount FROM bikini_order WHERE bikini_order.`top_sku` = bikini_top.`sku` AND bikini_order.`order_id` = @order_id);

    UPDATE bikini_bottom SET bikini_bottom.`stock_amount` =
    bikini_bottom.`stock_amount` + (SELECT amount FROM bikini_order WHERE bikini_order.`bottom_sku` = bikini_bottom.`sku` AND bikini_order.`order_id` = @order_id);

    UPDATE one_piece_stockable SET one_piece_stockable.`stock_amount` =
    one_piece_stockable.`stock_amount` + (SELECT amount FROM one_piece_order WHERE one_piece_order.`top_sku` = one_piece_stockable.`sku` AND one_piece_order.`order_id` = @order_id);
  END IF;
  COMMIT;
END$$

DELIMITER ;
