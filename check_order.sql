SET GLOBAL event_scheduler = ON;

delimiter |
CREATE EVENT `check_order` ON SCHEDULE AT current_timestamp() + INTERVAL 2 HOUR DO
  BEGIN
    START TRANSACTION;
    SET @order_id = '2-1479699956';
    SET @order_status = (SELECT order_status FROM baobab_order WHERE baobab_order.`id` = @order_id);
    IF(@order_status = 'pending') THEN
      UPDATE baobab_order SET baobab_order.`order_status` = 1 WHERE baobab_order.`id` = @order_id;

      # update bikini tops stocks
      UPDATE bikini_top SET bikini_top.`stock_amount` =
      bikini_top.`stock_amount` + (SELECT amount FROM bikini_order WHERE bikini_order.`top_sku` = bikini_top.`sku`);

      # update bikini bottoms stocks
      UPDATE bikini_bottom SET bikini_bottom.`stock_amount` =
      bikini_bottom.`stock_amount` + (SELECT amount FROM bikini_order WHERE bikini_order.`bottom_sku` = bikini_bottom.`sku`);

      # update one_piece stocks
      UPDATE one_piece_stockable SET one_piece_stockable.`stock_amount` =
      one_piece_stockable.`stock_amount` + (SELECT amount FROM one_piece_order WHERE one_piece_order.`top_sku` = one_piece_stockable.`sku`);
    END IF;
    COMMIT;
  END |
delimiter ;
