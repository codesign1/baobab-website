/**
 * Created by jsrolon on 08-07-2016.
 */

var innerHeight;
var baobabTitle;
var navTitle;
var COLLAPSED_HEIGHT_CONSTANT = 1.2;
var documentHeight;

window.addEventListener('load', function () {
    innerHeight = window.innerHeight;
    var headerBackground = document.getElementById('header-background');
    var header = document.querySelector('header');
    baobabTitle = document.querySelector('#nav-title > h2');
    navTitle = document.querySelector('#nav-title');
    documentHeight = $(document).height();

    if (headerBackground) {
        handleScrollInHome();
    } else {
        handleScroll();
    }

    if (window.pageYOffset > innerHeight - 1) {
        if (!header.classList.contains('scrolled')) {
            header.classList.add('scrolled');
        }
    }

    var hwga = $('#how-we-give a');
    if (hwga) {
        hwga.smoothScroll();
    }
});

function handleScrollInHome() {
    var textColorScale = chroma.scale(['#fff', '#000']).domain([0, innerHeight]);
    var titleMarginBeforeScroll = interpolate([2, 0], [0, innerHeight]);
    var titleBottomMarginBeforeScroll = interpolate([3.5, 2], [0, innerHeight]);
    var titleMarginAfterScroll = interpolate([0, -2.5], [innerHeight, innerHeight * COLLAPSED_HEIGHT_CONSTANT]);
    var navTitleSize = interpolate([4, 2.5], [innerHeight, innerHeight * COLLAPSED_HEIGHT_CONSTANT]);
    var backgroundOpacityScale = interpolate([0, 1], [0, innerHeight]);

    var header = document.querySelector('header');
    var baobabNavLinks = document.querySelector('.nav-menu a');
    var headerBackground = document.getElementById('header-background');

    var socialMediaIcons = document.getElementById('social-media-header');

    var shopMenu = $('.shop-menu');

    var previousYOffset = window.pageYOffset;

    window.addEventListener('scroll', debounce(function (e) {
        var scrolledDistance = window.pageYOffset;
        header.style.color = textColorScale(scrolledDistance).hex();
        baobabTitle.style.color = textColorScale(scrolledDistance).hex();
        $('.nav-menu a').css('color', textColorScale(scrolledDistance).hex());
        headerBackground.style.opacity = backgroundOpacityScale(scrolledDistance);

        if (shopMenu.hasClass('nav-menu-position-fixed')) {
            shopMenu.removeClass('nav-menu-position-fixed');
        }

        if (shopMenu.hasClass('nav-menu-position-bottom')) {
            shopMenu.removeClass('nav-menu-position-bottom');
        }

        if (scrolledDistance >= innerHeight) {
            if (!shopMenu.hasClass('nav-menu-position-fixed')) {
                shopMenu.addClass('nav-menu-position-fixed');
            }

            baobabTitle.style.fontSize = navTitleSize(scrolledDistance) + 'rem';
            navTitle.style.marginTop = titleMarginAfterScroll(scrolledDistance) + 'rem';
            socialMediaIcons.style.opacity = 0;

            header.classList.add('collapsed');
            document.getElementById('nav-item-home').classList.remove('active');
            document.getElementById('nav-item-shop').classList.add('active');

            if (scrolledDistance >= innerHeight * COLLAPSED_HEIGHT_CONSTANT) {
                baobabTitle.style.fontSize = '2.5rem';
                if (!header.classList.contains('collapsed')) {
                    header.classList.add('collapsed');
                }
            }

            if (scrolledDistance >= documentHeight - ($('footer').height() + shopMenu.height() + 250)) {
                if (shopMenu.hasClass('nav-menu-position-fixed')) {
                    shopMenu.removeClass('nav-menu-position-fixed');
                    shopMenu.addClass('nav-menu-position-bottom');
                }
            }
        } else {
            baobabTitle.style.fontSize = '4rem';
            navTitle.style.marginTop = titleMarginBeforeScroll(scrolledDistance) + 'rem';
            navTitle.style.marginBottom = titleBottomMarginBeforeScroll(scrolledDistance) + 'rem';
            socialMediaIcons.style.opacity = 1;

            if (header.classList.contains('scrolled')) {
                header.classList.remove('scrolled');
            }
            document.getElementById('nav-item-shop').classList.remove('active');
            document.getElementById('nav-item-home').classList.add('active');
        }
        if (scrolledDistance < innerHeight) {
            if (header.classList.contains('collapsed')) {
                header.classList.remove('collapsed');
            }
        }
        previousYOffset = scrolledDistance;
    }, 15));
}

function handleScroll() {
    var previousScrollVal = 0;
    window.addEventListener('scroll', debounce(function (e) {
        var scrolledDistance = window.pageYOffset;
        var header = document.querySelector('header');
        var navTitleSize = interpolate([4, 2.5], [0, innerHeight * 0.1]);
        var titleMarginAfterScroll = interpolate([0, -2.5], [0, innerHeight * 0.1]);
        navTitle.style.marginTop = titleMarginAfterScroll(scrolledDistance) + 'rem';
        var hwg = $('#hwg-menu');

        if (document.title.indexOf('Shop') !== -1) {
            var shopMenu = $('.shop-menu');
        }

        if (scrolledDistance > 0) {
            if (shopMenu) {
                if (shopMenu.hasClass('nav-menu-position-fixed')) {
                    shopMenu.removeClass('nav-menu-position-fixed');
                }

                if (shopMenu.hasClass('nav-menu-position-bottom')) {
                    shopMenu.removeClass('nav-menu-position-bottom');
                }
            }

            if(scrolledDistance < innerHeight * 0.1) {
                if (hwg) {
                    if(hwg.hasClass('fixed')) {
                        hwg.removeClass('fixed');
                    }
                }
            }

            baobabTitle.style.fontSize = navTitleSize(scrolledDistance) + 'rem';

            if (scrolledDistance >= innerHeight * 0.1) {
                baobabTitle.style.fontSize = '2.5rem';
                navTitle.style.marginTop = '-2.5rem';
                if (!header.classList.contains('collapsed')) {
                    header.classList.add('collapsed');
                }

                if (shopMenu) {
                    if (!shopMenu.hasClass('nav-menu-position-fixed')) {
                        shopMenu.addClass('nav-menu-position-fixed');
                    }
                }

                if (shopMenu && scrolledDistance >= documentHeight - ($('footer').height() + shopMenu.height() + 200)) {
                    if (shopMenu.hasClass('nav-menu-position-fixed')) {
                        shopMenu.removeClass('nav-menu-position-fixed');
                        shopMenu.addClass('nav-menu-position-bottom');
                    }
                }

                if (hwg) {
                    if(!hwg.hasClass('fixed')) {
                        hwg.addClass('fixed');
                    }
                }

                if (hwg && scrolledDistance >= documentHeight - ($('footer').height() + 550)) {
                    if(hwg.hasClass('fixed')) {
                        hwg.removeClass('fixed');
                    }
                }
            }
        } else {
            baobabTitle.style.fontSize = '4rem';
            if (header.classList.contains('collapsed')) {
                header.classList.remove('collapsed');
            }
        }
        previousScrollVal = scrolledDistance;
    }));
}

/**
 * Creates a linear interpolation function that gives values in range for each value in the domain
 * @param range
 * @param domain
 * @returns {Function}
 */
function interpolate(range, domain) {
    return function (value) {
        return range[0] + (range[1] - range[0]) * ((value - domain[0]) / (domain[1] - domain[0]));
    }
}

/**
 * Limits the amount of times func can be called, depending on a wait
 * @param func
 * @param wait
 * @param immediate
 * @returns {Function}
 */
function debounce(func, wait, immediate) {
    var timeout;
    return function () {
        var context = this, args = arguments;
        var later = function () {
            timeout = null;
            if (!immediate) func.apply(context, args);
        };
        var callNow = immediate && !timeout;
        clearTimeout(timeout);
        timeout = setTimeout(later, wait);
        if (callNow) func.apply(context, args);
    };
}
