/**
 * Created by jsrolon on 14-07-2016.
 */

window.addEventListener('load', function () {
    $('div.item-image').zoom();
    if ($('#item-slug').length) {
        setUpItemOptionsChanges();
    }
});

function submitPaginationForm(actionURL) {
    var form = $('#shop-filter-form');
    form.attr('action', actionURL);
    form.submit();
}

function getItemRefString() {
    var selectedBottoms = $('input[type=radio][name=bottom-size]:checked').val();
    var refString = $('#item-slug').html()
        + '::'
        + $('#item-category').html()
        + '::'
        + $('input[type=radio][name=top-size]:checked').val()
    if (selectedBottoms) {
        refString +=
            '::'
            + selectedBottoms
            + '::'
            + $('input[type=radio][name=pushup-cups]:checked').val();
    }
    console.log(refString);
    return refString;
}

function setUpItemOptionsChanges() {
    var slug = $('#item-slug').html();
    $.post("/api/get_availability", {
        slug: slug
    }, function (res) {
        res = JSON.parse(res);
        console.log(res);

        // execute one availability change, check the selection string status,
        // then bind the functions to recheck the status
        drawAvailability(res);
        checkAddToCartFeasibility();

        $('.item-info .item-availability-selector input[type=radio]').on('change', function () {
            // console.log('changed pushup');
            // change the displayed price
            var realPrice = parseInt($('div#real-item-price').html());
            console.log(realPrice);

            var newPrice = realPrice;

            var currency = $('#currency-display').html();
            var status = $('.item-info .item-availability-selector input[type=radio][name=pushup-cups]:checked').val();
            if (status === 'true') {
                newPrice = realPrice + ((currency === 'COP') ? 2000 : 2);
            } else {
                newPrice = realPrice;
            }
            var newPriceString = (newPrice + "").replace(/(.)(?=(\d{3})+$)/g, '$1,');
            $('#item-price-display').html(newPriceString);
            // drawAvailability(res);
        });

        $('.item-info input[type=radio]').on('change', function () {
            checkAddToCartFeasibility();
        });
    });
}

function drawAvailability(availability) {
    // for top and bottom
    $('.item-sizes-selection').each(function (i, piece) {

        // for each one of the sizes
        $(this).find('input[type=radio]').each(function (j, sizeSelector) {
            // split by dash to know which size it represents
            var elemData = sizeSelector.id.split('-');
            var size = elemData[2].toUpperCase();

            var sizeAvailability = availability[i][size] || [];
            console.log(sizeSelector);

            var disabled = true;
            if (sizeAvailability.length > 1) { // means that it exists, and it's a top
                // pushUpEnabled is the second array member
                $('.item-availability-selector input[type=radio]').attr('disabled', !sizeAvailability[1]);
                disabled = false;
            } else if (sizeAvailability.length > 0) { // means there is availability
                disabled = false;
            }
            $(sizeSelector).prop('disabled', !sizeAvailability[0]);
            sizeSelector.checked = false;
        });
    });
}

function checkAddToCartFeasibility() {
    // set up the strings
    var topSizeSelectors = $('#top-size-selection input[type=radio]:not([disabled])');
    var bottomSizeSelectors = $('#bottom-size-selection input[type=radio]:not([disabled])');

    var condition;

    var atLeastOneTopSelected = false;
    topSizeSelectors.each(function (i, selector) {
        var checked = $(selector).prop('checked');
        atLeastOneTopSelected = atLeastOneTopSelected || checked;
    });
    condition = atLeastOneTopSelected;

    if (bottomSizeSelectors.length > 0) {
        var atLeastOneBottomSelected = false;
        bottomSizeSelectors.each(function (i, selector) {
            // console.log(selector);
            var checked = $(selector).prop('checked');
            atLeastOneBottomSelected = atLeastOneBottomSelected || checked;
        });
        condition &= atLeastOneBottomSelected;
    }


    // get the button to disable/enable
    var addToCartButton = $('#item-add-to-cart-button');
    addToCartButton.prop('disabled', !condition);
}

function toggleCurrencyMenu() {
    $('#currency-selection-list').toggle();
}

function changeCurrency(currency) {
    Cookies.set('currency', currency);
    $("#currency-display").html(currency);
    location.reload();
}

function discoverYourSize() {
    var topSize = $('.sizing-form #top-size').val();
    var bottomSize = parseInt($('.sizing-form #bottom-size').val());
    var sizingCountry = parseInt($('.sizing-form #bottom-sizing-country').val());

    if (topSize && bottomSize) {
        topSize = topSize.toUpperCase();
        var baobabTopSize = 'M';
        if (topSize === 'AA' || topSize === 'A') {
            baobabTopSize = 'S';
        } else if (topSize >= 'D') {
            baobabTopSize = 'L';
        }

        $('p#baobab-top-size').html(baobabTopSize);//

        /**
         * We use the very HTML table to find the sizes. Otherwise it would be an if-fest.
         */
        var htmlTable = $('#sizes-table');
        for (var i = 1; i < htmlTable.children().children().length; i++) {
            var rangeString = $(htmlTable.children().children()[i].cells[sizingCountry]).html().split(' - ');
            var minRange = parseInt(rangeString[0]); // the ranges specified in the table are not complete,
            var maxRange = parseInt(rangeString[1]) + 1; // so we add 1 to make them overlap
            if(bottomSize >= minRange && bottomSize <= maxRange) {
                $('p#baobab-bottom-size').html($(htmlTable.children().children()[i].children[0]).html());
            }
        }
        openDiscoverYourSize();
    }
}
