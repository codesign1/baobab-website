/**
 * Created by jpc on 21-07-2016.
 */

function get_random_tree() {
	var random_trees = ['almendro', 'balso', 'acacio', 'cedro'];
	var randomNumber = Math.floor(Math.random()*random_trees.length);
	return random_trees[randomNumber];
}

$('#openwhatyougive').click(function(event){
	event.stopPropagation();
	if(!$('#whatwegive').hasClass('open')) {
		$('#whatwegive').addClass('open');
		$('#whatwegive').fadeToggle('open');
		var tree = get_random_tree();
		$('#whatwegivehimg').attr('src', "/assets/img/" + tree + ".png");
		$('#whatwegiveh2').html(tree);
	} 
});

$('#cerrarwhatwegive').click(function(event){
	event.stopPropagation();
	if($('#whatwegive').hasClass('open')) {
		$('#whatwegive').removeClass('open');
		$('#whatwegive').fadeToggle('open');
	} 
});

$('#opensizeing-pop').click(function(event){
	event.stopPropagation();
	if(!$('#sizeing-pop').hasClass('open')) {
		$('#sizeing-pop').addClass('open');
		$('#sizeing-pop').fadeToggle('open');
	}
});

$('#cerrarsizeing-pop').click(function(event){
	event.stopPropagation();
	if($('#sizeing-pop').hasClass('open')) {
		$('#sizeing-pop').removeClass('open');
		$('#sizeing-pop').fadeToggle('open');
	}
});

$('#openshipping-pop').click(function(event){
	event.stopPropagation();
	if(!$('#shipping-pop').hasClass('open')) {
		$('#shipping-pop').addClass('open');
		$('#shipping-pop').fadeToggle('open');
	}
});

$('#cerrarshipping-pop').click(function(event){
	event.stopPropagation();
	if($('#shipping-pop').hasClass('open')) {
		$('#shipping-pop').removeClass('open');
		$('#shipping-pop').fadeToggle('open');
	}
});

$('#openvideo-pop').click(function(event){
	event.stopPropagation();
	if(!$('#video-pop').hasClass('open')) {
		$('#video-pop').addClass('open');
		$('#video-pop').fadeToggle('open');
	}
	document.getElementById('myvideo').play();
});

$('#cerrarvideo-pop').click(function(event){
	event.stopPropagation();
	if($('#video-pop').hasClass('open')) {
		$('#video-pop').removeClass('open');
		$('#video-pop').fadeToggle('open');
	}
	document.getElementById('myvideo').pause();
});

$('#whatwegive .cont60').click(function(event){
	event.stopPropagation();
});

$('html').click(function(event){
	if($('#whatwegive').hasClass('open')) {
		$('#whatwegive').removeClass('open');
		$('#whatwegive').fadeToggle('open');
	} 
});

$('#icon-mobile').click(function(){
	$('#menu-mobile-div').fadeIn();
});

$('#closenavmob').click(function(){
	$('#menu-mobile-div').fadeOut();
});

/**
 * Discover your size code
 */
function openDiscoverYourSize() {
    event.stopPropagation();
    if(!$('#discoveryoursize').hasClass('open')) {
        $('#discoveryoursize').addClass('open');
        $('#discoveryoursize').fadeToggle('open');
        var tree = get_random_tree();
        $('#discoveryoursizehimg').attr('src', "/assets/img/" + tree + ".png");
        $('#discoveryoursizeh2').html(tree);
    }
};

function cerrarDiscoverYourSize() {
    // event.stopPropagation();
    if($('#discoveryoursize').hasClass('open')) {
        $('#discoveryoursize').removeClass('open');
        $('#discoveryoursize').fadeToggle('open');
    }
};

function cerrarSuccessRegister() {
    $('#successregister').css('display', 'none');
};

$('#discoveryoursize .cont60').click(function(event){
    event.stopPropagation();
});

$('html').click(function(event){
    if($('#discoveryoursize').hasClass('open')) {
        $('#discoveryoursize').removeClass('open');
        $('#discoveryoursize').fadeToggle('open');
    }
});
