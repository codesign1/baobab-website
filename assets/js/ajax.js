/**
 * Created by jsrolon on 08-07-2016.
 */

$(document).ready(function() {
    NProgress.configure({ showSpinner: false });
});

function addToFavorites(sku) {
    return $.post( "/api/add_to_favorites", {
        sku: sku
    }, function(res) {
        // flash the heart sign
        var icon = $('#icon-favorites');
        icon.fadeOut(250).fadeIn(250).fadeOut(250).fadeIn(250);
    });
}

function removeFromFavorites(sku) {
    return $.post( "/api/remove_from_favorites", {
        sku: sku
    }, function(res) {
        // flash the heart sign
        var icon = $('#icon-favorites');
        icon.fadeOut(250).fadeIn(250).fadeOut(250).fadeIn(250);
    });
}

function toggleItemFavorites(sku) {
    var button = $('#favorite-item-button');
    if(button.hasClass('active')) {
        removeFromFavorites(sku).done(function() {
            button.removeClass('active');
        });
    } else {
        addToFavorites(sku).done(function() {
            button.addClass('active');
        });
    }
}

function addToCart(ref) {
    NProgress.start();
    $.post( "/api/add_to_cart", {
        ref: ref
    }, function(res) {
        NProgress.done();
        $('#cart-box').html(res);
        openCartBox();
    });
}

function openCartBox() {
    var cartBox = $('#cart-box');
    if(cartBox.hasClass('transparent')) {
        cartBox.removeClass('transparent');
        cartBox.removeClass('send-to-back');
        cartBox.addClass('opaque');
        cartBox.addClass('bring-to-top');
    }
}

function removeFromCart(ref) {
    NProgress.start();
    $.post( "/api/remove_from_cart", {
        ref: ref
    }, function() {
        NProgress.done();
        location.reload();
    });
}

function emptyCart() {
    NProgress.start();
    $.post( "/api/empty_cart", function(res) {
        NProgress.done();
        $('#cart-box').html(res);
        openCartBox();
    });
}

function increaseAmountInCart(ref) {
    NProgress.start();
    $.post( "/api/modify_amount_in_cart", {
        ref: ref,
        amount: 1
    }, function(res) {
        NProgress.done();
        modifyCartPageDisplay(res);
    });
}

function decreaseAmountInCart(ref) {
    NProgress.start();
    $.post( "/api/modify_amount_in_cart", {
        ref: ref,
        amount: -1
    }, function(res) {
        NProgress.done();
        modifyCartPageDisplay(res);
    });
}

function openSearch() {
    var searchBox = $('#search-box');
    if(searchBox.hasClass('transparent')) {
        searchBox.removeClass('transparent');
        searchBox.removeClass('send-to-back');
        searchBox.addClass('opaque');
        searchBox.addClass('bring-to-top');
    } else {
        searchBox.removeClass('opaque');
        searchBox.addClass('transparent');
        setTimeout(function () {
            searchBox.removeClass('bring-to-top');
            searchBox.addClass('send-to-back');
        }, 550);
    }
}


function toggleCartBox() {
    var cartBox = $('#cart-box');
    if(cartBox.hasClass('transparent')) {
        cartBox.removeClass('transparent');
        cartBox.removeClass('send-to-back');
        cartBox.addClass('opaque');
        cartBox.addClass('bring-to-top');
    } else {
        cartBox.removeClass('opaque');
        cartBox.addClass('transparent');
        setTimeout(function () {
            cartBox.removeClass('bring-to-top');
            cartBox.addClass('send-to-back');
        }, 550);
    }
}

function modifyCartPageDisplay(res) {
    res = JSON.parse(res);
    console.log(res);
    var divId = '#cart-item-' + res['item_div_id'];
    var div = $(divId);
    console.log(div);
    // change the values for the item
    div.find('.cart-item-amount').html(res['amount']);
    div.find('.price-display span').html(res['item_total']);

    // change the total values
    $('#total-price span').html(res['cart_total']);
    // $('#total-shipping-price').html(res['cart_total']);

    // cart box item modifications
    $('#cart-box-item-' + res['item_div_id']).find('.cart-box-item-amount').html(res['amount']);
    $('#cart-box-total-price').html(res['cart_total']);
}

function toggleSizes() {
    $('#sizes-sublist').toggle(400);
}

window.addEventListener('load', function () {
    $(document).on("keyup", "#search-input", function(event){
        event.preventDefault();
        var query = $("#search-input").val()
        if($("#search-input").val().length >= 2) {
            $.ajax({
                type:"POST",
                url: "/api/search",
                data:"query=" + query,
                success:function(e) {
                    console.log(e);
                    $("#search-results").html(e)
                }
            });
        }
        if($("#search-input").val().length < 2) {
            $.ajax({
                type:"POST",
                url: "/api/search",
                data:"query=featured",
                success:function(e) {
                    console.log(e);
                    $("#search-results").html(e)
                }
            });
        }
    });
});
