/**
 * Created by jsrolon on 21-11-2016.
 */

$(document).ready(function () {
    var countryInput = $('select#checkout-info-country-input');
    if (countryInput.length > 0) {
        $(countryInput).change(function () {
            NProgress.start();
            var countryCode = countryInput.val();
            $.post('/api/shipping_price_for', {
                countryCode: countryCode
            }).then(function (res) {
                var resString = (res + "").replace(/(.)(?=(\d{3})+$)/g,'$1,');
                var currentPrice = parseInt($('span#checkout-info-total-value').html().replace(/(\.|,)/g, ''));

                $('span#checkout-info-shipping-cost').html(resString);
                var newPrice = ((parseInt(res) + currentPrice) + "").replace(/(.)(?=(\d{3})+$)/g,'$1,');
                $('span#checkout-info-total-with-shipping').html(newPrice);
                NProgress.done();
            });
        });
    }
});

function addCommas(nStr) {
    nStr += '';
    x = nStr.split('.');
    x1 = x[0];
    x2 = x.length > 1 ? '.' + x[1] : '';
    var rgx = /(\d+)(\d{3})/;
    while (rgx.test(x1)) {
        x1 = x1.replace(rgx, '$1' + ',' + '$2');
    }
    return x1 + x2;
}
