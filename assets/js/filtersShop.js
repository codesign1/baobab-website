/**
 * Created by jsrolon on 21-07-2016.
 */

function onSubmitSave() {
    // alert($('#size-m').is(':checked'));
    window.localStorage.setItem('precio-min', $('#precio-min').val());
    window.localStorage.setItem('precio-max', $('#precio-max').val());
    window.localStorage.setItem('size-xs', $('#size-xs').prop('checked'));
    window.localStorage.setItem('size-s', $('#size-s').prop('checked'));
    window.localStorage.setItem('size-m', $('#size-m').prop('checked'));
    window.localStorage.setItem('size-l', $('#size-l').prop('checked'));
    window.localStorage.setItem('size-xl', $('#size-xl').prop('checked'));
}
