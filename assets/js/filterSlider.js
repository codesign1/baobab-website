/**
 * Created by jsrolon on 21-07-2016.
 */

window.onload = function () {
    // all the dom content is ready, get the DOM objects we're operating on
    var priceSlider = document.getElementById('price-slider'); // slider needs the plain DOM object
    if (priceSlider) {
        var priceMin = $('#precio-min');
        var priceMax = $('#precio-max');

        // get the price range from the server
        var minPrice = parseInt(priceMin.attr('min'));
        var maxPrice = parseInt(priceMax.attr('max'));

        // establish the money format for to and for functions
        var moneyFormat = wNumb({
            decimals: 0,
            thousand: ',',
            prefix: '$'
        });

        // create the UIslider with required max and min values
        var step = $('#currency-display').html() === 'COP' ? 10000 : 5;
        noUiSlider.create(priceSlider, {
            start: [priceMin.val(), priceMax.val()],
            step: step, // COP
            margin: step,
            connect: true,
            orientation: 'horizontal',
            tooltips: true,
            animate: false,
            range: {
                'min': minPrice,
                'max': maxPrice
            },
            format: moneyFormat
        });

        // modify the values on range drag
        priceSlider.noUiSlider.on('update', function (values, handle) {
            if (handle) { // 1 evaluates to true
                priceMax.val(
                    moneyFormat.from(values[handle])
                );
            } else { // 0 evaluates to false
                priceMin.val(
                    moneyFormat.from(values[handle])
                );
            }
        });
    }

    loadValsFromLocalStorage();
};

function loadValsFromLocalStorage() {
    var slider = document.getElementById('price-slider');

    if (slider) {
        if (window.localStorage.getItem('precio-min') !== null) {
            var minPrice = parseInt(window.localStorage.getItem('precio-min'));
            var maxPrice = parseInt(window.localStorage.getItem('precio-max'));

            $('#precio-min').val(minPrice);
            $('#precio-max').val(maxPrice);

            // get the slider and set the values
            slider.noUiSlider.set([minPrice, maxPrice]);

            $('#size-xs').prop('checked', window.localStorage.getItem('size-xs') === 'true');
            $('#size-s').prop('checked', window.localStorage.getItem('size-s') === 'true');
            $('#size-m').prop('checked', window.localStorage.getItem('size-m') === 'true');
            $('#size-l').prop('checked', window.localStorage.getItem('size-l') === 'true');
            $('#size-xl').prop('checked', window.localStorage.getItem('size-xl') === 'true');
        }
    }
}
